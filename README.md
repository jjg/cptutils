cptutils
--------

A collection of tools for manipulating colour gradients, in particular
for converting between different formats.


### Obtaining

- The [cptutils homepage][1] has stable source releases and documentation
- Development resources are available on [GitLab][2] and [Codeberg][3]
- Prebuilt binary packages are available for [several systems][4]
- If you just have a few gradients to convert, try [cptutils online][5]


### Installing

This is the usual

    ./configure
    make
    sudo make install

which installs the binaries and manpages into `/usr/local`. See

    ./configure --help

to install elsewhere and for other compilation options.

You will need to have [libxml2][6] and [libpng][7] installed to
compile the programs.  The [Jansson][8] library is needed only if
you wish to have backtraces in the JSON format.  All are common
libraries, typically available in Linux, Mac and BSD package
respoitories.

The `en_US UTF-8` locale needs to be available; the command

    locale -a | grep -i en_US.utf8

should show if this is the case.  If not, how this is enabled depends
rather on your operating system.

The `gradient-convert` script needs Python (at least 3.7).  If your
version of Python has an unusual location or name, then you can use
the `--with-python` configuration option to specify it.  If you do
not want to use Python at all, then add the `--without-python` option.


[1]: https://jjg.gitlab.io/en/code/cptutils/
[2]: https://gitlab.com/jjg/cptutils
[3]: https://codeberg.org/jjgreen/cptutils
[4]: https://repology.org/project/cptutils/
[5]: http://seaviewsensing.com/pub/cptutils-online/
[6]: http://xmlsoft.org/
[7]: http://www.libpng.org/pub/png/libpng.html
[8]: http://www.digip.org/jansson/
