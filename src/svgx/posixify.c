#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "posixify.h"

#include <cptutils/utf8.h>


/*
  given a utf8 string, modify it in-place so that is is
  a reasonable filename on a modern Unix system.
*/

int posixify(char *str)
{
  /* cannot have an empty string */

  if (*str == '\0')
    return 1;

  /* convert leading dot to underscore */

  if ((u8_seqlen(str) == 1) && (*str == '.'))
    *str = '_';

  /* convert forward slash to underscore */

  char *p = str;
  int cn;

  while ((p = u8_strchr(p, '/', &cn)) != NULL)
    *p = '_';

  return 0;
}
