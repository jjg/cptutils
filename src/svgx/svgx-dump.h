#ifndef SVGX_DUMP_H
#define SVGX_DUMP_H

#include "svgx.h"

#include <cptutils/svg.h>

int svgcpt_dump(svg_t*, void*);
int svgcss3_dump(svg_t*, void*);
int svggrd3_dump(svg_t*, void*);
int svggpt_dump(svg_t*, void*);
int svgggr_dump(svg_t*, void*);
int svgmap_dump(svg_t*, void*);
int svgpg_dump(svg_t*, void*);
int svgpov_dump(svg_t*, void*);
int svgpng_dump(svg_t*, void*);
int svgqgs_dump(svg_t*, void*);
int svgsao_dump(svg_t*, void*);
int svgsvg_dump(svg_t*, void*);

#endif
