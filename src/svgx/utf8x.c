#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "utf8x.h"

#include <cptutils/btrace.h>

#include <iconv.h>
#include <locale.h>

#include <stdio.h>
#include <string.h>
#include <errno.h>

int utf8_to_x(const char *type,
              const unsigned char *in,
              char *out,
              size_t lenout)
{
  const char icopt[] = "TRANSLIT";
  size_t icnamelen = strlen(type) + 2 + strlen(icopt) + 1;
  char icname[icnamelen];

  int err = 1;

  if (snprintf(icname, icnamelen, "%s//%s", type, icopt) <= icnamelen)
    {
      const char lname[] = "en_US.UTF-8";

      if (setlocale(LC_CTYPE, lname) != NULL)
        {
          iconv_t cv = iconv_open(icname, "UTF-8");

          if (cv != (iconv_t)(-1))
            {
              size_t lenin = strlen((const char*)in) + 1;

              if (iconv(cv,
                        (char**)&(in), &(lenin),
                        (char**)&(out), &(lenout)) != (size_t)-1)
                err = 0;
              else
                btrace("error in iconv: %s", strerror(errno));

              if (iconv_close(cv) == -1)
                {
                  btrace("error closing iconv descriptor: %s",
                         strerror(errno));
                  err = 1;
                }
            }
          else
            btrace("error opening iconv descriptor: %s", strerror(errno));
        }
      else
        btrace("failed to set locale to %s", lname);
    }
  else
    btrace("failed to create iconv string");

  return err;
}
