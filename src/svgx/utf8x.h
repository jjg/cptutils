#ifndef UTF8X_H
#define UTF8X_H

#include <stddef.h>

int utf8_to_x(const char*, const unsigned char*, char*, size_t);

#endif
