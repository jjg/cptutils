#ifndef GIMPSVG_H
#define GIMPSVG_H

#include <cptutils/colour.h>
#include <cptutils/comment.h>
#include <cptutils/svg-preview.h>
#include <cptutils/svg-version.h>

#include <stdbool.h>
#include <stdlib.h>

typedef struct
{
  bool verbose, reverse;
  comment_opt_t comment;
  size_t samples;
  double tol;
  svg_preview_t preview;
  svg_version_t svg_version;
  struct
  {
    const char *path;
  } input, output;
} gimpsvg_opt_t;

int gimpsvg(const gimpsvg_opt_t*);

#endif
