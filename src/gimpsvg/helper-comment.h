/* autogenerated: do not edit */

#ifndef HELPER_COMMENT_H
#define HELPER_COMMENT_H

#include "gimpsvg.h"
#include "options.h"

int helper_comment(const struct gengetopt_args_info*, gimpsvg_opt_t*);

#endif
