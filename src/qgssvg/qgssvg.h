#ifndef QGSSVG_H
#define QGSSVG_H

#include <cptutils/comment.h>
#include <cptutils/svg-version.h>

#include <stdbool.h>
#include <stdio.h>

typedef struct
{
  bool verbose;
  comment_opt_t comment;
  svg_version_t svg_version;
  struct
  {
    const char *path;
  } input, output;
} qgssvg_opt_t;

int qgssvg(const qgssvg_opt_t*);

#endif
