#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptinfo.h"

#include <cptutils/cpt-read.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <float.h>

typedef struct
{
  int size;
  model_t model;
  struct
  {
    int total;
    int hatch;
    int grey;
    int colour;
  } segments;
  struct
  {
    bool discrete;
    bool continuous;
  } type;
  struct
  {
    double min, max;
  } z;
} info_t;

#define NNSTR(x) ((x) ? (x) : NULL)

static int cptinfo_analyse(const cpt_t*, info_t*);

static int output_plain(const info_t*, FILE*);
static int output_csv(const info_t*, FILE*);

int cptinfo(const cptinfo_opt_t *opt)
{
  struct stat sbuf;
  FILE  *stream;
  cpt_t *cpt;
  info_t info;

  if ((cpt = cpt_read(opt->input.path)) == NULL)
    {
      fprintf(stderr, "failed to read %s\n", NNSTR(opt->input.path));
      return 1;
    }

  int err = 0;

  if (cptinfo_analyse(cpt, &info) != 0)
    {
      fprintf(stderr, "failed to analyse %s\n", NNSTR(opt->input.path));
      err++;
    }

  cpt_destroy(cpt);

  if (err) return 1;

  if (opt->input.path)
    {
      if (stat(opt->input.path, &sbuf) == 0)
	info.size = sbuf.st_size;
      else
	{
	  fprintf(stderr,
		  "failed to stat %s : %s\n",
		  opt->input.path,
		  strerror(errno));
	  return 1;
	}
    }
  else
    info.size = 0;

  if (opt->output.path)
    {
      if ((stream = fopen(opt->output.path, "w")) == NULL)
	{
	  fprintf(stderr,
		  "failed to open %s : %s",
		  opt->output.path,
		  strerror(errno));
	  return 1;
	}
    }
  else
    stream = stdout;

  switch (opt->format)
    {
    case plain:
      if (output_plain(&info, stream) != 0)
	{
	  fprintf(stderr, "error in plain output\n");
	  return 1;
	}
      break;

    case csv:
      if (output_csv(&info, stream) != 0)
	{
	  fprintf(stderr, "error in csv output\n");
	  return 1;
	}
      break;

   default:
      fprintf(stderr, "strange output specified\n");
      return 1;
    }

  fclose(stream);

  return 0;
}

static int analyse_segment(const cpt_seg_t*, info_t*);

static int cptinfo_analyse(const cpt_t *cpt, info_t *info)
{
  info->segments.total = 0;
  info->segments.hatch = 0;
  info->segments.grey = 0;
  info->segments.colour = 0;

  info->model = cpt->model;

  info->type.discrete = true;
  info->type.continuous = true;

  info->z.min = DBL_MAX;
  info->z.max = DBL_MIN;

  for (cpt_seg_t *s = cpt->segment ; s ; s = s->right)
    {
      if (analyse_segment(s, info) != 0)
	{
	  fprintf(stderr, "failed to analyse segment\n");
	  return 1;
	}
    }

  if (cpt->range.present)
    {
      info->z.min = cpt->range.min;
      info->z.max = cpt->range.max;
    }

  return 0;
}

static int analyse_segment(const cpt_seg_t *seg, info_t *info)
{
  info->segments.total++;

  switch (seg->sample.left.fill.type)
    {
    case cpt_fill_empty:
      break;
    case cpt_fill_hatch:
    case cpt_fill_file:
      info->segments.hatch++;
      break;
    case cpt_fill_grey:
      info->segments.grey++;
      break;
    case cpt_fill_colour_rgb:
    case cpt_fill_colour_hsv:
      info->segments.colour++;
      break;
    }

  const cpt_seg_t *right = seg->right;

  if ((right) && (! cpt_fill_eq(seg->sample.right.fill,
                                right->sample.left.fill)))
    info->type.continuous = false;

  if (! cpt_fill_eq(seg->sample.right.fill,
                    seg->sample.left.fill))
    info->type.discrete = false;

  double zmin, zmax;

  if (seg->sample.left.val < seg->sample.right.val)
    {
      zmin = seg->sample.left.val;
      zmax = seg->sample.right.val;
    }
  else
    {
      zmax = seg->sample.left.val;
      zmin = seg->sample.right.val;
    }

  if (info->z.min > zmin) info->z.min = zmin;
  if (info->z.max < zmax) info->z.max = zmax;

  return 0;
}

#define BOOLSTR(x) ((x) ? "yes" : "no")

static int output_plain(const info_t *info, FILE *stream)
{
  fprintf(stream, "model:      %s\n", model_name(info->model));
  fprintf(stream, "continuous: %s\n", BOOLSTR(info->type.continuous));
  fprintf(stream, "discrete:   %s\n", BOOLSTR(info->type.discrete));
  fprintf(stream, "segments:\n");
  fprintf(stream, "  hachure   %i\n", info->segments.hatch);
  fprintf(stream, "  greyscale %i\n", info->segments.grey);
  fprintf(stream, "  colour    %i\n", info->segments.colour);
  fprintf(stream, "  total     %i\n", info->segments.total);
  fprintf(stream, "z-range:    %.3f - %.3f\n", info->z.min, info->z.max);
  fprintf(stream, "file size:  %i\n", info->size);

  return 0;
}

static int output_csv(const info_t *info, FILE *stream)
{
  fprintf(stream, "%s,%s,%s,%i,%i,%i,%i,%.3f,%.3f,%i\n",
	  model_name(info->model),
	  BOOLSTR(info->type.continuous),
	  BOOLSTR(info->type.discrete),
	  info->segments.hatch,
	  info->segments.grey,
	  info->segments.colour,
	  info->segments.total,
	  info->z.min,
	  info->z.max,
	  info->size);

  return 0;
}
