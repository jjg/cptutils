#ifndef CPTINFO_H
#define CPTINFO_H

#include <stdbool.h>

typedef enum { plain, csv } format_t;

typedef struct
{
  struct {
    const char *path;
  } input, output;
  bool verbose;
  format_t format;
} cptinfo_opt_t;

int cptinfo(const cptinfo_opt_t*);

#endif
