#ifndef CPTCLIP_H
#define CPTCLIP_H

#include <stdbool.h>
#include <stddef.h>

#include <cptutils/cpt-coerce.h>
#include <cptutils/comment.h>

typedef struct
{
  bool segments, verbose, normalise, denormalise;
  int gmt_version;
  comment_opt_t comment;
  union
  {
    struct {
      double min, max;
    } z;
    struct {
      size_t min, max;
    } segs;
  };
  struct
  {
    bool active;
    double value;
  } hinge;
  struct
  {
    const char *path;
  } input;
  struct
  {
    const char *path;
    cpt_coerce_model_t model;
  } output;
} cptclip_opt_t;

int cptclip(const cptclip_opt_t*);

#endif
