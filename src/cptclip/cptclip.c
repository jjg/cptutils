#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptclip.h"

#include <cptutils/cpt-read.h>
#include <cptutils/cpt-write.h>
#include <cptutils/cpt-normalise.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

static int cptclip_convert(cpt_t*, const cptclip_opt_t*);

int cptclip(const cptclip_opt_t *opt)
{
  cpt_t *cpt;
  int err = 1;

  if ((cpt = cpt_read(opt->input.path)) != NULL)
    {
      if (cptclip_convert(cpt, opt) == 0)
        {
          if (cpt_nseg(cpt) > 0)
            {
              if (cpt_coerce_model(cpt, opt->output.model) == 0)
                {
                  if (comment_process(cpt->comment, &(opt->comment)) == 0)
                    {
                      cptwrite_opt_t write_opt;

                      if (cpt_write_options(opt->gmt_version, &write_opt) == 0)
                        {
                          if (cpt_write(cpt, &write_opt, opt->output.path) == 0)
                            err = 0;
                          else
                            btrace("error writing cpt");
                        }
                      else
                        btrace("bad GMT version %i", opt->gmt_version);
                    }
                  else
                    btrace("failed to process comments");
                }
              else
                btrace("failed to coerce colour model in output");
            }
          else
            btrace("clipped result has no segments");
        }
      else
        btrace("failed to convert");

      cpt_destroy(cpt);
    }
  else
    btrace("failed to load cpt from %s",
           IFNULL(opt->input.path, "<stdin>"));

  if (err)
    btrace("failed to write cpt to %s",
           IFNULL(opt->output.path, "<stdout>"));

  return err;
}

static int cptclip_z_inc(cpt_t*, const cptclip_opt_t*);
static int cptclip_z_dec(cpt_t*, const cptclip_opt_t*);

/*
  clipping is easier on a denormalised cpt, so we coerce the
  input denormalised, clip, then renormalise if the input was
  normalised and -Z not given, or if -z was given.
*/

static int cptclip_convert(cpt_t *cpt, const cptclip_opt_t *opt)
{
  if (cpt->segment == NULL)
    {
      btrace("cpt has no segments");
      return 1;
    }

  bool input_normalised = cpt->range.present;

  if (input_normalised)
    if (cpt_denormalise(cpt, opt->hinge.active, opt->hinge.value) != 0)
      return 1;

  int err = 0;

  if (opt->segments)
    {
      int nseg = cpt_nseg(cpt);

      for (int i = 1 ; i < opt->segs.min ; i++)
	{
	  cpt_seg_t *s = cpt_pop(cpt);
	  cpt_seg_destroy(s);
	}

      for (int i = nseg ; i > opt->segs.max ; i--)
	{
	  cpt_seg_t *s = cpt_shift(cpt);
	  cpt_seg_destroy(s);
	}
    }
  else
    {
      if (cpt_increasing(cpt))
        err = cptclip_z_inc(cpt, opt);
      else
        err = cptclip_z_dec(cpt, opt);
    }

  if (cpt->hinge.present)
    {
      double value = 0;

      if (cpt->hinge.type == hinge_explicit)
        value = cpt->hinge.value;

      if ((value < opt->z.min) || (opt->z.max < value))
        {
          if (opt->verbose)
            printf("hinge at %g excised\n", value);
          cpt->hinge.present = false;
        }
    }

  if ((err == 0) &&
      (opt->normalise ||
       (input_normalised && (! opt->denormalise))))
    err = cpt_normalise(cpt, opt->hinge.active, opt->hinge.value);

  return err;
}

/*
  handles cptclip_z_inc and _dec by considering the target
  interval in the gradient's left-right frame of reference.
*/

static int cptclip_z_id(cpt_t *cpt,
			double zleft,
			double zright,
			bool (*left_of)(double, double),
			bool (*right_of)(double, double))
{
  cpt_seg_t *seg;

  /*
    pop segments from the left until one of them
    intersects the requested interval
  */

  while ((seg = cpt_pop(cpt)) != NULL)
    {
      if (right_of(seg->sample.right.val, zleft))
	break;

      cpt_seg_destroy(seg);
    }

  if (seg)
    {
      /*
	if this segment is not completely inside the
	requested interval then clip it to the interval
      */

      if (left_of(seg->sample.left.val, zleft))
	{
	  cpt_fill_t fill;
	  double z =
	    (zleft - seg->sample.left.val) /
	    (seg->sample.right.val - seg->sample.left.val);

	  if  (cpt_fill_interpolate(cpt->interpolate,
                                    z,
                                    seg->sample.left.fill,
                                    seg->sample.right.fill,
                                    &fill) != 0)
            {
              btrace("fill interpolation failed");
              return 1;
            }

	  seg->sample.left.fill = fill;
	  seg->sample.left.val = zleft;
	}

      /*
	push the (possibly modified) segment back to
	where it came
      */

      cpt_prepend(seg, cpt);
    }

  /* likewise on the right */

  while ((seg = cpt_shift(cpt)) != NULL)
    {
      if (left_of(seg->sample.left.val, zright))
	break;

      cpt_seg_destroy(seg);
    }

  if (seg)
    {
      if (right_of(seg->sample.right.val, zright))
	{
	  cpt_fill_t fill;
	  double z =
	    (zright - seg->sample.left.val) /
	    (seg->sample.right.val - seg->sample.left.val);

	  if  (cpt_fill_interpolate(cpt->interpolate,
                                    z,
                                    seg->sample.left.fill,
                                    seg->sample.right.fill,
                                    &fill) != 0)
            {
              btrace("fill interpolation failed");
              return 1;
            }

	  seg->sample.right.fill = fill;
	  seg->sample.right.val = zright;
	}

      cpt_append(seg, cpt);
    }

  return 0;
}

static bool lt(double a, double b){ return a < b; }
static bool gt(double a, double b){ return a > b; }

static int cptclip_z_inc(cpt_t *cpt, const cptclip_opt_t *opt)
{
  return cptclip_z_id(cpt, opt->z.min, opt->z.max, lt, gt);
}

static int cptclip_z_dec(cpt_t *cpt, const cptclip_opt_t *opt)
{
  return cptclip_z_id(cpt, opt->z.max, opt->z.min, gt, lt);
}
