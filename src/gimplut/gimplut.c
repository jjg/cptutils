#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gimplut.h"

#include <cptutils/ggr-read.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define SCALE(x, opt) ((opt.min) + ((opt.max) - (opt.min)) * (x))

#define ERR_CMOD 1
#define ERR_NULL 2
#define ERR_INSERT 3

static int gimplut_st(FILE*, ggr_t*, const gimplut_opt_t*);

int gimplut(const gimplut_opt_t *opt)
{
  ggr_t *gradient = ggr_read(opt->input.path);

  if (! gradient)
    {
      btrace("failed to load gradient from %s",
             IFNULL(opt->input.path, "<stdin>"));
      return 1;
    }

  if (! ggr_valid(gradient))
    {
      btrace("input gradient is invalid");
      return 1;
    }

  int err = 0;

  if (opt->output.path)
    {
      FILE *lutst = fopen(opt->output.path, "w");

      if (!lutst)
	{
	  btrace("failed to open %s", opt->output.path);
	  err++;
	}
      else
	{
	  err = gimplut_st(lutst, gradient, opt);
	  fclose(lutst);
	}
    }
  else
    err = gimplut_st(stdout, gradient, opt);

  if ((!err) && opt->verbose)
    printf("converted to %zu entry LUT\n", opt->numsamp);

  ggr_destroy(gradient);

  return err;
}

static int gimplut_st(FILE *st, ggr_t *g, const gimplut_opt_t *opt)
{
  size_t n = opt->numsamp;
  double bg[3] = {0.0, 0.0, 0.0}, c[3];
  rgb_t rgb;
  char lut[n*3];

  for (size_t i = 0 ; i < n ; i++)
    {
      double pos = ((double)i) / ((double)(n - 1));

      if (ggr_colour(pos, g, bg, c) != 0)
	{
	  btrace("could not get colour at z = %f", pos);
	  return 1;
	}

      if (rgbD_to_rgb(c, &rgb) != 0)
	{
	  btrace("failed convert to rgb for rgb %f/%f/%f",
		 c[0], c[1], c[2]);
	  return 1;
	}

      lut[i] = rgb.red;
      lut[n + i] = rgb.green;
      lut[2 * n + i] = rgb.blue;
    }

  return fwrite(lut, 1, 3 * n, st) != 3 * n;
}
