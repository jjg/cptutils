#ifndef GIMPLUT_H
#define GIMPLUT_H

#include <stddef.h>
#include <stdbool.h>

typedef struct
{
  bool verbose;
  size_t numsamp;
  struct
  {
    const char *path;
  } input, output;
} gimplut_opt_t;

int gimplut(const gimplut_opt_t*);

#endif
