/* autogenerated: do not edit */

#ifndef HELPER_MODEL_H
#define HELPER_MODEL_H

#include "options.h"
#include "cptupdate.h"

int helper_model(const struct gengetopt_args_info*, cptupdate_opt_t*);

#endif
