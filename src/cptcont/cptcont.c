#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptcont.h"

#include <cptutils/cpt-read.h>
#include <cptutils/cpt-write.h>
#include <cptutils/cpt-normalise.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

static int cptcont_convert(cpt_t*, const cptcont_opt_t*);

int cptcont(const cptcont_opt_t *opt)
{
  cpt_t *cpt;
  int err = 1;

  if ((cpt = cpt_read(opt->input.path)) != NULL)
    {
      if (cpt_coerce_model(cpt, opt->output.model) == 0)
        {
          if (cptcont_convert(cpt, opt) == 0)
            {
              if (comment_process(cpt->comment, &(opt->comment)) == 0)
                {
                  cptwrite_opt_t write_opt;

                  if (cpt_write_options(opt->gmt_version, &write_opt) == 0)
                    {
                      if (cpt_write(cpt, &write_opt, opt->output.path) == 0)
                        err = 0;
                      else
                        btrace("error writing cpt struct");
                    }
                  else
                    btrace("bad GMT version %i", opt->gmt_version);
                }
              else
                btrace("failed to process comments");
            }
          else
            btrace("failed to convert");
        }
      else
        btrace("failed to coerce colour model in output");

      cpt_destroy(cpt);
    }
  else
    btrace("failed to load cpt from %s",
           IFNULL(opt->input.path, "<stdin>"));

  if (err)
    btrace("failed to write cpt to %s",
           IFNULL(opt->output.path, "<stdout>"));

  return err;
}

static int midpoint_split(cpt_seg_t*);

static int cptcont_convert(cpt_t *cpt, const cptcont_opt_t *opt)
{
  if (cpt->segment == NULL)
    {
      btrace("cpt has no segments");
      return 1;
    }

  if (opt->midpoint)
    {
      if (opt->verbose)
	printf("splitting at midpoints\n");

      if (midpoint_split(cpt->segment) != 0)
	{
	  btrace("splitting at midpoints");
	  return 1;
	}
    }

  if (opt->normalise)
    {
      if (cpt_normalise(cpt, opt->hinge.active, opt->hinge.value) != 0)
        {
          btrace("normalising");
          return 1;
        }
    }

  if (opt->denormalise)
    {
      if (cpt_denormalise(cpt, opt->hinge.active, opt->hinge.value) != 0)
        {
          btrace("denormalising");
          return 1;
        }
    }

  cpt_seg_t *s1, *s2;
  int n = 0;
  double p = opt->partial / 2;

  for (s1 = cpt->segment, s2 = s1->right ;
       s2 ;
       s1 = s2, s2 = s1->right)
    {
      if (s1->sample.right.fill.type != s2->sample.left.fill.type)
        {
          btrace("sorry, can't convert mixed fill types");
          return 1;
        }

      if (! cpt_fill_eq(s1->sample.right.fill, s2->sample.left.fill))
	{
	  cpt_fill_t F1, F2;

	  if ((cpt_fill_interpolate(cpt->interpolate, p,
                                    s1->sample.right.fill,
                                    s2->sample.left.fill,
                                    &F1) != 0) ||
              (cpt_fill_interpolate(cpt->interpolate, p,
                                    s2->sample.left.fill,
                                    s1->sample.right.fill,
                                    &F2) != 0))
	    {
	      btrace("failed fill interpolate");
	      return 1;
	    }

	  s1->sample.right.fill = F1;
	  s2->sample.left.fill = F2;

	  n++;
	}
    }

  if (opt->verbose)
    printf("modified %i discontinuities\n", n);

  return 0;
}

/*
  recursive function which splits search segment into
  two -- the old segment on the left, the new one on
  the right
*/

static int midpoint_split(cpt_seg_t *s)
{
  if (!s) return 0;

  int err;

  if ((err = midpoint_split(s->right)) != 0)
    return err + 1;

  double zm = (s->sample.left.val + s->sample.right.val) / 2;
  cpt_seg_t
    *sl = s,
    *sr = cpt_seg_new();

  if (!sr)
    {
      btrace("failed to create new segment");
      return 1;
    }

  sr->sample.right.val = sl->sample.right.val;
  sr->sample.right.fill = sl->sample.right.fill;
  sr->right = sl->right;

  sr->sample.left.val = zm;
  sr->sample.left.fill = sl->sample.left.fill;
  sr->left = sl;

  sl->sample.right.val = zm;
  sl->right = sr;

  return 0;
}
