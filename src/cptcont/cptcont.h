#ifndef CPTCONT_H
#define CPTCONT_H

#include <stdbool.h>

#include <cptutils/cpt-coerce.h>
#include <cptutils/comment.h>

typedef struct
{
  double partial;
  bool verbose, midpoint, normalise, denormalise;
  int gmt_version;
  comment_opt_t comment;
  struct
  {
    bool active;
    double value;
  } hinge;
  struct
  {
    const char *path;
  } input;
  struct
  {
    cpt_coerce_model_t model;
    const char *path;
  } output;
} cptcont_opt_t;

int cptcont(const cptcont_opt_t*);

#endif
