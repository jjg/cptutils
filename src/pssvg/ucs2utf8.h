#ifndef UCS2UTF8_H
#define UCS2UTF8_H

#include <stddef.h>

int ucs2_to_utf8(const char*, size_t, char*, size_t);

#endif
