#ifndef PSSVG_H
#define PSSVG_H

#include <cptutils/colour.h>
#include <cptutils/comment.h>
#include <cptutils/svg-version.h>

#include <stdbool.h>
#include <stdio.h>

typedef struct
{
  bool verbose, debug;
  comment_opt_t comment;
  char *id_format;
  rgb_t fg, bg;
  struct
  {
    const char *path;
  } input, output;
  svg_version_t svg_version;
} pssvg_opt_t;

int pssvg(const pssvg_opt_t*);

#endif
