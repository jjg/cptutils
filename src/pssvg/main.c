#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-btrace.h"
#include "helper-comment.h"
#include "helper-input.h"
#include "helper-output.h"
#include "helper-svg2.h"
#include "pssvg.h"

#include <cptutils/btrace.h>

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  pssvg_opt_t opt = {
    .verbose = info->verbose_flag,
    .id_format = (info->id_format_given ? info->id_format_arg : NULL)
  };

  if (parse_rgb(info->background_arg, &(opt.bg)) != 0)
    {
      fprintf(stderr, "bad background %s\n", info->background_arg);
      return EXIT_FAILURE;
    }

  if (parse_rgb(info->foreground_arg, &(opt.fg)) != 0)
    {
      fprintf(stderr, "bad foreground %s\n", info->foreground_arg);
      return EXIT_FAILURE;
    }

  if ((helper_comment(info, &opt) != 0) ||
      (helper_output(info, &opt) != 0) ||
      (helper_input(info, &opt) != 0) ||
      (helper_svg2(info, &opt) != 0))
    return EXIT_FAILURE;

  if (opt.verbose)
    {
      printf("This is pssvg (version %s)\n", VERSION);
      if (opt.id_format)
	printf("with id-format '%s'\n", opt.id_format);
      printf("background %3i/%3i/%3i\n",
	     opt.bg.red,  opt.bg.green,  opt.bg.blue);
      printf("foreground %3i/%3i/%3i\n",
	     opt.fg.red,  opt.fg.green,  opt.fg.blue);
    }

  btrace_enable("pssvg");

  int err;

  if ((err = pssvg(&opt)) != 0)
    helper_btrace(info);

  btrace_reset();
  btrace_disable();

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
