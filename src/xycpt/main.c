#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-btrace.h"
#include "helper-comment.h"
#include "helper-gmt-ver.h"
#include "helper-gmt-aux.h"
#include "helper-input.h"
#include "helper-output.h"
#include "xycpt.h"

#include <cptutils/btrace.h>

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  xycpt_opt_t opt = {
    .verbose = info->verbose_flag,
    .discrete = info->discrete_flag,
    .unital = info->unit_flag
  };

  if ((helper_output(info, &opt) != 0) ||
      (helper_input(info, &opt) != 0) ||
      (helper_gmt_ver(info, &opt) != 0) ||
      (helper_gmt_aux(info, &opt) != 0) ||
      (helper_comment(info, &opt) != 0))
    return EXIT_FAILURE;

  if (info->register_given)
    {
      if (opt.discrete)
	{
	  switch (*info->register_arg)
	    {
	    case 'l': case 'L':
	      opt.reg = reg_lower;
	      break;
	    case 'm': case 'M':
	      opt.reg = reg_middle;
	      break;
	    case 'u': case 'U': case 'h': case 'H':
	      opt.reg = reg_upper;
	      break;
	    default:
	      fprintf(stderr,
		      "unknown registration type \"%s\"\n",
		      info->register_arg);
	      return EXIT_FAILURE;
	    }
	}
      else
	{
	  fprintf(stderr, "registration only applies to discrete output!\n");
	  fprintf(stderr, "(drop the -r option or use the -d option)\n");
	  return EXIT_FAILURE;
	}
    }
  else
    opt.reg = reg_middle;

  if (opt.verbose)
    printf("This is xycpt (version %s)\n", VERSION);

  if (opt.verbose)
    {
      if (opt.discrete)
	{
	  const char *regstr;

	  switch (opt.reg)
	    {
	    case reg_lower: regstr = "lower"; break;
	    case reg_middle: regstr = "middle"; break;
	    case reg_upper: regstr = "upper"; break;
	    default:
	      fprintf(stderr, "bad registration\n");
	      return EXIT_FAILURE;
	    }

	  printf("converting to discrete palette (%s registration)\n", regstr);
	}
    }

  btrace_enable("xycpt");

  int err;

  if ((err = xycpt(&opt)) != 0)
    helper_btrace(info);

  btrace_reset();
  btrace_disable();

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
