#ifndef GPLCPT_H
#define GPLCPT_H

#include <cptutils/colour.h>
#include <cptutils/comment.h>
#include <stdbool.h>

typedef struct
{
  bool verbose, normalise;
  int gmt_version;
  comment_opt_t comment;
  rgb_t fg, bg, nan;
  struct
  {
    const char *path;
  } input, output;
} gplcpt_opt_t;

int gplcpt(const gplcpt_opt_t*);

#endif
