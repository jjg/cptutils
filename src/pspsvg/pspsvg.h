#ifndef PSPSVG_H
#define PSPSVG_H

#include <cptutils/svg-preview.h>
#include <cptutils/svg-version.h>
#include <cptutils/comment.h>

#include <stdbool.h>

typedef struct
{
  bool verbose, debug;
  comment_opt_t comment;
  struct
  {
    const char *path;
  } input, output;
  svg_preview_t preview;
  svg_version_t svg_version;
} pspsvg_opt_t;

int pspsvg(const pspsvg_opt_t*);

#endif
