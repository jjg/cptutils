#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "pspsvg.h"

#include <cptutils/grd3-read.h>
#include <cptutils/svg-write.h>
#include <cptutils/grdx-svg.h>
#include <cptutils/gstack.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

static int pspsvg_convert(grd3_t*, svg_t*, const pspsvg_opt_t*);

int pspsvg(const pspsvg_opt_t *opt)
{
  grd3_t *grd3;
  int err = 1;

  if ((grd3 = grd3_read(opt->input.path)) == NULL)
    {
      const char* path = IFNULL(opt->input.path, "<stdin>");
      btrace("failed read from %s", path);
    }
  else
    {
      svg_t *svg;

      if ((svg = svg_new()) == NULL)
        btrace("failed to get svg from list");
      else
        {
          if (pspsvg_convert(grd3, svg, opt) != 0)
            btrace("failed to convert data");
          else
            {
              if (comment_process(svg->comment, &(opt->comment)) != 0)
                btrace("failed to process comments");
              else
                {
                  const char *path = opt->output.path;
                  const svg_preview_t *preview = &(opt->preview);
                  svg_version_t version = opt->svg_version;

                  if (svg_write(svg, path, preview, version) == 0)
                    err = 0;
                  else
                    btrace("failed write to %s", IFNULL(path, "<stdout>"));
                }
            }

          svg_destroy(svg);
        }

      grd3_destroy(grd3);
    }

  return err;
}

/* convert grd3 to intermediate types */

static double grd3_rgb_it(unsigned short x)
{
  return (double)x / 65535.0;
}

static double grd3_op_it(unsigned short x)
{
  return (double)x / 255.0;
}

static unsigned int grd3_z_it(unsigned short z)
{
  return (unsigned int)z * 100;
}

static unsigned int grd3_zmid_it(unsigned short z0,
                                 unsigned short z1,
                                 unsigned short M)
{
  return (unsigned int)z0 * 100 + ((unsigned int)z1 - (unsigned int)z0) * M;
}

/* trim off excessive stops */

static int trim_rgb(gstack_t *stack)
{
  size_t n = gstack_size(stack);
  gstack_t *stack0;

  if ((stack0 = gstack_new(sizeof(rgb_stop_t), n, n)) != NULL)
    {
      while (! gstack_empty(stack))
        {
          rgb_stop_t stop;

          gstack_pop(stack, &stop);
          gstack_push(stack0, &stop);

          if (stop.z >= 409600)
            {
              while (! gstack_empty(stack))
                gstack_pop(stack, &stop);
            }
        }

      while (! gstack_empty(stack0))
        {
          rgb_stop_t stop;

          gstack_pop(stack0, &stop);
          gstack_push(stack, &stop);
        }

      gstack_destroy(stack0);

      return 0;
    }

  return 1;
}

static int trim_op(gstack_t *stack)
{
  size_t n = gstack_size(stack);
  gstack_t *stack0;

  if ((stack0 = gstack_new(sizeof(op_stop_t), n, n)) != NULL)
    {
      while (! gstack_empty(stack))
        {
          op_stop_t stop;

          gstack_pop(stack, &stop);
          gstack_push(stack0, &stop);

          if (stop.z >= 409600)
            {
              while (! gstack_empty(stack))
                gstack_pop(stack,&stop);
            }
        }

      while (! gstack_empty(stack0))
        {
          op_stop_t stop;

          gstack_pop(stack0, &stop);
          gstack_push(stack, &stop);
        }

      gstack_destroy(stack0);

      return 0;
    }

  return 1;
}

/*
  convert the grd3 stops to the intermediate types,
  and rectify -- replace the midpoints by explicit
  mid-point stops
*/

static void assign_stop(const grd3_rgbseg_t *pseg, rgb_stop_t *stop)
{
  stop->z = grd3_z_it(pseg->z);
  stop->r = grd3_rgb_it(pseg->r);
  stop->g = grd3_rgb_it(pseg->g);
  stop->b = grd3_rgb_it(pseg->b);
}

static int rectify_rgb2(grd3_t *grd3, gstack_t *stack)
{
  int n = grd3->rgb.n;
  grd3_rgbseg_t *pseg = grd3->rgb.seg;
  rgb_stop_t stop;

  if (pseg[0].z > 0)
    {
      assign_stop(pseg, &stop);
      stop.z = 0;
      if (gstack_push(stack, &stop) != 0)
	return 1;
    }

  for (int i = 0 ; i < n-1 ; i++)
    {
      assign_stop(pseg + i, &stop);
      if (gstack_push(stack, &stop) != 0)
	return 1;

      if (pseg[i].midpoint != 50)
	{
	  stop.z = grd3_zmid_it(pseg[i].z, pseg[i + 1].z, pseg[i].midpoint);
	  stop.r = 0.5*(grd3_rgb_it(pseg[i].r) + grd3_rgb_it(pseg[i + 1].r));
	  stop.g = 0.5*(grd3_rgb_it(pseg[i].g) + grd3_rgb_it(pseg[i + 1].g));
	  stop.b = 0.5*(grd3_rgb_it(pseg[i].b) + grd3_rgb_it(pseg[i + 1].b));

	  if (gstack_push(stack, &stop) != 0)
	    return 1;
	}
    }

  assign_stop(pseg + (n - 1), &stop);
  if (gstack_push(stack, &stop) != 0)
    return 1;

  if (stop.z < 409600)
    {
      stop.z = 409600;
      if (gstack_push(stack, &stop) != 0)
	return 1;
    }

  if (gstack_reverse(stack) != 0)
    return 1;

  if (trim_rgb(stack) != 0)
    return 1;

  return 0;
}

static gstack_t* rectify_rgb(grd3_t* grd3)
{
  int n = grd3->rgb.n;

  if (n < 2)
    {
      btrace("input (grd) has %i rgb stop(s)", n);
      return NULL;
    }

  gstack_t *stack = gstack_new(sizeof(rgb_stop_t), 2 * n, n);

  if (stack != NULL)
    {
      if (rectify_rgb2(grd3, stack) == 0)
        return stack;

      gstack_destroy(stack);
    }

  return NULL;
}

static int rectify_op2(grd3_t *grd3, gstack_t *stack)
{
  int n = grd3->op.n;
  grd3_opseg_t *pseg = grd3->op.seg;
  op_stop_t stop;

  if (pseg[0].z > 0)
    {
      stop.z  = 0;
      stop.op = grd3_op_it(pseg[0].opacity);

      if (gstack_push(stack, &stop) != 0)
	return 1;
    }

  for (int i = 0 ; i < n - 1 ; i++)
    {
      stop.z  = grd3_z_it(pseg[i].z);
      stop.op = grd3_op_it(pseg[i].opacity);

      if (gstack_push(stack, &stop) != 0)
	return 1;

      if (pseg[i].midpoint != 50)
	{
	  stop.z  = grd3_zmid_it(pseg[i].z, pseg[i + 1].z, pseg[i].midpoint);
	  stop.op = 0.5 * (grd3_op_it(pseg[i].opacity) +
                           grd3_op_it(pseg[i + 1].opacity));

	  if (gstack_push(stack, &stop) != 0)
	    return 1;
	}
    }

  stop.z  = grd3_z_it(pseg[n - 1].z);
  stop.op = grd3_op_it(pseg[n - 1].opacity);

  if (gstack_push(stack, &stop) != 0)
    return 1;

  if (stop.z < 409600)
    {
      stop.z = 409600;
      if (gstack_push(stack, &stop) != 0)
	return 1;
    }

  if (gstack_reverse(stack) != 0)
    return 1;

  if (trim_op(stack) != 0)
    return 1;

  return 0;
}

static gstack_t* rectify_op(grd3_t *grd3)
{
  int n = grd3->op.n;

  if (n<2)
    {
      btrace("input (grd) has %i opacity stop(s)", n);
      return NULL;
    }

  gstack_t *stack = gstack_new(sizeof(op_stop_t), 2 * n, n);

  if (stack == NULL)
    return NULL;

  if (rectify_op2(grd3, stack) == 0)
    return stack;

  gstack_destroy(stack);
  return NULL;
}

/*
   In grd3 files, names are non null-terminated
   arrays of unsigned char, but we store them as
   null-terminated arrays of unsigned char.  In
   the wild one sees the upper half of the range
   being used, and it seems to be latin-1.
*/

static int latin1_to_utf8(const unsigned char *in,
			  unsigned char *out,
			  size_t lenout)
{
  const unsigned char *end = out + lenout;

  while (*in)
    {
      if (*in < 128 && end - out >= 2)
        *out++ = *in++;
      else if (*in >= 192 && end - out >= 3)
        {
          *out++ = 0xc2 + (*in > 0xbf);
          *out++ = (*in++ & 0x3f) + 0x80;
        }
      else
        return 1;
    }

  *out++ = '\0';
  return 0;
}

static int pspsvg_convert(grd3_t *grd3,
                          svg_t *svg,
                          const pspsvg_opt_t *opt)
{
  gstack_t *rgbrec, *oprec;

  if (latin1_to_utf8(grd3->name, svg->name, SVG_NAME_LEN) != 0)
    {
      btrace("failed latin1 to unicode name conversion");
      return 1;
    }

  if (opt->verbose)
    printf("processing \"%s\"\n", svg->name);

  if ((rgbrec = rectify_rgb(grd3)) == NULL)
    return 1;

  if ((oprec = rectify_op(grd3)) == NULL)
    return 1;

  if (opt->verbose)
    printf("stops: rgb %i/%zu, opacity %i/%zu\n",
	   grd3->rgb.n,
	   gstack_size(rgbrec),
	   grd3->op.n,
	   gstack_size(oprec));

  if (grdxsvg(rgbrec, oprec, svg) != 0)
    {
      btrace("failed conversion of rectified stops to svg");
      return 1;
    }

  gstack_destroy(rgbrec);
  gstack_destroy(oprec);

  return svg_complete(svg);
}
