'\" t
.\"     Title: CPTCONT
.\"    Author: [see the "AUTHOR" section]
.\" Generator: DocBook XSL Stylesheets v1.79.2 <http://docbook.sf.net/>
.\"      Date:  7 November 2024
.\"    Manual: User commands
.\"    Source: cptutils 1.83
.\"  Language: English
.\"
.TH "CPTCONT" "1" "7 November 2024" "cptutils 1\&.83" "User commands"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
cptcont \- create a continuous GMT colour palette table (cpt) file based on the colours of a non\-continuous cpt file\&.
.SH "SYNOPSIS"
.HP \w'\fBcptcont\fR\ 'u
\fBcptcont\fR [\-h] [\-M] [\-o\ \fIpath\fR] [\-p\ \fIpercent\fR] [\-v] [\-V] [\-z] [\-Z] [\-4] [\-5] [\-6] [\fIpath\fR]
.SH "DESCRIPTION"
.PP
The
\fBcptcont\fR
program converts a (possibly) discontinuous colour palette in the input to a continuous palette in the output\&. This is achieved by replacing the endpoints of segments at which a discontinuity occurs by their mean colour\&.
.PP
The program will read from
stdin
if a file is not specified as the final argument, and write to
stdout
if the
\fB\-\-output\fR
option is not specified\&.
.SH "OPTIONS"
.PP
\fB\-\-backtrace\-file\fR \fIpath\fR
.RS 4
Specify a file to which to write a formatted backtrace\&. The file will only be created if there is a backtrace created, typically when an error occurs\&.
.RE
.PP
\fB\-\-backtrace\-format\fR \fIformat\fR
.RS 4
Specify the
\fIformat\fR
of the backtrace written to the files specified by
\fB\-\-backtrace\-file\fR, one of
\fBplain\fR,
\fBxml\fR
or
\fBjson\fR\&.
.RE
.PP
\fB\-\-comments\-read\fR \fIpath\fR
.RS 4
Read the comments from the specified
\fIpath\fR
and add them to the output gradient\&.
.sp
The format is custom XML which should be fairly easy to generate, see the output of
\fB\-\-comments\-write\fR
for examples\&.
.RE
.PP
\fB\-\-comments\-write\fR \fIpath\fR
.RS 4
Write the comments in the input to the specified
\fIpath\fR\&.
.RE
.PP
\fB\-\-comments\-retain\fR
.RS 4
Use the comments in the input file as the comments for the output file\&.
.RE
.PP
\fB\-\-comments\-generate\fR
.RS 4
Create a comment with summary data (the date of creation, name and version of the
cptutils
package) in the output file\&.
.RE
.PP
\fB\-h\fR, \fB\-\-help\fR
.RS 4
Brief help\&.
.RE
.PP
\fB\-\-hinge\fR \fIvalue\fR
.RS 4
Specify the z\-\fIvalue\fR
of the hinge in the cpt file\&. If there is no hinge directive (i\&.e\&., a
\fBSOFT_HINGE\fR
or
\fBHARD_HINGE\fR) in the input, then this option has no effect\&.
.sp
When normalising (with the
\fB\-\-z\-normalise\fR
option), this gives the z\-value in the input which is mapped to zero\&. That z\-value must be one of the stops in the input\&.
.sp
When denormalising (with the
\fB\-\-z\-denormalise\fR
option), this gives the value in the output to which zero in the input is mapped\&. This option can be viewed as the counterpart to the
\fB+h\fR\fB\fIvalue\fR\fR
appended to the
\fB\-C\fR
option for the
\m[blue]\fB\fBmakecpt\fR(1)\fR\m[]\&.
.RE
.PP
\fB\-\-hinge\-active\fR
.RS 4
If the input cpt has a
\fBSOFT_HINGE\fR
directive, then activate that hinge (resulting in independent scaling of the two halves of the gradient either side of the hinge)\&.
.sp
If the input does not have such a directive, then this option has no effect\&.
.RE
.PP
\fB\-M\fR, \fB\-\-midpoint\fR
.RS 4
Split each input segment into two output segments with the colour at the common point being the colour of the original segment\&. This gives a more faithful conversion of gradients with "corners", such as diverging gradients, albeit at the cost of larger files\&.
.RE
.PP
\fB\-o\fR, \fB\-\-output\fR \fIpath\fR
.RS 4
Write the output to
\fIpath\fR, rather than
stdout\&.
.RE
.PP
\fB\-p\fR, \fB\-\-partial\fR \fIpercentage\fR
.RS 4
The endpoints are moved the specified
\fIpercentage\fR
towards the mean colour, so that a value of 100 (the default) moves the endpoints to the mean colour, 50 moves them half\-way there, and so on\&.
.sp
Negative values and values greater than 100 are permitted (these can give interesting effects, but are not really suitable for publication\-quality plots)\&.
.RE
.PP
\fB\-v\fR, \fB\-\-verbose\fR
.RS 4
Verbose operation\&.
.RE
.PP
\fB\-V\fR, \fB\-\-version\fR
.RS 4
Version information\&.
.RE
.PP
\fB\-z\fR, \fB\-\-z\-normalise\fR
.RS 4
Normalise the z\-values in the cpt output into the range 0/1 (or to \-1/1 if a hinge is present) and add a
\fBRANGE\fR
directive if not present in the input\&. This is the form used in GMT master files\&.
.sp
This option requires that output cpt version is at least 5\&.
.RE
.PP
\fB\-Z\fR, \fB\-\-z\-denormalise\fR
.RS 4
Set the z\-values in the cpt output into the range given by the
\fBRANGE\fR
directive, and remove that directive\&. If there is no
\fBRANGE\fR
then this option does nothing\&.
.RE
.PP
\fB\-4\fR, \fB\-\-gmt4\fR
.RS 4
Use GMT 4 conventions when writing the cpt output: the colour\-model code is uppercase, and the colours are separated by spaces\&.
.sp
This is incompatible with the
\fB\-5\fR
and
\fB\-6\fR
options of course\&.
.sp
At present this option is the default, but that will change at some point\&. So specify this option if your use of the output depends on the GMT 4 layout (consumed by a custom parser, for example)\&.
.RE
.PP
\fB\-5\fR, \fB\-\-gmt5\fR
.RS 4
Use GMT 5 conventions when writing the cpt output: the colour\-model code is lowercase, and the colours are separated by a solidus for RGB, CMYK, by a dash for HSV\&.
.sp
This is incompatible with the
\fB\-4\fR
and
\fB\-6\fR
options of course\&.
.RE
.PP
\fB\-6\fR, \fB\-\-gmt6\fR
.RS 4
As the
\fB\-5\fR
option, but allows the
\fBHARD_HINGE\fR
and
\fBSOFT_HINGE\fR
directives in place of the explicit
\fBHINGE =\fR
directive\&.
.sp
This is incompatible with the
\fB\-4\fR
and
\fB\-5\fR
options of course\&.
.RE
.SH "EXAMPLE"
.PP
Create an almost\-continuous table:
.sp
.if n \{\
.RS 4
.\}
.nf
cptcont \-v \-p 66 \-o new\&.cpt old\&.cpt
.fi
.if n \{\
.RE
.\}
.SH "AUTHOR"
.PP
J\&.J\&. Green
