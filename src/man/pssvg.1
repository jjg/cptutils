'\" t
.\"     Title: PSSVG
.\"    Author: [see the "AUTHOR" section]
.\" Generator: DocBook XSL Stylesheets v1.79.2 <http://docbook.sf.net/>
.\"      Date:  7 November 2024
.\"    Manual: User commands
.\"    Source: cptutils 1.83
.\"  Language: English
.\"
.TH "PSSVG" "1" "7 November 2024" "cptutils 1\&.83" "User commands"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
pssvg \- convert Photoshop\(rg gradients to SVG\&.
.SH "SYNOPSIS"
.HP \w'\fBpssvg\fR\ 'u
\fBpssvg\fR [\-b\ \fIrgb\fR] [\-f\ \fIrgb\fR] [\-F\ \fIformat\fR] [\-h] [\-o\ \fIpath\fR] [\-v] [\-V] [\-2] \fIpath\fR
.SH "DESCRIPTION"
.PP
The
\fBpssvg\fR
program converts Photoshop grd5 gradients to the SVG format\&. Unlike most of the other file formats handled by the
cptutils
package, grd5 files may contain multiple gradients and all of the input gradients will be converted to gradients in the output file\&. One can then use
\m[blue]\fB\fBsvgx\fR(1)\fR\m[]
to extract single gradients and convert them to other formats\&.
.PP
Photoshop gradient files will often contain a number of gradients with the same name\&. These names are used as the basis for the SVG
id
attributes, but since these must be unique, the program will append
\-01,
\-02, \&.\&.\&. to duplicate names in order to form the
ids, as well a replacing non\-alphanumeric by hyphens and prepending the string
id\-\&. Alternatively once can employ the
\fB\-\-id\-format\fR
option to generate a uniform sequence of titles\&.
.PP
The program will read from
stdin
if a file is not specified as the final argument, and write to
stdout
if the
\fB\-\-output\fR
option is not specified\&.
.SH "OPTIONS"
.PP
In the following, all
\fIrgb\fR
specifications should be of the form
\fIred\fR/\fIgreen\fR/\fIblue\fR
where the colour components are integers in the range 0 to 255\&.
.PP
\fB\-b\fR, \fB\-\-background\fR \fIrgb\fR
.RS 4
Set the background colour of the output\&.
.RE
.PP
\fB\-\-backtrace\-file\fR \fIpath\fR
.RS 4
Specify a file to which to write a formatted backtrace\&. The file will only be created if there is a backtrace created, typically when an error occurs\&.
.RE
.PP
\fB\-\-backtrace\-format\fR \fIformat\fR
.RS 4
Specify the
\fIformat\fR
of the backtrace written to the files specified by
\fB\-\-backtrace\-file\fR, one of
\fBplain\fR,
\fBxml\fR
or
\fBjson\fR\&.
.RE
.PP
\fB\-\-comments\-read\fR \fIpath\fR
.RS 4
Read the comments from the specified
\fIpath\fR
and add them to the output gradient\&.
.sp
The format is custom XML which should be fairly easy to generate, see the output of
\fB\-\-comments\-write\fR
for examples\&.
.RE
.PP
\fB\-\-comments\-generate\fR
.RS 4
Create a comment with summary data (the date of creation, name and version of the
cptutils
package) in the output file\&.
.RE
.PP
\fB\-f\fR, \fB\-\-foreground\fR \fIrgb\fR
.RS 4
Set the foreground colour of the output\&.
.RE
.PP
\fB\-F\fR, \fB\-\-id\-format\fR \fIformat\fR
.RS 4
This option generates the SVG gradient
id
attributes according to the specified
\fIformat\fR\&.
.sp
The
\fIformat\fR
argument should be a
\m[blue]\fB\fBprintf\fR(3)\fR\m[]
format string containing a single integer directive (and that directive will be replaced by the gradient number)\&. Thus "id\-%03i" will produce
ids "id\-001", "id\-002", \&.\&.\&.
.sp
Note that passing a format string which does not contain a single integer directive will result in undefined behaviour (in the C sense) so one should only use trusted sources for this string\&.
.RE
.PP
\fB\-h\fR, \fB\-\-help\fR
.RS 4
Brief help\&.
.RE
.PP
\fB\-o\fR, \fB\-\-output\fR \fIpath\fR
.RS 4
Write the output to
\fIpath\fR, rather than
stdout\&.
.RE
.PP
\fB\-v\fR, \fB\-\-verbose\fR
.RS 4
Verbose operation\&.
.RE
.PP
\fB\-V\fR, \fB\-\-version\fR
.RS 4
Version information\&.
.RE
.PP
\fB\-2\fR, \fB\-\-svg2\fR
.RS 4
SVG output is version 2\&.0 of the format\&. Note that version 2\&.0 is not yet completed and browser support is minimal\&.
.RE
.SH "EXAMPLE"
.PP
Convert a Photoshop gradient, foo\&.grd, to a set of GIMP gradients, foo\-001\&.ggr, foo\-002\&.ggr, \&.\&.\&.
.sp
.if n \{\
.RS 4
.\}
.nf
pssvg \-v \-F "foo\-%03i" \-o tmp\&.svg foo\&.grd
svggimp \-v \-a tmp\&.svg
.fi
.if n \{\
.RE
.\}
.SH "CAVEATS"
.PP
The program handles RGB, HSB, LAB, CMYK and greyscale colour stops, converting them all to RGB and then merging the results with the opacity channel to create SVG RGBA stops\&. Conversion of non\-RGB stops is naive and takes no account of colour profiles, so the results are generally sub\-optimal\&. Integration of a CMS (colour management system) into the program to perform these conversions is under investigation\&.
.PP
The program does not handle "Book Colours" (PANTONE, COLOR FINDER, etc) for legal reasons\&.
.PP
The SVG output format does not support back/foreground colours; consequently, if the input specifies any stops with back/foreground colours then these will be converted to the colour specified by the
\fB\-\-background\fR
and
\fB\-\-foreground\fR
options, respectively\&.
.PP
The smoothness parameter used by Photoshop (which seems to parameterise some kind of spline) is not yet handled; the output contains only linear splines (corresponding to a smoothness of 0%)\&. The result is that the SVG output will sometimes look more "angular" than the Photoshop input\&. Fixing this will require research into the precise nature of the spline parameterisation used in the input\&.
.PP
Noise gradients would seem to be difficult to convert to SVG without using a huge number of stops in the output, so these will probably not be supported by this program\&.
.SH "NOTES"
.PP
Adobe
Photoshop\(rg
is a trademark of Adobe Systems Inc\&.
PANTONE\(rg
is a trademark of Pantone LLC\&.
COLOR FINDER\(rg
is a trademark of Toyo Ink Mfg\&. Co\&., Ltd\&.
.PP
This program was written using the reverse\-engineered specification of the grd5 format by Valek Filippov, Alexandre Prokoudine and Michel Mariani\&.
.SH "AUTHOR"
.PP
J\&.J\&. Green
.SH "SEE ALSO"
.PP
\m[blue]\fB\fBsvgx\fR(1)\fR\m[]\&.
