<?xml version="1.0" encoding="UTF-8"?>
<refentry
    id='qgssvg1'
    xmlns='http://docbook.org/ns/docbook'
    xmlns:xi='http://www.w3.org/2001/XInclude'
    xmlns:xlink='http://www.w3.org/1999/xlink'
    version='5.0'
    xml:lang='en'>

<xi:include href="lib/refentryinfo-date.xml"/>

<refmeta>
  <refentrytitle>QGSSVG</refentrytitle>
</refmeta>
<xi:include href='lib/refmeta.xml'/>
<xi:include href='lib/refmeta-1.xml'/>

<refnamediv id='purpose'>
<refname>qgssvg</refname>
<refpurpose>
  convert QGIS colour-ramps to SVG gradients.
</refpurpose>
</refnamediv>

<refsynopsisdiv id='synopsis'>
<cmdsynopsis>
  <command>qgssvg</command>
  <arg choice='opt'>-h </arg>
  <arg choice='opt'>-o <replaceable>path</replaceable></arg>
  <arg choice='opt'>-v </arg>
  <arg choice='opt'>-V </arg>
  <arg choice='opt'>-2 </arg>
  <arg choice='plain'><replaceable>path</replaceable></arg>
</cmdsynopsis>
</refsynopsisdiv>

<refsect1 id='description'><title>DESCRIPTION</title>
<para>
  The <command>qgssvg</command> program converts QGIS colour-ramps
  to the SVG format.  Unlike most of the other file formats handled
  by the <package>cptutils</package> package, QGIS ramps may contain
  multiple gradients and all of the input gradients will be converted
  to gradients in the output file. One can then use
  <link xlink:href='svgx-1.html'>
    <citerefentry>
      <refentrytitle>svgx</refentrytitle>
      <manvolnum>1</manvolnum>
    </citerefentry>
  </link>
  to extract single gradients and/or convert them to other formats.
</para>

<para>
  Atypically, the <replaceable>path</replaceable> to the input QGIS
  colour ramp is required, but the program will write to
  <filename moreinfo="refentry" path="/dev">stdout</filename>
  if the <option>--output</option> option is not specified.
</para>

<para>
  At present, only colour-ramps of type <literal>gradient</literal>
  can be converted (so version 2 <literal>preset</literal> ramps
  will be skipped), similarly those gradients which have the
  <literal>discrete</literal> attribute set true, but this restriction
  will hopefully be lifted in a later version.
</para>

</refsect1>

<refsect1 id='options'><title>OPTIONS</title>

<variablelist remap='TP'>
  <xi:include href="lib/varlistentry-backtrace-file.xml"/>
  <xi:include href="lib/varlistentry-backtrace-format.xml"/>
  <xi:include href="lib/varlistentry-comments-read.xml"/>
  <xi:include href="lib/varlistentry-comments-write.xml"/>
  <xi:include href="lib/varlistentry-comments-retain.xml"/>
  <xi:include href="lib/varlistentry-comments-generate.xml"/>
  <xi:include href="lib/varlistentry-help.xml"/>
  <xi:include href="lib/varlistentry-output.xml"/>
  <xi:include href="lib/varlistentry-verbose.xml"/>
  <xi:include href="lib/varlistentry-version.xml"/>
  <xi:include href="lib/varlistentry-svg2.xml"/>
</variablelist>
</refsect1>


<refsect1 id='example'><title>EXAMPLE</title>

<para>
  Convert a QGIS colour-ramp, <filename>foo.qgs</filename>, to a
  set of GIMP gradients:
</para>

<programlisting>
qgssvg -v -o tmp.svg foo.qgs
svggimp -v -a tmp.svg
</programlisting>

</refsect1>

<xi:include href="lib/refsect1-author.xml"/>

<refsect1 id='see_also'><title>SEE ALSO</title>
<para>
  <link xlink:href='svgx-1.html'>
    <citerefentry>
      <refentrytitle>svgx</refentrytitle>
      <manvolnum>1</manvolnum>
    </citerefentry>
  </link>
</para>
</refsect1>

</refentry>
