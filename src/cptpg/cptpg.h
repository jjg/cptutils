#ifndef CPTPG_H
#define CPTPG_H

#include <cptutils/comment.h>

#include <stdbool.h>

typedef struct
{
  bool verbose, percentage;
  comment_opt_t comment;
  struct
  {
    bool active;
  } hinge;
  struct
  {
    const char *path;
  } input, output;
} cptpg_opt_t;

int cptpg(const cptpg_opt_t*);

#endif
