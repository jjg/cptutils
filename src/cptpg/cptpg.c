#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptpg.h"

#include <cptutils/cpt-read.h>
#include <cptutils/cpt-normalise.h>
#include <cptutils/comment.h>
#include <cptutils/pg-write.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

static int cptpg_convert(const cpt_t*, pg_t*, const cptpg_opt_t*);

int cptpg(const cptpg_opt_t *opt)
{
  cpt_t *cpt;
  int err = 1;

  if ((cpt = cpt_read(opt->input.path)) != NULL)
    {
      if (cpt_denormalise(cpt, opt->hinge.active, 0) == 0)
        {
          if (comment_process(cpt->comment, &(opt->comment)) == 0)
            {
              pg_t *pg;

              if ((pg = pg_new()) != NULL)
                {
                  if (cptpg_convert(cpt, pg, opt) == 0)
                    {
                      if (pg_write(pg, opt->output.path) == 0)
                        err = 0;
                      else
                        btrace("error writing pg");
                    }
                  else
                    btrace("failed to convert cpt to pg");

                  pg_destroy(pg);
                }
              else
                btrace("failed to allocate pg");
            }
          else
            btrace("failed to process comment");
        }
      else
        btrace("failed denormalise of input cpt");

      cpt_destroy(cpt);
    }
  else
    btrace("failed to load cpt from %s",
           IFNULL(opt->input.path, "<stdin>"));

  if (err)
    btrace("failed to write pg to %s",
           IFNULL(opt->output.path, "<stdout>"));

  return err;
}

static int cptpg_convert(const cpt_t *cpt,
                         pg_t *pg,
                         const cptpg_opt_t *opt)
{
  pg_set_percentage(pg, opt->percentage);

  if (cpt->segment == NULL)
    {
      btrace("cpt has no segments");
      return 1;
    }

  /* FIXME: when HSV interpolation done */

  switch (cpt->interpolate)
    {
    case model_rgb:
      break;
    case model_hsv:
    case model_cmyk:
      btrace("conversion of %s interpolation not implemented",
             model_name(cpt->interpolate));
      return 1;
    default:
      btrace("unknown colour model");
      return 1;
    }

  int m = 0, n;
  cpt_seg_t *seg;

  for (n = 0, seg = cpt->segment ; seg ; seg = seg->right)
    {
      cpt_sample_t
        lsmp = seg->sample.left,
        rsmp = seg->sample.right;

      if (lsmp.fill.type != rsmp.fill.type)
        {
          btrace("sorry, can't convert mixed fill types");
          return 1;
        }

      cpt_fill_type_t type = lsmp.fill.type;
      rgb_t rcol, lcol;

      switch (type)
        {
        case cpt_fill_colour_rgb:
          lcol = lsmp.fill.colour.rgb;
          rcol = rsmp.fill.colour.rgb;
          break;

        case cpt_fill_colour_hsv:
          if (hsv_to_rgb(lsmp.fill.colour.hsv, &lcol) != 0 ||
              hsv_to_rgb(rsmp.fill.colour.hsv, &rcol) != 0)
            {
              btrace("failed conversion of HSV to RGB");
              return 1;
            }
          break;

        case cpt_fill_grey:
        case cpt_fill_hatch:
        case cpt_fill_file:
        case cpt_fill_empty:
          btrace("fill type not implemented yet");
          return 1;

        default:
          btrace("strange fill type");
          return 1;
        }

      /* always insert the left colour */

      if (pg_push(pg, lsmp.val, lcol, 255) == 0)
	m++;
      else
        {
          btrace("error adding stop for segment %i left", n);
          return 1;
        }

      /*
        if there is a segment to the right, and if its left
        segment is the same colour at the our right segment
        then don't insert it. Otherwise do.
      */

      if ( ! ((seg->right) &&
              cpt_fill_eq(rsmp.fill,
                          seg->right->sample.left.fill)))
        {
	  if (pg_push(pg, rsmp.val, rcol, 255) == 0)
	    m++;
	  else
	    {
	      btrace("error adding stop for segment %i right", n);
	      return 1;
	    }
        }

      n++;
    }

  if (opt->verbose)
    printf("converted %i segments to %i stops\n", n, m);

  return 0;
}
