#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptcat.h"

#include <cptutils/cpt-normalise.h>
#include <cptutils/cpt-read.h>
#include <cptutils/cpt-write.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>
#include <cptutils/comment.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

typedef struct
{
  cpt_t *cpt;
  const char *path;
  double range[2];
  bool normalised;
} cptdat_t;

static int datcmp(const void *va, const void *vb)
{
  const cptdat_t *a = va, *b = vb;
  if (a->range[0] < b->range[0])
    return -1;
  if (a->range[0] > b->range[0])
    return 1;
  return 0;
}

static int cptcat3(const cptcat_opt_t *opt, cpt_t *cpt, cptdat_t *dat)
{
  size_t n = opt->input.n;
  cptwrite_opt_t write_opt;

  if (cpt_write_options(opt->gmt_version, &write_opt) != 0)
    {
      btrace("bad GMT version %i", opt->gmt_version);
      return 1;
    }

  model_t
    model = dat[0].cpt->model,
    interpolate = dat[0].cpt->interpolate;

  /*
    if using per-segment colour-models (colour-join is glyph), then
    the colour model is ignored, so we don't need all inputs to share
    the same model, ... but this is now checked on cpt write, so we
    don't need to check here
  */

  cpt->model = model;

  /*
    however, interpolation is global uncoditionally, so we either
    need to convert all to the same model (non-trivial) or require
    that all are the same, rare use-case, so we do the latter for
    now ...
  */

  for (size_t i = 1 ; i < n ; i++)
    {
      if (dat[i].cpt->interpolate != interpolate)
        {
          btrace("incompatible colour interpolations");
          return 1;
        }
    }

  cpt->interpolate = interpolate;

  cpt->nan = dat[0].cpt->nan;
  cpt->bg = dat[0].cpt->bg;
  cpt->fg = dat[n - 1].cpt->fg;

  for (size_t i = 0 ; i < n ; i++)
    {
      cpt_seg_t *seg;

      while ((seg = cpt_pop(dat[i].cpt)) != NULL)
	{
	  if (cpt_append(seg, cpt) != 0)
	    {
	      btrace("failed prepend");
	      return 1;
	    }
	}
    }

  /*
    create hyphenated name for new path, this has length which is
    the sum of lengths of the substrings, plus n - 1  hyphens,
    plus a termating null.  If n were to be zero then the malloc
    would be zero and the first strcpy would be out-of-bounds.
    In fact, n cannot be zero, that is ensured in main.c, but gcc-11
    warns of out-of-bounds, so we assert to suppress that warning.
  */

  assert(n > 0);

  size_t nctot = 0;

  for (size_t i = 0 ; i < n ; i++)
    nctot += (dat[i].cpt->name ? strlen(dat[i].cpt->name) : 1);

  cpt->name = malloc(nctot + n);

  strcpy(cpt->name, IFNULL(dat[0].cpt->name, "x"));

  for (size_t i = 1 ; i < n ; i++)
    {
      strcat(cpt->name, "-");
      strcat(cpt->name, IFNULL(dat[i].cpt->name, "x"));
    }

  /* hinge */

  if (opt->hinge.present)
    {
      cpt->hinge.present = true;
      cpt->hinge.type = opt->hinge.type;
    }
  else
    cpt->hinge.present = false;

  /* (de)normalisation */

  bool input_normalised = false;

  for (size_t i = 0 ; i < n ; i++)
    if (dat[i].normalised)
      input_normalised = true;

  if (opt->normalise ||
      (input_normalised && (! opt->denormalise)))
    if (cpt_normalise(cpt, false, 0) != 0)
      return 1;

  /* coerce model */

  if (cpt_coerce_model(cpt, opt->output.model) != 0)
    {
      btrace("failed to coerce colour model in output");
      return 1;
    }

  /* concatenate comments */

  const comment_t *comment[n];

  for (size_t i = 0 ; i < n ; i++)
    comment[i] = dat[i].cpt->comment;

  if (comment_cat(comment, n, "--", cpt->comment) != 0)
    {
      btrace("failed concatenation of comments");
      return 1;
    }

  if (comment_process(cpt->comment, &(opt->comment)) != 0)
    {
      btrace("failed processing of comments");
      return 1;
    }

  if (cpt_write(cpt, &write_opt, opt->output.path) != 0)
    {
      btrace("failed write to %s", opt->output.path);
      return 1;
    }

  return 0;
}

static int cptcat2(const cptcat_opt_t *opt, cptdat_t *dat)
{
  size_t n = opt->input.n;

  for (size_t i = 0 ; i < n ; i++)
    {
      double *range = dat[i].range;

      if (range[0] >= range[1])
	{
	  btrace("%s is decreasing", dat[i].path);
	  return 1;
	}
    }

  qsort(dat, n, sizeof(cptdat_t), datcmp);

  for (size_t i = 0 ; i + 1 < n ; i++)
    {
      if (dat[i].range[1] != dat[i + 1].range[0])
	{
	  btrace("non-contiguous input:");
	  btrace("  %s : %g < z < %g",
                 dat[i].path,
                 dat[i].range[0],
                 dat[i].range[1]);
	  btrace("  %s : %g < z < %g",
                 dat[i + 1].path,
                 dat[i + 1].range[0],
                 dat[i + 1].range[1]);
	  return 1;
	}
    }

  cpt_t *cpt;

  if ((cpt = cpt_new()) == NULL)
    return 1;

  int err = cptcat3(opt, cpt, dat);

  cpt_destroy(cpt);

  return err;
}

/*
  reads the i-th path of opt->inputs into the i-th element of
  dats recursively, cleans up previously allocated inputs on
  failure
*/

static int load_input(size_t i, size_t n,
                      cptdat_t *dats,
                      const cptcat_opt_t *opt)
{
  if (i >= n)
    return 0;

  cptdat_t *dat = dats + i;
  const char *path = opt->input.path[i];

  if ((dat->cpt = cpt_read(path)) == NULL)
    {
      btrace("failed to read %s", path);
      return 1;
    }

  if (dat->cpt->hinge.present)
    fprintf(stderr, "ignoring hinge in %s\n", path);

  dat->path = path;
  dat->normalised = dat->cpt->range.present;

  if ((! dat->normalised) ||
      (cpt_denormalise(dat->cpt, false, 0) == 0))
    {
      if (cpt_range_implicit(dat->cpt, dat->range) == 0)
        {
          if (opt->verbose)
            printf("  %s\n", dat->path);

          if (load_input(i + 1, n, dats, opt) == 0)
            return 0;
        }
      else
        btrace("failed to get zrange");
    }
  else
    btrace("failed denormalise");

  cpt_destroy(dat->cpt);
  dat->cpt = NULL;

  return 1;
}

static int load_inputs(size_t n, cptdat_t *dats, const cptcat_opt_t *opt)
{
  return load_input(0, n, dats, opt);
}

int cptcat(const cptcat_opt_t *opt)
{
  size_t n = opt->input.n;
  cptdat_t dat[n];

  if (load_inputs(n, dat, opt) != 0)
    return 1;

  int err = cptcat2(opt, dat);

  for (size_t i = 0 ; i < n ; i++)
    cpt_destroy(dat[i].cpt);

  return err;
}
