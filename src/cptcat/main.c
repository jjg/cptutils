#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "options.h"
#include "helper-btrace.h"
#include "helper-comment.h"
#include "helper-gmt-ver.h"
#include "helper-model.h"
#include "helper-normalise.h"
#include "helper-output.h"
#include "cptcat.h"

#include <cptutils/btrace.h>

#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
  this helper is different in behaviour and only used here, so
  we make it static
*/

static int helper_hinge(const struct gengetopt_args_info *info,
                        cptcat_opt_t *opt)
{
  if (info->hinge_hard_flag && info->hinge_soft_flag)
    {
      fprintf(stderr, "cannot have --hinge-hard and --hinge-soft\n");
      return 1;
    }

  if (info->hinge_hard_flag)
    {
      opt->hinge.present = true;
      opt->hinge.type = hinge_hard;
    }
  else if (info->hinge_soft_flag)
    {
      opt->hinge.present = true;
      opt->hinge.type = hinge_soft;
    }
  else
    opt->hinge.present = false;

  return 0;
}

static int main_wrap(const struct gengetopt_args_info *info)
{
  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  cptcat_opt_t opt = {
    .verbose = info->verbose_flag
  };

  if ((helper_output(info, &opt) != 0) ||
      (helper_comment(info, &opt) != 0) ||
      (helper_model(info, &opt) != 0) ||
      (helper_normalise(info, &opt) != 0) ||
      (helper_hinge(info, &opt) != 0))
    return EXIT_FAILURE;

  if (info->inputs_num < 1)
    {
      fprintf(stderr, "at least 1 input file required\n");
      return EXIT_FAILURE;
    }

  opt.input.n = info->inputs_num;
  opt.input.path = (const char**)info->inputs;

  if (helper_gmt_ver(info, &opt) != 0)
    return EXIT_FAILURE;

  if (opt.verbose)
    printf("This is cptcat (version %s)\n", VERSION);

  btrace_enable("cptcat");

  int err;

  if ((err = cptcat(&opt)) != 0)
    helper_btrace(info);

  btrace_reset();
  btrace_disable();

  if (opt.verbose)
    printf("done.\n");

  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  int status = EXIT_FAILURE;

  if (options(argc, argv, &info) != 0)
    fprintf(stderr, "failed to parse command line\n");
  else
    {
      status = main_wrap(&info);
      options_free(&info);
    }

  return status;
}
