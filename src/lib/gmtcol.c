/* ANSI-C code produced by gperf version 3.1 */
/* Command-line: /usr/bin/gperf --delimiters , --struct-type --language ANSI-C --lookup-function-name gmtcol_lookup --ignore-case --readonly-tables --enum --output-file gmtcol.c gmtcol.gperf  */
/* Computed positions: -k'1,3,5-8,12-13,$' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gperf@gnu.org>."
#endif

#line 1 "gmtcol.gperf"
 /* -*- c -*-

  This is part of cptutils package and is derived from the
  files gmt_colorname.h and gmt_colors_rgb.h from the GMT
  package.

  Modifications are Copyright (c) J.J. Green 2013

  The original header follows

  ---
  $Id: gmt_init.c 9923 2012-12-18 20:45:53Z pwessel $

  Copyright (c) 1991-2013 by P. Wessel and W. H. F. Smith
  See LICENSE.TXT file for copying and redistribution conditions.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 or any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  Contact info: gmt.soest.hawaii.edu
  ---
 */

#include <stdlib.h>
#include <string.h>
#include "cptutils/gmtcol.h"

#line 36 "gmtcol.gperf"
struct gmtcol_t;
/* maximum key range = 4079, duplicates = 0 */

#ifndef GPERF_DOWNCASE
#define GPERF_DOWNCASE 1
static unsigned char gperf_downcase[256] =
  {
      0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
     15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
     30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,
     45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
     60,  61,  62,  63,  64,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106,
    107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121,
    122,  91,  92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103, 104,
    105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
    120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
    135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
    150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164,
    165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
    180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194,
    195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
    210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224,
    225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254,
    255
  };
#endif

#ifndef GPERF_CASE_STRCMP
#define GPERF_CASE_STRCMP 1
static int
gperf_case_strcmp (register const char *s1, register const char *s2)
{
  for (;;)
    {
      unsigned char c1 = gperf_downcase[(unsigned char)*s1++];
      unsigned char c2 = gperf_downcase[(unsigned char)*s2++];
      if (c1 != 0 && c1 == c2)
        continue;
      return (int)c1 - (int)c2;
    }
}
#endif

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (register const char *str, register size_t len)
{
  static const unsigned short asso_values[] =
    {
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,  800,   25,
        20,    5,    0,  845,  785,  780,  640,  620, 4084, 4084,
      4084, 4084, 4084, 4084, 4084,   80,    0,  686,    5,    0,
       260,    0,   85,  770,   20,  950,  210,   95,  160,  195,
       995,  335,    0,    0,  135,  425,   55,  863, 4084,   60,
      4084, 4084, 4084, 4084, 4084, 4084, 4084,   80,    0,  686,
         5,    0,  260,    0,   85,  770,   20,  950,  210,   95,
       160,  195,  995,  335,    0,    0,  135,  425,   55,  863,
      4084,   60, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084, 4084,
      4084, 4084, 4084, 4084, 4084, 4084
    };
  register unsigned int hval = len;

  switch (hval)
    {
      default:
        hval += asso_values[(unsigned char)str[12]];
      /*FALLTHROUGH*/
      case 12:
        hval += asso_values[(unsigned char)str[11]];
      /*FALLTHROUGH*/
      case 11:
      case 10:
      case 9:
      case 8:
        hval += asso_values[(unsigned char)str[7]];
      /*FALLTHROUGH*/
      case 7:
        hval += asso_values[(unsigned char)str[6]];
      /*FALLTHROUGH*/
      case 6:
        hval += asso_values[(unsigned char)str[5]];
      /*FALLTHROUGH*/
      case 5:
        hval += asso_values[(unsigned char)str[4]];
      /*FALLTHROUGH*/
      case 4:
      case 3:
        hval += asso_values[(unsigned char)str[2]];
      /*FALLTHROUGH*/
      case 2:
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval + asso_values[(unsigned char)str[len - 1]];
}

const struct gmtcol_t *
gmtcol_lookup (register const char *str, register size_t len)
{
  enum
    {
      TOTAL_KEYWORDS = 663,
      MIN_WORD_LENGTH = 3,
      MAX_WORD_LENGTH = 20,
      MIN_HASH_VALUE = 5,
      MAX_HASH_VALUE = 4083
    };

  static const struct gmtcol_t wordlist[] =
    {
      {""}, {""}, {""}, {""}, {""},
#line 503 "gmtcol.gperf"
      {"grey4", 10, 10, 10},
#line 543 "gmtcol.gperf"
      {"grey44", 112, 112, 112},
      {""}, {""},
#line 438 "gmtcol.gperf"
      {"red4", 139, 0, 0},
      {""},
#line 533 "gmtcol.gperf"
      {"grey34", 87, 87, 87},
      {""},
#line 151 "gmtcol.gperf"
      {"red", 255, 0, 0},
#line 437 "gmtcol.gperf"
      {"red3", 205, 0, 0},
#line 502 "gmtcol.gperf"
      {"grey3", 8, 8, 8},
#line 542 "gmtcol.gperf"
      {"grey43", 110, 110, 110},
      {""}, {""}, {""}, {""},
#line 532 "gmtcol.gperf"
      {"grey33", 84, 84, 84},
#line 153 "gmtcol.gperf"
      {"darkred", 139, 0, 0},
      {""}, {""}, {""},
#line 523 "gmtcol.gperf"
      {"grey24", 61, 61, 61},
      {""}, {""},
#line 436 "gmtcol.gperf"
      {"red2", 238, 0, 0},
      {""},
#line 513 "gmtcol.gperf"
      {"grey14", 36, 36, 36},
      {""}, {""},
#line 435 "gmtcol.gperf"
      {"red1", 255, 0, 0},
      {""},
#line 522 "gmtcol.gperf"
      {"grey23", 59, 59, 59},
      {""}, {""}, {""}, {""},
#line 512 "gmtcol.gperf"
      {"grey13", 33, 33, 33},
      {""}, {""}, {""},
#line 501 "gmtcol.gperf"
      {"grey2", 5, 5, 5},
#line 541 "gmtcol.gperf"
      {"grey42", 107, 107, 107},
      {""}, {""}, {""}, {""},
#line 531 "gmtcol.gperf"
      {"grey32", 82, 82, 82},
      {""}, {""}, {""},
#line 500 "gmtcol.gperf"
      {"grey1", 3, 3, 3},
#line 540 "gmtcol.gperf"
      {"grey41", 105, 105, 105},
      {""}, {""}, {""}, {""},
#line 530 "gmtcol.gperf"
      {"grey31", 79, 79, 79},
      {""}, {""},
#line 181 "gmtcol.gperf"
      {"grey", 190, 190, 190},
      {""},
#line 521 "gmtcol.gperf"
      {"grey22", 56, 56, 56},
      {""}, {""}, {""}, {""},
#line 511 "gmtcol.gperf"
      {"grey12", 31, 31, 31},
      {""}, {""}, {""}, {""},
#line 520 "gmtcol.gperf"
      {"grey21", 54, 54, 54},
      {""}, {""}, {""}, {""},
#line 510 "gmtcol.gperf"
      {"grey11", 28, 28, 28},
      {""}, {""}, {""},
#line 604 "gmtcol.gperf"
      {"gray4", 10, 10, 10},
#line 644 "gmtcol.gperf"
      {"gray44", 112, 112, 112},
      {""}, {""}, {""}, {""},
#line 634 "gmtcol.gperf"
      {"gray34", 87, 87, 87},
      {""}, {""}, {""},
#line 603 "gmtcol.gperf"
      {"gray3", 8, 8, 8},
#line 643 "gmtcol.gperf"
      {"gray43", 110, 110, 110},
      {""}, {""}, {""}, {""},
#line 633 "gmtcol.gperf"
      {"gray33", 84, 84, 84},
      {""}, {""}, {""}, {""},
#line 624 "gmtcol.gperf"
      {"gray24", 61, 61, 61},
      {""}, {""}, {""}, {""},
#line 614 "gmtcol.gperf"
      {"gray14", 36, 36, 36},
      {""}, {""}, {""}, {""},
#line 623 "gmtcol.gperf"
      {"gray23", 59, 59, 59},
      {""}, {""}, {""}, {""},
#line 613 "gmtcol.gperf"
      {"gray13", 33, 33, 33},
      {""}, {""}, {""},
#line 602 "gmtcol.gperf"
      {"gray2", 5, 5, 5},
#line 642 "gmtcol.gperf"
      {"gray42", 107, 107, 107},
      {""}, {""}, {""}, {""},
#line 632 "gmtcol.gperf"
      {"gray32", 82, 82, 82},
      {""},
#line 179 "gmtcol.gperf"
      {"darkgrey", 169, 169, 169},
      {""},
#line 601 "gmtcol.gperf"
      {"gray1", 3, 3, 3},
#line 641 "gmtcol.gperf"
      {"gray41", 105, 105, 105},
      {""}, {""}, {""}, {""},
#line 631 "gmtcol.gperf"
      {"gray31", 79, 79, 79},
      {""}, {""},
#line 180 "gmtcol.gperf"
      {"gray", 190, 190, 190},
      {""},
#line 622 "gmtcol.gperf"
      {"gray22", 56, 56, 56},
      {""}, {""},
#line 81 "gmtcol.gperf"
      {"slategrey", 112, 128, 144},
      {""},
#line 612 "gmtcol.gperf"
      {"gray12", 31, 31, 31},
      {""}, {""}, {""}, {""},
#line 621 "gmtcol.gperf"
      {"gray21", 54, 54, 54},
      {""}, {""}, {""}, {""},
#line 611 "gmtcol.gperf"
      {"gray11", 28, 28, 28},
      {""}, {""}, {""}, {""},
#line 330 "gmtcol.gperf"
      {"green4", 0, 139, 0},
      {""}, {""}, {""},
#line 274 "gmtcol.gperf"
      {"slategray4", 108, 123, 139},
      {""},
#line 326 "gmtcol.gperf"
      {"springgreen4", 0, 139, 69},
      {""},
#line 97 "gmtcol.gperf"
      {"darkgreen", 0, 100, 0},
#line 273 "gmtcol.gperf"
      {"slategray3", 159, 182, 205},
#line 329 "gmtcol.gperf"
      {"green3", 0, 205, 0},
      {""}, {""}, {""}, {""}, {""},
#line 325 "gmtcol.gperf"
      {"springgreen3", 0, 205, 102},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 272 "gmtcol.gperf"
      {"slategray2", 185, 211, 238},
      {""}, {""}, {""}, {""},
#line 271 "gmtcol.gperf"
      {"slategray1", 198, 226, 255},
      {""}, {""}, {""}, {""},
#line 190 "gmtcol.gperf"
      {"snow4", 139, 137, 137},
      {""}, {""}, {""}, {""}, {""},
#line 328 "gmtcol.gperf"
      {"green2", 0, 238, 0},
      {""}, {""}, {""},
#line 189 "gmtcol.gperf"
      {"snow3", 205, 201, 201},
      {""},
#line 324 "gmtcol.gperf"
      {"springgreen2", 0, 238, 118},
#line 178 "gmtcol.gperf"
      {"darkgray", 169, 169, 169},
      {""},
#line 362 "gmtcol.gperf"
      {"gold4", 139, 117, 0},
#line 327 "gmtcol.gperf"
      {"green1", 0, 255, 0},
      {""}, {""},
#line 123 "gmtcol.gperf"
      {"gold", 255, 215, 0},
      {""}, {""},
#line 323 "gmtcol.gperf"
      {"springgreen1", 0, 255, 127},
      {""}, {""},
#line 361 "gmtcol.gperf"
      {"gold3", 205, 173, 0},
      {""},
#line 177 "gmtcol.gperf"
      {"dimgrey", 105, 105, 105},
      {""},
#line 80 "gmtcol.gperf"
      {"slategray", 112, 128, 144},
#line 73 "gmtcol.gperf"
      {"dodgerblue", 30, 144, 255},
#line 254 "gmtcol.gperf"
      {"dodgerblue4", 16, 78, 139},
      {""}, {""}, {""}, {""},
#line 253 "gmtcol.gperf"
      {"dodgerblue3", 24, 116, 205},
      {""}, {""}, {""},
#line 188 "gmtcol.gperf"
      {"snow2", 238, 233, 233},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 382 "gmtcol.gperf"
      {"sienna4", 139, 71, 38},
      {""},
#line 318 "gmtcol.gperf"
      {"seagreen4", 46, 139, 87},
#line 187 "gmtcol.gperf"
      {"snow1", 255, 250, 250},
#line 252 "gmtcol.gperf"
      {"dodgerblue2", 28, 134, 238},
      {""}, {""},
#line 317 "gmtcol.gperf"
      {"seagreen3", 67, 205, 128},
#line 360 "gmtcol.gperf"
      {"gold2", 238, 201, 0},
#line 251 "gmtcol.gperf"
      {"dodgerblue1", 30, 144, 255},
#line 381 "gmtcol.gperf"
      {"sienna3", 205, 104, 57},
#line 314 "gmtcol.gperf"
      {"darkseagreen4", 105, 139, 105},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 359 "gmtcol.gperf"
      {"gold1", 255, 215, 0},
      {""}, {""},
#line 313 "gmtcol.gperf"
      {"darkseagreen3", 155, 205, 155},
#line 316 "gmtcol.gperf"
      {"seagreen2", 78, 238, 148},
      {""},
#line 162 "gmtcol.gperf"
      {"darkmagenta", 139, 0, 139},
      {""}, {""},
#line 315 "gmtcol.gperf"
      {"seagreen1", 84, 255, 159},
      {""}, {""}, {""}, {""},
#line 62 "gmtcol.gperf"
      {"navy", 0, 0, 128},
      {""},
#line 145 "gmtcol.gperf"
      {"orange", 255, 165, 0},
#line 418 "gmtcol.gperf"
      {"orange4", 139, 90, 0},
      {""}, {""},
#line 434 "gmtcol.gperf"
      {"orangered4", 139, 37, 0},
      {""},
#line 380 "gmtcol.gperf"
      {"sienna2", 238, 121, 66},
      {""},
#line 150 "gmtcol.gperf"
      {"orangered", 255, 69, 0},
#line 433 "gmtcol.gperf"
      {"orangered3", 205, 55, 0},
      {""},
#line 417 "gmtcol.gperf"
      {"orange3", 205, 133, 0},
      {""}, {""}, {""}, {""},
#line 379 "gmtcol.gperf"
      {"sienna1", 255, 130, 71},
#line 312 "gmtcol.gperf"
      {"darkseagreen2", 180, 238, 180},
#line 394 "gmtcol.gperf"
      {"tan4", 139, 90, 43},
      {""}, {""}, {""}, {""},
#line 393 "gmtcol.gperf"
      {"tan3", 205, 133, 63},
#line 432 "gmtcol.gperf"
      {"orangered2", 238, 64, 0},
      {""},
#line 176 "gmtcol.gperf"
      {"dimgray", 105, 105, 105},
#line 311 "gmtcol.gperf"
      {"darkseagreen1", 193, 255, 193},
      {""},
#line 431 "gmtcol.gperf"
      {"orangered1", 255, 69, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 392 "gmtcol.gperf"
      {"tan2", 238, 154, 73},
      {""}, {""},
#line 416 "gmtcol.gperf"
      {"orange2", 238, 154, 0},
      {""},
#line 391 "gmtcol.gperf"
      {"tan1", 255, 165, 79},
#line 106 "gmtcol.gperf"
      {"green", 0, 255, 0},
#line 129 "gmtcol.gperf"
      {"sienna", 160, 82, 45},
      {""}, {""}, {""}, {""},
#line 104 "gmtcol.gperf"
      {"springgreen", 0, 255, 127},
#line 415 "gmtcol.gperf"
      {"orange1", 255, 165, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 60 "gmtcol.gperf"
      {"mistyrose", 255, 228, 225},
#line 234 "gmtcol.gperf"
      {"mistyrose4", 139, 125, 123},
#line 406 "gmtcol.gperf"
      {"brown4", 139, 35, 35},
      {""},
#line 152 "gmtcol.gperf"
      {"lightred", 255, 128, 128},
      {""},
#line 233 "gmtcol.gperf"
      {"mistyrose3", 205, 183, 181},
      {""}, {""}, {""}, {""}, {""},
#line 405 "gmtcol.gperf"
      {"brown3", 205, 51, 51},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 232 "gmtcol.gperf"
      {"mistyrose2", 238, 213, 210},
      {""}, {""}, {""}, {""},
#line 231 "gmtcol.gperf"
      {"mistyrose1", 255, 228, 225},
#line 128 "gmtcol.gperf"
      {"saddlebrown", 139, 69, 19},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 466 "gmtcol.gperf"
      {"violetred4", 139, 34, 82},
      {""}, {""}, {""},
#line 161 "gmtcol.gperf"
      {"violetred", 208, 32, 144},
#line 465 "gmtcol.gperf"
      {"violetred3", 205, 50, 120},
#line 404 "gmtcol.gperf"
      {"brown2", 238, 59, 59},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 100 "gmtcol.gperf"
      {"seagreen", 46, 139, 87},
      {""}, {""},
#line 403 "gmtcol.gperf"
      {"brown1", 255, 64, 64},
      {""}, {""},
#line 182 "gmtcol.gperf"
      {"lightgrey", 211, 211, 211},
#line 464 "gmtcol.gperf"
      {"violetred2", 238, 58, 140},
      {""},
#line 99 "gmtcol.gperf"
      {"darkseagreen", 143, 188, 143},
      {""}, {""},
#line 463 "gmtcol.gperf"
      {"violetred1", 255, 62, 150},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 71 "gmtcol.gperf"
      {"blue", 0, 0, 255},
#line 250 "gmtcol.gperf"
      {"blue4", 0, 0, 139},
#line 47 "gmtcol.gperf"
      {"bisque", 255, 228, 196},
#line 202 "gmtcol.gperf"
      {"bisque4", 139, 125, 107},
      {""}, {""}, {""}, {""}, {""},
#line 58 "gmtcol.gperf"
      {"lavender", 230, 230, 250},
      {""},
#line 249 "gmtcol.gperf"
      {"blue3", 0, 0, 205},
      {""},
#line 201 "gmtcol.gperf"
      {"bisque3", 205, 183, 158},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 146 "gmtcol.gperf"
      {"darkorange", 255, 140, 0},
#line 422 "gmtcol.gperf"
      {"darkorange4", 139, 69, 0},
      {""}, {""}, {""}, {""},
#line 421 "gmtcol.gperf"
      {"darkorange3", 205, 102, 0},
#line 462 "gmtcol.gperf"
      {"maroon4", 139, 28, 98},
#line 135 "gmtcol.gperf"
      {"tan", 210, 180, 140},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 461 "gmtcol.gperf"
      {"maroon3", 205, 41, 144},
      {""}, {""},
#line 248 "gmtcol.gperf"
      {"blue2", 0, 0, 238},
#line 420 "gmtcol.gperf"
      {"darkorange2", 238, 118, 0},
#line 200 "gmtcol.gperf"
      {"bisque2", 238, 213, 183},
      {""},
#line 110 "gmtcol.gperf"
      {"limegreen", 50, 205, 50},
      {""},
#line 419 "gmtcol.gperf"
      {"darkorange1", 255, 127, 0},
      {""},
#line 470 "gmtcol.gperf"
      {"magenta4", 139, 0, 139},
      {""},
#line 247 "gmtcol.gperf"
      {"blue1", 0, 0, 255},
      {""},
#line 199 "gmtcol.gperf"
      {"bisque1", 255, 228, 196},
      {""}, {""}, {""}, {""}, {""},
#line 469 "gmtcol.gperf"
      {"magenta3", 205, 0, 205},
      {""}, {""}, {""}, {""}, {""},
#line 183 "gmtcol.gperf"
      {"lightgray", 211, 211, 211},
      {""}, {""},
#line 460 "gmtcol.gperf"
      {"maroon2", 238, 48, 167},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 459 "gmtcol.gperf"
      {"maroon1", 255, 52, 179},
      {""}, {""},
#line 56 "gmtcol.gperf"
      {"azure", 240, 255, 255},
#line 238 "gmtcol.gperf"
      {"azure4", 131, 139, 139},
      {""}, {""}, {""},
#line 98 "gmtcol.gperf"
      {"lightgreen", 144, 238, 144},
      {""}, {""},
#line 468 "gmtcol.gperf"
      {"magenta2", 238, 0, 238},
      {""},
#line 139 "gmtcol.gperf"
      {"brown", 165, 42, 42},
#line 237 "gmtcol.gperf"
      {"azure3", 193, 205, 205},
      {""}, {""}, {""}, {""},
#line 165 "gmtcol.gperf"
      {"violet", 238, 130, 238},
      {""},
#line 467 "gmtcol.gperf"
      {"magenta1", 255, 0, 255},
#line 230 "gmtcol.gperf"
      {"lavenderblush4", 139, 131, 134},
      {""}, {""}, {""}, {""},
#line 229 "gmtcol.gperf"
      {"lavenderblush3", 205, 193, 197},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 228 "gmtcol.gperf"
      {"lavenderblush2", 238, 224, 229},
      {""},
#line 236 "gmtcol.gperf"
      {"azure2", 224, 238, 238},
      {""}, {""},
#line 227 "gmtcol.gperf"
      {"lavenderblush1", 255, 240, 245},
      {""}, {""},
#line 163 "gmtcol.gperf"
      {"magenta", 255, 0, 255},
      {""}, {""},
#line 141 "gmtcol.gperf"
      {"darksalmon", 233, 150, 122},
#line 235 "gmtcol.gperf"
      {"azure1", 240, 255, 255},
      {""},
#line 94 "gmtcol.gperf"
      {"darkslategrey", 47, 79, 79},
      {""}, {""},
#line 112 "gmtcol.gperf"
      {"forestgreen", 34, 139, 34},
#line 430 "gmtcol.gperf"
      {"tomato4", 139, 54, 38},
      {""}, {""}, {""}, {""},
#line 410 "gmtcol.gperf"
      {"salmon4", 139, 76, 57},
      {""}, {""},
#line 366 "gmtcol.gperf"
      {"goldenrod4", 139, 105, 20},
      {""},
#line 429 "gmtcol.gperf"
      {"tomato3", 205, 79, 57},
      {""},
#line 124 "gmtcol.gperf"
      {"goldenrod", 218, 165, 32},
#line 365 "gmtcol.gperf"
      {"goldenrod3", 205, 155, 29},
      {""},
#line 409 "gmtcol.gperf"
      {"salmon3", 205, 112, 84},
      {""},
#line 306 "gmtcol.gperf"
      {"darkslategray4", 82, 139, 139},
#line 134 "gmtcol.gperf"
      {"sandybrown", 244, 164, 96},
      {""}, {""}, {""},
#line 305 "gmtcol.gperf"
      {"darkslategray3", 121, 205, 205},
      {""}, {""}, {""}, {""},
#line 194 "gmtcol.gperf"
      {"seashell4", 139, 134, 130},
#line 364 "gmtcol.gperf"
      {"goldenrod2", 238, 180, 34},
      {""}, {""}, {""},
#line 193 "gmtcol.gperf"
      {"seashell3", 205, 197, 191},
#line 363 "gmtcol.gperf"
      {"goldenrod1", 255, 193, 37},
      {""}, {""}, {""},
#line 304 "gmtcol.gperf"
      {"darkslategray2", 141, 238, 238},
      {""}, {""},
#line 428 "gmtcol.gperf"
      {"tomato2", 238, 92, 66},
      {""},
#line 303 "gmtcol.gperf"
      {"darkslategray1", 151, 255, 255},
      {""}, {""},
#line 408 "gmtcol.gperf"
      {"salmon2", 238, 130, 98},
#line 59 "gmtcol.gperf"
      {"lavenderblush", 255, 240, 245},
#line 192 "gmtcol.gperf"
      {"seashell2", 238, 229, 222},
      {""},
#line 159 "gmtcol.gperf"
      {"maroon", 176, 48, 96},
#line 427 "gmtcol.gperf"
      {"tomato1", 255, 99, 71},
      {""},
#line 191 "gmtcol.gperf"
      {"seashell1", 255, 245, 238},
      {""}, {""},
#line 407 "gmtcol.gperf"
      {"salmon1", 255, 140, 105},
      {""}, {""}, {""},
#line 593 "gmtcol.gperf"
      {"grey94", 240, 240, 240},
#line 354 "gmtcol.gperf"
      {"lightyellow4", 139, 139, 122},
      {""},
#line 370 "gmtcol.gperf"
      {"darkgoldenrod4", 139, 101, 8},
      {""},
#line 144 "gmtcol.gperf"
      {"lightorange", 255, 192, 128},
      {""},
#line 125 "gmtcol.gperf"
      {"darkgoldenrod", 184, 134, 11},
#line 369 "gmtcol.gperf"
      {"darkgoldenrod3", 205, 149, 12},
      {""},
#line 592 "gmtcol.gperf"
      {"grey93", 237, 237, 237},
#line 353 "gmtcol.gperf"
      {"lightyellow3", 205, 205, 180},
      {""}, {""}, {""}, {""}, {""},
#line 93 "gmtcol.gperf"
      {"darkslategray", 47, 79, 79},
      {""}, {""},
#line 583 "gmtcol.gperf"
      {"grey84", 214, 214, 214},
#line 414 "gmtcol.gperf"
      {"lightsalmon4", 139, 87, 66},
#line 72 "gmtcol.gperf"
      {"darkblue", 0, 0, 139},
#line 368 "gmtcol.gperf"
      {"darkgoldenrod2", 238, 173, 14},
      {""}, {""}, {""}, {""},
#line 367 "gmtcol.gperf"
      {"darkgoldenrod1", 255, 185, 15},
      {""},
#line 582 "gmtcol.gperf"
      {"grey83", 212, 212, 212},
#line 413 "gmtcol.gperf"
      {"lightsalmon3", 205, 129, 98},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 591 "gmtcol.gperf"
      {"grey92", 235, 235, 235},
#line 352 "gmtcol.gperf"
      {"lightyellow2", 238, 238, 209},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 590 "gmtcol.gperf"
      {"grey91", 232, 232, 232},
#line 351 "gmtcol.gperf"
      {"lightyellow1", 255, 255, 224},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 581 "gmtcol.gperf"
      {"grey82", 209, 209, 209},
#line 412 "gmtcol.gperf"
      {"lightsalmon2", 238, 149, 114},
      {""}, {""}, {""}, {""},
#line 164 "gmtcol.gperf"
      {"lightmagenta", 255, 128, 255},
      {""}, {""},
#line 43 "gmtcol.gperf"
      {"linen", 250, 240, 230},
#line 580 "gmtcol.gperf"
      {"grey81", 207, 207, 207},
#line 411 "gmtcol.gperf"
      {"lightsalmon1", 255, 160, 122},
      {""}, {""}, {""}, {""},
#line 76 "gmtcol.gperf"
      {"skyblue", 135, 206, 235},
#line 266 "gmtcol.gperf"
      {"skyblue4", 74, 112, 139},
      {""}, {""},
#line 694 "gmtcol.gperf"
      {"gray94", 240, 240, 240},
      {""}, {""},
#line 79 "gmtcol.gperf"
      {"lightslategrey", 119, 136, 153},
#line 138 "gmtcol.gperf"
      {"lightbrown", 235, 190, 85},
      {""}, {""},
#line 265 "gmtcol.gperf"
      {"skyblue3", 108, 166, 205},
      {""}, {""},
#line 693 "gmtcol.gperf"
      {"gray93", 237, 237, 237},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 66 "gmtcol.gperf"
      {"slateblue", 106, 90, 205},
#line 242 "gmtcol.gperf"
      {"slateblue4", 71, 60, 139},
#line 684 "gmtcol.gperf"
      {"gray84", 214, 214, 214},
      {""}, {""}, {""},
#line 241 "gmtcol.gperf"
      {"slateblue3", 105, 89, 205},
#line 142 "gmtcol.gperf"
      {"salmon", 250, 128, 114},
      {""}, {""}, {""}, {""},
#line 683 "gmtcol.gperf"
      {"gray83", 212, 212, 212},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 264 "gmtcol.gperf"
      {"skyblue2", 126, 192, 238},
      {""},
#line 240 "gmtcol.gperf"
      {"slateblue2", 122, 103, 238},
#line 692 "gmtcol.gperf"
      {"gray92", 235, 235, 235},
      {""}, {""}, {""},
#line 239 "gmtcol.gperf"
      {"slateblue1", 131, 111, 255},
      {""}, {""},
#line 263 "gmtcol.gperf"
      {"skyblue1", 135, 206, 255},
      {""}, {""},
#line 691 "gmtcol.gperf"
      {"gray91", 232, 232, 232},
      {""},
#line 102 "gmtcol.gperf"
      {"lightseagreen", 32, 178, 170},
      {""}, {""},
#line 149 "gmtcol.gperf"
      {"tomato", 255, 99, 71},
      {""}, {""}, {""}, {""},
#line 682 "gmtcol.gperf"
      {"gray82", 209, 209, 209},
      {""}, {""}, {""}, {""},
#line 302 "gmtcol.gperf"
      {"cyan4", 0, 139, 139},
      {""}, {""}, {""},
#line 132 "gmtcol.gperf"
      {"beige", 245, 245, 220},
#line 681 "gmtcol.gperf"
      {"gray81", 207, 207, 207},
      {""}, {""}, {""}, {""},
#line 301 "gmtcol.gperf"
      {"cyan3", 0, 205, 205},
      {""}, {""}, {""}, {""},
#line 573 "gmtcol.gperf"
      {"grey74", 189, 189, 189},
      {""}, {""},
#line 78 "gmtcol.gperf"
      {"lightslategray", 119, 136, 153},
      {""},
#line 563 "gmtcol.gperf"
      {"grey64", 163, 163, 163},
      {""}, {""},
#line 101 "gmtcol.gperf"
      {"mediumseagreen", 60, 179, 113},
      {""},
#line 572 "gmtcol.gperf"
      {"grey73", 186, 186, 186},
      {""}, {""}, {""}, {""},
#line 562 "gmtcol.gperf"
      {"grey63", 161, 161, 161},
      {""},
#line 41 "gmtcol.gperf"
      {"seashell", 255, 245, 238},
      {""}, {""},
#line 143 "gmtcol.gperf"
      {"lightsalmon", 255, 160, 122},
      {""}, {""}, {""}, {""},
#line 300 "gmtcol.gperf"
      {"cyan2", 0, 238, 238},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 299 "gmtcol.gperf"
      {"cyan1", 0, 255, 255},
      {""}, {""}, {""}, {""},
#line 571 "gmtcol.gperf"
      {"grey72", 184, 184, 184},
      {""}, {""}, {""}, {""},
#line 561 "gmtcol.gperf"
      {"grey62", 158, 158, 158},
      {""},
#line 482 "gmtcol.gperf"
      {"mediumorchid4", 122, 55, 139},
      {""}, {""},
#line 570 "gmtcol.gperf"
      {"grey71", 181, 181, 181},
#line 168 "gmtcol.gperf"
      {"mediumorchid", 186, 85, 211},
      {""}, {""},
#line 69 "gmtcol.gperf"
      {"mediumblue", 0, 0, 205},
#line 560 "gmtcol.gperf"
      {"grey61", 156, 156, 156},
      {""},
#line 481 "gmtcol.gperf"
      {"mediumorchid3", 180, 82, 205},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 553 "gmtcol.gperf"
      {"grey54", 138, 138, 138},
      {""}, {""},
#line 74 "gmtcol.gperf"
      {"steelblue", 70, 130, 180},
#line 258 "gmtcol.gperf"
      {"steelblue4", 54, 100, 139},
      {""}, {""},
#line 63 "gmtcol.gperf"
      {"navyblue", 0, 0, 128},
      {""},
#line 257 "gmtcol.gperf"
      {"steelblue3", 79, 148, 205},
#line 552 "gmtcol.gperf"
      {"grey53", 135, 135, 135},
      {""}, {""}, {""}, {""},
#line 674 "gmtcol.gperf"
      {"gray74", 189, 189, 189},
      {""},
#line 65 "gmtcol.gperf"
      {"darkslateblue", 72, 61, 139},
      {""}, {""},
#line 664 "gmtcol.gperf"
      {"gray64", 163, 163, 163},
      {""},
#line 480 "gmtcol.gperf"
      {"mediumorchid2", 209, 95, 238},
      {""},
#line 256 "gmtcol.gperf"
      {"steelblue2", 92, 172, 238},
#line 673 "gmtcol.gperf"
      {"gray73", 186, 186, 186},
      {""}, {""}, {""},
#line 255 "gmtcol.gperf"
      {"steelblue1", 99, 184, 255},
#line 663 "gmtcol.gperf"
      {"gray63", 161, 161, 161},
      {""},
#line 479 "gmtcol.gperf"
      {"mediumorchid1", 224, 102, 255},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 551 "gmtcol.gperf"
      {"grey52", 133, 133, 133},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 550 "gmtcol.gperf"
      {"grey51", 130, 130, 130},
#line 426 "gmtcol.gperf"
      {"coral4", 139, 62, 47},
      {""}, {""}, {""},
#line 672 "gmtcol.gperf"
      {"gray72", 184, 184, 184},
      {""}, {""}, {""}, {""},
#line 662 "gmtcol.gperf"
      {"gray62", 158, 158, 158},
#line 425 "gmtcol.gperf"
      {"coral3", 205, 91, 69},
#line 88 "gmtcol.gperf"
      {"darkturquoise", 0, 206, 209},
#line 70 "gmtcol.gperf"
      {"royalblue", 65, 105, 225},
#line 246 "gmtcol.gperf"
      {"royalblue4", 39, 64, 139},
#line 671 "gmtcol.gperf"
      {"gray71", 181, 181, 181},
      {""}, {""}, {""},
#line 245 "gmtcol.gperf"
      {"royalblue3", 58, 95, 205},
#line 661 "gmtcol.gperf"
      {"gray61", 156, 156, 156},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 91 "gmtcol.gperf"
      {"cyan", 0, 255, 255},
#line 654 "gmtcol.gperf"
      {"gray54", 138, 138, 138},
      {""}, {""}, {""},
#line 244 "gmtcol.gperf"
      {"royalblue2", 67, 110, 238},
      {""}, {""}, {""}, {""},
#line 243 "gmtcol.gperf"
      {"royalblue1", 72, 118, 255},
#line 653 "gmtcol.gperf"
      {"gray53", 135, 135, 135},
#line 424 "gmtcol.gperf"
      {"coral2", 238, 106, 80},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 423 "gmtcol.gperf"
      {"coral1", 255, 114, 86},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 350 "gmtcol.gperf"
      {"lightgoldenrod4", 139, 129, 76},
      {""}, {""}, {""},
#line 118 "gmtcol.gperf"
      {"lightgoldenrod", 238, 221, 130},
#line 349 "gmtcol.gperf"
      {"lightgoldenrod3", 205, 190, 112},
      {""}, {""}, {""}, {""}, {""},
#line 652 "gmtcol.gperf"
      {"gray52", 133, 133, 133},
      {""},
#line 42 "gmtcol.gperf"
      {"oldlace", 253, 245, 230},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 348 "gmtcol.gperf"
      {"lightgoldenrod2", 238, 220, 130},
#line 651 "gmtcol.gperf"
      {"gray51", 130, 130, 130},
#line 486 "gmtcol.gperf"
      {"darkorchid4", 104, 34, 139},
      {""}, {""},
#line 347 "gmtcol.gperf"
      {"lightgoldenrod1", 255, 236, 139},
#line 169 "gmtcol.gperf"
      {"darkorchid", 153, 50, 204},
#line 485 "gmtcol.gperf"
      {"darkorchid3", 154, 50, 205},
      {""},
#line 83 "gmtcol.gperf"
      {"lightblue", 173, 216, 230},
#line 282 "gmtcol.gperf"
      {"lightblue4", 104, 131, 139},
      {""}, {""}, {""}, {""},
#line 281 "gmtcol.gperf"
      {"lightblue3", 154, 192, 205},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 484 "gmtcol.gperf"
      {"darkorchid2", 178, 58, 238},
      {""},
#line 390 "gmtcol.gperf"
      {"wheat4", 139, 126, 102},
      {""}, {""},
#line 483 "gmtcol.gperf"
      {"darkorchid1", 191, 62, 255},
      {""}, {""},
#line 280 "gmtcol.gperf"
      {"lightblue2", 178, 223, 238},
      {""}, {""}, {""},
#line 389 "gmtcol.gperf"
      {"wheat3", 205, 186, 150},
#line 279 "gmtcol.gperf"
      {"lightblue1", 191, 239, 255},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 378 "gmtcol.gperf"
      {"indianred4", 139, 58, 58},
#line 75 "gmtcol.gperf"
      {"deepskyblue", 0, 191, 255},
#line 262 "gmtcol.gperf"
      {"deepskyblue4", 0, 104, 139},
      {""},
#line 127 "gmtcol.gperf"
      {"indianred", 205, 92, 92},
#line 377 "gmtcol.gperf"
      {"indianred3", 205, 85, 85},
#line 222 "gmtcol.gperf"
      {"ivory4", 139, 139, 131},
      {""}, {""}, {""}, {""}, {""},
#line 261 "gmtcol.gperf"
      {"deepskyblue3", 0, 154, 205},
      {""}, {""}, {""},
#line 221 "gmtcol.gperf"
      {"ivory3", 205, 205, 193},
      {""}, {""},
#line 388 "gmtcol.gperf"
      {"wheat2", 238, 216, 174},
#line 376 "gmtcol.gperf"
      {"indianred2", 238, 99, 99},
      {""}, {""}, {""}, {""},
#line 375 "gmtcol.gperf"
      {"indianred1", 255, 106, 106},
      {""}, {""}, {""},
#line 387 "gmtcol.gperf"
      {"wheat1", 255, 231, 186},
#line 67 "gmtcol.gperf"
      {"mediumslateblue", 123, 104, 238},
      {""}, {""}, {""},
#line 113 "gmtcol.gperf"
      {"olivedrab", 107, 142, 35},
#line 338 "gmtcol.gperf"
      {"olivedrab4", 105, 139, 34},
      {""},
#line 38 "gmtcol.gperf"
      {"snow", 255, 250, 250},
      {""}, {""},
#line 337 "gmtcol.gperf"
      {"olivedrab3", 154, 205, 50},
      {""},
#line 260 "gmtcol.gperf"
      {"deepskyblue2", 0, 178, 238},
#line 374 "gmtcol.gperf"
      {"rosybrown4", 139, 105, 105},
      {""}, {""},
#line 220 "gmtcol.gperf"
      {"ivory2", 238, 238, 224},
      {""},
#line 373 "gmtcol.gperf"
      {"rosybrown3", 205, 155, 155},
      {""}, {""}, {""},
#line 259 "gmtcol.gperf"
      {"deepskyblue1", 0, 191, 255},
      {""}, {""},
#line 336 "gmtcol.gperf"
      {"olivedrab2", 179, 238, 58},
#line 219 "gmtcol.gperf"
      {"ivory1", 255, 255, 240},
      {""}, {""}, {""},
#line 335 "gmtcol.gperf"
      {"olivedrab1", 192, 255, 62},
      {""}, {""},
#line 372 "gmtcol.gperf"
      {"rosybrown2", 238, 180, 180},
      {""},
#line 53 "gmtcol.gperf"
      {"ivory", 255, 255, 240},
      {""}, {""},
#line 371 "gmtcol.gperf"
      {"rosybrown1", 255, 193, 193},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 61 "gmtcol.gperf"
      {"midnightblue", 25, 25, 112},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 147 "gmtcol.gperf"
      {"coral", 255, 127, 80},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""},
#line 55 "gmtcol.gperf"
      {"mintcream", 245, 255, 250},
      {""}, {""}, {""},
#line 82 "gmtcol.gperf"
      {"lightsteelblue", 176, 196, 222},
#line 278 "gmtcol.gperf"
      {"lightsteelblue4", 110, 123, 139},
#line 95 "gmtcol.gperf"
      {"mediumaquamarine", 102, 205, 170},
      {""}, {""}, {""},
#line 277 "gmtcol.gperf"
      {"lightsteelblue3", 162, 181, 205},
      {""}, {""},
#line 133 "gmtcol.gperf"
      {"wheat", 245, 222, 179},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 276 "gmtcol.gperf"
      {"lightsteelblue2", 188, 210, 238},
      {""}, {""}, {""}, {""},
#line 275 "gmtcol.gperf"
      {"lightsteelblue1", 202, 225, 255},
      {""}, {""}, {""},
#line 92 "gmtcol.gperf"
      {"darkcyan", 0, 139, 139},
#line 450 "gmtcol.gperf"
      {"pink4", 139, 99, 108},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 184 "gmtcol.gperf"
      {"gainsboro", 220, 220, 220},
#line 449 "gmtcol.gperf"
      {"pink3", 205, 145, 158},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 286 "gmtcol.gperf"
      {"lightcyan4", 122, 139, 139},
#line 226 "gmtcol.gperf"
      {"honeydew4", 131, 139, 131},
      {""}, {""}, {""},
#line 285 "gmtcol.gperf"
      {"lightcyan3", 180, 205, 205},
#line 225 "gmtcol.gperf"
      {"honeydew3", 193, 205, 193},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""},
#line 448 "gmtcol.gperf"
      {"pink2", 238, 169, 184},
#line 284 "gmtcol.gperf"
      {"lightcyan2", 209, 238, 238},
#line 224 "gmtcol.gperf"
      {"honeydew2", 224, 238, 224},
      {""}, {""}, {""},
#line 283 "gmtcol.gperf"
      {"lightcyan1", 224, 255, 255},
#line 223 "gmtcol.gperf"
      {"honeydew1", 240, 255, 240},
      {""}, {""},
#line 447 "gmtcol.gperf"
      {"pink1", 255, 181, 197},
#line 172 "gmtcol.gperf"
      {"purple", 160, 32, 240},
#line 490 "gmtcol.gperf"
      {"purple4", 85, 26, 139},
      {""}, {""},
#line 322 "gmtcol.gperf"
      {"palegreen4", 84, 139, 84},
      {""}, {""}, {""}, {""},
#line 321 "gmtcol.gperf"
      {"palegreen3", 124, 205, 124},
      {""},
#line 489 "gmtcol.gperf"
      {"purple3", 125, 38, 205},
      {""}, {""}, {""}, {""},
#line 126 "gmtcol.gperf"
      {"rosybrown", 188, 143, 143},
      {""}, {""},
#line 46 "gmtcol.gperf"
      {"blanchedalmond", 255, 235, 205},
      {""},
#line 140 "gmtcol.gperf"
      {"darkbrown", 120, 60, 30},
      {""}, {""},
#line 320 "gmtcol.gperf"
      {"palegreen2", 144, 238, 144},
      {""}, {""}, {""}, {""},
#line 319 "gmtcol.gperf"
      {"palegreen1", 154, 255, 154},
      {""},
#line 105 "gmtcol.gperf"
      {"lawngreen", 124, 252, 0},
      {""}, {""},
#line 508 "gmtcol.gperf"
      {"grey9", 23, 23, 23},
#line 548 "gmtcol.gperf"
      {"grey49", 125, 125, 125},
      {""}, {""}, {""},
#line 342 "gmtcol.gperf"
      {"darkolivegreen4", 110, 139, 61},
#line 538 "gmtcol.gperf"
      {"grey39", 99, 99, 99},
#line 488 "gmtcol.gperf"
      {"purple2", 145, 44, 238},
      {""}, {""},
#line 341 "gmtcol.gperf"
      {"darkolivegreen3", 162, 205, 90},
      {""},
#line 174 "gmtcol.gperf"
      {"thistle", 216, 191, 216},
#line 498 "gmtcol.gperf"
      {"thistle4", 139, 123, 139},
      {""}, {""}, {""},
#line 487 "gmtcol.gperf"
      {"purple1", 155, 48, 255},
      {""}, {""}, {""},
#line 528 "gmtcol.gperf"
      {"grey29", 74, 74, 74},
      {""},
#line 497 "gmtcol.gperf"
      {"thistle3", 205, 181, 205},
      {""},
#line 340 "gmtcol.gperf"
      {"darkolivegreen2", 188, 238, 104},
#line 518 "gmtcol.gperf"
      {"grey19", 48, 48, 48},
      {""}, {""}, {""},
#line 339 "gmtcol.gperf"
      {"darkolivegreen1", 202, 255, 112},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 68 "gmtcol.gperf"
      {"lightslateblue", 132, 112, 255},
#line 507 "gmtcol.gperf"
      {"grey8", 20, 20, 20},
#line 547 "gmtcol.gperf"
      {"grey48", 122, 122, 122},
      {""}, {""}, {""}, {""},
#line 537 "gmtcol.gperf"
      {"grey38", 97, 97, 97},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 496 "gmtcol.gperf"
      {"thistle2", 238, 210, 238},
      {""}, {""}, {""}, {""}, {""},
#line 109 "gmtcol.gperf"
      {"greenyellow", 173, 255, 47},
      {""},
#line 527 "gmtcol.gperf"
      {"grey28", 71, 71, 71},
      {""},
#line 495 "gmtcol.gperf"
      {"thistle1", 255, 225, 255},
      {""}, {""},
#line 517 "gmtcol.gperf"
      {"grey18", 46, 46, 46},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 386 "gmtcol.gperf"
      {"burlywood4", 139, 115, 85},
      {""},
#line 609 "gmtcol.gperf"
      {"gray9", 23, 23, 23},
#line 649 "gmtcol.gperf"
      {"gray49", 125, 125, 125},
#line 131 "gmtcol.gperf"
      {"burlywood", 222, 184, 135},
#line 385 "gmtcol.gperf"
      {"burlywood3", 205, 170, 125},
      {""}, {""},
#line 639 "gmtcol.gperf"
      {"gray39", 99, 99, 99},
      {""}, {""}, {""},
#line 358 "gmtcol.gperf"
      {"yellow4", 139, 139, 0},
#line 107 "gmtcol.gperf"
      {"chartreuse", 127, 255, 0},
#line 334 "gmtcol.gperf"
      {"chartreuse4", 69, 139, 0},
      {""}, {""},
#line 84 "gmtcol.gperf"
      {"lightcyan", 224, 255, 255},
      {""},
#line 333 "gmtcol.gperf"
      {"chartreuse3", 102, 205, 0},
#line 384 "gmtcol.gperf"
      {"burlywood2", 238, 197, 145},
      {""},
#line 357 "gmtcol.gperf"
      {"yellow3", 205, 205, 0},
#line 629 "gmtcol.gperf"
      {"gray29", 74, 74, 74},
      {""},
#line 383 "gmtcol.gperf"
      {"burlywood1", 255, 211, 155},
      {""}, {""},
#line 619 "gmtcol.gperf"
      {"gray19", 48, 48, 48},
      {""}, {""}, {""}, {""}, {""},
#line 332 "gmtcol.gperf"
      {"chartreuse2", 118, 238, 0},
#line 122 "gmtcol.gperf"
      {"darkyellow", 128, 128, 0},
      {""}, {""}, {""},
#line 331 "gmtcol.gperf"
      {"chartreuse1", 127, 255, 0},
      {""}, {""},
#line 608 "gmtcol.gperf"
      {"gray8", 20, 20, 20},
#line 648 "gmtcol.gperf"
      {"gray48", 122, 122, 122},
#line 77 "gmtcol.gperf"
      {"lightskyblue", 135, 206, 250},
#line 270 "gmtcol.gperf"
      {"lightskyblue4", 96, 123, 139},
      {""}, {""},
#line 638 "gmtcol.gperf"
      {"gray38", 97, 97, 97},
      {""}, {""},
#line 103 "gmtcol.gperf"
      {"palegreen", 152, 251, 152},
#line 356 "gmtcol.gperf"
      {"yellow2", 238, 238, 0},
      {""}, {""},
#line 269 "gmtcol.gperf"
      {"lightskyblue3", 141, 182, 205},
      {""},
#line 170 "gmtcol.gperf"
      {"darkviolet", 148, 0, 211},
      {""}, {""}, {""}, {""},
#line 355 "gmtcol.gperf"
      {"yellow1", 255, 255, 0},
#line 628 "gmtcol.gperf"
      {"gray28", 71, 71, 71},
      {""}, {""},
#line 49 "gmtcol.gperf"
      {"navajowhite", 255, 222, 173},
#line 210 "gmtcol.gperf"
      {"navajowhite4", 139, 121, 94},
#line 618 "gmtcol.gperf"
      {"gray18", 46, 46, 46},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 209 "gmtcol.gperf"
      {"navajowhite3", 205, 179, 139},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 268 "gmtcol.gperf"
      {"lightskyblue2", 164, 211, 238},
#line 114 "gmtcol.gperf"
      {"darkolivegreen", 85, 107, 47},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 267 "gmtcol.gperf"
      {"lightskyblue1", 176, 226, 255},
      {""}, {""}, {""}, {""}, {""},
#line 130 "gmtcol.gperf"
      {"peru", 205, 133, 63},
#line 478 "gmtcol.gperf"
      {"plum4", 139, 102, 139},
      {""}, {""}, {""}, {""},
#line 208 "gmtcol.gperf"
      {"navajowhite2", 238, 207, 161},
      {""}, {""}, {""}, {""},
#line 477 "gmtcol.gperf"
      {"plum3", 205, 150, 205},
      {""}, {""}, {""}, {""},
#line 207 "gmtcol.gperf"
      {"navajowhite1", 255, 222, 173},
      {""}, {""}, {""}, {""}, {""},
#line 148 "gmtcol.gperf"
      {"lightcoral", 240, 128, 128},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""},
#line 96 "gmtcol.gperf"
      {"aquamarine", 127, 255, 212},
#line 310 "gmtcol.gperf"
      {"aquamarine4", 69, 139, 116},
      {""}, {""}, {""},
#line 476 "gmtcol.gperf"
      {"plum2", 238, 174, 238},
#line 309 "gmtcol.gperf"
      {"aquamarine3", 102, 205, 170},
      {""}, {""}, {""},
#line 87 "gmtcol.gperf"
      {"cadetblue", 95, 158, 160},
#line 294 "gmtcol.gperf"
      {"cadetblue4", 83, 134, 139},
      {""}, {""}, {""},
#line 475 "gmtcol.gperf"
      {"plum1", 255, 187, 255},
#line 293 "gmtcol.gperf"
      {"cadetblue3", 122, 197, 205},
      {""}, {""}, {""}, {""},
#line 308 "gmtcol.gperf"
      {"aquamarine2", 118, 238, 198},
      {""}, {""}, {""}, {""},
#line 307 "gmtcol.gperf"
      {"aquamarine1", 127, 255, 212},
      {""}, {""},
#line 119 "gmtcol.gperf"
      {"lightyellow", 255, 255, 224},
      {""},
#line 292 "gmtcol.gperf"
      {"cadetblue2", 142, 229, 238},
      {""}, {""},
#line 57 "gmtcol.gperf"
      {"aliceblue", 240, 248, 255},
      {""},
#line 291 "gmtcol.gperf"
      {"cadetblue1", 152, 245, 255},
      {""}, {""},
#line 111 "gmtcol.gperf"
      {"yellowgreen", 154, 205, 50},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 136 "gmtcol.gperf"
      {"chocolate", 210, 105, 30},
#line 398 "gmtcol.gperf"
      {"chocolate4", 139, 69, 19},
      {""}, {""}, {""}, {""},
#line 397 "gmtcol.gperf"
      {"chocolate3", 205, 102, 29},
      {""}, {""},
#line 167 "gmtcol.gperf"
      {"plum", 221, 160, 221},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 396 "gmtcol.gperf"
      {"chocolate2", 238, 118, 33},
      {""}, {""},
#line 90 "gmtcol.gperf"
      {"turquoise", 64, 224, 208},
#line 298 "gmtcol.gperf"
      {"turquoise4", 0, 134, 139},
#line 395 "gmtcol.gperf"
      {"chocolate1", 255, 127, 36},
      {""}, {""}, {""},
#line 297 "gmtcol.gperf"
      {"turquoise3", 0, 197, 205},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 296 "gmtcol.gperf"
      {"turquoise2", 0, 229, 238},
      {""}, {""}, {""}, {""},
#line 295 "gmtcol.gperf"
      {"turquoise1", 0, 245, 255},
      {""}, {""}, {""}, {""},
#line 506 "gmtcol.gperf"
      {"grey7", 18, 18, 18},
#line 546 "gmtcol.gperf"
      {"grey47", 120, 120, 120},
      {""}, {""}, {""}, {""},
#line 536 "gmtcol.gperf"
      {"grey37", 94, 94, 94},
      {""}, {""}, {""},
#line 505 "gmtcol.gperf"
      {"grey6", 15, 15, 15},
#line 545 "gmtcol.gperf"
      {"grey46", 117, 117, 117},
      {""}, {""}, {""}, {""},
#line 535 "gmtcol.gperf"
      {"grey36", 92, 92, 92},
      {""}, {""}, {""}, {""},
#line 526 "gmtcol.gperf"
      {"grey27", 69, 69, 69},
      {""}, {""}, {""}, {""},
#line 516 "gmtcol.gperf"
      {"grey17", 43, 43, 43},
      {""}, {""}, {""}, {""},
#line 525 "gmtcol.gperf"
      {"grey26", 66, 66, 66},
      {""}, {""}, {""},
#line 160 "gmtcol.gperf"
      {"mediumvioletred", 199, 21, 133},
#line 515 "gmtcol.gperf"
      {"grey16", 41, 41, 41},
      {""}, {""}, {""},
#line 499 "gmtcol.gperf"
      {"grey0", 0, 0, 0},
#line 539 "gmtcol.gperf"
      {"grey40", 102, 102, 102},
      {""}, {""}, {""}, {""},
#line 529 "gmtcol.gperf"
      {"grey30", 77, 77, 77},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 519 "gmtcol.gperf"
      {"grey20", 51, 51, 51},
      {""}, {""}, {""}, {""},
#line 509 "gmtcol.gperf"
      {"grey10", 26, 26, 26},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 186 "gmtcol.gperf"
      {"white", 255, 255, 255},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 607 "gmtcol.gperf"
      {"gray7", 18, 18, 18},
#line 647 "gmtcol.gperf"
      {"gray47", 120, 120, 120},
      {""}, {""}, {""}, {""},
#line 637 "gmtcol.gperf"
      {"gray37", 94, 94, 94},
      {""}, {""}, {""},
#line 606 "gmtcol.gperf"
      {"gray6", 15, 15, 15},
#line 646 "gmtcol.gperf"
      {"gray46", 117, 117, 117},
      {""}, {""}, {""}, {""},
#line 636 "gmtcol.gperf"
      {"gray36", 92, 92, 92},
      {""},
#line 474 "gmtcol.gperf"
      {"orchid4", 139, 71, 137},
      {""}, {""},
#line 627 "gmtcol.gperf"
      {"gray27", 69, 69, 69},
#line 166 "gmtcol.gperf"
      {"orchid", 218, 112, 214},
      {""}, {""}, {""},
#line 617 "gmtcol.gperf"
      {"gray17", 43, 43, 43},
      {""},
#line 473 "gmtcol.gperf"
      {"orchid3", 205, 105, 201},
      {""}, {""},
#line 626 "gmtcol.gperf"
      {"gray26", 66, 66, 66},
      {""}, {""}, {""}, {""},
#line 616 "gmtcol.gperf"
      {"gray16", 41, 41, 41},
      {""}, {""}, {""},
#line 600 "gmtcol.gperf"
      {"gray0", 0, 0, 0},
#line 640 "gmtcol.gperf"
      {"gray40", 102, 102, 102},
      {""}, {""}, {""}, {""},
#line 630 "gmtcol.gperf"
      {"gray30", 77, 77, 77},
      {""}, {""}, {""},
#line 504 "gmtcol.gperf"
      {"grey5", 13, 13, 13},
#line 544 "gmtcol.gperf"
      {"grey45", 115, 115, 115},
      {""}, {""}, {""}, {""},
#line 534 "gmtcol.gperf"
      {"grey35", 89, 89, 89},
      {""},
#line 472 "gmtcol.gperf"
      {"orchid2", 238, 122, 233},
#line 40 "gmtcol.gperf"
      {"floralwhite", 255, 250, 240},
      {""},
#line 620 "gmtcol.gperf"
      {"gray20", 51, 51, 51},
      {""}, {""}, {""}, {""},
#line 610 "gmtcol.gperf"
      {"gray10", 26, 26, 26},
      {""},
#line 471 "gmtcol.gperf"
      {"orchid1", 255, 131, 250},
      {""}, {""},
#line 524 "gmtcol.gperf"
      {"grey25", 64, 64, 64},
      {""}, {""}, {""}, {""},
#line 514 "gmtcol.gperf"
      {"grey15", 38, 38, 38},
      {""}, {""}, {""}, {""},
#line 402 "gmtcol.gperf"
      {"firebrick4", 139, 26, 26},
      {""}, {""}, {""}, {""},
#line 401 "gmtcol.gperf"
      {"firebrick3", 205, 38, 38},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 400 "gmtcol.gperf"
      {"firebrick2", 238, 44, 44},
      {""}, {""}, {""}, {""},
#line 399 "gmtcol.gperf"
      {"firebrick1", 255, 48, 48},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 605 "gmtcol.gperf"
      {"gray5", 13, 13, 13},
#line 645 "gmtcol.gperf"
      {"gray45", 115, 115, 115},
      {""}, {""}, {""}, {""},
#line 635 "gmtcol.gperf"
      {"gray35", 89, 89, 89},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 108 "gmtcol.gperf"
      {"mediumspringgreen", 0, 250, 154},
      {""}, {""}, {""},
#line 625 "gmtcol.gperf"
      {"gray25", 64, 64, 64},
      {""}, {""}, {""},
#line 171 "gmtcol.gperf"
      {"blueviolet", 138, 43, 226},
#line 615 "gmtcol.gperf"
      {"gray15", 38, 38, 38},
      {""}, {""}, {""}, {""},
#line 346 "gmtcol.gperf"
      {"khaki4", 139, 134, 78},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 345 "gmtcol.gperf"
      {"khaki3", 205, 198, 115},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 120 "gmtcol.gperf"
      {"lightgoldenrodyellow", 250, 250, 210},
      {""}, {""}, {""}, {""},
#line 117 "gmtcol.gperf"
      {"palegoldenrod", 238, 232, 170},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""},
#line 344 "gmtcol.gperf"
      {"khaki2", 238, 230, 133},
      {""}, {""}, {""},
#line 44 "gmtcol.gperf"
      {"antiquewhite", 250, 235, 215},
#line 198 "gmtcol.gperf"
      {"antiquewhite4", 139, 131, 120},
      {""}, {""}, {""}, {""},
#line 343 "gmtcol.gperf"
      {"khaki1", 255, 246, 143},
      {""}, {""}, {""}, {""},
#line 197 "gmtcol.gperf"
      {"antiquewhite3", 205, 192, 176},
      {""}, {""}, {""}, {""},
#line 598 "gmtcol.gperf"
      {"grey99", 252, 252, 252},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 588 "gmtcol.gperf"
      {"grey89", 227, 227, 227},
      {""}, {""}, {""}, {""},
#line 196 "gmtcol.gperf"
      {"antiquewhite2", 238, 223, 204},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 195 "gmtcol.gperf"
      {"antiquewhite1", 255, 239, 219},
      {""}, {""}, {""}, {""},
#line 597 "gmtcol.gperf"
      {"grey98", 250, 250, 250},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 587 "gmtcol.gperf"
      {"grey88", 224, 224, 224},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 185 "gmtcol.gperf"
      {"whitesmoke", 245, 245, 245},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""},
#line 699 "gmtcol.gperf"
      {"gray99", 252, 252, 252},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""},
#line 50 "gmtcol.gperf"
      {"moccasin", 255, 228, 181},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 689 "gmtcol.gperf"
      {"gray89", 227, 227, 227},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 175 "gmtcol.gperf"
      {"black", 0, 0, 0},
#line 698 "gmtcol.gperf"
      {"gray98", 250, 250, 250},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 688 "gmtcol.gperf"
      {"gray88", 224, 224, 224},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 578 "gmtcol.gperf"
      {"grey79", 201, 201, 201},
      {""}, {""}, {""}, {""},
#line 568 "gmtcol.gperf"
      {"grey69", 176, 176, 176},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""},
#line 54 "gmtcol.gperf"
      {"honeydew", 240, 255, 240},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 173 "gmtcol.gperf"
      {"mediumpurple", 147, 112, 219},
#line 494 "gmtcol.gperf"
      {"mediumpurple4", 93, 71, 139},
      {""}, {""}, {""}, {""},
#line 39 "gmtcol.gperf"
      {"ghostwhite", 248, 248, 255},
      {""}, {""}, {""}, {""},
#line 493 "gmtcol.gperf"
      {"mediumpurple3", 137, 104, 205},
      {""}, {""},
#line 577 "gmtcol.gperf"
      {"grey78", 199, 199, 199},
      {""}, {""}, {""}, {""},
#line 567 "gmtcol.gperf"
      {"grey68", 173, 173, 173},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 85 "gmtcol.gperf"
      {"powderblue", 176, 224, 230},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""},
#line 558 "gmtcol.gperf"
      {"grey59", 150, 150, 150},
      {""},
#line 492 "gmtcol.gperf"
      {"mediumpurple2", 159, 121, 238},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 491 "gmtcol.gperf"
      {"mediumpurple1", 171, 130, 255},
      {""}, {""},
#line 679 "gmtcol.gperf"
      {"gray79", 201, 201, 201},
      {""},
#line 446 "gmtcol.gperf"
      {"hotpink4", 139, 58, 98},
#line 156 "gmtcol.gperf"
      {"pink", 255, 192, 203},
      {""},
#line 669 "gmtcol.gperf"
      {"gray69", 176, 176, 176},
      {""},
#line 86 "gmtcol.gperf"
      {"paleturquoise", 175, 238, 238},
#line 290 "gmtcol.gperf"
      {"paleturquoise4", 102, 139, 139},
      {""}, {""}, {""},
#line 445 "gmtcol.gperf"
      {"hotpink3", 205, 96, 144},
#line 289 "gmtcol.gperf"
      {"paleturquoise3", 150, 205, 205},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 557 "gmtcol.gperf"
      {"grey58", 148, 148, 148},
      {""}, {""},
#line 288 "gmtcol.gperf"
      {"paleturquoise2", 174, 238, 238},
      {""}, {""}, {""}, {""},
#line 287 "gmtcol.gperf"
      {"paleturquoise1", 187, 255, 255},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 678 "gmtcol.gperf"
      {"gray78", 199, 199, 199},
      {""},
#line 444 "gmtcol.gperf"
      {"hotpink2", 238, 106, 167},
      {""}, {""},
#line 668 "gmtcol.gperf"
      {"gray68", 173, 173, 173},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 443 "gmtcol.gperf"
      {"hotpink1", 255, 110, 180},
      {""},
#line 89 "gmtcol.gperf"
      {"mediumturquoise", 72, 209, 204},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 659 "gmtcol.gperf"
      {"gray59", 150, 150, 150},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 214 "gmtcol.gperf"
      {"lemonchiffon4", 139, 137, 112},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 596 "gmtcol.gperf"
      {"grey97", 247, 247, 247},
      {""}, {""},
#line 213 "gmtcol.gperf"
      {"lemonchiffon3", 205, 201, 165},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 595 "gmtcol.gperf"
      {"grey96", 245, 245, 245},
#line 121 "gmtcol.gperf"
      {"yellow", 255, 255, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 586 "gmtcol.gperf"
      {"grey87", 222, 222, 222},
      {""}, {""}, {""}, {""},
#line 658 "gmtcol.gperf"
      {"gray58", 148, 148, 148},
      {""}, {""}, {""}, {""},
#line 585 "gmtcol.gperf"
      {"grey86", 219, 219, 219},
      {""}, {""},
#line 212 "gmtcol.gperf"
      {"lemonchiffon2", 238, 233, 191},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 589 "gmtcol.gperf"
      {"grey90", 229, 229, 229},
      {""}, {""},
#line 211 "gmtcol.gperf"
      {"lemonchiffon1", 255, 250, 205},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 579 "gmtcol.gperf"
      {"grey80", 204, 204, 204},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 697 "gmtcol.gperf"
      {"gray97", 247, 247, 247},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 696 "gmtcol.gperf"
      {"gray96", 245, 245, 245},
      {""}, {""}, {""},
#line 454 "gmtcol.gperf"
      {"lightpink4", 139, 95, 101},
      {""}, {""}, {""}, {""},
#line 453 "gmtcol.gperf"
      {"lightpink3", 205, 140, 149},
#line 687 "gmtcol.gperf"
      {"gray87", 222, 222, 222},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 686 "gmtcol.gperf"
      {"gray86", 219, 219, 219},
      {""}, {""}, {""},
#line 452 "gmtcol.gperf"
      {"lightpink2", 238, 162, 173},
      {""}, {""}, {""}, {""},
#line 451 "gmtcol.gperf"
      {"lightpink1", 255, 174, 185},
#line 690 "gmtcol.gperf"
      {"gray90", 229, 229, 229},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 594 "gmtcol.gperf"
      {"grey95", 242, 242, 242},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 680 "gmtcol.gperf"
      {"gray80", 204, 204, 204},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 584 "gmtcol.gperf"
      {"grey85", 217, 217, 217},
      {""},
#line 51 "gmtcol.gperf"
      {"lemonchiffon", 255, 250, 205},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 576 "gmtcol.gperf"
      {"grey77", 196, 196, 196},
      {""}, {""}, {""}, {""},
#line 566 "gmtcol.gperf"
      {"grey67", 171, 171, 171},
      {""}, {""}, {""}, {""},
#line 575 "gmtcol.gperf"
      {"grey76", 194, 194, 194},
      {""}, {""}, {""}, {""},
#line 565 "gmtcol.gperf"
      {"grey66", 168, 168, 168},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 569 "gmtcol.gperf"
      {"grey70", 179, 179, 179},
      {""}, {""}, {""}, {""},
#line 559 "gmtcol.gperf"
      {"grey60", 153, 153, 153},
      {""}, {""}, {""}, {""},
#line 695 "gmtcol.gperf"
      {"gray95", 242, 242, 242},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 556 "gmtcol.gperf"
      {"grey57", 145, 145, 145},
      {""}, {""}, {""}, {""},
#line 685 "gmtcol.gperf"
      {"gray85", 217, 217, 217},
      {""}, {""}, {""}, {""},
#line 555 "gmtcol.gperf"
      {"grey56", 143, 143, 143},
      {""}, {""}, {""}, {""},
#line 677 "gmtcol.gperf"
      {"gray77", 196, 196, 196},
      {""}, {""}, {""}, {""},
#line 667 "gmtcol.gperf"
      {"gray67", 171, 171, 171},
#line 599 "gmtcol.gperf"
      {"grey100", 255, 255, 255},
      {""}, {""}, {""},
#line 676 "gmtcol.gperf"
      {"gray76", 194, 194, 194},
      {""}, {""}, {""}, {""},
#line 666 "gmtcol.gperf"
      {"gray66", 168, 168, 168},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 549 "gmtcol.gperf"
      {"grey50", 127, 127, 127},
      {""}, {""},
#line 458 "gmtcol.gperf"
      {"palevioletred4", 139, 71, 93},
      {""}, {""}, {""},
#line 158 "gmtcol.gperf"
      {"palevioletred", 219, 112, 147},
#line 457 "gmtcol.gperf"
      {"palevioletred3", 205, 104, 137},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 670 "gmtcol.gperf"
      {"gray70", 179, 179, 179},
      {""}, {""}, {""}, {""},
#line 660 "gmtcol.gperf"
      {"gray60", 153, 153, 153},
      {""}, {""},
#line 456 "gmtcol.gperf"
      {"palevioletred2", 238, 121, 159},
      {""},
#line 574 "gmtcol.gperf"
      {"grey75", 191, 191, 191},
      {""}, {""},
#line 455 "gmtcol.gperf"
      {"palevioletred1", 255, 130, 171},
      {""},
#line 564 "gmtcol.gperf"
      {"grey65", 166, 166, 166},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 657 "gmtcol.gperf"
      {"gray57", 145, 145, 145},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 656 "gmtcol.gperf"
      {"gray56", 143, 143, 143},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 700 "gmtcol.gperf"
      {"gray100", 255, 255, 255},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 650 "gmtcol.gperf"
      {"gray50", 127, 127, 127},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 554 "gmtcol.gperf"
      {"grey55", 140, 140, 140},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 675 "gmtcol.gperf"
      {"gray75", 191, 191, 191},
      {""}, {""}, {""}, {""},
#line 665 "gmtcol.gperf"
      {"gray65", 166, 166, 166},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""},
#line 116 "gmtcol.gperf"
      {"khaki", 240, 230, 140},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 655 "gmtcol.gperf"
      {"gray55", 140, 140, 140},
      {""}, {""}, {""},
#line 218 "gmtcol.gperf"
      {"cornsilk4", 139, 136, 120},
      {""}, {""}, {""}, {""},
#line 217 "gmtcol.gperf"
      {"cornsilk3", 205, 200, 177},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 216 "gmtcol.gperf"
      {"cornsilk2", 238, 232, 205},
      {""}, {""}, {""}, {""},
#line 215 "gmtcol.gperf"
      {"cornsilk1", 255, 248, 220},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 137 "gmtcol.gperf"
      {"firebrick", 178, 34, 34},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 115 "gmtcol.gperf"
      {"darkkhaki", 189, 183, 107},
#line 206 "gmtcol.gperf"
      {"peachpuff4", 139, 119, 101},
      {""}, {""}, {""}, {""},
#line 205 "gmtcol.gperf"
      {"peachpuff3", 205, 175, 149},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 64 "gmtcol.gperf"
      {"cornflowerblue", 100, 149, 237},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 204 "gmtcol.gperf"
      {"peachpuff2", 238, 203, 173},
      {""}, {""}, {""}, {""},
#line 203 "gmtcol.gperf"
      {"peachpuff1", 255, 218, 185},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""},
#line 442 "gmtcol.gperf"
      {"deeppink4", 139, 10, 80},
      {""}, {""}, {""}, {""},
#line 441 "gmtcol.gperf"
      {"deeppink3", 205, 16, 118},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 440 "gmtcol.gperf"
      {"deeppink2", 238, 18, 137},
      {""}, {""}, {""}, {""},
#line 439 "gmtcol.gperf"
      {"deeppink1", 255, 20, 147},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 155 "gmtcol.gperf"
      {"hotpink", 255, 105, 180},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 48 "gmtcol.gperf"
      {"peachpuff", 255, 218, 185},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 157 "gmtcol.gperf"
      {"lightpink", 255, 182, 193},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 52 "gmtcol.gperf"
      {"cornsilk", 255, 248, 220},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 154 "gmtcol.gperf"
      {"deeppink", 255, 20, 147},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""},
#line 45 "gmtcol.gperf"
      {"papayawhip", 255, 239, 213}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register unsigned int key = hash (str, len);

      if (key <= MAX_HASH_VALUE)
        {
          register const char *s = wordlist[key].name;

          if ((((unsigned char)*str ^ (unsigned char)*s) & ~32) == 0 && !gperf_case_strcmp (str, s))
            return &wordlist[key];
        }
    }
  return 0;
}
#line 701 "gmtcol.gperf"


const struct gmtcol_t* gmtcol(const char *s)
{
  return (s ? gmtcol_lookup(s, strlen(s)) : NULL);
}
