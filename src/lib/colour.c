#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/colour.h"
#include "cptutils/btrace.h"
#include "cptutils/macro.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

static double colourD(int);
static int colour8(double);

static int rgbD_to_hsv(const double[static 3], hsv_t*);
static int hsv_to_rgbD(hsv_t, double[static 3]);
static int hsvD_to_rgb(const double[static 3], rgb_t*);
static int rgb_to_hsvD(rgb_t, double [static 3]);

int hsv_to_rgb(hsv_t hsv, rgb_t *rgb)
{
  double rgbD[3], hsvD[3];

  return
    (hsv_to_hsvD(hsv, hsvD) != 0) ||
    (hsvD_to_rgbD(hsvD, rgbD) != 0) ||
    (rgbD_to_rgb(rgbD, rgb) != 0);
}

int rgb_to_hsv(rgb_t rgb, hsv_t *hsv)
{
  double rgbD[3], hsvD[3];

  return
    (rgb_to_rgbD(rgb, rgbD) != 0) ||
    (rgbD_to_hsvD(rgbD, hsvD) != 0) ||
    (hsvD_to_hsv(hsvD, hsv) != 0);
}

static double d3_dist(double a[static 3], double b[static 3])
{
  double sum = 0;

  for (size_t i = 0 ; i < 3 ; i++)
    {
      double d = a[i] - b[i];
      sum += d * d;
    }

  return sqrt(sum);
}

double rgb_dist(rgb_t a, rgb_t b)
{
  double da[3], db[3];

  rgb_to_rgbD(a, da);
  rgb_to_rgbD(b, db);

  return d3_dist(da, db);
}

double hsv_dist(hsv_t a, hsv_t b)
{
  double da[3], db[3];

  hsv_to_hsvD(a, da);
  hsv_to_hsvD(b, db);

  return d3_dist(da, db);
}

/*
  convert a gimp triple of [0..1] vals to/from an rgb_t struct
*/

int rgbD_to_rgb(const double col[static 3], rgb_t *rgb)
{
  rgb->red = colour8(col[0]);
  rgb->green = colour8(col[1]);
  rgb->blue = colour8(col[2]);

  return 0;
}

int rgb_to_rgbD(rgb_t rgb, double col[static 3])
{
  col[0] = colourD(rgb.red);
  col[1] = colourD(rgb.green);
  col[2] = colourD(rgb.blue);

  return 0;
}

int hsvD_to_hsv(const double hsvD[static 3], hsv_t *hsv)
{
  hsv->hue = hsvD[0] * 360;
  hsv->sat = hsvD[1];
  hsv->val = hsvD[2];

  return 0;
}

int hsv_to_hsvD(hsv_t hsv, double hsvD[static 3])
{
  hsvD[0] = hsv.hue / 360;
  hsvD[1] = hsv.sat;
  hsvD[2] = hsv.val;

  return 0;
}

int grey_to_rgbD(int g, double col[static 3])
{
  rgb_t rgb;

  rgb.red = g;
  rgb.green = g;
  rgb.blue = g;

  return rgb_to_rgbD(rgb, col);
}

/*
  not exported, makes the interpolation function a little
  more compact
*/

static int rgbD_to_hsv(const double rgbD[static 3], hsv_t *hsv)
{
  double hsvD[3];

  return
    (rgbD_to_hsvD(rgbD, hsvD) != 0) ||
    (hsvD_to_hsv(hsvD, hsv) != 0);
}

static int hsv_to_rgbD(hsv_t hsv, double rgbD[static 3])
{
  double hsvD[3];

  return
    (hsv_to_hsvD(hsv, hsvD) != 0) ||
    (hsvD_to_rgbD(hsvD, rgbD) != 0);
}

static int hsvD_to_rgb(const double hsvD[static 3], rgb_t *rgb)
{
  double rgbD[3];

  return
    (hsvD_to_rgbD(hsvD, rgbD) != 0) ||
    (rgbD_to_rgb(rgbD, rgb) != 0);
}

static int rgb_to_hsvD(rgb_t rgb, double hsvD[static 3])
{
  double rgbD[3];

  return
    (rgb_to_rgbD(rgb, rgbD) != 0) ||
    (rgbD_to_hsvD(rgbD, hsvD) != 0);
}

/* convert a number in [0, 1] to/from something in 0...255 */

static int colour8(double x)
{
  int i;

  i = nearbyint(255 * x);
  i = MAX(i, 0);
  i = MIN(i, 255);

  return i;
}

static double colourD(int i)
{
  double x;

  x = (double)i / 255;

  x = (x < 0 ? 0 : x);
  x = (x > 1 ? 1 : x);

  return x;
}

/*
  read an rgb_t in a string "r/g/b/"
*/

int parse_rgb(const char *string, rgb_t *col)
{
  char *dup, *token;

  if (string == NULL)
    {
      btrace("cannot parse NULL string");
      return 1;
    }

  if ((dup = strdup(string)) == NULL)
    {
      btrace("failed strdup()");
      return 1;
    }

  token = strtok(dup, "/");

  col->red = atoi(token);

  if ((token = strtok(NULL, "/")) == NULL)
    {
      col->blue = col->green = col->red;
      free(dup);
      return 0;
    }

  col->green = atoi(token);

  if ((token = strtok(NULL, "/")) == NULL)
    {
      btrace("ill-formed colour string : %s", string);
      free(dup);
      return 1;
    }

  col->blue = atoi(token);
  free(dup);

  return 0;
}

/* from libgimp/gimpcolorspace.c */

int rgbD_to_hsvD(const double rgbD[static 3], double hsvD[static 3])
{
  double min, max;
  double r, g, b, h, s, v;

  r = rgbD[0];
  g = rgbD[1];
  b = rgbD[2];

  h = 0.0;

  if (r > g)
    {
      max = MAX(r, b);
      min = MIN(g, b);
    }
  else
    {
      max = MAX(g, b);
      min = MIN(r, b);
    }
  v = max;

  if (max != 0.0)
    s = (max - min) / max;
  else
    s = 0.0;

  if (s == 0.0)
    {
      h = 0.0;
    }
  else
    {
      double delta = max - min;

      if (r == max)
	h = (g - b) / delta;
      else if (g == max)
	h = 2 + (b - r) / delta;
      else if (b == max)
	h = 4 + (r - g) / delta;

      h /= 6.0;

      if (h < 0.0)
	h += 1.0;
      else if (h > 1.0)
	h -= 1.0;
    }

  hsvD[0] = h;
  hsvD[1] = s;
  hsvD[2] = v;

  return 0;
}

int hsvD_to_rgbD(const double hsvD[static 3], double rgbD[static 3])
{
  double f, p, q, t, h, s, v;

  h = hsvD[0];
  s = hsvD[1];
  v = hsvD[2];

  if (s == 0.0)
    {
      rgbD[0] = v;
      rgbD[1] = v;
      rgbD[2] = v;

      return 0;
    }

  h = h * 6.0;

  if (h == 6.0) h = 0.0;

  f = h - (int)h;
  p = v * (1.0 - s);
  q = v * (1.0 - s * f);
  t = v * (1.0 - s * (1.0 - f));

  switch ((int)h)
    {
    case 0:
      rgbD[0] = v;
      rgbD[1] = t;
      rgbD[2] = p;
      break;
    case 1:
      rgbD[0] = q;
      rgbD[1] = v;
      rgbD[2] = p;
      break;
    case 2:
      rgbD[0] = p;
      rgbD[1] = v;
      rgbD[2] = t;
      break;
    case 3:
      rgbD[0] = p;
      rgbD[1] = q;
      rgbD[2] = v;
      break;
    case 4:
      rgbD[0] = t;
      rgbD[1] = p;
      rgbD[2] = v;
      break;
    case 5:
      rgbD[0] = v;
      rgbD[1] = p;
      rgbD[2] = q;
      break;
    default:
      return 1;
    }

  return 0;
}

static int interpolate_1d(double z, double a, double b, double *c)
{
  *c = (1 - z) * a + z * b;

  return 0;
}

static int interpolate_3d(double z,
                          const double a[static 3],
                          const double b[static 3],
                          double c[static 3])
{
  int err = 0;

  for (size_t i = 0 ; i < 3 ; i++)
    err |= interpolate_1d(z, a[i], b[i], c + i);

  return err;
}

/* model here is the interpolation space */

int rgb_interpolate(model_t model, double z,
                    rgb_t a, rgb_t b, rgb_t *c)
{
  double aD[3], bD[3], cD[3];
  int err = 1;

  switch (model)
    {
    case model_rgb:
      err =
        (rgb_to_rgbD(a, aD) != 0) ||
        (rgb_to_rgbD(b, bD) != 0) ||
        (interpolate_3d(z, aD, bD, cD) != 0) ||
        (rgbD_to_rgb(cD, c) != 0);
      break;

    case model_hsv:
      err =
        (rgb_to_hsvD(a, aD) != 0) ||
        (rgb_to_hsvD(b, bD) != 0) ||
        (interpolate_3d(z, aD, bD, cD) != 0) ||
        (hsvD_to_rgb(cD, c) != 0);
      break;

    default:
      btrace("bad interpolation model %s", model_name(model));
    }

  return err;
}

int hsv_interpolate(model_t model, double z,
                    hsv_t a, hsv_t b, hsv_t *c)
{
  double aD[3], bD[3], cD[3];
  int err = 1;

  switch (model)
    {
    case model_rgb:
      err =
        (hsv_to_rgbD(a, aD) != 0) ||
        (hsv_to_rgbD(b, bD) != 0) ||
        (interpolate_3d(z, aD, bD, cD) != 0) ||
        (rgbD_to_hsv(cD, c) != 0);
      break;

    case model_hsv:
      err =
        (hsv_to_hsvD(a, aD) != 0) ||
        (hsv_to_hsvD(b, bD) != 0) ||
        (interpolate_3d(z, aD, bD, cD) != 0) ||
        (hsvD_to_hsv(cD, c) != 0);
      break;

    default:
      btrace("bad interpolation model %s", model_name(model));
    }

  return err;
}
