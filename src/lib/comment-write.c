#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/comment-write.h"
#include "cptutils/comment-write-base.h"

int comment_write(const comment_t *comment, const char *path)
{
  return comment_write_base(&comment, 1, path);
}
