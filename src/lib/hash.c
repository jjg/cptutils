#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/hash.h"

#include <stddef.h>

static uint32_t crc32(const char *s)
{
  uint32_t crc = 0xffffffff;

  for (size_t i = 0 ; s[i] != '\0' ; i++)
    {
      uint8_t byte = s[i];
      crc = crc ^ byte;

      for (uint8_t j = 8; j > 0; --j)
        crc = (crc >> 1) ^ (0xEDB88320 & (-(crc & 1)));
    }

  return crc ^ 0xffffffff;
}

uint32_t hash32(const char *s)
{
  return crc32(s);
}
