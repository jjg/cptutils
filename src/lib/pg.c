#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/pg.h"
#include "cptutils/pg-type.h"
#include "cptutils/btrace.h"

pg_t* pg_new(void)
{
  pg_t *pg = malloc(sizeof(pg_t));

  if (pg == NULL)
    btrace("failed to allocate pg_t struct");
  else
    {
      gstack_t *stack = gstack_new(sizeof(pg_stop_t), 0, 16);

      if (stack == NULL)
	btrace("failed to allocate pg stack");
      else
        {
          pg->stack = stack;
          pg->percentage = false;
          return pg;
        }

      free(pg);
    }

  return NULL;
}

void pg_set_percentage(pg_t *pg, bool value)
{
  pg->percentage = value;
}

int pg_push(pg_t *pg, double value, rgb_t rgb, unsigned char alpha)
{
  pg_stop_t stop = {
    .value  = value,
    .rgb = rgb,
    .alpha = alpha
  };

  if (gstack_push(pg->stack, &stop) != 0)
    {
      btrace("failed push of stop");
      return 1;
    }

  return 0;
}

static int pg_clone_stop(void *vstop, void *vstack2)
{
  const pg_stop_t *stop = vstop;
  gstack_t *stack2 = vstack2;

  return (gstack_push(stack2, stop) == 0) ? 1 : -1;
}

pg_t* pg_clone(const pg_t *pg1)
{
  pg_t *pg2;

  if ((pg2 = pg_new()) != NULL)
    {
      pg2->percentage = pg1->percentage;

      if (gstack_foreach(pg1->stack, pg_clone_stop, pg2->stack) == 0)
        return pg2;

      pg_destroy(pg2);
    }

  return NULL;
}

void pg_destroy(pg_t *pg)
{
  if (pg != NULL)
    gstack_destroy(pg->stack);
  free(pg);
}
