#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/ggr-read.h"
#include "cptutils/path-base.h"
#include "cptutils/btrace.h"
#include "cptutils/macro.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define MAX_SEGMENTS 4096

static ggr_t* load_stream(const char*, FILE*);

ggr_t* ggr_read(const char *path)
{
  ggr_t *grad = NULL;

  if (path == NULL)
    grad = load_stream(path, stdin);
  else
    {
      FILE *stream;

      if ((stream = fopen(path, "rb")) != NULL)
        {
          grad = load_stream(path, stream);
          fclose(stream);
        }
      else
        btrace("failed to open %s : %s", path, strerror(errno));
    }

  return grad;
}

static int load_grad(const char*, FILE*, ggr_t*);
static void warn_truncated(const char*);

static ggr_t* load_stream(const char *path, FILE *stream)
{
  char line[1024];

  if (fgets(line, 1024, stream) == NULL)
    {
      warn_truncated("at start of file");
      return NULL;
    }

  if (strncmp(line, "GIMP Gradient", 13) != 0)
    {
      btrace("file does not seem to be a GIMP gradient");
      return NULL;
    }

  ggr_t *grad = NULL;

  if ((grad = ggr_new()) != NULL)
    {
      grad->filename = (path ? strdup(path) : strdup("<stdin>"));

      if (load_grad(path, stream, grad) != 0)
        {
          ggr_destroy(grad);
          grad = NULL;
        }
    }

  return grad;
}

static void warn_truncated(const char *where)
{
  btrace("unexpected end of file %s", where);
}

static int load_grad(const char *path, FILE *stream, ggr_t *grad)
{
  char line[1024];

  if (fgets(line, 1024, stream) == NULL)
    {
      warn_truncated("after header");
      return 1;
    }

  /*
    In 1.3 gradients there is a line with the name of the
    gradient : if we find it then we use that name and read
    another line, otherwise we use the path (or the <stdin>
    string>)
  */

  if (strncmp(line, "Name:", 5) == 0)
    {
      char *s, *e;

      for (s = line + 5 ; *s == ' ' ; s++);

      if ((e = strchr(s, '\n')) != NULL) *e = '\0';
      if ((e = strchr(s, '\r')) != NULL) *e = '\0';

      grad->name = strdup(s);

      if (fgets(line, 1024, stream) == NULL)
	{
	  warn_truncated("after segments line");
	  return 1;
	}
    }
  else
    {
      if (path == NULL)
        grad->name = strdup("cptutils-output");
      else
        grad->name = path_base(path, "*");
    }

  int num_segments = atoi(line);

  if ((num_segments < 1) || (num_segments > MAX_SEGMENTS))
    {
      btrace("invalid number of segments in %s", path);
      return 1;
    }

  ggr_segment_t *prev = NULL;

  for (int i = 0 ; i < num_segments ; i++)
    {
      ggr_segment_t *seg;
      int type, color, ect_left, ect_right;

      seg = ggr_segment_new();
      seg->prev = prev;

      if (prev)
	prev->next = seg;
      else
	grad->segments = seg;

      if (fgets(line, 1024, stream) == NULL)
	{
	  btrace("unexpected end of file at segment %i", i+1);
	  return 1;
	}

      switch (sscanf(line, "%lf%lf%lf%lf%lf%lf%lf%lf%lf%lf%lf%d%d%d%d",
		     &(seg->left), &(seg->middle), &(seg->right),
		     &(seg->r0), &(seg->g0), &(seg->b0), &(seg->a0),
		     &(seg->r1), &(seg->g1), &(seg->b1), &(seg->a1),
		     &type, &color,
		     &ect_left, &ect_right))
	{
	case 13:

	  ect_left = ect_right = GGR_FIXED;

	  /* fallthrough */
	case 15:

	  seg->ect_left  = ect_left;
	  seg->ect_right = ect_right;

          seg->type  = (ggr_type_t)type;

	  /* determine colour model (see below) */

	  switch (color)
	    {
	    case GGR_RGB:
	    case GGR_HSV_CW:
	    case GGR_HSV_CCW:

	      seg->color = (ggr_colour_r)color;
	      break;

	    default:

	      btrace("unknown colour model (%i)", color);
	      return 1;
	    }

	  break;

	default:
	  btrace("badly formatted gradient segment %d in %s", i, path);
	  return 1;
	}
      prev = seg;
    }

  return 0;
}
