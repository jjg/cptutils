#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/comment-list-write.h"
#include "cptutils/comment-write-base.h"
#include "cptutils/btrace.h"

int comment_list_write(const comment_list_t *list, const char *path)
{
  size_t n = comment_list_size(list);

  if (n == 0)
    return 1;

  const comment_t *comment[n];

  for (size_t i = 0 ; i < n ; i++)
    {
      if ((comment[i] = comment_list_entry(list, i)) == NULL)
        return 1;
    }

  return comment_write_base(comment, n, path);
}
