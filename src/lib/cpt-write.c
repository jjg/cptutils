#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/cpt-write.h"
#include "cptutils/btrace.h"
#include "cptutils/macro.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <assert.h>

#include <unistd.h>

#define FORMAT_LEN 16
#define BUFFER_LEN 128
#define SEG_LEN_RATIO_MAX 100.0
#define SEG_LEN_MAX 1e5
#define SEG_LEN_MIN 1e-5
#define SIG_FIGS_MIN 8
#define EXPONENT_DP 8


typedef char sample_style_format_t[FORMAT_LEN];

typedef struct
{
  sample_style_format_t value, fill;
} sample_style_t;

static int snprintf_fill(char *str,
                         size_t size,
                         cpt_fill_t fill,
                         const cptwrite_opt_t *opt)
{
  int n = -1;

  switch (fill.type)
    {
    case cpt_fill_empty:
      n = snprintf(str, size, "-");
      break;
    case cpt_fill_grey:
      n = snprintf(str, size, "%i", fill.grey);
      break;
    case cpt_fill_hatch:
      n = snprintf(str, size, "%c%i/%i",
		   (fill.hatch.sign == 1 ? 'p' : 'P' ),
		   fill.hatch.dpi,
		   fill.hatch.n);
      break;
    case cpt_fill_file:
      n = snprintf(str, size, "%s", fill.file);
      break;
    case cpt_fill_colour_rgb:
      switch (opt->colour_join)
        {
        case cpt_colour_join_space:
          n = snprintf(str, size,
                       "%3i %3i %3i",
                       fill.colour.rgb.red,
                       fill.colour.rgb.green,
                       fill.colour.rgb.blue);
          break;
        case cpt_colour_join_glyph:
          n = snprintf(str, size,
                       "%i/%i/%i",
                       fill.colour.rgb.red,
                       fill.colour.rgb.green,
                       fill.colour.rgb.blue);
          break;
        }
      break;
    case cpt_fill_colour_hsv:
      switch (opt->colour_join)
        {
        case cpt_colour_join_space:
          n = snprintf(str, size,
                       "%7.3f %7.5f %7.5f",
                       fill.colour.hsv.hue,
                       fill.colour.hsv.sat,
                       fill.colour.hsv.val);
          break;
        case cpt_colour_join_glyph:
          n = snprintf(str, size,
                       "%7.3f-%7.5f-%7.5f",
                       fill.colour.hsv.hue,
                       fill.colour.hsv.sat,
                       fill.colour.hsv.val);
          break;
        }
      break;
    }

  return n;
}

static int fprintf_fill(FILE *stream,
                        cpt_fill_t fill,
                        const cptwrite_opt_t *opt,
                        const sample_style_t *style)
{
  size_t n = BUFFER_LEN;
  char tmp[n];
  int m = snprintf_fill(tmp, n, fill, opt);

  if (m < 0)
    {
      btrace("error rendering fill");
      return -1;
    }

  if (m >= n)
    {
      btrace("buffer overflow");
      return -1;
    }

  const char *format;

  if (style)
    format = style->fill;
  else
    format = "%s";

  return fprintf(stream, format, tmp);
}

static int fprintf_aux(FILE *stream,
                       char c,
                       cpt_fill_t fill,
                       const cptwrite_opt_t *opt)
{
  int n = 0;

  if (fill.type == cpt_fill_empty)
    return 0;

  n += fprintf(stream, "%c", c);
  n += fprintf(stream, " ");
  n += fprintf_fill(stream, fill, opt, NULL);
  n += fprintf(stream, "\n");

  return n;
}

static int fprintf_sample(FILE *stream,
                          cpt_sample_t sample,
                          const cptwrite_opt_t *opt,
                          const sample_style_t *style)
{
  int n = 0;

  n += fprintf(stream, style->value, sample.val);
  n += fprintf(stream, " ");
  n += fprintf_fill(stream, sample.fill, opt, style);

  return n;
}

/*
  given a decimal number string like "0.002300", assign the number of
  decimal places used in that string, in this case 4, using this value
  of precision in a sprintf format string, as in "%.4f", will produce
  a string with the same numerical value, so "0.0023"
*/

static int precision_used(const char *str, size_t *used)
{
  char *c;

  if ((c = strchr(str, '.')) == NULL)
    return 1;

  size_t len = strlen(c);

  if (len < 2)
    return 1;

  size_t i;

  for (i = len - 1 ; (i > 0) && (c[i] == '0') ; i--);

  *used = i;

  return 0;
}

/*
  inspect the values in the cpt segments to determine a good format
  string for them -- the same format is used for all of the values
  (that keeps things aligned and attractive) and a certain amount of
  heuristics is needed;  the behaviour can be tweaked by the macros
  defined at the start of the file.  The format string is written
  to the style struct.
*/

static int sample_style_value(const cpt_t *cpt, sample_style_t *style)
{
  size_t
    n_seg = cpt_nseg(cpt),
    n_stop = n_seg + 1;

  if (n_stop < 2)
    {
      btrace("empty cpt file");
      return 1;
    }

  double z[n_stop];
  cpt_seg_t *seg = cpt->segment;

  z[0] = seg->sample.left.val;

  for (size_t i = 0 ; i < n_seg ; i++)
    {
      z[i + 1] = seg->sample.right.val;
      seg = seg->right;
    }

  /* get min/max segment-lengths */

  double
    seg_len_min = fabs(z[0] - z[1]),
    seg_len_max = seg_len_min;

  for (size_t i = 1 ; i < n_stop - 1 ; i++)
    {
      double w = fabs(z[i] - z[i + 1]);
      seg_len_max = MAX(w, seg_len_max);
      seg_len_min = MIN(w, seg_len_min);
    }

  /*
    if seg_len is large/small or ratio is large then fall back to
    exponential in the format
  */

  if ((seg_len_max > SEG_LEN_RATIO_MAX * seg_len_min) ||
      (seg_len_max > SEG_LEN_MAX) ||
      (seg_len_min < SEG_LEN_MIN))
    {
      /*
        any negative sign-bits in the z values, if so we will add a
        space in the format string
      */

      bool has_negative = false;

      for (size_t i = 0 ; i < n_stop ; i++)
        if (signbit(z[i]) != 0)
          {
            has_negative = true;
            break;
          }

      /*
        EXPONENT_DP is the number of decimal places in the exponent
        form of the format, so a value of 3 would  give a string
        like 1.023+e23
      */

      if (snprintf(style->value,
                   FORMAT_LEN,
                   has_negative ? "%% .%ie" : "%%.%ie",
                   EXPONENT_DP) < FORMAT_LEN)
        return 0;

      btrace("format-string overflow");
      return 1;
    }

  /*
    default precision: SIG_FIGS_MIN orders of magnitude less than
    seg_len_min
  */

  int precision = ceil(SIG_FIGS_MIN - log10(seg_len_min));
  precision = MAX(precision, 0);

  /*
    we may be able to use a smaller value of precision, if all of the
    values happen to have trailing zeros
  */

  if (precision > 0)
    {
      size_t
        used_max = 0,
        n = BUFFER_LEN;
      char tmp[n];

      for (size_t i = 0 ; i < n_stop ; i++)
	{
	  if (snprintf(tmp, n, "%.*f", precision, z[i]) >= n)
            {
              btrace("buffer overflow");
              return 1;
            }

	  size_t used = 0;

	  if (precision_used(tmp, &used) != 0)
	    {
	      btrace("finding decimal places used");
	      return 1;
	    }

	  used_max = MAX(used_max, used);
	}

      precision = used_max;
    }

  /*
    find the maximal width used across all of the values at this
    precision
  */

  size_t width_max = 0;

  {
    size_t n = BUFFER_LEN;
    char tmp[n];

    for (size_t i = 0 ; i < n_stop ; i++)
      {
        if (snprintf(tmp, n, "%.*f", precision, z[i]) >= n)
          {
            btrace("buffer overflow");
            return 1;
          }

        size_t width = strlen(tmp);
        width_max = MAX(width, width_max);
      }
  }

  /* generate the format string */

  {
    char *format = style->value;
    size_t len =
      snprintf(format, FORMAT_LEN, "%%%zu.%df", width_max, precision);

    if (len >= FORMAT_LEN)
      {
        btrace("format string overflow");
        return 1;
      }
  }

  return 0;
}

/*
  much easier than the value string, we use the snprintf_fill
  function to print the fills to a temporary buffer, then get
  the maximum of their sizes; we then create a format string
  for %s with that max as width, when we print to stream in
  anger, we do the spprinf_fill to temporary buffer again, but
  the fprintf to the stream with this %s string format, so all
  are of the same width (and the fill is right aligned)
*/

static int sample_style_fill(const cpt_t *cpt,
                             const cptwrite_opt_t *opt,
                             sample_style_t *style)
{
  /* get max widthgth of cpt-fill strings */

  size_t width_max = 0;

  {
    size_t n = BUFFER_LEN;
    char tmp[n];

    for (cpt_seg_t *seg = cpt->segment ; seg ; seg = seg->right)
      {
        cpt_fill_t fills[2] = {
          seg->sample.left.fill,
          seg->sample.right.fill
        };

        for (size_t i = 0 ; i < 2 ; i++)
          {
            int width = snprintf_fill(tmp, n, fills[i], opt);

            if (width >= n)
              {
                btrace("buffer overflow");
                return 1;
              }

            if (width < 0)
              return 1;

            width_max = MAX(width, width_max);
          }
      }
  }

  /* create format string like "%6s" */

  if (snprintf(style->fill, FORMAT_LEN, "%%%zus", width_max) >= FORMAT_LEN)
    {
      btrace("format string overflow");
      return 1;
    }

  return 0;
}

/*
  populate the format strings in the style struct from the
  contents of the cpt struct
*/

static int sample_style(const cpt_t *cpt,
                        const cptwrite_opt_t *opt,
                        sample_style_t *style)
{
  if (sample_style_value(cpt, style) != 0)
    {
      btrace("failed to build sample value style");
      return 1;
    }

  if (sample_style_fill(cpt, opt, style) != 0)
    {
      btrace("failed to build sample fill style");
      return 1;
    }

  return 0;
}

/*
  These gets the string for the "COLOR MODEL" which encodes the
  colour and interpolation models
*/

/*
  GMT4 upper-case variants, these specify the model and the
  interpolation space (since the form of the colours does not
  encode their space: "HSV" means model HSV but interpolate RGB
  (the default); "+HSV" means model HSV, interpolate HSV.
*/

static int colour_model_upper(const cpt_t *cpt,
                              const cptwrite_opt_t *opt,
                              const char **model)
{
  switch (cpt->model)
    {
    case model_rgb:
      switch (cpt->interpolate)
        {
        case model_rgb:
          *model = NULL;
          return 0;
        case model_hsv:
          btrace("RGB model and HSV interpolate");
          return 1;
        case model_cmyk:
          btrace("RGB model and CMYK interpolate");
          return 1;
        }
      break;

    case model_hsv:
      switch (cpt->interpolate)
        {
        case model_rgb:
          *model = "HSV";
          return 0;
        case model_hsv:
          *model = "+HSV";
          return 0;
        case model_cmyk:
          btrace("HSV model and CMYK interpolate");
          return 1;
        }
      break;

    case model_cmyk:
      switch (cpt->interpolate)
        {
        case model_rgb:
          *model = "CMYK";
          return 0;
        case model_hsv:
          btrace("CMYK model and HSV interpolate");
          return 1;
        case model_cmyk:
          *model = "+CMYK";
          return 0;
        }
      break;
    }

  return 1;
}

/*
  The GMT5 lower-case model string encodes the interpolation
  space only, since the form of the colours encodes their space
*/

static int colour_model_lower(const cpt_t *cpt,
                              const cptwrite_opt_t *opt,
                              const char **model)
{
  if (opt->colour_join != cpt_colour_join_glyph)
    {
      btrace("GMT5 lower-case COLOR_MODEL needs glyph-joined colours");
      return 1;
    }

  switch (cpt->interpolate)
    {
    case model_rgb:
      *model = NULL;
      return 0;
    case model_hsv:
      *model = "hsv";
      return 0;
    case model_cmyk:
      *model = "cmyk";
      return 0;
    }

  return 1;
}

static int colour_model(const cpt_t *cpt,
                        const cptwrite_opt_t *opt,
                        const char **model)
{
  switch (opt->model_case)
    {
    case cpt_model_case_upper:
      return colour_model_upper(cpt, opt, model);
    case cpt_model_case_lower:
      return colour_model_lower(cpt, opt, model);
    }

  return 1;
}

static void fprintf_annote(FILE *stream, const cpt_seg_t *seg)
{
  char c;
  switch (seg->annote)
    {
    case annote_lower: c = 'L'; break;
    case annote_upper: c = 'U'; break;
    case annote_both: c = 'B'; break;
    case annote_none: c = ' '; break;
    default:
      assert(false);
    }
  fprintf(stream, " %c", c);
}

static void fprintf_label(FILE *stream, const cpt_seg_t *seg)
{
  fprintf(stream, " ; %s", seg->label);
}

static int fprintf_comment(const char *line, void *arg)
{
  FILE *stream = arg;

  if (line)
    fprintf(stream, "# %s\n", line);
  else
    fprintf(stream, "#\n");

  return 0;
}

static int cpt_write_stream(const cpt_t *cpt,
                            const cptwrite_opt_t *opt,
                            FILE *stream)
{
  if (comment_each(cpt->comment, fprintf_comment, stream) != 0)
    {
      btrace("failed to write comment");
      return 1;
    }

  const char *model = NULL;

  if (colour_model(cpt, opt, &model) != 0)
    {
      btrace("name of colour model");
      return 1;
    }

  if (model)
    fprintf(stream, "# COLOR_MODEL = %s\n", model);

  cpt_range_t range = cpt->range;

  if (range.present)
    {
      if (opt->range)
        fprintf(stream, "# RANGE = %g/%g\n", range.min, range.max);
      else
        {
          btrace("range present but writing range disabled");
          return 1;
        }
    }

  cpt_hinge_t hinge = cpt->hinge;

  if (hinge.present)
    {
      switch (hinge.type)
        {
        case hinge_soft:
          if (opt->hinge.soft)
            fprintf(stream, "# SOFT_HINGE\n");
          else
            {
              btrace("writing soft hinge disabled");
              return 1;
            }
          break;

        case hinge_hard:
          if (opt->hinge.hard)
            fprintf(stream, "# HARD_HINGE\n");
          else
            {
              btrace("writing hard hinge disabled");
              return 1;
            }
          break;

        case hinge_explicit:
          if (opt->hinge.explicit)
            fprintf(stream, "# HINGE = %g\n", hinge.value);
          else
            {
              btrace("writing explicit hinge disabled");
              return 1;
            }
          break;
        }
    }

  bool annotated = cpt_annotated(cpt);

  sample_style_t style;

  if (sample_style(cpt, opt, &style) != 0)
    {
      btrace("failed to build sample style");
      return 1;
    }

  cpt_seg_t *seg = cpt->segment;

  /* needs to write annotation too */

  while (seg)
    {
      fprintf_sample(stream, seg->sample.left, opt, &style);
      fprintf(stream, "  ");
      fprintf_sample(stream, seg->sample.right, opt, &style);
      if (annotated)
        fprintf_annote(stream, seg);
      if (seg->label)
        fprintf_label(stream, seg);
      fprintf(stream, "\n");

      seg = seg->right;
    }

  fprintf_aux(stream, 'B', cpt->bg, opt);
  fprintf_aux(stream, 'F', cpt->fg, opt);
  fprintf_aux(stream, 'N', cpt->nan, opt);

  return 0;
}

static const cptwrite_opt_t cptwrite_opt_gmt4 = {
  .colour_join = cpt_colour_join_space,
  .model_case = cpt_model_case_upper,
  .hinge = {
    .hard = false,
    .soft = false,
    .explicit = false
  },
  .range = false
};

static const cptwrite_opt_t cptwrite_opt_gmt5 = {
  .colour_join = cpt_colour_join_glyph,
  .model_case = cpt_model_case_lower,
  .hinge = {
    .hard = false,
    .soft = false,
    .explicit = true
  },
  .range = true
};

static const cptwrite_opt_t cptwrite_opt_gmt6 = {
  .colour_join = cpt_colour_join_glyph,
  .model_case = cpt_model_case_lower,
  .hinge = {
    .hard = true,
    .soft = true,
    .explicit = false
  },
  .range = true
};

int cpt_write_options(int version, cptwrite_opt_t *opt)
{
  switch (version)
    {
    case 0:
      *opt = cptwrite_opt_gmt4;
      break;
    case 4:
      *opt = cptwrite_opt_gmt4;
      break;
    case 5:
      *opt = cptwrite_opt_gmt5;
      break;
    case 6:
      *opt = cptwrite_opt_gmt6;
      break;
    default:
      return 1;
    }

  return 0;
}

static int validate_v4_model(const cpt_t *cpt, const cptwrite_opt_t *opt)
{
  if (opt->colour_join != cpt_colour_join_space)
    return 0;

  model_t model = cpt->model;
  const cpt_seg_t *seg;

  switch (model)
    {
    case model_rgb:

      for (seg = cpt->segment ; seg ; seg = seg->right)
        {
          if ((seg->sample.left.fill.type == cpt_fill_colour_hsv) ||
              (seg->sample.right.fill.type == cpt_fill_colour_hsv))
            return 1;
        }
      break;

    case model_hsv:

      for (seg = cpt->segment ; seg ; seg = seg->right)
        {
          if ((seg->sample.left.fill.type == cpt_fill_colour_rgb) ||
              (seg->sample.right.fill.type == cpt_fill_colour_rgb))
            return 1;
        }
      break;

    default:
      break;
    }

  return 0;
}

static int validate(const cpt_t *cpt, const cptwrite_opt_t *opt)
{
  if (validate_v4_model(cpt, opt) != 0)
    {
      btrace("input has mixed-model segments, cannot write as GMTv4");
      return 1;
    }

  return 0;
}

int cpt_write(const cpt_t *cpt,
              const cptwrite_opt_t *opt,
              const char *path)
{
  if (opt == NULL)
    {
      cptwrite_opt_t opt_default;
      if (cpt_write_options(0, &opt_default) != 0)
        return 1;
      return cpt_write(cpt, &opt_default, path);
    }

  if (validate(cpt, opt) != 0)
    {
      btrace("failed write validation");
      return 1;
    }

  int err = 1;

  if (path)
    {
      FILE *stream = fopen(path, "wb");

      if (stream)
	{
	  err = cpt_write_stream(cpt, opt, stream);
	  fclose(stream);
          if (err != 0)
            unlink(path);
	}
      else
        btrace("failed to open %s: %s", path, strerror(errno));
    }
  else
    err = cpt_write_stream(cpt, opt, stdout);

  return err;
}
