#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/qgs-list-read.h"
#include "cptutils/xml-error-handler.h"
#include "cptutils/btrace.h"

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include <time.h>
#include <stdio.h>
#include <string.h>

#define ENCODING "utf-8"

typedef struct
{
  char *color1, *color2, *discrete, *rampType, *stops;
} ramp_values_t;

static int qgs_values_gradient(ramp_values_t *values, qgs_t *qgs)
{
  /*
    not sure if this is the correct interpretation, the XML has a
    type attribute for the colorramp element (always 'gradient'),
    one often (not always) sees a key-value prop element with key
    'rampType' and value 'gradient', also seemingly optional is
    the key-value 'discrete' with value '0' or '1', and that seems
    to correspond to a boolean in the code.
 */

  if (values->discrete == NULL)
    qgs->type = QGS_TYPE_UNSET;
  else
    {
      int val;

      if (sscanf(values->discrete, "%i", &val) == 1)
        {
          if (val == 0)
            qgs->type = QGS_TYPE_INTERPOLATED;
          else
            qgs->type = QGS_TYPE_DISCRETE;
        }
      else
        {
          btrace("bad discrete value: %s", values->discrete);
          return 1;
        }
    }

  if (values->color1 == NULL)
    {
      btrace("no 'color1' value");
      return 1;
    }

  if (values->color2 == NULL)
    {
      btrace("no 'color2' value");
      return 1;
    }

  size_t nentry = 2;

  if (values->stops != NULL)
    {
      nentry++;

      const char *stops = values->stops;

      for (size_t i = 0 ; stops[i] ; i++)
        if (stops[i] == ':')
          nentry++;
    }

  if (qgs_alloc_entries(qgs, nentry) != 0)
    {
      btrace("failed to allocate %zu entries", nentry);
      return 1;
    }

  int r, g, b, op;

  if (sscanf(values->color1, "%i,%i,%i,%i", &r, &g, &b, &op) != 4)
    {
      btrace("parsing color1");
      return 1;
    }

  qgs->entries[0].value = 0;
  qgs->entries[0].rgb.red = r;
  qgs->entries[0].rgb.green = g;
  qgs->entries[0].rgb.blue = b;
  qgs->entries[0].opacity = op;

  const char *p = values->stops;

  for (size_t i = 1 ; i + 1 < nentry ; i++)
    {
      double z;

      if (sscanf(p, "%lf;%i,%i,%i,%i", &z, &r, &g, &b, &op) != 5)
        {
          btrace("parsing stop %zi", i);
          return 1;
        }

      qgs->entries[i].value = z;
      qgs->entries[i].rgb.red = r;
      qgs->entries[i].rgb.green = g;
      qgs->entries[i].rgb.blue = b;
      qgs->entries[i].opacity = op;

      if ((p = strchr(p, ':')) == NULL)
        {
          /*
            this should happen for the final stop but for any other
            we have some kind of arithmetic bug
          */

          if (i + 2 != nentry)
            {
              btrace("token counting error");
              return 1;
            }
        }
      else
        p++;
    }

  if (sscanf(values->color2, "%i,%i,%i,%i", &r, &g, &b, &op) != 4)
    {
      btrace("parsing color2");
      return 1;
    }

  qgs->entries[nentry - 1].value = 1;
  qgs->entries[nentry - 1].rgb.red = r;
  qgs->entries[nentry - 1].rgb.green = g;
  qgs->entries[nentry - 1].rgb.blue = b;
  qgs->entries[nentry - 1].opacity = op;

  return 0;
}

static int qgs_read_gradient(xmlNodePtr ramp, qgs_t *qgs)
{
  xmlNode *nodes = ramp->children;
  comment_t *comment = qgs->comment;
  ramp_values_t values = {
    .color1 = NULL,
    .color2 = NULL,
    .discrete = NULL,
    .rampType = NULL,
    .stops = NULL
  };

  for (xmlNode *node = nodes ; node ; node = node->next)
    {
      if (node->type == XML_ELEMENT_NODE)
        {
          if (strcmp((const char*)node->name, "prop") != 0)
            {
              btrace("unexpected %s node", node->name);
              continue;
            }

          xmlChar *k;

          if ((k = xmlGetProp(node, (xmlChar*)"k")) == NULL)
            {
              btrace("prop has no k attribute");
              return 1;
            }

          xmlChar *v;

          if ((v = xmlGetProp(node, (xmlChar*)"v")) == NULL)
            {
              btrace("prop has no v attribute");
              return 1;
            }

          if (strcmp((const char*)k, "color1") == 0)
            values.color1 = (char*)v;
          else if (strcmp((const char*)k, "color2") == 0)
            values.color2 = (char*)v;
          else if (strcmp((const char*)k, "discrete") == 0)
            values.discrete = (char*)v;
          else if (strcmp((const char*)k, "rampType") == 0)
            values.rampType = (char*)v;
          else if (strcmp((const char*)k, "stops") == 0)
            values.stops = (char*)v;
          else
            xmlFree(v);

          xmlFree(k);
        }
      else if (node->type == XML_COMMENT_NODE)
        {
          xmlChar
            *content = xmlStrdup(node->content),
            *line = content;

          while (line != NULL)
            {
              xmlChar *line_end;

              if ((line_end = (xmlChar*)xmlStrchr(line, '\n')) != NULL)
                *line_end = '\0';

              if (comment_push(comment, (const char*)line) != 0)
                {
                  btrace("error pushing comment");
                  return 1;
                }

              line = line_end ? (line_end + 1) : NULL;
            }

          free(content);
        }
    }

  int err =
    qgs_values_gradient(&values, qgs) ||
    comment_finalise(comment);

  xmlFree(values.color1);
  xmlFree(values.color2);
  xmlFree(values.discrete);
  xmlFree(values.rampType);
  xmlFree(values.stops);

  return err;
}

static int qgs_read_colorramps(xmlNodeSetPtr nodes, qgs_list_t *list)
{
  size_t size = (nodes ? nodes->nodeNr : 0);

  for (size_t i = 0 ; i < size ; ++i)
    {
      xmlNodePtr cur = nodes->nodeTab[i];

      if (cur == NULL)
        {
          btrace("xpath result null");
          return 1;
        }

      if (cur->type != XML_ELEMENT_NODE)
        {
          btrace("bad qgs: colorramp is not a node!");
          return 1;
        }

      xmlChar *type;

      if ((type = xmlGetProp(cur, (xmlChar*)"type")) == NULL)
        {
          btrace("colorramp has no type attribute, skipping");
          continue;
        }

      xmlChar *name;

      if ((name = xmlGetProp(cur, (xmlChar*)"name")) == NULL)
        {
          btrace("colorramp has no name attribute, skipping");
          continue;
        }

      qgs_t *qgs;

      if ((qgs = qgs_list_next(list)) == NULL)
        {
          btrace("failed to get qgs object from list");
          return 1;
        }

      if (qgs_set_name(qgs, (const char*)name) != 0)
        {
          btrace("failed to set name");
          return 1;
        }

      xmlFree(name);

      int err = 1;

      if (xmlStrcmp(type, (xmlChar*)"gradient") == 0)
        err = qgs_read_gradient(cur, qgs);
      else
        btrace("coloramp type %s not handled", (const char*)type);

      xmlFree(type);

      if (err && (qgs_list_revert(list) != 0))
        {
          btrace("failed qgs list revert");
          return 1;
        }
    }

  return 0;
}

static int qgs_read2(const char *file, qgs_list_t *list)
{
  int err = 1;
  xmlDocPtr doc;

  xmlInitParser();
  xmlSetStructuredErrorFunc(NULL, xml_error_handler);

  if ((doc = xmlParseFile(file)) == NULL)
    btrace("error: unable to parse file %s", file);
  else
    {
      xmlXPathContextPtr xpc;

      if ((xpc = xmlXPathNewContext(doc)) == NULL)
        btrace("error: unable to create new XPath context");
      else
        {
          const xmlChar xpe[] = "//colorramp";
          xmlXPathObjectPtr xpo;

          if ((xpo = xmlXPathEvalExpression(xpe, xpc)) == NULL)
            btrace("unable to evaluate xpath expression %s", xpe);
          else
            {
              err = qgs_read_colorramps(xpo->nodesetval, list);
              xmlXPathFreeObject(xpo);
            }

          xmlXPathFreeContext(xpc);
        }

      xmlFreeDoc(doc);
    }

  xmlCleanupParser();

  return err;
}

qgs_list_t* qgs_list_read(const char *path)
{
  qgs_list_t *list;

  if ((list = qgs_list_new()) != NULL)
    {
      if (qgs_read2(path, list) == 0)
        return list;

      qgs_list_destroy(list);
    }

  return NULL;
}
