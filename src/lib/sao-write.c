#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/sao-write.h"
#include "cptutils/btrace.h"

#include <stdio.h>

static int writestop(double x, double v, void *arg)
{
  FILE *st = arg;
  return fprintf(st, "(%.5g, %.5g)", x, v);
}

static int write_comment(const char *line, void *arg)
{
  FILE *st = arg;

  if (line)
    fprintf(st, "# %s\n", line);
  else
    fprintf(st, "#\n");

  return 0;
}

static int sao_write_stream(const sao_t *sao, const char *name, FILE *st)
{
  if (comment_each(sao_comment(sao), write_comment, st) != 0)
    {
      btrace("failed to write comment");
      return 1;
    }

  fprintf(st, "PSEUDOCOLOR\n");
  fprintf(st, "RED:\n");
  sao_eachred(sao, writestop, st); fprintf(st, "\n");
  fprintf(st, "GREEN:\n");
  sao_eachgreen(sao, writestop, st); fprintf(st, "\n");
  fprintf(st, "BLUE:\n");
  sao_eachblue(sao, writestop, st); fprintf(st, "\n");

  return 0;
}

int sao_write(const sao_t *sao, const char *path, const char *name)
{
  int err = 1;

  if (path)
    {
      FILE *st;

      if ((st = fopen(path, "w")) != NULL)
        {
          err = sao_write_stream(sao, name, st);
          fclose(st);
        }
    }
  else
    err = sao_write_stream(sao, name, stdout);

  return err;
}
