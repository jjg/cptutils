#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/qgs.h"

#include <stdlib.h>
#include <string.h>

int qgs_init(qgs_t *qgs)
{
  if ((qgs->comment = comment_new()) == NULL)
    return 1;

  qgs->type = QGS_TYPE_UNSET;
  qgs->name = NULL;
  qgs->n = 0;
  qgs->entries = NULL;

  return 0;
}

qgs_t* qgs_new(void)
{
  qgs_t *qgs;

  if ((qgs = malloc(sizeof(qgs_t))) != NULL)
    {
      if (qgs_init(qgs) == 0)
        return qgs;

      free(qgs);
    }

  return NULL;
}

int qgs_set_name(qgs_t *qgs, const char *name)
{
  if ((qgs->name = strdup(name)) == NULL)
    return 1;
  else
    return 0;
}

int qgs_set_type(qgs_t *qgs, int type)
{
  qgs->type = type;
  return 0;
}

int qgs_alloc_entries(qgs_t *qgs, size_t n)
{
  if ((qgs->entries = calloc(n, sizeof(qgs_entry_t))) == NULL)
    return 1;

  qgs->n = n;
  return 0;
}

void qgs_deinit(qgs_t *qgs)
{
  if (qgs != NULL)
    {
      free(qgs->name);
      free(qgs->entries);
      comment_destroy(qgs->comment);
    }
}

void qgs_destroy(qgs_t *qgs)
{
  qgs_deinit(qgs);
  free(qgs);
}

int qgs_set_entry(qgs_t *qgs, size_t i, qgs_entry_t *entry)
{
  if (i >= qgs->n) return 1;

  qgs_entry_t *dest = qgs->entries + i;

  memcpy(dest, entry, sizeof(qgs_entry_t));

  return 0;
}
