#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/css3.h"
#include "cptutils/btrace.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>

static int css3_write_comments(const char *line, void *arg)
{
  FILE *st = arg;

  if (line == NULL)
    fprintf(st, "\n");
  else
    fprintf(st, "  %s\n", line);

  return 0;
}

static int css3_write_comment(const char *line, void *arg)
{
  FILE *st = arg;

  if (line != NULL)
    fprintf(st, "/* %s */\n", line);

  return 0;
}

static int css3_write_stream(const css3_t *css3, FILE *st)
{
  switch (comment_count(css3->comment))
    {
    case 0:
      break;
    case 1:
      if (comment_each(css3->comment, css3_write_comment, st) != 0)
        return 1;
      break;
    default:
      fprintf(st, "/*\n");
      if (comment_each(css3->comment, css3_write_comments, st) != 0)
        return 1;
      fprintf(st, "*/\n");
    }

  fprintf(st, "linear-gradient(\n");
  fprintf(st, "  %.3gdeg,\n", css3->angle);

  size_t n = css3->n;

  for (size_t i = 0 ; i < n ; i++)
    {
      css3_stop_t stop = css3->stop[i];

      if (stop.alpha < 1.0)
	fprintf(st, "  rgba(%3i, %3i, %3i, %5.3f) ",
		stop.rgb.red,
		stop.rgb.green,
		stop.rgb.blue,
		stop.alpha);
      else
	fprintf(st, "  rgb(%3i, %3i, %3i) ",
		stop.rgb.red,
		stop.rgb.green,
		stop.rgb.blue);

      fprintf(st, "%7.3f%%%s\n", stop.z, ((n - 1 - i) ? "," : ""));
    }

  fprintf(st, ");\n");

  return 0;
}

int css3_write(const css3_t *css3, const char *path)
{
  if (css3->n < 2)
    {
      btrace("CSS3 does not support %zi stops\n", css3->n);
      return 1;
    }

  int err = 1;

  if (path == NULL)
    err = css3_write_stream(css3, stdout);
  else
    {
      FILE *st;

      if ((st = fopen(path, "w")) != NULL)
        {
          err = css3_write_stream(css3, st);
          fclose(st);
	}
      else
        btrace("failed to open %s: %s", strerror(errno));
    }

  return err;
}
