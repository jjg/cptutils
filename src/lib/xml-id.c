#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/xml-id.h"

#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#define PREFIX "id-"

static bool id_char(char c)
{
  if (isalnum(c))
    return true;

  /*
    note that the standard permits an underscore in an id,
    we deliberately do not, so those will be replaced by
    hyphens and make a more-consistent id
  */

  switch (c)
    {
    case '-':
    case '.':
      return true;
    default:
      return false;
    }
}

static int sanitise(char *src, size_t n)
{
  /* any character not an id character, replace by a hyphen */

  for (size_t i = 0 ; i < n ; i++)
    if (! id_char(src[i]))
      src[i] = '-';

  /*
    compress hyphens, we keep 2 indices, i is the source, j the
    destination -- first the leading hyphens
  */

  size_t i, j;

  for (i = 0, j = 0 ; i < n ; i++)
    if (src[i] != '-')
      break;

  while (i < n)
    {
      src[j++] = src[i];
      if (src[i] == '-')
        while ((i < n) && (src[i] == '-'))
          i++;
      else
        i++;
    }

  /* may be a single hypehn at end */

  if ((j > 0) && (src[j - 1] == '-'))
    j--;

  src[j] = '\0';

  /* possibly we've nothing left, so error */

  if (j == 0)
    return 1;

  return 0;
}

char* xml_id_sanitise(const char *src)
{
  if (src == NULL)
    return NULL;

  size_t n = strlen(src);

  if (n == 0)
    return NULL;

  const char prefix[] = PREFIX;
  size_t m = strlen(prefix);
  char dest[n + m + 1];

  memcpy(dest, prefix, m);
  memcpy(dest + m, src, n + 1);

  if (sanitise(dest, n + m) == 0)
    return strdup(dest);

  return NULL;
}
