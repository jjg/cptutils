/* ANSI-C code produced by gperf version 3.1 */
/* Command-line: /usr/bin/gperf --delimiters , --struct-type --language ANSI-C --lookup-function-name stdcol_lookup --ignore-case --readonly-tables --enum --output-file stdcol.c stdcol.gperf  */
/* Computed positions: -k'1,3,6-8,12-13' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gperf@gnu.org>."
#endif

#line 1 "stdcol.gperf"
 /* -*- c -*-
  This is part of svgvpt and is a derived from the file
  cr-rgb.c, part of the croco library (a css parsing library).
  Modifications are

  Copyright (c) J.J. Green 2004

  The original header follows

  ----
  This file is part of The Croco Library

  This program is free software; you can redistribute it and/or
  modify it under the terms of version 2.1 of the GNU Lesser General Public
  License as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, write to the Free
  Software Foundation, Inc.,  51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301 USA

  Author: Dodji Seketeli
  See COPYRIGHTS file for copyrights information.
  ----
 */

#include <stdlib.h>
#include <string.h>

#include "cptutils/stdcol.h"

#line 39 "stdcol.gperf"
struct stdcol_t;
/* maximum key range = 562, duplicates = 0 */

#ifndef GPERF_DOWNCASE
#define GPERF_DOWNCASE 1
static unsigned char gperf_downcase[256] =
  {
      0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
     15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
     30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,
     45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
     60,  61,  62,  63,  64,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106,
    107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121,
    122,  91,  92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103, 104,
    105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
    120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
    135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
    150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164,
    165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
    180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194,
    195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
    210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224,
    225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
    240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254,
    255
  };
#endif

#ifndef GPERF_CASE_STRCMP
#define GPERF_CASE_STRCMP 1
static int
gperf_case_strcmp (register const char *s1, register const char *s2)
{
  for (;;)
    {
      unsigned char c1 = gperf_downcase[(unsigned char)*s1++];
      unsigned char c2 = gperf_downcase[(unsigned char)*s2++];
      if (c1 != 0 && c1 == c2)
        continue;
      return (int)c1 - (int)c2;
    }
}
#endif

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (register const char *str, register size_t len)
{
  static const unsigned short asso_values[] =
    {
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566,   5,  55,   0,  35,   0,
       75,  10,   5,   0, 566, 250,  10,  40,  85,  60,
       70, 144,   0,  20,  45,  10,  30, 185,  95, 195,
      566,   0, 566, 566, 566, 566, 566,   5,  55,   0,
       35,   0,  75,  10,   5,   0, 566, 250,  10,  40,
       85,  60,  70, 144,   0,  20,  45,  10,  30, 185,
       95, 195, 566,   0, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566, 566, 566,
      566, 566, 566, 566, 566, 566, 566, 566
    };
  register unsigned int hval = len;

  switch (hval)
    {
      default:
        hval += asso_values[(unsigned char)str[12]];
      /*FALLTHROUGH*/
      case 12:
        hval += asso_values[(unsigned char)str[11]];
      /*FALLTHROUGH*/
      case 11:
      case 10:
      case 9:
      case 8:
        hval += asso_values[(unsigned char)str[7]];
      /*FALLTHROUGH*/
      case 7:
        hval += asso_values[(unsigned char)str[6]];
      /*FALLTHROUGH*/
      case 6:
        hval += asso_values[(unsigned char)str[5]];
      /*FALLTHROUGH*/
      case 5:
      case 4:
      case 3:
        hval += asso_values[(unsigned char)str[2]+2];
      /*FALLTHROUGH*/
      case 2:
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval;
}

const struct stdcol_t *
stdcol_lookup (register const char *str, register size_t len)
{
  enum
    {
      TOTAL_KEYWORDS = 148,
      MIN_WORD_LENGTH = 3,
      MAX_WORD_LENGTH = 20,
      MIN_HASH_VALUE = 4,
      MAX_HASH_VALUE = 565
    };

  static const struct stdcol_t wordlist[] =
    {
      {""}, {""}, {""}, {""},
#line 61 "stdcol.gperf"
      {"cyan", 0, 255, 255, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 94 "stdcol.gperf"
      {"gray", 128, 128, 128, 0},
      {""}, {""}, {""}, {""}, {""},
#line 55 "stdcol.gperf"
      {"chartreuse", 127, 255, 0, 0},
      {""}, {""}, {""},
#line 95 "stdcol.gperf"
      {"grey", 128, 128, 128, 0},
#line 96 "stdcol.gperf"
      {"green", 0, 128, 0, 0},
      {""}, {""}, {""},
#line 114 "stdcol.gperf"
      {"lightgrey", 211, 211, 211, 0},
#line 113 "stdcol.gperf"
      {"lightgreen", 144, 238, 144, 0},
      {""}, {""}, {""},
#line 112 "stdcol.gperf"
      {"lightgray", 211, 211, 211, 0},
      {""}, {""},
#line 170 "stdcol.gperf"
      {"skyblue", 135, 206, 235, 0},
      {""},
#line 173 "stdcol.gperf"
      {"slategrey", 112, 128, 144, 0},
      {""},
#line 168 "stdcol.gperf"
      {"sienna", 160, 82, 45, 0},
      {""}, {""},
#line 172 "stdcol.gperf"
      {"slategray", 112, 128, 144, 0},
      {""}, {""}, {""},
#line 167 "stdcol.gperf"
      {"seashell", 255, 245, 238, 0},
#line 178 "stdcol.gperf"
      {"teal", 0, 128, 128, 0},
#line 57 "stdcol.gperf"
      {"coral", 255, 127, 80, 0},
      {""}, {""}, {""}, {""}, {""},
#line 116 "stdcol.gperf"
      {"lightsalmon", 255, 160, 122, 0},
      {""}, {""},
#line 120 "stdcol.gperf"
      {"lightslategrey", 119, 136, 153, 0},
#line 48 "stdcol.gperf"
      {"black", 0, 0, 0, 0},
      {""}, {""}, {""},
#line 119 "stdcol.gperf"
      {"lightslategray", 119, 136, 153, 0},
      {""},
#line 146 "stdcol.gperf"
      {"orange", 255, 165, 0, 0},
      {""}, {""},
#line 147 "stdcol.gperf"
      {"orangered", 255, 69, 0, 0},
      {""},
#line 47 "stdcol.gperf"
      {"bisque", 255, 228, 196, 0},
      {""}, {""},
#line 123 "stdcol.gperf"
      {"lime", 0, 255, 0, 0},
      {""}, {""}, {""},
#line 160 "stdcol.gperf"
      {"red", 255, 0, 0, 0},
#line 124 "stdcol.gperf"
      {"limegreen", 50, 205, 50, 0},
#line 109 "stdcol.gperf"
      {"lightcoral", 240, 128, 128, 0},
      {""}, {""}, {""},
#line 162 "stdcol.gperf"
      {"royalblue", 65, 105, 225, 0},
#line 125 "stdcol.gperf"
      {"linen", 250, 240, 230, 0},
      {""},
#line 89 "stdcol.gperf"
      {"fuchsia", 255, 0, 255, 0},
      {""},
#line 66 "stdcol.gperf"
      {"darkgreen", 0, 100, 0, 0},
      {""}, {""}, {""}, {""},
#line 108 "stdcol.gperf"
      {"lightblue", 173, 216, 230, 0},
#line 72 "stdcol.gperf"
      {"darkorchid", 153, 50, 204, 0},
#line 175 "stdcol.gperf"
      {"springgreen", 0, 255, 127, 0},
#line 126 "stdcol.gperf"
      {"magenta", 255, 0, 255, 0},
      {""},
#line 92 "stdcol.gperf"
      {"gold", 255, 215, 0, 0},
      {""},
#line 148 "stdcol.gperf"
      {"orchid", 218, 112, 214, 0},
      {""}, {""},
#line 171 "stdcol.gperf"
      {"slateblue", 106, 90, 205, 0},
      {""},
#line 69 "stdcol.gperf"
      {"darkmagenta", 139, 0, 139, 0},
      {""},
#line 62 "stdcol.gperf"
      {"darkblue", 0, 0, 139, 0},
#line 121 "stdcol.gperf"
      {"lightsteelblue", 176, 196, 222, 0},
      {""},
#line 169 "stdcol.gperf"
      {"silver", 192, 192, 192, 0},
      {""},
#line 166 "stdcol.gperf"
      {"seagreen", 46, 139, 87, 0},
#line 176 "stdcol.gperf"
      {"steelblue", 70, 130, 180, 0},
      {""}, {""}, {""},
#line 177 "stdcol.gperf"
      {"tan", 210, 180, 140, 0},
#line 155 "stdcol.gperf"
      {"peru", 205, 133, 63, 0},
      {""},
#line 159 "stdcol.gperf"
      {"purple", 128, 0, 128, 0},
#line 73 "stdcol.gperf"
      {"darkred", 139, 0, 0, 0},
      {""},
#line 138 "stdcol.gperf"
      {"mintcream", 245, 255, 250, 0},
      {""}, {""}, {""}, {""},
#line 86 "stdcol.gperf"
      {"firebrick", 178, 34, 34, 0},
      {""},
#line 188 "stdcol.gperf"
      {"transparent", 255, 255, 255, 1},
      {""},
#line 117 "stdcol.gperf"
      {"lightseagreen", 32, 178, 170, 0},
#line 70 "stdcol.gperf"
      {"darkolivegreen", 85, 107, 47, 0},
      {""}, {""}, {""}, {""},
#line 139 "stdcol.gperf"
      {"mistyrose", 255, 228, 225, 0},
      {""},
#line 101 "stdcol.gperf"
      {"indigo", 75, 0, 130, 0},
#line 143 "stdcol.gperf"
      {"oldlace", 253, 245, 230, 0},
      {""},
#line 156 "stdcol.gperf"
      {"pink", 255, 192, 203, 0},
#line 74 "stdcol.gperf"
      {"darksalmon", 233, 150, 122, 0},
      {""}, {""},
#line 104 "stdcol.gperf"
      {"lavender", 230, 230, 250, 0},
#line 102 "stdcol.gperf"
      {"ivory", 255, 255, 240, 0},
      {""}, {""}, {""},
#line 140 "stdcol.gperf"
      {"moccasin", 255, 228, 181, 0},
      {""}, {""}, {""}, {""}, {""},
#line 54 "stdcol.gperf"
      {"cadetblue", 95, 158, 160, 0},
#line 80 "stdcol.gperf"
      {"darkviolet", 148, 0, 211, 0},
#line 163 "stdcol.gperf"
      {"saddlebrown", 139, 69, 19, 0},
      {""},
#line 76 "stdcol.gperf"
      {"darkslateblue", 72, 61, 139, 0},
#line 150 "stdcol.gperf"
      {"palegreen", 152, 251, 152, 0},
      {""}, {""}, {""},
#line 174 "stdcol.gperf"
      {"snow", 255, 250, 250, 0},
#line 100 "stdcol.gperf"
      {"indianred", 205, 92, 92, 0},
#line 111 "stdcol.gperf"
      {"lightgoldenrodyellow", 250, 250, 210, 0},
#line 180 "stdcol.gperf"
      {"tomato", 255, 99, 71, 0},
#line 107 "stdcol.gperf"
      {"lemonchiffon", 255, 250, 205, 0},
      {""},
#line 115 "stdcol.gperf"
      {"lightpink", 255, 182, 193, 0},
      {""},
#line 127 "stdcol.gperf"
      {"maroon", 128, 0, 0, 0},
      {""},
#line 105 "stdcol.gperf"
      {"lavenderblush", 255, 240, 245, 0},
#line 181 "stdcol.gperf"
      {"turquoise", 64, 224, 208, 0},
#line 71 "stdcol.gperf"
      {"darkorange", 255, 140, 0, 0},
      {""}, {""}, {""},
#line 142 "stdcol.gperf"
      {"navy", 0, 0, 128, 0},
#line 85 "stdcol.gperf"
      {"dodgerblue", 30, 144, 255, 0},
#line 88 "stdcol.gperf"
      {"forestgreen", 34, 139, 34, 0},
#line 137 "stdcol.gperf"
      {"midnightblue", 25, 25, 112, 0},
      {""},
#line 132 "stdcol.gperf"
      {"mediumseagreen", 60, 179, 113, 0},
      {""}, {""},
#line 75 "stdcol.gperf"
      {"darkseagreen", 143, 188, 143, 0},
      {""},
#line 43 "stdcol.gperf"
      {"aqua", 0, 255, 255, 0},
#line 45 "stdcol.gperf"
      {"azure", 240, 255, 255, 0},
#line 164 "stdcol.gperf"
      {"salmon", 250, 128, 114, 0},
      {""}, {""}, {""},
#line 183 "stdcol.gperf"
      {"wheat", 245, 222, 179, 0},
      {""}, {""}, {""},
#line 52 "stdcol.gperf"
      {"brown", 165, 42, 42, 0},
#line 44 "stdcol.gperf"
      {"aquamarine", 127, 255, 212, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 56 "stdcol.gperf"
      {"chocolate", 210, 105, 30, 0},
#line 106 "stdcol.gperf"
      {"lawngreen", 124, 252, 0, 0},
#line 165 "stdcol.gperf"
      {"sandybrown", 244, 164, 96, 0},
      {""}, {""}, {""},
#line 110 "stdcol.gperf"
      {"lightcyan", 224, 255, 255, 0},
      {""}, {""}, {""}, {""}, {""},
#line 182 "stdcol.gperf"
      {"violet", 238, 130, 238, 0},
#line 122 "stdcol.gperf"
      {"lightyellow", 255, 255, 224, 0},
      {""}, {""}, {""},
#line 129 "stdcol.gperf"
      {"mediumblue", 0, 0, 205, 0},
      {""}, {""}, {""},
#line 154 "stdcol.gperf"
      {"peachpuff", 255, 218, 185, 0},
      {""},
#line 97 "stdcol.gperf"
      {"greenyellow", 173, 255, 47, 0},
      {""}, {""}, {""}, {""}, {""},
#line 42 "stdcol.gperf"
      {"antiquewhite", 250, 235, 215, 0},
      {""},
#line 50 "stdcol.gperf"
      {"blue", 0, 0, 255, 0},
#line 136 "stdcol.gperf"
      {"mediumvioletred", 199, 21, 133, 0},
      {""},
#line 131 "stdcol.gperf"
      {"mediumpurple", 147, 112, 219, 0},
      {""},
#line 93 "stdcol.gperf"
      {"goldenrod", 218, 165, 32, 0},
      {""}, {""}, {""}, {""},
#line 49 "stdcol.gperf"
      {"blanchedalmond", 255, 235, 205, 0},
#line 103 "stdcol.gperf"
      {"khaki", 240, 230, 140, 0},
      {""}, {""}, {""},
#line 157 "stdcol.gperf"
      {"plum", 221, 160, 221, 0},
      {""}, {""},
#line 130 "stdcol.gperf"
      {"mediumorchid", 186, 85, 211, 0},
      {""},
#line 161 "stdcol.gperf"
      {"rosybrown", 188, 143, 143, 0},
#line 133 "stdcol.gperf"
      {"mediumslateblue", 123, 104, 238, 0},
      {""},
#line 79 "stdcol.gperf"
      {"darkturquoise", 0, 206, 209, 0},
      {""}, {""}, {""}, {""}, {""},
#line 152 "stdcol.gperf"
      {"palevioletred", 219, 112, 147, 0},
      {""},
#line 153 "stdcol.gperf"
      {"papayawhip", 255, 239, 213, 0},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 134 "stdcol.gperf"
      {"mediumspringgreen", 0, 250, 154, 0},
#line 67 "stdcol.gperf"
      {"darkgrey", 169, 169, 169, 0},
      {""},
#line 135 "stdcol.gperf"
      {"mediumturquoise", 72, 209, 204, 0},
      {""}, {""},
#line 65 "stdcol.gperf"
      {"darkgray", 169, 169, 169, 0},
      {""}, {""}, {""}, {""},
#line 64 "stdcol.gperf"
      {"darkgoldenrod", 184, 134, 11, 0},
      {""}, {""}, {""},
#line 84 "stdcol.gperf"
      {"dimgrey", 105, 105, 105, 0},
      {""}, {""}, {""}, {""},
#line 83 "stdcol.gperf"
      {"dimgray", 105, 105, 105, 0},
#line 98 "stdcol.gperf"
      {"honeydew", 240, 255, 240, 0},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 46 "stdcol.gperf"
      {"beige", 245, 245, 220, 0},
      {""},
#line 179 "stdcol.gperf"
      {"thistle", 216, 191, 216, 0},
#line 59 "stdcol.gperf"
      {"cornsilk", 255, 248, 220, 0},
      {""},
#line 144 "stdcol.gperf"
      {"olive", 128, 128, 0, 0},
      {""}, {""}, {""}, {""},
#line 51 "stdcol.gperf"
      {"blueviolet", 138, 43, 226, 0},
      {""}, {""}, {""}, {""},
#line 128 "stdcol.gperf"
      {"mediumaquamarine", 102, 205, 170, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 58 "stdcol.gperf"
      {"cornflowerblue", 100, 149, 237, 0},
      {""}, {""}, {""}, {""},
#line 41 "stdcol.gperf"
      {"aliceblue", 240, 248, 255,},
#line 158 "stdcol.gperf"
      {"powderblue", 176, 224, 230, 0},
      {""},
#line 151 "stdcol.gperf"
      {"paleturquoise", 175, 238, 238, 0},
      {""}, {""}, {""}, {""}, {""},
#line 78 "stdcol.gperf"
      {"darkslategrey", 47, 79, 79, 0},
#line 68 "stdcol.gperf"
      {"darkkhaki", 189, 183, 107, 0},
      {""}, {""}, {""},
#line 77 "stdcol.gperf"
      {"darkslategray", 47, 79, 79, 0},
#line 91 "stdcol.gperf"
      {"ghostwhite", 248, 248, 255, 0},
      {""}, {""}, {""}, {""},
#line 145 "stdcol.gperf"
      {"olivedrab", 107, 142, 35, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 149 "stdcol.gperf"
      {"palegoldenrod", 238, 232, 170, 0},
      {""}, {""}, {""}, {""},
#line 63 "stdcol.gperf"
      {"darkcyan", 0, 139, 139, 0},
      {""}, {""}, {""},
#line 99 "stdcol.gperf"
      {"hotpink", 255, 105, 180, 0},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 90 "stdcol.gperf"
      {"gainsboro", 220, 220, 220, 0},
      {""}, {""}, {""},
#line 81 "stdcol.gperf"
      {"deeppink", 255, 20, 147, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""},
#line 60 "stdcol.gperf"
      {"crimson", 220, 20, 60, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 53 "stdcol.gperf"
      {"burlywood", 222, 184, 135, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""},
#line 87 "stdcol.gperf"
      {"floralwhite", 255, 250, 240, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 184 "stdcol.gperf"
      {"white", 255, 255, 255, 0},
#line 141 "stdcol.gperf"
      {"navajowhite", 255, 222, 173, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""},
#line 186 "stdcol.gperf"
      {"yellow", 255, 255, 0, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 187 "stdcol.gperf"
      {"yellowgreen", 154, 205, 50, 0},
#line 118 "stdcol.gperf"
      {"lightskyblue", 135, 206, 250, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
      {""}, {""}, {""}, {""}, {""},
#line 82 "stdcol.gperf"
      {"deepskyblue", 0, 191, 255, 0},
      {""}, {""}, {""}, {""}, {""}, {""}, {""}, {""},
#line 185 "stdcol.gperf"
      {"whitesmoke", 245, 245, 245, 0}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register unsigned int key = hash (str, len);

      if (key <= MAX_HASH_VALUE)
        {
          register const char *s = wordlist[key].name;

          if ((((unsigned char)*str ^ (unsigned char)*s) & ~32) == 0 && !gperf_case_strcmp (str, s))
            return &wordlist[key];
        }
    }
  return 0;
}
#line 189 "stdcol.gperf"


extern const struct stdcol_t* stdcol(char *s)
{
  if (!s) return NULL;
  return stdcol_lookup(s, strlen(s));
}
