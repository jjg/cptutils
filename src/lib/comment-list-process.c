#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/comment-list-process.h"
#include "cptutils/comment-list-write.h"
#include "cptutils/comment-list-read.h"

#include "cptutils/btrace.h"

#include <stdbool.h>

typedef struct {
  size_t n;
  comment_list_t *comment_list;
} comment_list_copy_t;

static int f_copy(comment_t *comment_dst, void *arg)
{
  comment_list_copy_t *copy = arg;
  comment_list_t *list_src = copy->comment_list;
  size_t n = copy->n;
  const comment_t *comment_src = comment_list_entry(list_src, n);

  if (comment_src == NULL)
    return 1;

  if (comment_copy(comment_src, comment_dst) != 0)
    return 1;

  copy->n++;

  return 0;
}

static int comment_list_copy(comment_list_t *list_src,
                             comment_list_t *list_dst)
{
  comment_list_copy_t arg = {.n = 0, .comment_list = list_src};
  return comment_list_each((comment_list_t*)list_dst, f_copy, &arg);
}

static int f_generate(comment_t *comment, void *arg)
{
  return comment_generate(comment);
}

static int comment_list_generate(comment_list_t *list)
{
  return comment_list_each((comment_list_t*)list, f_generate, NULL);
}

static int f_clear(comment_t *comment, void *arg)
{
  comment_deinit(comment);
  return 0;
}

static void comment_list_clear(comment_list_t *list)
{
  comment_list_each((comment_list_t*)list, f_clear, NULL);
}

static int f_finalised(comment_t *comment, void *arg)
{
  return comment_finalised(comment) ? 0 : 1;
}

static bool comment_list_finalised(const comment_list_t *list)
{
  return comment_list_each((comment_list_t*)list, f_finalised, NULL) == 0;
}

/*
  This is the equivalent to comment_process() which is in
  comment.c, but acting on a comment_list_t rather than on a
  comment_t.  We define it outside of comment_list.c since I
  have an idea to have *-list.c share code (possibly generated,
  or void* with thin wrappers).
*/

int comment_list_process(comment_list_t *list,
                         const comment_opt_t *opt)
{
  if (opt->path.output != NULL)
    {
      if (comment_list_finalised(list) == false)
        {
          btrace("comment not finalised");
          return 1;
        }

      if (comment_list_write(list, opt->path.output) != 0)
        {
          btrace("failed comment write");
          return 1;
        }
    }

  if (opt->action != action_retain)
    {
      comment_list_clear(list);

      if (opt->action == action_generate)
        {
          if (comment_list_generate(list) != 0)
            {
              btrace("failed comment generate");
              return 1;
            }
        }
      else if (opt->action == action_read)
        {
          comment_list_t *list0;
          int err = 1;

          if ((list0 = comment_list_read(opt->path.input)) != NULL)
            {
              size_t
                n = comment_list_size(list),
                n0 = comment_list_size(list0);

              if (n == n0)
                err = comment_list_copy(list0, list);
              else
                btrace("read %zu comment, but target has %zu gradients", n0, n);

              comment_list_destroy(list0);
            }

          if (err != 0)
            {
              btrace("failed comment read from '%s'", opt->path.input);
              return 1;
            }
        }
    }

  return 0;
}
