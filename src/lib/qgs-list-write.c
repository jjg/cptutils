#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/qgs-list-write.h"
#include "cptutils/btrace.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include <time.h>
#include <stdio.h>
#include <string.h>

#define ENCODING "utf-8"
#define BUFSZ 128

static int qgs_attribute(xmlTextWriter *writer,
                         const char *name,
                         const char *value,
                         const char *element)
{
  if (xmlTextWriterWriteAttribute(writer,
                                  BAD_CAST name,
                                  BAD_CAST value) < 0)
    {
      btrace("error setting %s %s attribute", element, name);
      return 1;
    }

  return 0;
}

/* 4*3 + 3 + 1 */

#define END_STOP_LEN 16

static char* end_stop(const qgs_entry_t *entry)
{
  char *str;

  if ((str = malloc(END_STOP_LEN)) != NULL)
    {

      if (snprintf(str, END_STOP_LEN, "%i,%i,%i,%i",
                   entry->rgb.red,
                   entry->rgb.green,
                   entry->rgb.blue,
                   entry->opacity) < END_STOP_LEN)
        return str;

      free(str);
    }

  return NULL;
}

/* 6 + 4*3 + 4 + 1 */

#define MID_STOP_LEN 23

static char* mid_stop(const qgs_entry_t *entry)
{
  char *str;

  if ((str = malloc(MID_STOP_LEN)) != NULL)
    {
      if (snprintf(str, MID_STOP_LEN, "%.4f;%i,%i,%i,%i",
                   entry->value,
                   entry->rgb.red,
                   entry->rgb.green,
                   entry->rgb.blue,
                   entry->opacity) < MID_STOP_LEN)
        return str;

      free(str);
    }

  return NULL;
}

static int qgs_write_endstop(xmlTextWriter *writer,
                             const char *name,
                             const qgs_entry_t *entry)
{
  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "prop") >= 0)
    {
      if (qgs_attribute(writer, "k", name, "prop") == 0)
        {
          char *stop;

          if ((stop = end_stop(entry)) != NULL)
            {
              if (qgs_attribute(writer, "v", stop, "prop") == 0)
                {
                  if (xmlTextWriterEndElement(writer) >= 0)
                    err = 0;
                  else
                    btrace("error from close prop");
                }

              free(stop);
            }
          else
            btrace("error creating endstop");
        }
    }
  else
    btrace("error from open prop");

  return err;
}

static char* join_strings(char **strings, size_t n, char sep)
{
  size_t sz = n;
  for (size_t i = 0 ; i < n ; i++)
    sz += strlen(strings[i]);

  char *buffer;

  if ((buffer = malloc(sz)) != NULL)
    {
      char *p = buffer;
      size_t k;
      int err = 0;

      if ((k = snprintf(buffer, sz, "%s", strings[0])) >= sz)
        err++;

      for (int i = 1 ; i < n ; i++)
        {
          p += k;
          sz -= k;
          if ((k = snprintf(p, sz, "%c%s", sep, strings[i])) >= sz)
            err++;
        }

      if (err == 0) return buffer;

      btrace("string truncation");
      free(buffer);
    }

  return NULL;
}

static char* mid_stops_string(const qgs_t *qgs)
{
  size_t n = qgs->n;

  if (n < 3)
    return NULL;

  char *stops[n - 2];
  int err = 0;

  for (size_t i = 0 ; i < n - 2 ; i++)
    {
      if ((stops[i] = mid_stop(qgs->entries + i + 1)) == NULL)
        err++;
    }

  char *buffer = NULL;

  if (err == 0)
    buffer = join_strings(stops, n - 2, ':');

  for (size_t i = 0 ; i < n - 2 ; i++)
    free(stops[i]);

  return buffer;
}

static int qgs_write_midstops(xmlTextWriter *writer, const qgs_t *qgs)
{
  int n = qgs->n;

  if (n < 3) return 0;

  if (xmlTextWriterStartElement(writer, BAD_CAST "prop") < 0)
    {
      btrace("error from open prop");
      return 1;
    }

  if (qgs_attribute(writer, "k", "stops", "prop") != 0)
    return 1;

  char *str;

  if ((str = mid_stops_string(qgs)) == NULL)
    return 1;

  int err = 0;

  if (qgs_attribute(writer, "v", str, "prop") != 0)
    {
      btrace("error writing attribute");
      err++;
    }

  free(str);

  if (xmlTextWriterEndElement(writer) < 0)
    {
      btrace("error from close prop");
      err++;
    }

  return err > 0;
}

static int qgs_write_discrete(xmlTextWriter *writer, const qgs_t *qgs)
{
  const char *discrete;

  switch (qgs->type)
    {
    case QGS_TYPE_UNSET:
      return 0;
    case QGS_TYPE_DISCRETE:
      discrete = "1";
      break;
    case QGS_TYPE_INTERPOLATED:
      discrete = "0";
      break;
    default:
      btrace("unknown type");
      return 1;
    }

  int err = 1;

  if (xmlTextWriterStartElement(writer, BAD_CAST "prop") >= 0)
    {
      if (qgs_attribute(writer, "k", "discrete", "prop") == 0)
        {
          if (qgs_attribute(writer, "v", discrete, "prop") == 0)
            {
              if (xmlTextWriterEndElement(writer) >= 0)
                err = 0;
              else
                btrace("error from close prop");
            }
          else
            btrace("error in attribute v");
        }
      else
        btrace("error in attribute k");
    }
  else
    btrace("error from open prop");

  return err;
}

static int qgs_write_comment(const char *line, void *arg)
{
  xmlTextWriter *writer = arg;

  if (line != NULL)
    {
      if (xmlTextWriterWriteFormatComment(writer, " %s ", line) < 0)
        {
          btrace("single-line comment");
          return 1;
        }
    }

  return 0;
}

#define SPACE6 "      "
#define SPACE8 "        "

static int qgs_write_comments(const char *line, void *arg)
{
  xmlTextWriter *writer = arg;
  int err = 1;

  if (line == NULL)
    {
      if (xmlTextWriterWriteString(writer, BAD_CAST "\n") < 0)
        btrace("empty comment");
      else
        err = 0;
    }
  else
    {
      size_t n = strlen(line) + 10;
      char buffer[n];
      snprintf(buffer, n, SPACE8 "%s\n", line);

      if (xmlTextWriterWriteString(writer, BAD_CAST buffer) < 0)
        btrace("comment");
      else
        err = 0;
    }

  return err;
}

static int qgs_write_colorramp_comment(xmlTextWriter *writer, const qgs_t *qgs)
{
  int err = 1;

  if (comment_count(qgs->comment) == 1)
    {
      if (comment_each(qgs->comment, qgs_write_comment, writer) == 0)
        err = 0;
    }
  else if (comment_count(qgs->comment) > 1)
    {
      if (xmlTextWriterStartComment(writer) < 0)
        btrace("open comment");
      else
        {
          if (xmlTextWriterWriteString(writer, BAD_CAST "\n") < 0)
            btrace("initial newline");
          else
            {
              if (comment_each(qgs->comment, qgs_write_comments, writer) != 0)
                btrace("comment body");
              else
                {
                  if (xmlTextWriterWriteString(writer, BAD_CAST SPACE6) < 0)
                    btrace("comment end indent");
                  else
                    {
                      if (xmlTextWriterEndComment(writer) < 0)
                        btrace("close comment");
                      else
                        err = 0;
                    }
                }
            }
        }
    }
  else
    err = 0;

  return err;
}

static int qgs_write_colorramp_props(xmlTextWriter *writer, const qgs_t *qgs)
{
  int n = qgs->n;

  if (qgs_write_endstop(writer, "color1", qgs->entries) != 0)
    return 1;

  if (qgs_write_endstop(writer, "color2", qgs->entries + (n - 1)) != 0)
    return 1;

  if (qgs_write_midstops(writer, qgs) != 0)
    return 1;

  if (qgs_write_discrete(writer, qgs) != 0)
    return 1;

  return 0;
}

static int qgs_write_colorramp(qgs_t *qgs, void *arg)
{
  xmlTextWriter *writer = arg;

  if (xmlTextWriterStartElement(writer, BAD_CAST "colorramp") < 0 )
    {
      btrace("error from open coloramp");
      return 1;
    }

  if (qgs_attribute(writer, "type", "gradient", "colorramp") != 0)
    return 1;

  if (qgs_attribute(writer, "name", qgs->name, "colorramp") != 0)
    return 1;

  if (qgs_write_colorramp_comment(writer, qgs) != 0)
    return 1;

  if (qgs_write_colorramp_props(writer, qgs) != 0)
    return 1;

  if (xmlTextWriterEndElement(writer) < 0)
    {
      btrace("error from close colorramp");
      return 1;
    }

  return 0;
}

static int qgs_write_colorramps(xmlTextWriter *writer, const qgs_list_t *list)
{
  if (xmlTextWriterStartElement(writer, BAD_CAST "colorramps") < 0 )
    {
      btrace("error from open colorramps");
      return 1;
    }

  if (qgs_list_each((qgs_list_t*)list, qgs_write_colorramp, writer) != 0)
    {
      btrace("failed to write colorramp");
      return 1;
    }

  if (xmlTextWriterEndElement(writer) < 0)
    {
      btrace("error from close colorramps");
      return 1;
    }

  return 0;
}

static int qgs_write_symbols(xmlTextWriter *writer, const qgs_list_t *list)
{
  if (xmlTextWriterStartElement(writer, BAD_CAST "symbols") < 0 )
    {
      btrace("error from open symbols");
      return 1;
    }

  if (xmlTextWriterEndElement(writer) < 0)
    {
      btrace("error from close symbols");
      return 1;
    }

  return 0;
}

static int qgs_write_qgis(xmlTextWriter *writer, const qgs_list_t *list)
{
  if (xmlTextWriterStartElement(writer, BAD_CAST "qgis_style") < 0 )
    {
      btrace("error from open qgis");
      return 1;
    }

  if (qgs_attribute(writer, "version", "1", "qgis_style") != 0)
    return 1;

  if (qgs_write_symbols(writer, list) != 0)
    {
      btrace("failed to write symbols");
      return 1;
    }

  if (qgs_write_colorramps(writer, list) != 0)
    {
      btrace("failed to write colorramps");
      return 1;
    }

  if (xmlTextWriterEndElement(writer) < 0)
    {
      btrace("error from close qgis");
      return 1;
    }

  return 0;
}

static int qgs_write_mem(const qgs_list_t *list, xmlTextWriter *writer)
{
  if (xmlTextWriterWriteDTD(writer,
                            BAD_CAST "qgis_style",
                            NULL, NULL, NULL) < 0)
    {
      btrace("error writing doctype");
      return 1;
    }

  if (qgs_write_qgis(writer, list) != 0)
    {
      btrace("failed to write qgis");
      return 1;
    }

  if (xmlTextWriterEndDocument(writer) < 0)
    {
      btrace("error from end document");
      return 1;
    }

  return 0;
}

int qgs_list_write(const qgs_list_t *list, const char *path)
{
  xmlBuffer *buffer;
  int err = 1;

  if ((buffer = xmlBufferCreate()) == NULL)
    btrace("error creating xml writer buffer");
  else
    {
      xmlTextWriter *writer;

      if ((writer = xmlNewTextWriterMemory(buffer, 0)) == NULL)
        btrace("error creating the xml writer");
      else
        {
          /* set two-space indentation, not fatal if this fails */

          if (xmlTextWriterSetIndent(writer, 1) != 0)
            btrace("failed enable indent");

          unsigned char indent[] = {' ', ' ', '\0'};

          if (xmlTextWriterSetIndentString(writer, indent) != 0)
            btrace("failed to set indent string");

          /*
            we free the writer at the start of each of these branches
            since this must be done before using the buffer
          */

          if (qgs_write_mem(list, writer) != 0)
            {
              xmlFreeTextWriter(writer);
              btrace("failed memory write");
            }
          else
            {
              xmlFreeTextWriter(writer);

              if (path)
                {
                  FILE *fp;

                  if ((fp = fopen(path, "w")) == NULL)
                    btrace("error opening file %s", path);
                  else
                    {
                      fprintf(fp, "%s", buffer->content);

                      if (fclose(fp) != 0)
                        btrace("error closing file %s", path);
                      else
                        err = 0;
                    }
                }
              else
                {
                  fprintf(stdout, "%s", buffer->content);
                  err = 0;
                }
            }
        }
      xmlBufferFree(buffer);
    }

  return err;
}
