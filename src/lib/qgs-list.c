#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/qgs-list.h"

#include <stdlib.h>

struct qgs_list_t
{
  size_t n, alloc;
  qgs_t *qgs;
};

#define LIST_INI 20
#define LIST_INC 20

qgs_list_t* qgs_list_new(void)
{
  qgs_list_t *list;

  if ((list = calloc(1, sizeof(qgs_list_t))) != NULL)
    {
      qgs_t *qgs;

      if ((qgs = calloc(LIST_INI, sizeof(qgs_t))) != NULL)
	{
	  list->qgs = qgs;
	  list->alloc = LIST_INI;
	  list->n = 0;

	  return list;
	}

      free(list);
    }

  return NULL;
}

void qgs_list_destroy(qgs_list_t *list)
{
  if (list != NULL)
    {
      for (size_t i = 0 ; i < list->n ; i++)
        qgs_deinit(list->qgs + i);
      free(list->qgs);
    }
  free(list);
}

int qgs_list_each(qgs_list_t *list,
                  int (*f)(qgs_t*, void*),
                  void *opt)
{
  for (size_t i = 0 ; i < list->n ; i++)
    {
      int err;
      qgs_t *qgs = list->qgs + i;

      if ((err = f(qgs, opt)) != 0)
        return err;
    }

  return 0;
}

qgs_t* qgs_list_find(qgs_list_t *list,
                     int (*f)(qgs_t*, void*),
                     void *opt)
{
  for (size_t i = 0 ; i < list->n ; i++)
    {
      qgs_t *qgs = list->qgs + i;

      if (f(qgs, opt) != 0)
        return qgs;
    }

  return NULL;
}

qgs_t* qgs_list_next(qgs_list_t *list)
{
  size_t
    n = list->n,
    a = list->alloc;

  if (n < a)
    {
      list->n++;

      qgs_t *qgs = list->qgs + n;

      if (qgs_init(qgs) == 0)
        return qgs;
    }
  else
    {
      a += LIST_INC;

      qgs_t *qgs;

      if ((qgs = realloc(list->qgs, sizeof(qgs_t) * a)) != NULL)
        {
          list->alloc = a;
          list->qgs = qgs;

          return qgs_list_next(list);
        }
    }

  return NULL;
}

int qgs_list_revert(qgs_list_t *list)
{
  size_t n = list->n;

  if (n == 0)
    return 1;

  qgs_t *qgs = &(list->qgs[n - 1]);
  qgs_deinit(qgs);
  list->n--;

  return 0;
}

size_t qgs_list_size(const qgs_list_t *list)
{
  return list->n;
}

const qgs_t* qgs_list_entry(const qgs_list_t *list, size_t i)
{
  if (i >= list->n)
    return NULL;
  return list->qgs + i;
}
