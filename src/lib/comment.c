#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/comment.h"
#include "cptutils/comment-type.h"
#include "cptutils/comment-read.h"
#include "cptutils/comment-write.h"
#include "cptutils/btrace.h"

#include <stdint.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

int comment_init(comment_t *comment)
{
  comment->finalised = false;
  comment->line = NULL;

  return 0;
}

comment_t* comment_new(void)
{
  comment_t *comment;

  if ((comment = malloc(sizeof(comment_t))) != NULL)
    {
      if (comment_init(comment) == 0)
        return comment;

      free(comment);
    }

  return NULL;
}

static void line_destroy(comment_line_t *line)
{
  if (line != NULL)
    {
      line_destroy(line->next);
      free(line->text);
      free(line);
    }
}

void comment_deinit(comment_t *comment)
{
  if (comment != NULL)
    {
      line_destroy(comment->line);
      comment_init(comment);
    }
}

void comment_destroy(comment_t *comment)
{
  comment_deinit(comment);
  free(comment);
}

static int line_each(const comment_line_t *line,
                     size_t offset,
                     int (*f)(const char*, void*),
                     void *arg)
{
  if (line == NULL)
    return 0;

  return
    line_each(line->next, offset, f, arg) ||
    f(line->text ? line->text + offset : NULL, arg);
}

int comment_each(const comment_t *comment,
                 int (*f)(const char*, void*),
                 void *arg)
{
  return line_each(comment->line, comment->offset, f, arg);
}

static int line_count(const char *text, void *arg)
{
  size_t *n = arg;
  (*n)++;
  return 0;
}

size_t comment_count(const comment_t *comment)
{
  size_t n = 0;
  comment_each(comment, line_count, &n);
  return n;
}

static int line_copy(const char *text, void *arg)
{
  comment_t *dst = arg;
  return comment_push(dst, text ? text : "");
}

int comment_copy(const comment_t *src, comment_t *dst)
{
  dst->finalised = src->finalised;
  dst->offset = 0;
  return comment_each(src, line_copy, dst);
}

int comment_cat(const comment_t **src,
                size_t n,
                const char *sep,
                comment_t *dst)
{
  if (n == 0)
    return 0;

  if (comment_each(src[0], line_copy, dst) != 0)
    {
      btrace("concatenate first");
      return 1;
    }

  for (size_t i = 1 ; i < n ; i++)
    {
      if ((line_copy(sep, dst) != 0) ||
          (comment_each(src[i], line_copy, dst) != 0))
        {
          btrace("concatenate item %zi", i);
          return 1;
        }
    }

  dst->finalised = true;
  dst->offset = 0;

  return 0;
}

static void chomp(char *text)
{
  char *c;

  if ((c = strchr(text, '\n')) != NULL)
    *c = '\0';
}

static bool string_is_whitespace(const char *string)
{
  for (const char *c = string ; *c != '\0' ; c++)
    if (! isspace(*c))
      return false;

  return true;
}

int comment_push(comment_t *comment, const char *text)
{
  int err = 1;
  comment_line_t *line;

  if ((line = malloc(sizeof(comment_line_t))) != NULL)
    {
      if (string_is_whitespace(text))
        {
          line->text = NULL;
          line->next = comment->line;
          comment->line = line;
          err = 0;
        }
      else
        {
          char *text_copy = NULL;

          if ((text_copy = strdup(text)) == NULL)
            free(line);
          else
            {
              chomp(text_copy);
              line->text = text_copy;
              line->next = comment->line;
              comment->line = line;
              err = 0;
            }
        }
    }

  return err;
}

static void comment_line_clear(comment_t *comment)
{
  line_destroy(comment->line);
  comment->line = NULL;
  comment->offset = 0;
}

static int line_offset(const char *text, void *arg)
{
  if (text == NULL)
    return 0;

  size_t n = 0, *nmax = arg;

  for (const char *c = text ; (*c != '\0') && isspace(*c) ; c++)
    n++;

  if (n < *nmax)
    *nmax = n;

  return 0;
}

static int comment_offset(comment_t *comment)
{
  size_t offset = SIZE_MAX;

  if (line_each(comment->line, 0, line_offset, &offset) != 0)
    {
      btrace("failed to get comment offset");
      return 1;
    }

  if (offset == SIZE_MAX)
    offset = 0;

  comment->offset = offset;

  return 0;
}

static int lines_header_trim(comment_line_t *line)
{
  if (line == NULL)
    return 0;

  if (lines_header_trim(line->next) == 0)
    {
      comment_line_t *line_next = line->next;

      if ((line_next != NULL) &&
          (line_next->next == NULL) &&
          (line_next->text == NULL))
        {
          free(line_next);
          line->next = NULL;
        }
    }

  return 0;
}

static int comment_header_trim(comment_t *comment)
{
  return lines_header_trim(comment->line);
}

static int comment_footer_trim(comment_t *comment)
{
  comment_line_t *line;

  while (((line = comment->line) != NULL) &&
         (line->text == NULL))
    {
      comment->line = line->next;
      free(line);
    }

  return 0;
}

int comment_finalise(comment_t *comment)
{
  if ((comment_offset(comment) == 0) &&
      (comment_header_trim(comment) == 0) &&
      (comment_footer_trim(comment) == 0))
    {
      comment->finalised = true;
      return 0;
    }
  else
    return 1;
}

bool comment_finalised(const comment_t *comment)
{
  return comment->finalised;
}

int comment_generate(comment_t *comment)
{
  size_t n = 256;
  char line[n];
  time_t t = time(NULL);

  if (snprintf(line, n, "cptutils %s, %s", VERSION, ctime(&t)) > n)
    {
      btrace("generated comment too large for buffer");
      return 1;
    }

  if (comment_push(comment, line) != 0)
    {
      btrace("failed comment push");
      return 1;
    }

  return 0;
}

/*
  this is the main comment-handling function, it will typically
  be called between read and write, and is passed the comment as
  determined by read (in which case it may write those comments to
  file).  Then (depending on options) we may replace the comments
  by generation or read from file, which the program's writer can
  the add to the output.  This modifies the first argument.
*/

int comment_process(comment_t *comment, const comment_opt_t *opt)
{
  if (opt->path.output != NULL)
    {
      if (comment_finalised(comment) == false)
        {
          btrace("comment not finalised");
          return 1;
        }

      if (comment_write(comment, opt->path.output) != 0)
        {
          btrace("failed comment write");
          return 1;
        }
    }

  if (opt->action != action_retain)
    {
      comment_line_clear(comment);

      if (opt->action == action_generate)
        {
          if (comment_generate(comment) != 0)
            {
              btrace("failed comment generate");
              return 1;
            }
        }
      else if (opt->action == action_read)
        {
          comment_t *c;
          int err = 1;

          if ((c = comment_read(opt->path.input)) != NULL)
            {
              err = comment_copy(c, comment);
              comment_destroy(c);
            }

          if (err != 0)
            {
              btrace("failed comment read from '%s'", opt->path.input);
              return 1;
            }
        }
    }

  return 0;
}
