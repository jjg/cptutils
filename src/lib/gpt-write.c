#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/gpt.h"
#include "cptutils/btrace.h"

#include <stdlib.h>
#include <stdio.h>

static int write_comment(const char *line, void *arg)
{
  FILE *st = arg;

  if (line)
    fprintf(st, "# %s\n", line);
  else
    fprintf(st, "#\n");

  return 0;
}

static int gpt_write_stream(const gpt_t *gpt, FILE *st)
{
  if (comment_each(gpt->comment, write_comment, st) != 0)
    {
      btrace("failed to write comment");
      return 1;
    }

  for (size_t i = 0 ; i < gpt->n ; i++)
    {
      gpt_stop_t stop = gpt->stop[i];

      fprintf(st, "%7.5f %7.5f %7.5f %7.5f\n",
	      stop.z,
	      stop.rgb[0],
	      stop.rgb[1],
	      stop.rgb[2]);
    }

  return 0;
}

int gpt_write(const gpt_t *gpt, const char *path)
{
  if (gpt->n < 2)
    {
      btrace("gnuplot does not support %i stops", gpt->n);
      return 1;
    }

  int err = 1;

  if (path != NULL)
    {
      FILE *st;

      if ((st = fopen(path, "w")) != NULL)
        {
          err = gpt_write_stream(gpt, st);
          fclose(st);
        }
      else
        btrace("failed to open %s", path);
    }
  else
    err = gpt_write_stream(gpt, stdout);

  return err;
}
