#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/tpm.h"
#include "cptutils/gstack.h"
#include "cptutils/btrace.h"

#include <stdlib.h>

struct tpm_t {
  gstack_t *stack;
  comment_t *comment;
};

tpm_t* tpm_new(void)
{
  tpm_t *tpm;

  if ((tpm = malloc(sizeof(tpm_t))) != NULL)
    {
      gstack_t *stack;

      if ((stack = gstack_new(sizeof(tpm_point_t), 64, 64)) != NULL)
        {
          comment_t *comment;

          if ((comment = comment_new()) != NULL)
            {
              tpm->stack = stack;
              tpm->comment = comment;
              return tpm;
            }
          comment_destroy(comment);
        }
      free(tpm);
    }

  return NULL;
}

void tpm_destroy(tpm_t *tpm)
{
  if (tpm != NULL)
    {
      gstack_t *stack = tpm->stack;

      if (stack != NULL)
        gstack_destroy(stack);

      comment_t *comment = tpm->comment;

      if (comment != NULL)
        comment_destroy(comment);

      free(tpm);
    }
}

comment_t* tpm_comment(const tpm_t *tpm)
{
  return tpm->comment;
}

int tpm_push(tpm_t *tpm, tpm_point_t *point)
{
  return gstack_push(tpm->stack, point);
}

int tpm_each_point(const tpm_t *tpm, int (*f)(void*, void*), void *ctx)
{
  return gstack_foreach(tpm->stack, f, ctx);
}

size_t tpm_npoint(const tpm_t *tpm)
{
  return gstack_size(tpm->stack);
}

static int valid_fraction(void *vpt, void *unused)
{
  const tpm_point_t *pt = vpt;
  double f = pt->fraction;

  return ((f < 0.0) || (f > 1.0)) ? -1 : 1;
}

static int tpm_valid_fraction(const tpm_t *tpm)
{
  if ((gstack_foreach(tpm->stack, valid_fraction, NULL)) != 0)
    {
      btrace("a map fraction is not in [0, 1]");
      return 1;
    }

  return 0;
}

static int valid_order(void *vpt, void *vlast)
{
  const tpm_point_t *pt = vpt;
  double f = pt->fraction, *last = vlast;
  int err = (f < *last) ? -1 : 1;
  *last = f;

  return err;
}

static int tpm_valid_order(const tpm_t *tpm)
{
  double last = 0.0;

  if ((gstack_foreach(tpm->stack, valid_order, &last)) != 0)
    {
      btrace("map fractions are not ordered");
      return 1;
    }

  return 0;
}

static int tpm_validate(const tpm_t *tpm)
{
  if (tpm_valid_fraction(tpm) != 0)
    return 1;

  if (tpm_valid_order(tpm) != 0)
    return 1;

  return 0;
}

/*
  manicuring: for SVG, discontinuities are achieved by
  coincidemt x-values with different colours, but for map
  (where each "fraction" has lead and trail colours)  we
  can replace these coincident points with a single one
  with differing lead and trail colours.
*/

typedef struct
{
  tpm_point_t *last;
  gstack_t *manicured;
} mp_context_t;

static int manicure(void *vthis, void *vctx)
{
  tpm_point_t *this = vthis;
  mp_context_t *ctx = vctx;

  if (ctx->last == NULL)
    {
      ctx->last = this;
      return 1;
    }

  double df = this->fraction - ctx->last->fraction;

  if (df > 0)
    {
      if (gstack_push(ctx->manicured, ctx->last) != 0)
        {
          btrace("failed manicured push");
          return -1;
        }
      ctx->last = this;
    }
  else if (df < 0)
    {
      btrace("unordered map");
      return -1;
    }
  else
    {
      ctx->last->lead = this->lead;
    }

  return 1;
}

static gstack_t* manicured_points(gstack_t *old)
{
  gstack_t *new = gstack_new(sizeof(tpm_point_t), gstack_size(old), 10);

  if (new == NULL) return NULL;

  mp_context_t context = { .last = NULL, .manicured = new };

  if (gstack_foreach(old, manicure, &context) != 0)
    {
      btrace("failed manicure");
      return NULL;
    }

  if (gstack_push(new, context.last) != 0)
    {
      btrace("failed final manicure");
      return NULL;
    }

  return new;
}

int tpm_manicure(tpm_t *tpm)
{
  if (tpm_validate(tpm) != 0)
    {
      btrace("invalid input");
      return 1;
    }

  gstack_t *stack_new = manicured_points(tpm->stack);

  if (stack_new == NULL)
    {
      btrace("failed to create manicured points");
      return 1;
    }

  gstack_destroy(tpm->stack);

  tpm->stack = stack_new;

  return 0;
}
