#ifndef QGS_H
#define QGS_H

#include <cptutils/colour.h>
#include <cptutils/comment.h>

#include <stdlib.h>

#define QGS_TYPE_UNSET 0
#define QGS_TYPE_DISCRETE 1
#define QGS_TYPE_INTERPOLATED 2

typedef struct {
  rgb_t rgb;
  unsigned char opacity;
  double value;
} qgs_entry_t;

typedef struct {
  size_t n;
  qgs_entry_t *entries;
  int type;
  char *name;
  comment_t *comment;
} qgs_t;

qgs_t* qgs_new(void);
void qgs_destroy(qgs_t*);

int qgs_init(qgs_t*);
void qgs_deinit(qgs_t*);

int qgs_set_name(qgs_t*, const char*);
int qgs_set_type(qgs_t*, int);
int qgs_alloc_entries(qgs_t*, size_t);
int qgs_set_entry(qgs_t*, size_t, qgs_entry_t*);

#endif
