#ifndef SVG_LIST_READ_H
#define SVG_LIST_READ_H

#include <cptutils/svg-list.h>

svg_list_t* svg_list_read(const char*);

#endif
