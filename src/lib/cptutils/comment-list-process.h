#ifndef COMMENT_LIST_PROCESS_H
#define COMMENT_LIST_PROCESS_H

#include <cptutils/comment.h>
#include <cptutils/comment-list.h>

int comment_list_process(comment_list_t*, const comment_opt_t*);

#endif
