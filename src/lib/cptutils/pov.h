#ifndef POV_H
#define POV_H

#include <stddef.h>
#include <cptutils/comment.h>

#define POV_NAME_LEN 256
#define POV_STOPS_MAX 256

typedef struct pov_stop_t
{
  double z;
  double rgbt[4];
} pov_stop_t;

typedef struct pov_t
{
  char name[POV_NAME_LEN];
  size_t n;
  pov_stop_t *stop;
  comment_t *comment;
} pov_t;

pov_t* pov_new(void);
void pov_destroy(pov_t*);

int pov_stops_alloc(pov_t*, size_t);
int pov_set_name(pov_t*, const char*, size_t*);

#endif
