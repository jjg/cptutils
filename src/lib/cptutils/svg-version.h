#ifndef SVG_VERSION_H
#define SVG_VERSION_H

typedef enum {
  svg_version_11,
  svg_version_20
} svg_version_t;

#endif
