#ifndef SVG_PREVIEW_H
#define SVG_PREVIEW_H

#include <stdlib.h>
#include <stdbool.h>

typedef struct
{
  bool use;
  size_t width, height;
} svg_preview_t;

int svg_preview_geometry(const char*, svg_preview_t*);

#endif
