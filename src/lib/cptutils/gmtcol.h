#ifndef GMTCOL_H
#define GMTCOL_H

struct gmtcol_t
{
  char *name;
  unsigned char r, g, b;
};

const struct gmtcol_t* gmtcol(const char*);

#endif
