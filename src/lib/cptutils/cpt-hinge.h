#ifndef CPT_HINGE_H
#define CPT_HINGE_H

typedef enum {
  hinge_explicit,
  hinge_hard,
  hinge_soft
} cpt_hinge_type_t;

#endif
