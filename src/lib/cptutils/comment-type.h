/*
  This header is private (in that it should only be used by the
  library, not outside of it).
*/

#ifndef COMMENT_TYPE_H
#define COMMENT_TYPE_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct comment_line_t comment_line_t;

struct comment_line_t
{
  char *text;
  comment_line_t *next;
};

struct comment_t
{
  bool finalised;
  size_t offset;
  comment_line_t *line;
};

#endif
