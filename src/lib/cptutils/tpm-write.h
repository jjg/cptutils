#ifndef TPM_WRITE_H
#define TPM_WRITE_H

#include <cptutils/tpm.h>

int tpm_write(const tpm_t*, const char*);

#endif
