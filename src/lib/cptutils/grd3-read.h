#ifndef GRD3_READ_H
#define GRD3_READ_H

#include <cptutils/grd3.h>

grd3_t* grd3_read(const char*);

#endif
