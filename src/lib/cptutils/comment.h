#ifndef COMMENT_H
#define COMMENT_H

#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>

typedef enum {
  action_none,
  action_retain,
  action_generate,
  action_read
} comment_action_t;

typedef struct
{
  comment_action_t action;
  struct {
    const char *input, *output;
  } path;
} comment_opt_t;

typedef struct comment_t comment_t;

comment_t* comment_new(void);
void comment_destroy(comment_t*);

int comment_init(comment_t*);
void comment_deinit(comment_t*);

size_t comment_count(const comment_t*);
int comment_push(comment_t*, const char*);
int comment_each(const comment_t*, int (*)(const char*, void*), void*);
int comment_copy(const comment_t*, comment_t*);
int comment_cat(const comment_t**, size_t, const char*, comment_t*);
int comment_finalise(comment_t*);
bool comment_finalised(const comment_t*);
int comment_generate(comment_t*);
int comment_process(comment_t*, const comment_opt_t*);

#endif
