/*
  gstack.h
  generic stack module
  J.J. Green
*/

#ifndef GSTACK_H
#define GSTACK_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include <stdbool.h>

typedef struct gstack_t gstack_t;

gstack_t* gstack_new(size_t, size_t, size_t);
void gstack_destroy(gstack_t*);
int gstack_push(gstack_t*, const void*);
int gstack_pop(gstack_t*, void*);
int gstack_reverse(gstack_t*);
int gstack_foreach(gstack_t*, int (*)(void*, void*), void*);
bool gstack_empty(const gstack_t*);
size_t gstack_size(const gstack_t*);

#ifdef __cplusplus
}
#endif

#endif
