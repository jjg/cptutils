#ifndef CPT_READ_H
#define CPT_READ_H

#include <cptutils/cpt.h>

cpt_t* cpt_read(const char*);

#endif
