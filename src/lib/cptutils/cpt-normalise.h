#ifndef CPT_NORMALISE_H
#define CPT_NORMALISE_H

#include <cptutils/cpt.h>

int cpt_normalise(cpt_t*, bool, double);
int cpt_denormalise(cpt_t*, bool, double);

#endif
