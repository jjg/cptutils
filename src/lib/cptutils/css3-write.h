#ifndef CSS3_WRITE_H
#define CSS3_WRITE_H

#include <cptutils/css3.h>

int css3_write(const css3_t*, const char*);

#endif
