#ifndef CPT_H
#define CPT_H

#include <cptutils/cpt-fill.h>
#include <cptutils/cpt-hinge.h>
#include <cptutils/comment.h>

#include <stdbool.h>

typedef struct
{
  double val;
  cpt_fill_t fill;
} cpt_sample_t;

typedef enum {
  annote_both,
  annote_lower,
  annote_upper,
  annote_none
} cpt_annote_t;

typedef struct cpt_seg_t
{
  struct cpt_seg_t *left, *right;
  struct {
    cpt_sample_t left, right;
  } sample;
  cpt_annote_t annote;
  char *label;
} cpt_seg_t;

typedef struct
{
  bool present;
  double min, max;
} cpt_range_t;

typedef struct
{
  bool present;
  cpt_hinge_type_t type;
  double value;
} cpt_hinge_t;

typedef struct
{
  char *name;
  model_t model, interpolate;
  cpt_fill_t fg, bg, nan;
  cpt_range_t range;
  cpt_hinge_t hinge;
  cpt_seg_t *segment;
  comment_t *comment;
} cpt_t;

cpt_t* cpt_new();
void cpt_destroy(cpt_t*);

int cpt_prepend(cpt_seg_t*, cpt_t*);
int cpt_append(cpt_seg_t*, cpt_t*);

cpt_seg_t* cpt_pop(cpt_t*);
cpt_seg_t* cpt_shift(cpt_t*);

double cpt_seg_z_mid(const cpt_seg_t*);

cpt_seg_t* cpt_seg_new(void);
void cpt_seg_destroy(cpt_seg_t*);

int cpt_nseg(const cpt_t*);
bool cpt_increasing(const cpt_t*);
bool cpt_annotated(const cpt_t*);

typedef enum {
  range_implicit,
  range_explicit,
  range_none
} cpt_range_type_t;

cpt_range_type_t cpt_range_type(const cpt_t*);
int cpt_range_implicit(const cpt_t*, double[static 2]);
int cpt_range_explicit(const cpt_t*, double[static 2]);


#endif
