#ifndef TPM_H
#define TPM_H

#include <cptutils/colour.h>
#include <cptutils/comment.h>

#include <stddef.h>

#define TPM_STOPS_MAX 50

typedef struct tpm_t tpm_t;

typedef struct
{
  double fraction;
  rgb_t lead, trail;
} tpm_point_t;

tpm_t* tpm_new(void);
void tpm_destroy(tpm_t*);
comment_t* tpm_comment(const tpm_t*);

int tpm_push(tpm_t*, tpm_point_t*);
int tpm_each_point(const tpm_t*, int (*)(void*, void*), void*);
int tpm_manicure(tpm_t*);
size_t tpm_npoint(const tpm_t*);

#endif
