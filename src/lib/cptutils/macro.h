#ifndef MACRO_H
#define MACRO_H

#define MIN(a, b) (((a) > (b)) ? (b) : (a))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define IFNULL(a, b) ((a) ? (a) : (b))
#define PLURAL(n) (((n) == 1) ? "" : "s")

#endif
