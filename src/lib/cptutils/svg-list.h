#ifndef SVG_LIST_H
#define SVG_LIST_H

#include <cptutils/svg.h>

#include <stddef.h>

typedef struct svg_list_t svg_list_t;

svg_list_t* svg_list_new(void);
void svg_list_destroy(svg_list_t*);
svg_t* svg_list_next(svg_list_t*);
int svg_list_revert(svg_list_t*);
size_t svg_list_size(const svg_list_t*);
const svg_t* svg_list_entry(const svg_list_t*, size_t);
int svg_list_each(svg_list_t*, int (*)(svg_t*, void*), void*);
svg_t* svg_list_find(svg_list_t*, int (*)(svg_t*, void*), void*);
int svg_list_coerce_ids(svg_list_t*);

#endif
