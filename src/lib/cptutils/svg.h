#ifndef SVG_H
#define SVG_H

#include <cptutils/colour.h>
#include <cptutils/comment.h>

typedef struct svg_stop_t
{
  double value;
  double opacity;
  rgb_t colour;
} svg_stop_t;

typedef struct svg_node_t
{
  struct svg_node_t *l, *r;
  svg_stop_t stop;
} svg_node_t;

#define SVG_NAME_LEN 128

typedef struct svg_t
{
  unsigned char name[SVG_NAME_LEN], id[SVG_NAME_LEN];
  comment_t *comment;
  svg_node_t *nodes;
} svg_t;

svg_t* svg_new();
void svg_destroy(svg_t*);
int svg_init(svg_t*);
void svg_deinit(svg_t*);

int svg_prepend(svg_stop_t, svg_t*);
int svg_append(svg_stop_t, svg_t*);

int svg_each_stop(const svg_t*, int (*)(svg_stop_t*, void*), void*);
int svg_interpolate(const svg_t*, double, rgb_t*, double*);
int svg_num_stops(const svg_t*);

char* svg_basename(const svg_t*);

int svg_explicit(svg_t*);
int svg_flatten(svg_t*, rgb_t);
int svg_complete(svg_t*);

#endif
