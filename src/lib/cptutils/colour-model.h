#ifndef COLOUR_MODEL_H
#define COLOUR_MODEL_H

typedef enum {
  model_rgb,
  model_hsv,
  model_cmyk
} model_t;

const char* model_name(model_t);

#endif
