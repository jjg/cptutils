#ifndef GGR_WRITE_H
#define GGR_WRITE_H

#include <cptutils/ggr.h>

int ggr_write(const ggr_t*, const char*);

#endif
