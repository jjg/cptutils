#ifndef CPT_COERCE_H
#define CPT_COERCE_H

#include <cptutils/cpt.h>

typedef enum {
  coerce_rgb,
  coerce_hsv,
  coerce_none
} cpt_coerce_model_t;

int cpt_coerce_model(cpt_t*, cpt_coerce_model_t);

#endif
