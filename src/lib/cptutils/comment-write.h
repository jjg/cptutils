#ifndef COMMENT_WRITE_H
#define COMMENT_WRITE_H

#include <cptutils/comment.h>

int comment_write(const comment_t*, const char*);

#endif
