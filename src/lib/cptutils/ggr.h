#ifndef GGR_H
#define GGR_H

#include <cptutils/colour.h>

#include <stdbool.h>

typedef enum {
  GGR_LINEAR = 0,
  GGR_CURVED,
  GGR_SINE,
  GGR_SPHERE_INCREASING,
  GGR_SPHERE_DECREASING
} ggr_type_t;

typedef enum {
  GGR_RGB = 0,
  GGR_HSV_CCW,
  GGR_HSV_CW
} ggr_colour_r;

typedef enum {
  GGR_FIXED = 0,
  GGR_FG,
  GGR_FG_TRANSP,
  GGR_BG,
  GGR_BG_TRANSP
} ggr_ect_t;

typedef struct ggr_segment_t
{
  double left, middle, right;
  double r0, g0, b0, a0;
  double r1, g1, b1, a1;
  ggr_type_t type;
  ggr_colour_r color;
  ggr_ect_t ect_left, ect_right;
  struct ggr_segment_t *prev, *next;
} ggr_segment_t;

typedef struct ggr_t
{
  char *name;
  char *filename;
  ggr_segment_t *segments;
  ggr_segment_t *last_visited;
} ggr_t;

ggr_segment_t* ggr_segment_new(void);

ggr_t* ggr_new(void);
bool ggr_valid(const ggr_t*);
void ggr_destroy(ggr_t*);

int ggr_colour(double, ggr_t*, double*, double*);
int ggr_segment_colour(double, const ggr_segment_t*, const double*, double*);
int ggr_segment_rgba(double, const ggr_segment_t*, double*, double*);

#endif
