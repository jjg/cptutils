#ifndef PNG_TYPE_H
#define PNG_TYPE_H

#include <stdlib.h>

typedef struct
{
  size_t width, height;
  unsigned char *row;
} png_t;

png_t* png_new(size_t, size_t);
void png_destroy(png_t*);

#endif
