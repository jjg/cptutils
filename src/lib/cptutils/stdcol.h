#ifndef STDCOL_H
#define STDCOL_H

struct stdcol_t
{
  char *name;
  int r, g, b;
  double t;
};

const struct stdcol_t* stdcol(char*);

#endif
