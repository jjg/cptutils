#ifndef QGS_LIST_WRITE_H
#define QGS_LIST_WRITE_H

#include <cptutils/qgs-list.h>

int qgs_list_write(const qgs_list_t*, const char*);

#endif
