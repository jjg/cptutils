#ifndef CPT_FILL_H
#define CPT_FILL_H

#include <cptutils/colour.h>
#include <stdbool.h>

typedef enum {
  cpt_fill_colour_rgb,
  cpt_fill_colour_hsv,
  cpt_fill_grey,
  cpt_fill_hatch,
  cpt_fill_file,
  cpt_fill_empty
} cpt_fill_type_t;

typedef struct
{
  int sign;
  int n;
  int dpi;
} hatch_t;

typedef struct
{
  cpt_fill_type_t type;
  union
  {
    colour_t colour;
    int grey;
    hatch_t hatch;
    char *file;
  };
} cpt_fill_t;

bool cpt_fill_eq(cpt_fill_t, cpt_fill_t);
int cpt_fill_interpolate(model_t, double, cpt_fill_t, cpt_fill_t, cpt_fill_t*);

#endif
