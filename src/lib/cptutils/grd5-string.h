#ifndef GRD5_STRING_H
#define GRD5_STRING_H

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
  uint32_t len;
  char *content;
} grd5_string_t;

void grd5_string_destroy(grd5_string_t*);
bool grd5_string_matches(const grd5_string_t*, const char*);

#endif
