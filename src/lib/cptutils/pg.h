#ifndef PG_H
#define PG_H

#include <cptutils/colour.h>

#include <stdbool.h>

typedef struct pg_t pg_t;

pg_t* pg_new(void);
void pg_destroy(pg_t*);
pg_t* pg_clone(const pg_t*);
void pg_set_percentage(pg_t*, bool);
int pg_push(pg_t*, double, rgb_t, unsigned char);

#endif
