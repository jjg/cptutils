#ifndef GPT_H
#define GPT_H

#include <cptutils/comment.h>
#include <stddef.h>

typedef struct
{
  double z;
  double rgb[3];
} gpt_stop_t;

typedef struct
{
  size_t n;
  gpt_stop_t *stop;
  comment_t *comment;
} gpt_t;

gpt_t* gpt_new(void);
void gpt_destroy(gpt_t*);

int gpt_stops_alloc(gpt_t*, size_t);

#endif
