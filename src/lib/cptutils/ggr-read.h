#ifndef GGR_READ_H
#define GGR_READ_H

#include <cptutils/ggr.h>

ggr_t* ggr_read(const char*);

#endif
