#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/colour.h"
#include "cptutils/cpt.h"
#include "cptutils/btrace.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int cpt_nseg(const cpt_t *cpt)
{
  int n = 0;

  for (cpt_seg_t *seg = cpt->segment ; seg ; seg = seg->right)
    n++;

  return n;
}

cpt_range_type_t cpt_range_type(const cpt_t *cpt)
{
  if (cpt->range.present)
    return range_explicit;
  else if (cpt->segment)
    return range_implicit;
  else
    return range_none;
}

int cpt_range_implicit(const cpt_t *cpt, double z[static 2])
{
  cpt_seg_t *seg;

  if ((seg = cpt->segment) != NULL)
    {
      z[0] = seg->sample.left.val;

      while (seg->right)
        seg = seg->right;

      z[1] = seg->sample.right.val;

      return 0;
    }

  return 1;
}

int cpt_range_explicit(const cpt_t *cpt, double z[static 2])
{
  if (cpt->range.present)
    {
      z[0] = cpt->range.min;
      z[1] = cpt->range.max;
      return 0;
    }
  else
    return 1;
}

bool cpt_increasing(const cpt_t *cpt)
{
  double z[2] = {0};

  cpt_range_implicit(cpt, z);

  return z[0] < z[1];
}

bool cpt_annotated(const cpt_t *cpt)
{
  for (cpt_seg_t *seg = cpt->segment ; seg ; seg = seg->right)
    if (seg->annote != annote_none)
      return true;

  return false;
}

cpt_t* cpt_new(void)
{
  cpt_t *cpt;

  if ((cpt = malloc(sizeof(cpt_t))) != NULL)
    {
      comment_t *comment;

      if ((comment = comment_new()) != NULL)
        {
          cpt->name = NULL;
          cpt->segment = NULL;
          cpt->model = model_rgb;
          cpt->interpolate = model_rgb;
          cpt->fg.type = cpt_fill_empty;
          cpt->bg.type = cpt_fill_empty;
          cpt->nan.type = cpt_fill_empty;
          cpt->range.present = false;
          cpt->hinge.present = false;
          cpt->comment = comment;
        }
      else
        {
          free(cpt);
          cpt = NULL;
        }
    }

  return cpt;
}

/* add a segment to the start (leftmost) */

int cpt_prepend(cpt_seg_t *seg, cpt_t *cpt)
{
  cpt_seg_t *s = cpt->segment;

  seg->left = NULL;
  seg->right = s;

  if (s != NULL)
    s->left = seg;

  cpt->segment = seg;

  return 0;
}

/* add a segment to the end (rightmost) */

int cpt_append(cpt_seg_t *seg, cpt_t *cpt)
{
  cpt_seg_t *s = cpt->segment;

  if (s)
    {
      while (s->right != NULL)
        s = s->right;

      s->right = seg;
      seg->left = s;
      seg->right = NULL;

      return 0;
    }
  else
    return cpt_prepend(seg, cpt);
}

/* remove first segment (and return it) */

cpt_seg_t* cpt_pop(cpt_t *cpt)
{
  cpt_seg_t *s1 = cpt->segment;

  if (s1)
    {
      cpt_seg_t *s2 = s1->right;

      if (s2) s2->left = NULL;

      cpt->segment = s2;

      s1->right = NULL;
      s1->left = NULL;

      return s1;
    }

  return NULL;
}

/* remove last segment (and return it) */

cpt_seg_t* cpt_shift(cpt_t *cpt)
{
  cpt_seg_t *s1;

  s1 = cpt->segment;

  /* no segment case */

  if (!s1) return NULL;

  /* single segment case */

  if (! s1->right)
    {
      cpt->segment = NULL;

      s1->left = NULL;
      s1->right = NULL;

      return s1;
    }

  /* multi segment case */

  while (s1->right->right) s1 = s1->right;

  cpt_seg_t *s2 = s1->right;

  s1->right = NULL;

  s2->left = NULL;
  s2->right = NULL;

  return s2;
}

void cpt_destroy(cpt_t *cpt)
{
  if (cpt)
    {
      free(cpt->name);
      comment_destroy(cpt->comment);

      cpt_seg_t *seg = cpt->segment;

      while (seg)
        {
	  cpt_seg_t *next = seg->right;
          cpt_seg_destroy(seg);
          seg = next;
	}
    }

  free(cpt);
}

double cpt_seg_z_mid(const cpt_seg_t *seg)
{
  return (seg->sample.left.val + seg->sample.right.val) / 2;
}

cpt_seg_t* cpt_seg_new(void)
{
  cpt_seg_t *seg;

  if ((seg = malloc(sizeof(cpt_seg_t))) != NULL)
    {
      seg->left = NULL;
      seg->right = NULL;
      seg->annote = annote_none;
      seg->label = NULL;
    }

  return seg;
}

void cpt_seg_destroy(cpt_seg_t *seg)
{
  if (seg) free(seg->label);
  free(seg);
}
