#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/cpt-fill.h"
#include "cptutils/btrace.h"

#include <stdio.h>
#include <assert.h>

bool cpt_fill_eq(cpt_fill_t a, cpt_fill_t b)
{
  if (a.type != b.type)
    return false;

  switch (a.type)
    {
    case cpt_fill_empty:
      return true;
    case cpt_fill_grey:
      return (a.grey == b.grey);
    case cpt_fill_hatch:
      return ((a.hatch.sign == b.hatch.sign) &&
	      (a.hatch.dpi == b.hatch.dpi) &&
	      (a.hatch.n == b.hatch.n));
    case cpt_fill_file:
      /* FIXME */
      return false;
    case cpt_fill_colour_rgb:
      return rgb_dist(a.colour.rgb, b.colour.rgb) < 1e-8;
    case cpt_fill_colour_hsv:
      return hsv_dist(a.colour.hsv, b.colour.hsv) < 1e-8;
    default:
      assert(false);
    }
}

int cpt_fill_interpolate(model_t model, double z,
                         cpt_fill_t a, cpt_fill_t b, cpt_fill_t *f)
{
  if (a.type != b.type)
    return 1;

  cpt_fill_type_t type = a.type;

  f->type = type;

  switch (type)
    {
    case cpt_fill_empty :
      break;

    case cpt_fill_grey :
      f->grey = (a.grey*(1-z) + b.grey*z);
      break;

    case cpt_fill_hatch :
    case cpt_fill_file :
      *f = a;
      break;

    case cpt_fill_colour_rgb:
      if (rgb_interpolate(model, z, a.colour.rgb, b.colour.rgb,
                          &(f->colour.rgb)) != 0)
	{
	  btrace("failed to interpolate RGB");
	  return 1;
	}
      break;

    case cpt_fill_colour_hsv:
      if (hsv_interpolate(model, z, a.colour.hsv, b.colour.hsv,
                          &(f->colour.hsv)) != 0)
	{
	  btrace("failed to interpolate HSV");
	  return 1;
	}
      break;
    }

  return 0;
}
