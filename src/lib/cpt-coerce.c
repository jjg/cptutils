#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/cpt-coerce.h"
#include "cptutils/btrace.h"

static int fill_coerce_rgb(cpt_fill_t *fill)
{
  if (fill->type == cpt_fill_colour_hsv)
    {
      if (hsv_to_rgb(fill->colour.hsv, &(fill->colour.rgb)) != 0)
        return 1;
      fill->type = cpt_fill_colour_rgb;
    }

  return 0;
}

static int fill_coerce_hsv(cpt_fill_t *fill)
{
  if (fill->type == cpt_fill_colour_rgb)
    {
      if (rgb_to_hsv(fill->colour.rgb, &(fill->colour.hsv)) != 0)
        return 1;
      fill->type = cpt_fill_colour_hsv;
    }

  return 0;
}

int cpt_coerce_model(cpt_t *cpt, cpt_coerce_model_t model)
{
  if (model == coerce_none)
    return 0;

  if (model == coerce_rgb)
    {
      cpt_seg_t *seg;

      for (seg = cpt->segment ; seg ; seg = seg->right)
        if ((fill_coerce_rgb(&(seg->sample.left.fill)) != 0) ||
            (fill_coerce_rgb(&(seg->sample.right.fill)) != 0))
          return 1;

      cpt->model = model_rgb;
    }
  else if (model == coerce_hsv)
    {
      cpt_seg_t *seg;

      for (seg = cpt->segment ; seg ; seg = seg->right)
        if ((fill_coerce_hsv(&(seg->sample.left.fill)) != 0) ||
            (fill_coerce_hsv(&(seg->sample.right.fill)) != 0))
          return 1;

      cpt->model = model_hsv;
    }

  return 0;
}
