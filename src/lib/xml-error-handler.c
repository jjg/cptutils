#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptutils/xml-error-handler.h"
#include "cptutils/btrace.h"

#include <string.h>
#include <ctype.h>

/*
  pass libxml error messages to btrace

  Note that version 2.12.0 of libxml2 makes the second argument
  of this function const, good idea but a breaking change if you
  have -Wincompatible-pointer-types (which RH do).
*/

void xml_error_handler(void *user_data,
#if LIBXML_VERSION < 21200
                       xmlError *error
#else
                       const xmlError *error
#endif
                       )
{
  size_t n = strlen(error->message);

  if (n > 0)
    {
      char copy[n + 1], *end;
      end = (char*)memcpy(copy, error->message, n + 1) + n - 1;
      while (end > copy && isspace(*end)) end--;
      *(end + 1) = '\0';
      btrace("libxml2: %s", copy);
    }
  else
    btrace("libxml2: empty error message");
}
