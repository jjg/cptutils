#ifndef CPTSVG_H
#define CPTSVG_H

#include <cptutils/svg-preview.h>
#include <cptutils/svg-version.h>
#include <cptutils/comment.h>

#include <stdbool.h>

typedef struct
{
  bool verbose;
  svg_preview_t preview;
  svg_version_t svg_version;
  comment_opt_t comment;
  struct
  {
    bool active;
  } hinge;
  struct
  {
    const char *path;
  } input, output;
} cptsvg_opt_t;

int cptsvg(const cptsvg_opt_t*);

#endif
