#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "cptsvg.h"

#include <cptutils/cpt-read.h>
#include <cptutils/cpt-normalise.h>
#include <cptutils/svg-write.h>
#include <cptutils/btrace.h>
#include <cptutils/macro.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static int cptsvg_convert(const cpt_t*, svg_t*, const cptsvg_opt_t*);

int cptsvg(const cptsvg_opt_t *opt)
{
  cpt_t *cpt;
  int err = 1;

  if ((cpt = cpt_read(opt->input.path)) != NULL)
    {
      if (cpt_denormalise(cpt, opt->hinge.active, 0) == 0)
        {
          if (comment_process(cpt->comment, &(opt->comment)) == 0)
            {
              svg_t *svg;

              if ((svg = svg_new()) != NULL)
                {
                  if (cptsvg_convert(cpt, svg, opt) == 0)
                    {
                      const char *path = opt->output.path;
                      const svg_preview_t *preview = &(opt->preview);
                      svg_version_t version = opt->svg_version;

                      if (svg_write(svg, path, preview, version) == 0)
                        err = 0;
                      else
                        btrace("error writing svg to %s", path);
                    }
                  else
                    btrace("failed to convert cpt to svg");

                  svg_destroy(svg);
                }
              else
                btrace("failed to allocate svg");
            }
          else
            btrace("failed to process comments");
        }
      else
        btrace("failed denormalise of cpt input");

      cpt_destroy(cpt);
    }
  else
    btrace("failed to load cpt from %s",
           IFNULL(opt->input.path, "<stdin>"));

  if (err)
    btrace("failed to write svg to %s",
           IFNULL(opt->output.path, "<stdout>"));

  return err;
}

static int cptsvg_convert(const cpt_t *cpt,
                          svg_t *svg,
                          const cptsvg_opt_t *opt)
{
  cpt_seg_t *seg;
  svg_stop_t lstop, rstop;

  if (cpt->segment == NULL)
    {
      btrace("cpt has no segments");
      return 1;
    }

  if (snprintf((char*)svg->name,
               SVG_NAME_LEN,
               "%s",
               cpt->name ? cpt->name : "stdin") >= SVG_NAME_LEN)
    btrace("truncated svg name!");

  if (comment_copy(cpt->comment, svg->comment) != 0)
    {
      btrace("failed to copy comments");
      return 1;
    }

  /* FIXME - cpt can be increasing or decreasing */

  double
    min = cpt->segment->sample.left.val,
    max = cpt->segment->sample.right.val;

  for (seg = cpt->segment ; seg ; seg = seg->right)
    max = seg->sample.right.val;

  switch (cpt->interpolate)
    {
    case model_rgb:
      break;
    case model_hsv:
    case model_cmyk:
      btrace("conversion of %s interpolation not implemented",
	     model_name(cpt->interpolate));
      return 1;
    default:
      btrace("unknown colour model");
      return 1;
    }

  int n, m = 0;

  for (n = 0, seg = cpt->segment ; seg ; seg = seg->right)
    {
      cpt_sample_t
        lsmp = seg->sample.left,
        rsmp = seg->sample.right;

      if (lsmp.fill.type != rsmp.fill.type)
        {
          btrace("sorry, can't convert mixed fill types");
          return 1;
        }

      cpt_fill_type_t type = lsmp.fill.type;
      rgb_t rcol, lcol;

      switch (type)
        {
        case cpt_fill_colour_rgb:
          lcol = lsmp.fill.colour.rgb;
          rcol = rsmp.fill.colour.rgb;
          break;

        case cpt_fill_colour_hsv:
          if (hsv_to_rgb(lsmp.fill.colour.hsv, &lcol) != 0 ||
              hsv_to_rgb(rsmp.fill.colour.hsv, &rcol) != 0)
            {
              btrace("failed conversion of HSV to RGB");
              return 1;
            }
          break;

        case cpt_fill_grey:
        case cpt_fill_hatch:
        case cpt_fill_file:
        case cpt_fill_empty:
	  /* FIXME */
	  btrace("fill type not implemented yet");
          return 1;

        default:
          btrace("strange fill type");
          return 1;
        }

      /* always insert the left colour */

      lstop.value = 100 * (lsmp.val - min) / (max - min);
      lstop.colour = lcol;
      lstop.opacity = 1;

      if (svg_append(lstop, svg) == 0)
        m++;
      else
	{
	  btrace("error adding stop for segment %i left", n);
	  return 1;
	}

      /*
	if there is a segment to the right, and if its left
	segment is the same colour at the our right segment
	then don't insert it, Otherwise do.
      */

      if ( ! ((seg->right) &&
	      cpt_fill_eq(rsmp.fill,
                          seg->right->sample.left.fill)))
	{
	  rstop.value = 100 * (rsmp.val - min) / (max - min);
	  rstop.colour = rcol;
	  rstop.opacity = 1.0;

	  if (svg_append(rstop, svg) == 0) m++;
	  else
	    {
	      btrace("error adding stop for segment %i right", n);
	      return 1;
	    }
	}

      n++;
    }

  if (svg_complete(svg) != 0)
    {
      btrace("failed SVG completion");
      return 1;
    }

  if (opt->verbose)
    printf("converted %i segments to %i stops", n, m);

  return 0;
}
