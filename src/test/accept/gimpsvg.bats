#!/usr/bin/env bats

load 'shared'
load 'assert-svg'

setup()
{
    program='gimpsvg'
    path="${src_dir}/${program}/${program}"
}

@test 'gimpsvg, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'gimpsvg, --help' {
    run "${path}" --help
}

@test 'gimpsvg, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'gimpsvg, no comment source' {
    base="Sunrise"
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'gimpsvg, --comments-generate' {
    base="Sunrise"
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-generate \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'gimpsvg, --comments-read (present)' {
    base="Sunrise"
    comment="${fixture_dir}/xco/realistic.xco"
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'gimpsvg, --comments-read (absent)' {
    base="Sunrise"
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'gimpsvg, multiple comment sources' {
    base="Sunrise"
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    echo 'some comment' > "${comment}"
    run "${path}" --comments-read "${comment}" --comments-generate \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'gimpsvg, Sunrise (with preview)' {
    base='Sunrise'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --preview -o "${svg}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal "${svg}" "gimpsvg/${base}.svg"
}

@test 'gimpsvg, mars --svg2' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --svg2 -o "${svg}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal "${svg}" "gimpsvg/${base}.svg"

}

@test 'gimpsvg, Rounded_edge' {
    base='Rounded_edge'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal "${svg}" "gimpsvg/${base}.svg"
}

@test 'gimpsvg, Wood_2' {
    base='Wood_2'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal "${svg}" "gimpsvg/${base}.svg"
}

@test 'gimpsvg, AFL 2023/10/25' {
    base='afl-2023-10-25-00'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'gimpsvg, bad --samples' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --samples 1 -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'gimpsvg, bad --geometry' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --geometry x -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'gimpsvg, too many input files' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${ggr}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'gimpsvg, input from stdin' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" < "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'gimpsvg, output to stdout' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    run "${path}" "${ggr}"
    [ "${status}" -eq '0' ]
}

@test 'gimpsvg, output to stdout, --verbose' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    run "${path}" --verbose "${ggr}"
    [ "${status}" -ne '0' ]
}

@test 'gimpsvg, backtrace, default format' {
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'gimpsvg, backtrace, plain format' {
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'gimpsvg, backtrace, XML format' {
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'gimpsvg, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'gimpsvg, backtrace, unknown format' {
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${svg}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ ! -e "${backtrace}" ]
}
