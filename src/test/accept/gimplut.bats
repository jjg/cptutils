#!/usr/bin/env bats

load 'shared'
load 'assert-lut'

setup()
{
    program='gimplut'
    path="${src_dir}/${program}/${program}"
}

@test 'gimplut, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'gimplut, --help' {
    run "${path}" --help
}

@test 'gimplut, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'gimplut, Sunrise' {
    base='Sunrise'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    run "${path}" -v -o "${lut}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${lut}" ]
}

@test 'gimplut, mars' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    run "${path}" -v -o "${lut}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${lut}" ]
    assert_lut_equal "${lut}" "gimplut/${base}.lut"
}

@test 'gimplut, Rounded_edge' {
    base='Rounded_edge'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    run "${path}" -v -o "${lut}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${lut}" ]
    assert_lut_equal "${lut}" "gimplut/${base}.lut"
}

@test 'gimplut, Wood_2' {
    base='Wood_2'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    run "${path}" -v -o "${lut}" "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${lut}" ]
    assert_lut_equal "${lut}" "gimplut/${base}.lut"
}

@test 'gimplut, AFL 2023/10/25' {
    base='afl-2023-10-25-00'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    run "${path}" -v -o "${lut}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${lut}" ]
}

@test 'gimplut, bad --samples' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    run "${path}" -v --samples 0 -o "${lut}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${lut}" ]
}

@test 'gimplut, too many input files' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    run "${path}" -v -o "${lut}" "${ggr}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${lut}" ]
}

@test 'gimplut, input from stdin' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    run "${path}" -v -o "${lut}" < "${ggr}"
    [ "${status}" -eq '0' ]
    [ -e "${lut}" ]
}

@test 'gimplut, output to stdout' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    run "${path}" "${ggr}"
    [ "${status}" -eq '0' ]
}

@test 'gimplut, output to stdout, --verbose' {
    base='mars'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    run "${path}" --verbose "${ggr}"
    [ "${status}" -ne '0' ]
}

@test 'gimplut, backtrace, default format' {
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${lut}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${lut}" ]
    [ -e "${backtrace}" ]
}

@test 'gimplut, backtrace, plain format' {
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${lut}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${lut}" ]
    [ -e "${backtrace}" ]
}

@test 'gimplut, backtrace, XML format' {
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${lut}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${lut}" ]
    [ -e "${backtrace}" ]
}

@test 'gimplut, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${lut}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${lut}" ]
    [ -e "${backtrace}" ]
}

@test 'gimplut, backtrace, unknown format' {
    base='no-such-file'
    ggr="${fixture_dir}/ggr/${base}.ggr"
    lut="${BATS_TEST_TMPDIR}/${base}.lut"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${lut}" "${ggr}"
    [ "${status}" -ne '0' ]
    [ ! -e "${lut}" ]
    [ ! -e "${backtrace}" ]
}
