#!/usr/bin/env bats

load 'shared'
load 'assert-cpt'
load 'assert-comments-valid'

setup()
{
    program='cptcont'
    path="${src_dir}/${program}/${program}"
}

@test 'cptcont, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'cptcont, --help' {
    run "${path}" --help
}

@test 'cptcont, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'cptcont, too many input files' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${fixture_dir}/cpt/${base}.cpt"
    cpt3="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt3}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt3}" ]
}

@test 'cptcont, input from stdin' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" < "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
}

@test 'cptcont, no comments source specified' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '0' ]
}

@test 'cptcont, --comments-generate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-generate \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '1' ]
}

@test 'cptcont, --comments-retain' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '7' ]
}

@test 'cptcont, --comments-read (present)' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${fixture_dir}/xco/realistic.xco"
    run "${path}" --comments-read "${comment}" \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '3' ]
}

@test 'cptcont, --comments-read (absent)' {
    base='RdBu_10'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, --comments-retain --comments-generate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain --comments-generate \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, --comments-write' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'cptcont, --gmt4' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --gmt4 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptcont/version-gmt4.cpt'
}

@test 'cptcont, --gmt5' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --gmt5 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptcont/version-gmt5.cpt'
}

@test 'cptcont, --gmt6' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --gmt6 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cptcont/version-gmt6.cpt'
}

@test 'cptcont, --gmt4 --gmt5' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --gmt4 --gmt5 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, GMT5 hinged fixtures' {
    for base in 'GMT5-etopo1' 'GMT5-earth'
    do
        cpt1="${fixture_dir}/cpt/${base}.cpt"
        cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
        run "${path}" -v -5 -o "${cpt2}" "${cpt1}"
        [ "${status}" -eq '0' ]
        [ -e "${cpt2}" ]
        assert_cpt_equal "${cpt2}" "cptcont/hinged-${base}.cpt"
    done
}

@test 'cptcont, GMT6 hinged fixtures' {
    for base in 'GMT6-globe' 'GMT6-mag' 'GMT6-split'
    do
        cpt1="${fixture_dir}/cpt/${base}.cpt"
        cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
        run "${path}" -v -6 -o "${cpt2}" "${cpt1}"
        [ "${status}" -eq '0' ]
        [ -e "${cpt2}" ]
        assert_cpt_equal "${cpt2}" "cptcont/hinged-${base}.cpt"
    done
}

@test 'cptcont, continuous' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/continuous.cpt"
}

@test 'cptcont, almost-continuous' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --partial 60 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/almost-continuous.cpt"
}

@test 'cptcont, midpoint' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --midpoint "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/midpoint.cpt"
}

@test 'cptcont, mixed-model' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, mixed-model, --model rgb' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --model rgb -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/mixed-model-rgb.cpt"
}

@test 'cptcont, mixed-model, --model hsv' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --model hsv -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/mixed-model-hsv.cpt"
}

@test 'cptcont, mixed-model, --model nonesuch' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --model nonesuch -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, normalise (to gmt4)' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --z-normalise --gmt4 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, normalise (to gmt5)' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --z-normalise --gmt5 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/normal-gmt5.cpt"
}

@test 'cptcont, normalise (from hard-hinged)' {
    base='hinge-hard-denorm'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -z -6 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/normal-hard-hinge-default.cpt"
}

@test 'cptcont, normalise (from hard-hinged) --hinge valid)' {
    base='hinge-hard-denorm'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -z -6 -o "${cpt2}" --hinge=-50 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/normal-hard-hinge-valid.cpt"
}

@test 'cptcont, normalise (from hard-hinged) --hinge invalid)' {
    base='hinge-hard-denorm'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -z -5 -o "${cpt2}" --hinge=-20 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, normalise (from explict-hinged, zero)' {
    base='hinge-explicit-zero'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -z -5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/normal-explicit-hinge-zero.cpt"
}

@test 'cptcont, normalise (from explict-hinged, nonzero)' {
    base='hinge-explicit-nonzero'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -z -5 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, denormalise (from normalised)' {
    base='GMT5-bathy'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --z-denormalise "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/denormal-from-normal.cpt"
}

@test 'cptcont, denormalise (from non-normalised)' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -Z -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/denormal-from-nonnormal.cpt"
}

@test 'cptcont, denormalise (from hard-hinged)' {
    base='hinge-hard-norm'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -Z -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/denormal-from-hard-hinge-default.cpt"
}

@test 'cptcont, denormalise (from hard-hinged) --hinge valid' {
    base='hinge-hard-norm'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -Z -o "${cpt2}" --hinge 1 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/denormal-from-hard-hinge-valid.cpt"
}

@test 'cptcont, denormalise (from hard-hinged) --hinge invalid' {
    base='hinge-hard-norm'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -Z -o "${cpt2}" --hinge 51 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, denormalise (from explicit-hinged, zero)' {
    base='hinge-explicit-zero'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -5 -Z -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cptcont/denormal-from-explicit-hinge.cpt"
}

@test 'cptcont, denormalise (from explicit-hinged, nonzero)' {
    base='hinge-explicit-nonzero'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -5 -Z -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, normalise and denormalise' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" --z-normalise --z-denormalise "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cptcont, output to stdout' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" "${cpt}"
    [ "${status}" -eq '0' ]
}

@test 'cptcont, output to stdout, --verbose' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" --verbose "${cpt}"
    [ "${status}" -ne '0' ]
}

@test 'cptcont, backtrace, default format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptcont, backtrace, plain format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptcont, backtrace, XML format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptcont, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cptcont, backtrace, unknown format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ ! -e "${backtrace}" ]
}
