#!/usr/bin/env bats

load 'shared'
load 'assert-csv'

setup()
{
    program='cptinfo'
    path="${src_dir}/${program}/${program}"
}

@test 'cptinfo, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'cptinfo, --help' {
    run "${path}" --help
}

@test 'cptinfo, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'cptinfo, too many input files' {
    base='GMT4-gebco'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    info="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" -v -o "${info}" --plain "${cpt}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${info}" ]
}

@test 'cptinfo, input from stdin' {
    base='GMT4-gebco'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    info="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" -v -o "${info}" < "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${info}" ]
}

@test 'cptinfo, no such file' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    info="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" -v -o "${info}" --plain "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${info}" ]
}

@test 'cptinfo, plain' {
    base='GMT4-gebco'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    info="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" -v -o "${info}" --plain "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${info}" ]
}

@test 'cptinfo (space), csv' {
    base='pakistan'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    csv="${BATS_TEST_TMPDIR}/${base}.csv"
    run "${path}" -v -o "${csv}" --csv "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${csv}" ]
}

@test 'cptinfo, GMT5-RGB csv' {
    base='pakistan-solidus'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    csv="${BATS_TEST_TMPDIR}/${base}.csv"
    run "${path}" -v -o "${csv}" --csv "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${csv}" ]
}

@test 'cptinfo, GMT5-HSV csv' {
    base='GMT5-cyclic'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    csv="${BATS_TEST_TMPDIR}/${base}.csv"
    run "${path}" -v -o "${csv}" --csv "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${csv}" ]
    assert_csv_equal "${csv}" "cptinfo/${base}.csv"
}

@test 'cptinfo, GMT5-bathy csv' {
    base='GMT5-bathy'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    csv="${BATS_TEST_TMPDIR}/${base}.csv"
    run "${path}" -v -o "${csv}" --csv "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${csv}" ]
    assert_csv_equal "${csv}" "cptinfo/${base}.csv"
}

@test 'cptinfo, output to stdout' {
    base='GMT4-gebco'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" "${cpt}"
    [ "${status}" -eq '0' ]
}

@test 'cptinfo, output to stdout, --verbose' {
    base='GMT4-gebco'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" --verbose "${cpt}"
    [ "${status}" -ne '0' ]
}
