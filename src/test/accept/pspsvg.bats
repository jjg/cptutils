#!/usr/bin/env bats

load 'shared'
load 'assert-svg'

setup()
{
    program='pspsvg'
    path="${src_dir}/${program}/${program}"
}

@test 'pspsvg, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'pspsvg, --help' {
    run "${path}" --help
}

@test 'pspsvg, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'pspsvg, no comment source' {
    base="accented"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${jgd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pspsvg, --comments-generate' {
    base="accented"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-generate \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pspsvg, --comments-read (present)' {
    base="accented"
    comment="${fixture_dir}/xco/realistic.xco"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pspsvg, --comments-read (absent)' {
    base="accented"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pspsvg, multiple comment sources' {
    base="accented"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    echo 'some comment' > "${comment}"
    run "${path}" --comments-read "${comment}" --comments-generate \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

testcase() {
    local -r n="$1"
    base="test.${n}"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -p -o "${svg}" "${jgd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal "${svg}" "pspsvg/${base}.svg"
}

@test 'pspsvg, test.1' {
    testcase '1'
}

@test 'pspsvg, test.2' {
    testcase '2'
}

@test 'pspsvg, test.3' {
    testcase '3'
}

@test 'pspsvg, test.4' {
    testcase '4'
}

@test 'pspsvg, test.5' {
    testcase '5'
}

@test 'pspsvg, test.6' {
    testcase '6'
}

@test 'pspsvg, --svg2' {
    base="test.1"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --svg2 -v -o "${svg}" "${jgd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pspsvg, bad --geometry' {
    base="test.1"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --geometry x -v -o "${svg}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pspsvg, too many input files' {
    base="test.1"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${jgd}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pspsvg, input from stdin' {
    base='test.1'
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -o "${svg}" < "${jgd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pspsvg, output to stdout' {
    base="test.1"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    run "${path}" "${jgd}"
    [ "${status}" -eq '0' ]
}

@test 'pspsvg, output to stdout, --verbose' {
    base="test.1"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    run "${path}" --verbose "${jgd}"
    [ "${status}" -ne '0' ]
}

@test 'pspsvg, backtrace, default format' {
    base="no-such-file"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'pspsvg, backtrace, plain format' {
    base="no-such-file"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'pspsvg, backtrace, XML format' {
    base="no-such-file"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'pspsvg, backtrace, JSON format' {
    skip_unless_with_json
    base="no-such-file"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'pspsvg, backtrace, unknown format' {
    base="no-such-file"
    jgd="${fixture_dir}/grd3/${base}.jgd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${svg}" "${jgd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ ! -e "${backtrace}" ]
}
