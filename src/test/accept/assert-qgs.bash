#!/usr/bin/env bash
# autogenerated: do not edit

assert_qgs_equal() {
    if [ $# -ne 2 ] ; then
        printf 'expected 2 arguments, got %i' $#
        return 1
    fi
    local actual=$1
    local expected=$2
    if [ ! -e $expected ] ; then
        cp $actual $expected
    fi
    run equal-qgs $actual $expected
    return $status
}
