#!/usr/bin/env bats

load 'shared'
load 'assert-cpt'
load 'assert-comments-valid'

setup()
{
    program='cpthsv'
    path="${src_dir}/${program}/${program}"
}

@test 'cpthsv, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'cpthsv, --help' {
    run "${path}" --help
}

@test 'cpthsv, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'cpthsv, too many input files' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s90 "${cpt1}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, input from stdin' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s90 < "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
}

@test 'cpthsv, no comments source specified' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" \
        -T s0 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '0' ]
}

@test 'cpthsv, --comments-generate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-generate \
        -T s0 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '1' ]
}

@test 'cpthsv, --comments-retain' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain \
        -T s0 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '7' ]
}

@test 'cpthsv, --comments-read (present)' {
    base='RdBu_10'
    comment="${fixture_dir}/xco/realistic.xco"
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -T s0 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ $(grep \# "${cpt2}" | wc -l) -eq '3' ]
}

@test 'cpthsv, --comments-read (absent)' {
    base='RdBu_10'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -T s0 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, --comments-retain --comments-generate' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-retain --comments-generate \
        -T s0 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, --comments-write' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        -T s0 -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'cpthsv, transform absent' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, transform defective' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -T x -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, saturation channel' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s90 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" 'cpthsv/channel-saturation.cpt'
}

@test 'cpthsv, value channel' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T v110 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/channel-value.cpt"
}

@test 'cpthsv, hue channel' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h101 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/channel-hue.cpt"
}

@test 'cpthsv, unknown channel' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T x101 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, percent operator' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s90% "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/operator-percent.cpt"
}

@test 'cpthsv, times operator' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s0.90x "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/operator-multiply.cpt"
}

@test 'cpthsv, addition operator' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s1+ "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/operator-add.cpt"
}

@test 'cpthsv, subtraction operator' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s1- "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/operator-subtract.cpt"
}

@test 'cpthsv, multiple operators' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cptr"
    run "${path}" -v -o "${cpt2}" -T s90,v110 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/operator-multiple.cpt"
}

@test 'cpthsv, unknown operator' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s90? "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, GMT4 HSV input' {
    base='GMT4-cyclic'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s90 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
}

@test 'cpthsv, GMT5 HSV input' {
    base='GMT5-cyclic'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T s90 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
}

@test 'cpthsv, hue-shift cycles, increasing (shift -ve)' {
    base='cpthsv-cycle-inc'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h40- "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-increasing.cpt"
}

@test 'cpthsv, hue-shift cycles, increasing (shift +ve)' {
    base='cpthsv-cycle-inc'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h320+ "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-increasing.cpt"
}

@test 'cpthsv, hue-shift cycles, decreasing (shift -ve)' {
    base='cpthsv-cycle-dec'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h40- "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-decreasing.cpt"
}

@test 'cpthsv, hue-shift cycles, decreasing (shift +ve)' {
    base='cpthsv-cycle-dec'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h320+ "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-decreasing.cpt"
}

@test 'cpthsv, hue-shift cycles, annote lower' {
    base='cpthsv-cycle-annote-lower'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h40- "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-annote-lower.cpt"
}

@test 'cpthsv, hue-shift cycles, annote upper' {
    base='cpthsv-cycle-annote-upper'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h40- "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-annote-upper.cpt"
}

@test 'cpthsv, hue-shift cycles, annote both' {
    base='cpthsv-cycle-annote-both'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h40- "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-annote-both.cpt"
}

@test 'cpthsv, hue-shift cycles, label' {
    base='cpthsv-cycle-label'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h40- "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-label.cpt"
}

@test 'cpthsv, hue-shift cycles, hsv colours, rgb interpolate' {
    base='cpthsv-cycle-hsv-rgb'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h40- "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-hsv-rgb.cpt"
}

@test 'cpthsv, hue-shift cycles, rgb colours, hsv interpolate' {
    base='cpthsv-cycle-rgb-hsv'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt2}" -T h40- -6 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/cycle-rgb-hsv.cpt"
}

@test 'cpthsv, mixed-model --gmt4' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --gmt4 -v -o "${cpt2}" -T h30+ "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, mixed-model --gmt4 --model rgb' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --gmt4 --model rgb -v -o "${cpt2}" -T h30+ "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/mixed-model-v4-rgb.cpt"
}

@test 'cpthsv, mixed-model --gmt4 --model hsv' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --gmt4 --model hsv -v -o "${cpt2}" -T h30+ "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/mixed-model-v4-hsv.cpt"
}

@test 'cpthsv, mixed-model --gmt4 --model nonesuch' {
    base='mixed-model'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --gmt4 --model nonsuch -v -o "${cpt2}" -T h30+ "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, --gmt4' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -4 -T s1 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/version-gmt4.cpt"
}

@test 'cpthsv, --gmt5' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -5 -T s1 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/version-gmt5.cpt"
}

@test 'cpthsv, --gmt6' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -6 -T s1 -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/version-gmt6.cpt"
}

@test 'cpthsv, --gmt4 --gmt5' {
    base='subtle'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -4 -5 -T s1 -o "${cpt2}" "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, normalise (to gmt4)' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -T s1 -o "${cpt2}" --z-normalise --gmt4 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, normalise (to gmt5)' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -T s1 -o "${cpt2}" --z-normalise --gmt5 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/normalise-gmt5.cpt"
}

@test 'cpthsv, normalise (to gmt6)' {
    base='RdBu_10'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -T s1 -o "${cpt2}" --z-normalise --gmt6 "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/normalise-gmt6.cpt"
}

@test 'cpthsv, denormalise (from normalised)' {
    base='GMT5-bathy'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -T s1 -o "${cpt2}" --z-denormalise "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/denormalise-from-normal.cpt"
}

@test 'cpthsv, denormalise (from non-normalised)' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -T s1 -Z -o "${cpt2}" "${cpt1}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt2}" ]
    assert_cpt_equal "${cpt2}" "cpthsv/denormalise-from-nonnormal.cpt"
}

@test 'cpthsv, normalise and denormalise' {
    base='GMT4-gebco'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -T s1 -o "${cpt2}" --z-normalise --z-denormalise "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
}

@test 'cpthsv, output to stdout' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" -T s90 "${cpt}"
    [ "${status}" -eq '0' ]
}

@test 'cpthsv, output to stdout, --verbose' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" --verbose -T s90 "${cpt}"
    [ "${status}" -ne '0' ]
}

@test 'cpthsv, backtrace, default format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${cpt2}" -T h101 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cpthsv, backtrace, plain format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${cpt2}" -T h101 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cpthsv, backtrace, XML format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${cpt2}" -T h101 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cpthsv, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${cpt2}" -T h101 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ -e "${backtrace}" ]
}

@test 'cpthsv, backtrace, unknown format' {
    base='no-such-file'
    cpt1="${fixture_dir}/cpt/${base}.cpt"
    cpt2="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${cpt2}" -T h101 "${cpt1}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt2}" ]
    [ ! -e "${backtrace}" ]
}
