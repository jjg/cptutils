#!/usr/bin/env bash

assert_comments_valid() {
    if [ $# -ne 1 ] ; then
        printf 'expected 1 argument, got %i' $#
        return 1
    fi
    local path="${1}"
    local xsd='../xsd/comments.xsd'
    run xmllint --noout --schema "${xsd}" "${path}"

    return $status
}
