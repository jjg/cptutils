#!/usr/bin/env bash

version=$(cat ../../../VERSION)
fixture_dir='../fixtures'
src_dir='../..'
PATH="${PATH}:../bin"

source 'config.bash'

function skip_unless_with_python
{
    if [ -z "$WITH_PYTHON" ] ; then
        skip 'no Python support'
    fi
}

function skip_unless_with_json
{
    if [ -z "$WITH_JSON" ] ; then
        skip 'no JSON support'
    fi
}
