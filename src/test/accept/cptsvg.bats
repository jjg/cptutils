#!/usr/bin/env bats

load 'shared'
load 'assert-svg'
load 'assert-comments-valid'

setup()
{
    program='cptsvg'
    path="${src_dir}/${program}/${program}"
}

@test 'cptsvg, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'cptsvg, --help' {
    run "${path}" --help
}

@test 'cptsvg, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'cptsvg, no comments source specified' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'cptsvg, --comments-generate' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-generate \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'cptsvg, --comments-retain' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-retain \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'cptsvg, --comments-read (present)' {
    base='RdBu_10'
    comment="${fixture_dir}/xco/realistic.xco"
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'cptsvg, --comments-read (absent)' {
    base='RdBu_10'
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'cptsvg, --comments-retain --comments-generate' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-retain --comments-generate \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'cptsvg, --comments-write' {
    base='RdBu_10'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-write "${comment}" \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'cptsvg, subtle' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --geometry 500x100 -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal ${svg} "cptsvg/${base}.svg"
}

@test 'cptsvg, honeycomb-20 --svg2' {
    base='honeycomb-20'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --svg2 -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal ${svg} "cptsvg/${base}.svg"
}

@test 'cptsvg, GMT4-nighttime' {
    base='GMT4-nighttime'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --geometry 500x100 -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal ${svg} "cptsvg/${base}.svg"
}

@test 'cptsvg, GMT5-cyclic' {
    base='GMT5-cyclic'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${cpt}"
    # FIXME
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'cptsvg, normalised, hard hinge' {
    base='GMT6-mag'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal ${svg} "cptsvg/${base}.svg"
}

@test 'cptsvg, normalised, soft hinge inactive' {
    base='hinge-soft-norm'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal ${svg} "cptsvg/${base}-inactive.svg"
}

@test 'cptsvg, normalised, soft hinge active' {
    base='hinge-soft-norm'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --hinge-active -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal ${svg} "cptsvg/${base}-active.svg"
}

@test 'cptsvg, bad-whitespace' {
    base='bad-whitespace'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --geometry 500x100 -o "${svg}" "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'cptsvg, too many input files' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --geometry 500x100 -o "${svg}" "${cpt}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'cptsvg, input from stdin' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" < "${cpt}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'cptsvg, bad --geometry' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v --geometry x -o "${svg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'cptsvg, output to stdout' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" "${cpt}"
    [ "${status}" -eq '0' ]
}

@test 'cptsvg, output to stdout, --verbose' {
    base='subtle'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    run "${path}" --verbose "${cpt}"
    [ "${status}" -ne '0' ]
}

@test 'cptsvg, backtrace, default format' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'cptsvg, backtrace, plain format' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'cptsvg, backtrace, XML format' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'cptsvg, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'cptsvg, backtrace, unknown format' {
    base='no-such-file'
    cpt="${fixture_dir}/cpt/${base}.cpt"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${svg}" "${cpt}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ ! -e "${backtrace}" ]
}
