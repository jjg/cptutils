#!/usr/bin/env bats

load 'shared'
load 'assert-cpt'

setup()
{
    program='xycpt'
    path="${src_dir}/${program}/${program}"
}

@test 'xycpt, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'xycpt, --help' {
    run "${path}" --help
}

@test 'xycpt, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

testcase_255()
{
    local -r n="$1"
    base="test-i${n}"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" "xycpt/case-255-${n}.cpt"
}

@test 'xycpt, 0-255, 1' {
    testcase_255 '1'
}

@test 'xycpt, 0-255, 2' {
    testcase_255 '2'
}

@test 'xycpt, 0-255, 3' {
    testcase_255 '3'
}

@test 'xycpt, 0-255, 4' {
    testcase_255 '4'
}

testcase_1()
{
    local -r n="$1"
    base="test-u${n}"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -u -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" "xycpt/case-1-${n}.cpt"
}

@test 'xycpt, 0-1, 1' {
    testcase_1 '1'
}

@test 'xycpt, 0-1, 2' {
    testcase_1 '2'
}

@test 'xycpt, 0-1, 3' {
    testcase_1 '3'
}

@test 'xycpt, 0-1, 4' {
    testcase_1 '4'
}

@test 'xycpt, too many input files' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt}" "${xy}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, no comment source' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    [ $(grep \# "${cpt}" | wc -l) -eq '0' ]
}

@test 'xycpt, --comments-generate' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-generate \
        -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    [ $(grep \# "${cpt}" | wc -l) -eq '1' ]
}

@test 'xycpt, --comments-read (present)' {
    base="test-i1"
    comment="${fixture_dir}/xco/realistic.xco"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --comments-read "${comment}" \
        -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    [ $(grep \# "${cpt}" | wc -l) -eq '3' ]
}

@test 'xycpt, --comments-read (absent)' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-read "${comment}" \
        -v -o "${cpt}" "${xy}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, multiple comment sources' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    echo 'some comment' > "${comment}"
    run "${path}" --comments-read "${comment}" --comments-generate \
        -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, --register low' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --discrete --register L -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'xycpt/register-low.cpt'
}

@test 'xycpt, --register mid' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --discrete --register M -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'xycpt/register-mid.cpt'
}

@test 'xycpt, --register high' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --discrete --register H -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'xycpt/register-high.cpt'
}

@test 'xycpt, --register unknown' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --discrete --register X -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, --register without --discrete' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --register M -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, bad --background' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --background 1/2 -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, bad --foreground' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --foreground 1/2 -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, bad --nan' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" --nan 1/2 -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, --gmt4' {
    base="test-i3"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -4 -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'xycpt/version-gmt4.cpt'
}

@test 'xycpt, --gmt5' {
    base="test-i3"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -5 -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'xycpt/version-gmt5.cpt'
}

@test 'xycpt, --gmt6' {
    base="test-i3"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -6 -v -o "${cpt}" "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
    assert_cpt_equal "${cpt}" 'xycpt/version-gmt6.cpt'
}

@test 'xycpt, --gmt4 --gmt5' {
    base="test-i3"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -4 -5 -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
}

@test 'xycpt, input from stdin' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    run "${path}" -o "${cpt}" < "${xy}"
    [ "${status}" -eq '0' ]
    [ -e "${cpt}" ]
}

@test 'xycpt, output to stdout' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    run "${path}" "${xy}"
    [ "${status}" -eq '0' ]
}

@test 'xycpt, output to stdout, --verbose' {
    base="test-i1"
    xy="${fixture_dir}/xy/${base}.dat"
    run "${path}" --verbose "${xy}"
    [ "${status}" -ne '0' ]
}

@test 'xycpt, backtrace, default format' {
    base="no-such-file"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'xycpt, backtrace, plain format' {
    base="no-such-file"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'xycpt, backtrace, XML format' {
    base="no-such-file"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}
@test 'xycpt, backtrace, JSON format' {
    skip_unless_with_json
    base="no-such-file"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ -e "${backtrace}" ]
}

@test 'xycpt, backtrace, unknown format' {
    base="no-such-file"
    xy="${fixture_dir}/xy/${base}.dat"
    cpt="${BATS_TEST_TMPDIR}/${base}.cpt"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${cpt}" "${xy}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpt}" ]
    [ ! -e "${backtrace}" ]
}
