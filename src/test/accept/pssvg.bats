#!/usr/bin/env bats

load 'shared'
load 'assert-svg'

setup()
{
    program='pssvg'
    path="${src_dir}/${program}/${program}"
}

@test 'pssvg, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'pssvg, --help' {
    run "${path}" --help
}

@test 'pssvg, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'pssvg, no comment source' {
    base="neverhappens-titi-montoya"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${grd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pssvg, --comments-generate' {
    base="neverhappens-titi-montoya"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-generate \
        -v -o "${svg}" "${grd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pssvg, --comments-read (present)' {
    base="neverhappens-titi-montoya"
    comment="${fixture_dir}/xco/${base}.xco"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${grd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pssvg, --comments-read (wrong size)' {
    base="neverhappens-titi-montoya"
    comment="${fixture_dir}/xco/single.xco"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pssvg, --comments-read (absent)' {
    base="neverhappens-titi-montoya"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    comment="${BATS_TEST_TMPDIR}/${base}.xco"
    run "${path}" --comments-read "${comment}" \
        -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pssvg, multiple comment sources' {
    base="neverhappens-titi-montoya"
    comment="${fixture_dir}/xco/${bade}.xco"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --comments-read "${comment}" --comments-generate \
        -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

testcase() {
    local -r base="$1"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${grd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
    assert_svg_equal "${svg}" "pssvg/${base}.svg"
}

@test 'pssvg, my-custom-gradient-3-rgb' {
    testcase 'my-custom-gradient-3-rgb'
}

@test 'pssvg, my-custom-gradient-3-hsb' {
    testcase 'my-custom-gradient-3-hsb'
}

@test 'pssvg, my-custom-gradient-3-lab' {
    testcase 'my-custom-gradient-3-lab'
}

@test 'pssvg, names which are invalid XML ids, no --id-format' {
    testcase 'neverhappens-titi-montoya'
}

@test 'pssvg, --id-format invalid' {
    base='neverhappens-titi-montoya'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --id-format '%i' -v -o "${svg}" "${grd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pssvg, --id-format valid' {
    base='neverhappens-titi-montoya'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --id-format 'id-%i' -v -o "${svg}" "${grd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

# tests that the output SVG name attributes are not unique, the input
# fixture has Photoshop gradient "titles" which are non-unique (which
# is fine according to Photoshop) and those are converted to the SVG
# linearGradient "name" attributes (which is non-standard, uniqueness
# is not required).  The fixture has 42 linearGradients, two of the names
# are repeated, so there are 40 unique names.  This is a bit fragile in
# that it relies on each <linearGradient ...> being on its own line, on
# the id being the 1st attribute, the name being the 2nd and so on, but
# that is the case here)

@test 'pssvg, names are not unique' {
    base='short-book-colours'
    grd="${fixture_dir}/grd5/${base}.grd"
    count=$("${path}" "${grd}" | grep '<linearGradient' |
                cut -d\" -f4 | sort | uniq | wc -l)
    [ ${count} = 40 ]
}

# by contrast, the id attributes should be unique

@test 'pssvg, ids are unique' {
    base='short-book-colours'
    grd="${fixture_dir}/grd5/${base}.grd"
    count=$("${path}" "${grd}" | grep '<linearGradient' |
                cut -d\" -f2 | sort | uniq | wc -l)
    [ ${count} = 42 ]
}

@test 'pssvg, noise-gradients' {
    base='noise-gradients'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pssvg, regression 1.53' {
    base='EX_Coffe'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

testcase_afl_2014_12_15() {
    local -r n="$1"
    base="afl-2014-12-15-${n}"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pssvg, AFL 2014/12/15 1' {
    testcase_afl_2014_12_15 '1'
}

@test 'pssvg, AFL 2014/12/15 2' {
    testcase_afl_2014_12_15 '2'
}

@test 'pssvg, AFL 2014/12/15 3' {
    testcase_afl_2014_12_15 '3'
}

@test 'pssvg, AFL 2014/12/15 4' {
    testcase_afl_2014_12_15 '4'
}

@test 'pssvg, AFL 2014/12/15 5' {
    testcase_afl_2014_12_15 '5'
}

@test 'pssvg, AFL 2014/12/15 6' {
    testcase_afl_2014_12_15 '6'
}

@test 'pssvg, AFL 2014/12/15 7' {
    testcase_afl_2014_12_15 '7'
}

@test 'pssvg, AFL 2014/12/15 8' {
    testcase_afl_2014_12_15 '8'
}

@test 'pssvg, AFL 2014/12/15 9' {
    testcase_afl_2014_12_15 '9'
}

@test 'pssvg, AFL 2014/12/15 10' {
    testcase_afl_2014_12_15 '10'
}

@test 'pssvg, AFL 2014/12/15 11' {
    testcase_afl_2014_12_15 '11'
}

@test 'pssvg, AFL 2014/12/15 12' {
    testcase_afl_2014_12_15 '12'
}

testcase_afl_2023_10_25() {
    local -r n="$1"
    base="afl-2023-10-25-${n}"
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pssvg, AFL 2023/10/25 0' {
    testcase_afl_2023_10_25 '00'
}

@test 'pssvg, --svg2' {
    base='my-custom-gradient-3-rgb'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --svg2 -v -o "${svg}" "${grd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pssvg, bad --foreground' {
    base='my-custom-gradient-3-rgb'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --foreground 2/3 -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pssvg, bad --background' {
    base='my-custom-gradient-3-rgb'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" --background 2/3 -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pssvg, too many input files' {
    base='my-custom-gradient-3-rgb'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -v -o "${svg}" "${grd}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
}

@test 'pssvg, input from stdin' {
    base='my-custom-gradient-3-rgb'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    run "${path}" -o "${svg}" < "${grd}"
    [ "${status}" -eq '0' ]
    [ -e "${svg}" ]
}

@test 'pssvg, output to stdout' {
    base='my-custom-gradient-3-rgb'
    grd="${fixture_dir}/grd5/${base}.grd"
    run "${path}" "${grd}"
    [ "${status}" -eq '0' ]
}

@test 'pssvg, output to stdout, --verbose' {
    base='my-custom-gradient-3-rgb'
    grd="${fixture_dir}/grd5/${base}.grd"
    run "${path}" --verbose "${grd}"
    [ "${status}" -ne '0' ]
}

@test 'pssvg, backtrace, default format' {
    base='no-such-file'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'pssvg, backtrace, plain format' {
    base='no-such-file'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.txt"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'pssvg, backtrace, XML format' {
    base='no-such-file'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'pssvg, backtrace, JSON format' {
    skip_unless_with_json
    base='no-such-file'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ -e "${backtrace}" ]
}

@test 'pssvg, backtrace, unknown format' {
    base='no-such-file'
    grd="${fixture_dir}/grd5/${base}.grd"
    svg="${BATS_TEST_TMPDIR}/${base}.svg"
    backtrace="${BATS_TEST_TMPDIR}/${base}.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format unknown \
        -v -o "${svg}" "${grd}"
    [ "${status}" -ne '0' ]
    [ ! -e "${svg}" ]
    [ ! -e "${backtrace}" ]
}
