#!/usr/bin/env bats

load 'shared'

load 'assert-pg'

# gradient-convert needs all of the cptutils programs (and the
# svgx wrapper-scripts) on the path, so we create a temporary
# directory, symlink them all into there, then add that to the
# path (but we only do this once in file scppe).

PROGS=(cptpg cptsvg gimplut gimpsvg gplcpt pspsvg pssvg qgssvg svgx)
WRAPS=(cpt css3 gimp gpt pg png pov psp qgs sao svg map)

setup_file()
{
    TMPBIN="${BATS_FILE_TMPDIR}/bin"
    mkdir -p "${TMPBIN}"
    for prog in ${PROGS[@]}
    do
        ln -s "${PWD}/../../${prog}/${prog}" "${TMPBIN}/${prog}"
    done
    for wrap in ${WRAPS[@]}
    do
        svgwrap="svg${wrap}"
        ln -s "${PWD}/../../svgx/wrapper/${svgwrap}" "${TMPBIN}/${svgwrap}"
    done
    PATH="${TMPBIN}:${PATH}"
}

setup()
{
    skip_unless_with_python
    program='gradient-convert'
    path="${src_dir}/${program}/${program}"
}

testcase_cpt()
{
    local -r ofmt="$1"
    ifmt='cpt'
    ext='cpt'
    dir='cpt'
    base='subtle'

    ifile="${fixture_dir}/${dir}/${base}.${ext}"
    ofile="${BATS_TEST_TMPDIR}/${base}.$ofmt"
    run "${path}" -v -i "${ifmt}" -o "${ofmt}" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, cpt to c3g' {
    testcase_cpt 'c3g'
}

@test 'gradient-convert, cpt to ggr' {
    testcase_cpt 'ggr'
}

@test 'gradient-convert, cpt to gpf' {
    testcase_cpt 'gpf'
}

@test 'gradient-convert, cpt to inc' {
    testcase_cpt 'inc'
}

@test 'gradient-convert, cpt to lut' {
    testcase_cpt 'lut'
}

@test 'gradient-convert, cpt to pg' {
    testcase_cpt 'pg'
}

@test 'gradient-convert, cpt to png' {
    testcase_cpt 'png'
}

@test 'gradient-convert, cpt to psp' {
    testcase_cpt 'psp'
}

@test 'gradient-convert, cpt to qgs' {
    testcase_cpt 'qgs'
}

@test 'gradient-convert, cpt to sao' {
    testcase_cpt 'sao'
}

@test 'gradient-convert, cpt to svg' {
    testcase_cpt 'svg'
}

@test 'gradient-convert, cpt to map' {
    testcase_cpt 'map'
}

testcase_ggr()
{
    local -r ofmt="$1"
    ifmt='ggr'
    ext='ggr'
    dir='ggr'
    base='Sunrise'

    ifile="${fixture_dir}/${dir}/${base}.${ext}"
    ofile="${BATS_TEST_TMPDIR}/${base}.$ofmt"
    run "${path}" -v -i "${ifmt}" -o "${ofmt}" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, ggr to cpt' {
    testcase_ggr 'cpt'
}

@test 'gradient-convert, ggr to c3g' {
    testcase_ggr 'c3g'
}

@test 'gradient-convert, ggr to gpf' {
    testcase_ggr 'gpf'
}

@test 'gradient-convert, ggr to inc' {
    testcase_ggr 'inc'
}

@test 'gradient-convert, ggr to lut' {
    testcase_ggr 'lut'
}

@test 'gradient-convert, ggr to pg' {
    testcase_ggr 'pg'
}

@test 'gradient-convert, ggr to png' {
    testcase_ggr 'png'
}

@test 'gradient-convert, ggr to psp' {
    testcase_ggr 'psp'
}

@test 'gradient-convert, ggr to qgs' {
    testcase_ggr 'qgs'
}

@test 'gradient-convert, ggr to sao' {
    testcase_ggr 'sao'
}

@test 'gradient-convert, ggr to svg' {
    testcase_ggr 'svg'
}

@test 'gradient-convert, ggr to map' {
    testcase_ggr 'map'
}

testcase_gpl()
{
    local -r ofmt="$1"
    ifmt='gpl'
    ext='gpl'
    dir='gpl'
    base='Caramel'

    ifile="${fixture_dir}/${dir}/${base}.${ext}"
    ofile="${BATS_TEST_TMPDIR}/${base}.$ofmt"
    run "${path}" -v -i "${ifmt}" -o "${ofmt}" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, gpl to cpt' {
    testcase_gpl 'cpt'
}

@test 'gradient-convert, gpl to c3g' {
    testcase_gpl 'c3g'
}

@test 'gradient-convert, gpl to ggr' {
    testcase_gpl 'ggr'
}

@test 'gradient-convert, gpl to gpf' {
    testcase_gpl 'gpf'
}

@test 'gradient-convert, gpl to inc' {
    testcase_gpl 'inc'
}

@test 'gradient-convert, gpl to lut' {
    testcase_gpl 'lut'
}

@test 'gradient-convert, gpl to pg' {
    testcase_gpl 'pg'
}

@test 'gradient-convert, gpl to png' {
    testcase_gpl 'png'
}

@test 'gradient-convert, gpl to psp' {
    testcase_gpl 'psp'
}

@test 'gradient-convert, gpl to qgs' {
    testcase_gpl 'qgs'
}

@test 'gradient-convert, gpl to sao' {
    testcase_gpl 'sao'
}

@test 'gradient-convert, gpl to svg' {
    testcase_gpl 'svg'
}

@test 'gradient-convert, gpl to map' {
    testcase_gpl 'map'
}

testcase_grd()
{
    local -r ofmt="$1"
    ifmt='grd'
    ext='grd'
    dir='grd5'
    base='my-custom-gradient-3-rgb'

    ifile="${fixture_dir}/${dir}/${base}.${ext}"
    ofile="${BATS_TEST_TMPDIR}/${base}.$ofmt"
    run "${path}" -v -i "${ifmt}" -o "${ofmt}" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, grd to cpt' {
    testcase_grd 'cpt'
}

@test 'gradient-convert, grd to c3g' {
    testcase_grd 'c3g'
}

@test 'gradient-convert, grd to ggr' {
    testcase_grd 'ggr'
}

@test 'gradient-convert, grd to gpf' {
    testcase_grd 'gpf'
}

@test 'gradient-convert, grd to inc' {
    testcase_grd 'inc'
}

@test 'gradient-convert, grd to lut' {
    testcase_grd 'lut'
}

@test 'gradient-convert, grd to pg' {
    testcase_grd 'pg'
}

@test 'gradient-convert, grd to png' {
    testcase_grd 'png'
}

@test 'gradient-convert, grd to psp' {
    testcase_grd 'psp'
}

@test 'gradient-convert, grd to qgs' {
    testcase_grd 'qgs'
}

@test 'gradient-convert, grd to sao' {
    testcase_grd 'sao'
}

@test 'gradient-convert, grd to svg' {
    testcase_grd 'svg'
}

@test 'gradient-convert, grd to map' {
    testcase_grd 'map'
}

testcase_qgs()
{
    local -r ofmt="$1"
    ifmt='qgs'
    ext='xml'
    dir='qgs'
    base='parula'

    ifile="${fixture_dir}/${dir}/${base}.${ext}"
    ofile="${BATS_TEST_TMPDIR}/${base}.$ofmt"
    run "${path}" -v -i "${ifmt}" -o "${ofmt}" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, qgs to cpt' {
    testcase_qgs 'cpt'
}

@test 'gradient-convert, qgs to c3g' {
    testcase_qgs 'c3g'
}

@test 'gradient-convert, qgs to ggr' {
    testcase_qgs 'ggr'
}

@test 'gradient-convert, qgs to gpf' {
    testcase_qgs 'gpf'
}

@test 'gradient-convert, qgs to inc' {
    testcase_qgs 'inc'
}

@test 'gradient-convert, qgs to lut' {
    testcase_qgs 'lut'
}

@test 'gradient-convert, qgs to pg' {
    testcase_qgs 'pg'
}

@test 'gradient-convert, qgs to png' {
    testcase_qgs 'png'
}

@test 'gradient-convert, qgs to psp' {
    testcase_qgs 'psp'
}

@test 'gradient-convert, qgs to sao' {
    testcase_qgs 'sao'
}

@test 'gradient-convert, qgs to svg' {
    testcase_qgs 'svg'
}

@test 'gradient-convert, qgs to map' {
    testcase_qgs 'map'
}

testcase_psp()
{
    local -r ofmt="$1"
    ifmt='psp'
    ext='jgd'
    dir='grd3'
    base='accented'

    ifile="${fixture_dir}/${dir}/${base}.${ext}"
    ofile="${BATS_TEST_TMPDIR}/${base}.$ofmt"
    run "${path}" -v -i "${ifmt}" -o "${ofmt}" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, psp to cpt' {
    testcase_psp 'cpt'
}

@test 'gradient-convert, psp to c3g' {
    testcase_psp 'c3g'
}

@test 'gradient-convert, psp to ggr' {
    testcase_psp 'ggr'
}

@test 'gradient-convert, psp to gpf' {
    testcase_psp 'gpf'
}

@test 'gradient-convert, psp to inc' {
    testcase_psp 'inc'
}

@test 'gradient-convert, psp to lut' {
    testcase_psp 'lut'
}

@test 'gradient-convert, psp to pg' {
    testcase_psp 'pg'
}

@test 'gradient-convert, psp to png' {
    testcase_psp 'png'
}

@test 'gradient-convert, psp to qgs' {
    testcase_psp 'qgs'
}

@test 'gradient-convert, psp to sao' {
    testcase_psp 'sao'
}

@test 'gradient-convert, psp to svg' {
    testcase_psp 'svg'
}

@test 'gradient-convert, psp to map' {
    testcase_psp 'map'
}

testcase_svg()
{
    local -r ofmt="$1"
    ifmt='svg'
    ext='svg'
    dir='svg'
    base='lemon-lime'

    ifile="${fixture_dir}/${dir}/${base}.${ext}"
    ofile="${BATS_TEST_TMPDIR}/${base}.$ofmt"
    run "${path}" -v -i "${ifmt}" -o "${ofmt}" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, svg to cpt' {
    testcase_svg 'cpt'
}

@test 'gradient-convert, svg to c3g' {
    testcase_svg 'c3g'
}

@test 'gradient-convert, svg to ggr' {
    testcase_svg 'ggr'
}

@test 'gradient-convert, svg to gpf' {
    testcase_svg 'gpf'
}

@test 'gradient-convert, svg to inc' {
    testcase_svg 'inc'
}

@test 'gradient-convert, svg to lut' {
    testcase_svg 'lut'
}

@test 'gradient-convert, svg to pg' {
    testcase_svg 'pg'
}

@test 'gradient-convert, svg to png' {
    testcase_svg 'png'
}

@test 'gradient-convert, svg to psp' {
    testcase_svg 'psp'
}

@test 'gradient-convert, svg to qgs' {
    testcase_svg 'qgs'
}

@test 'gradient-convert, svg to sao' {
    testcase_svg 'sao'
}

@test 'gradient-convert, svg to map' {
    testcase_svg 'map'
}

# there are two ways to convert GIMP to PG:
#
#   ggr -> svg -> pg
#   ggr -> svg -> cpt -> pg
#
# the first preserves the alpha channel, the second does not

@test 'gradient-convert, GIMP to PG preserves alpha' {
    ifile="${fixture_dir}/ggr/Coffee.ggr"
    ofile="${BATS_TEST_TMPDIR}/Coffee.pg"
    run "${path}" -v -i "ggr" -o "pg" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
    assert_pg_equal "${ofile}" gradient-convert/ggr-pg-alpha.pg
}

# similarly, there are two ways to convert CPT to PG:
#
#   cpt -> pg
#   cpt -> svg -> pg
#
# the first preserves the range, the second does not

@test 'gradient-convert, CPT to PG preserves range' {
    ifile="${fixture_dir}/cpt/pakistan.cpt"
    ofile="${BATS_TEST_TMPDIR}/pakistan.pg"
    run "${path}" -v -i "cpt" -o "pg" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
    assert_pg_equal "${ofile}" gradient-convert/cpt-pg-range.pg
}

# the --comments-retain option

@test 'gradient-convert, --comments-retain, qgs to sao' {
    ifile="${fixture_dir}/qgs/elevation-comments.xml"
    odir="${BATS_TEST_TMPDIR}/sao"
    mkdir "${odir}"
    run "${path}" -v -B -C -i "qgs" -o "sao" "${ifile}" "${odir}"
    [ "${status}" -eq '0' ]
    [ -e "${odir}/elevation1.sao" ]
    [ -e "${odir}/elevation2.sao" ]
}

@test 'gradient-convert, --comments-retain, svg to cpt' {
    ifile="${fixture_dir}/svg/comments.svg"
    odir="${BATS_TEST_TMPDIR}/cpt"
    mkdir "${odir}"
    run "${path}" -v -B -C -i "svg" -o "cpt" "${ifile}" "${odir}"
    [ "${status}" -eq '0' ]
    [ -e "${odir}/comments-01.cpt" ]
    [ -e "${odir}/comments-02.cpt" ]
    [ -e "${odir}/comments-03.cpt" ]
}

@test 'gradient-convert, --comments-retain, cpt to qgs' {
    ifile="${fixture_dir}/cpt/blue.cpt"
    ofile="${BATS_TEST_TMPDIR}/blue.qgs"
    run "${path}" -v -C -i "cpt" -o "qgs" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

# the --gmt4/5 options

@test 'gradient-convert, --gmt4' {
    ifile="${fixture_dir}/ggr/mars.ggr"
    ofile="${BATS_TEST_TMPDIR}/mars.cpt"
    run "${path}" -v -4 -i "ggr" -o "cpt" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, --gmt5' {
    ifile="${fixture_dir}/ggr/mars.ggr"
    ofile="${BATS_TEST_TMPDIR}/mars.cpt"
    run "${path}" -v -5 -i "ggr" -o "cpt" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, --gmt6' {
    ifile="${fixture_dir}/ggr/mars.ggr"
    ofile="${BATS_TEST_TMPDIR}/mars.cpt"
    run "${path}" -v -6 -i "ggr" -o "cpt" "${ifile}" "${ofile}"
    [ "${status}" -eq '0' ]
    [ -e "${ofile}" ]
}

@test 'gradient-convert, --gmt4 --gmt5' {
    ifile="${fixture_dir}/ggr/mars.ggr"
    ofile="${BATS_TEST_TMPDIR}/mars.cpt"
    run "${path}" -v -4 -5 -i "ggr" -o "cpt" "${ifile}" "${ofile}"
    [ "${status}" -ne '0' ]
    [ ! -e "${ofile}" ]
}

# informative modes

@test 'gradient-convert, help' {
    run "${path}" --help
}

@test 'gradient-convert, version' {
    run "${path}" --version
}

@test 'gradient-convert, capabilities' {
    run "${path}" --capabilities
}

@test 'gradient-convert, graphviz' {
    run "${path}" --graphviz
}
