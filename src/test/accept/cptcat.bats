#!/usr/bin/env bats

load 'shared'
load 'assert-cpt'
load 'assert-comments-valid'

setup()
{
    program='cptcat'
    path="${src_dir}/${program}/${program}"
}

@test 'cptcat, --version' {
    run "${path}" --version
    [ "${status}" -eq '0' ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'cptcat, --help' {
    run "${path}" --help
}

@test 'cptcat, --unknown' {
    run "${path}" --unknown
    [ "${status}" -ne '0' ]
}

@test 'cptcat, no argument' {
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -o "${cpto}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, no comments source specified' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    [ $(grep \# "${cpto}" | wc -l) -eq '0' ]
}

@test 'cptcat, --comments-generate' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --comments-generate \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    [ $(grep \# "${cpto}" | wc -l) -eq '1' ]
}

@test 'cptcat, --comments-retain' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --comments-retain \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    [ $(grep \# "${cpto}" | wc -l) -eq '6' ]
}

@test 'cptcat, --comments-read (present)' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    comment="${fixture_dir}/xco/realistic.xco"
    run "${path}" --comments-read "${comment}" \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    [ $(grep \# "${cpto}" | wc -l) -eq '3' ]
}

@test 'cptcat, --comments-read (absent)' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    comment="${BATS_TEST_TMPDIR}/comment.xco"
    run "${path}" --comments-read "${comment}" \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, --comments-retain --comments-generate' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --comments-retain --comments-generate \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, --comments-write' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    comment="${BATS_TEST_TMPDIR}/comment.xco"
    run "${path}" --comments-write "${comment}" \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    [ -e "${comment}" ]
    assert_comments_valid "${comment}"
}

@test 'cptcat, one argument' {
    cpti="${fixture_dir}/cpt/tpsfhm.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -o "${cpto}" "${cpti}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/arg1.cpt'
}

@test 'cptcat, tpsfhm/pakistan' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/arg2-increasing.cpt'
}

@test 'cptcat, pakistan/tpsfhm' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/tpsfhm.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/arg2-decreasing.cpt'
}

@test 'cptcat, GMT5-bathy/pakistan, denormalise, no hinge' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -Z -6 -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/denormalise-no-hinge.cpt'
}

@test 'cptcat, GMT5-bathy/pakistan, denormalise, soft hinge' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -Z -6 --hinge-soft -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/denormalise-soft-hinge.cpt'
}

@test 'cptcat, GMT5-bathy/pakistan, denormalise, hard hinge' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -Z -6 --hinge-hard -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/denormalise-hard-hinge.cpt'
}

@test 'cptcat, GMT5-bathy/pakistan, normalise, no hinge' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -z -6 -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/normalise-no-hinge.cpt'
}

@test 'cptcat, GMT5-bathy/pakistan, normalise, soft hinge' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -z -6 --hinge-soft -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/normalise-soft-hinge.cpt'
}

@test 'cptcat, GMT5-bathy/pakistan, normalise, hard hinge' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -z -6 --hinge-hard -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/normalise-hard-hinge.cpt'
}

@test 'cptcat, hard and soft hinge' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -6 --hinge-hard --hinge-soft \
        -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, normalised and denormalised' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -6 --z-normalise --z-denormalise \
        -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, non-contiguous inputs' {
    cpt1="${fixture_dir}/cpt/GMT5-bathy.cpt"
    cpt2="${fixture_dir}/cpt/Exxon88.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -6 -Z \
        -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, model +RGB/+RGB, GMT4' {
    cpt1="${fixture_dir}/cpt/cat-RR4A.cpt"
    cpt2="${fixture_dir}/cpt/cat-RR4B.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -4 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/gmt4-RGB-RGB.cpt'
}

@test 'cptcat, model +RGB/+RGB, GMT6' {
    cpt1="${fixture_dir}/cpt/cat-RR4A.cpt"
    cpt2="${fixture_dir}/cpt/cat-RR4B.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -6 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/gmt6-RGB-RGB.cpt'
}

@test 'cptcat, model rgb/+RGB, GMT4' {
    cpt1="${fixture_dir}/cpt/cat-r5A.cpt"
    cpt2="${fixture_dir}/cpt/cat-RR4B.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -4 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/gmt4-rgb-RGB.cpt'
}

@test 'cptcat, model rgb/+RGB, GMT6' {
    cpt1="${fixture_dir}/cpt/cat-r5A.cpt"
    cpt2="${fixture_dir}/cpt/cat-RR4B.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -6 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/gmt6-rgb-RGB.cpt'
}

@test 'cptcat, model HSV/+RGB, GMT4' {
    cpt1="${fixture_dir}/cpt/cat-HR4A.cpt"
    cpt2="${fixture_dir}/cpt/cat-RR4B.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -4 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, model HSV/+RGB, GMT6' {
    cpt1="${fixture_dir}/cpt/cat-HR4A.cpt"
    cpt2="${fixture_dir}/cpt/cat-RR4B.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -6 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/gmt6-HSV-RGB.cpt'
}

@test 'cptcat, model +HSV/+RGB, GMT4' {
    cpt1="${fixture_dir}/cpt/cat-HH4A.cpt"
    cpt2="${fixture_dir}/cpt/cat-RR4B.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -4 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, model +HSV/+RGB, GMT6' {
    cpt1="${fixture_dir}/cpt/cat-HH4A.cpt"
    cpt2="${fixture_dir}/cpt/cat-RR4B.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -6 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, mixed-model v6/v6 with --gmt4' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv6.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv6.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -4 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, mixed-model v6/v6 with --gmt4 --model rgb' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv6.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv6.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --gmt4 --model rgb -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/mixed-model-v6v6-gmt4-rgb.cpt'
}

@test 'cptcat, mixed-model v6/v6 with --gmt4 --model hsv' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv6.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv6.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --gmt4 --model hsv -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/mixed-model-v6v6-gmt4-hsv.cpt'
}

@test 'cptcat, mixed-model v4/v6 with --gmt4' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv4.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv6.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -4 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, mixed-model v4/v6 with --gmt4 --model rgb' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv4.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv6.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --gmt4 --model rgb -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/mixed-model-v4v6-gmt4-rgb.cpt'
}

@test 'cptcat, mixed-model v4/v6 with --gmt4 --model hsv' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv4.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv6.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --gmt4 --model hsv -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/mixed-model-v4v6-gmt4-hsv.cpt'
}

@test 'cptcat, mixed-model v4/v4 with --gmt4' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv4.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv4.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -4 -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, mixed-model v4/v4 with --gmt4 --model rgb' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv4.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv4.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --gmt4 --model rgb -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/mixed-model-v4v4-gmt4-rgb.cpt'
}

@test 'cptcat, mixed-model v4/v4 with --gmt4 --model hsv' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv4.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv4.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --gmt4 --model hsv -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/mixed-model-v4v4-gmt4-hsv.cpt'
}

@test 'cptcat, mixed-model with --model nonesuch' {
    cpt1="${fixture_dir}/cpt/cptcat-model-1-RGBv4.cpt"
    cpt2="${fixture_dir}/cpt/cptcat-model-2-HSVv4.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" --gmt4 --model nonesuch -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, interpolation regression' {
    cpti="${fixture_dir}/cpt/GMT5-cyclic.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -4 -o "${cpto}" "${cpti}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/interpolate-regression.cpt'
}

@test 'cptcat, --gmt4' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/tpsfhm.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -4 -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/version-gmt4.cpt'
}

@test 'cptcat, --gmt5' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/tpsfhm.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -5 -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/version-gmt5.cpt'
}

@test 'cptcat, --gmt6' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/tpsfhm.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -6 -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
    [ -e "${cpto}" ]
    assert_cpt_equal "${cpto}" 'cptcat/version-gmt6.cpt'
}

@test 'cptcat, --gmt4 --gmt5' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/tpsfhm.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    run "${path}" -v -4 -5 -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
}

@test 'cptcat, output to stdout' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    run "${path}" "${cpt1}" "${cpt2}"
    [ "${status}" -eq '0' ]
}

@test 'cptcat, output to stdout, --verbose' {
    cpt1="${fixture_dir}/cpt/tpsfhm.cpt"
    cpt2="${fixture_dir}/cpt/pakistan.cpt"
    run "${path}" --verbose "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
}

@test 'cptcat, backtrace, default format ' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/no-such-file.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.xml"
    run "${path}" --backtrace-file "${backtrace}" \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
    [ -e "${backtrace}" ]
}

@test 'cptcat, backtrace, plain format ' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/no-such-file.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format plain \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
    [ -e "${backtrace}" ]
}

@test 'cptcat, backtrace, XML format ' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/no-such-file.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format xml \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
    [ -e "${backtrace}" ]
}

@test 'cptcat, backtrace, JSON format ' {
    skip_unless_with_json
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/no-such-file.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.json"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format json \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
    [ -e "${backtrace}" ]
}

@test 'cptcat, backtrace, unknown format ' {
    cpt1="${fixture_dir}/cpt/pakistan.cpt"
    cpt2="${fixture_dir}/cpt/no-such-file.cpt"
    cpto="${BATS_TEST_TMPDIR}/output.cpt"
    backtrace="${BATS_TEST_TMPDIR}/backtrace.xml"
    run "${path}" \
        --backtrace-file "${backtrace}" \
        --backtrace-format meh \
        -v -o "${cpto}" "${cpt1}" "${cpt2}"
    [ "${status}" -ne '0' ]
    [ ! -e "${cpto}" ]
    [ ! -e "${backtrace}" ]
}
