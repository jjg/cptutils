This is the "Elevation color ramps" by _Helebrant_ kindly released
under a CC0 licence. Downloaded from [QGis plugins][1].

[1]: https://plugins.qgis.org/styles/28/
