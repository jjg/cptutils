This is the "Parula color ramp" by _Romaine_ kindly released
under a CC0 licence. Downloaded from [QGis plugins][1].

[1]: https://plugins.qgis.org/styles/55/
