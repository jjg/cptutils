#include <cptutils/qgs-list-read.h>
#include <cptutils/qgs-list.h>
#include <cptutils/qgs.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

static bool qgs_name_equal(const char *n1, const char *n2)
{
  return strcmp(n1, n2) == 0;
}

static bool qgs_value_equal(double x1, double x2)
{
  if (fabs(x1 - x2) > 1e-6)
    {
      printf("value %f != %f\n", x1, x2);
      return false;
    }

  return true;
}

static bool qgs_opacity_equal(double x1, double x2)
{
  if (fabs(x1 - x2) > 1e-6)
    {
      printf("opacity %f != %f\n", x1, x2);
      return false;
    }

  return true;
}

static bool qgs_colour_equal(rgb_t rgb1, rgb_t rgb2)
{
  if (rgb_dist(rgb1, rgb2) > 0.5)
    {
      printf("colour (%i, %i, %i) != (%i, %i, %i)\n",
             rgb1.red, rgb1.green, rgb1.blue,
             rgb2.red, rgb2.green, rgb2.blue);
      return false;
    }

  return true;
}

static bool qgs_entry_equal(const qgs_entry_t *e1, const qgs_entry_t *e2)
{
  return
    qgs_colour_equal(e1->rgb, e2->rgb) &&
    qgs_opacity_equal(e1->opacity, e2->opacity) &&
    qgs_value_equal(e1->value, e2->value);
}

static bool qgs_equal(const qgs_t *q1, const qgs_t *q2)
{
  if (! qgs_name_equal(q1->name, q2->name))
    {
      printf("names %s != %s\n", q1->name, q2->name);
      return false;
    }

  if (q1->n != q2->n)
    {
      printf("number of entries: %zu != %zu\n", q1->n, q2->n);
      return false;
    }

  if (q1->type != q2->type)
    {
      printf("types: %i != %i\n", q1->type, q2->type);
      return false;
    }

  for (size_t i = 0 ; i < q1->n ; i++)
    if (! qgs_entry_equal(q1->entries + i, q2->entries + i))
      {
        printf("entry %zu\n", i);
        return false;
      }

  return true;
}

static bool qgs_list_equal(const qgs_list_t *ql1, const qgs_list_t *ql2)
{
  if (qgs_list_size(ql1) != qgs_list_size(ql2))
    {
      printf("list sizes %zi != %zi\n",
             qgs_list_size(ql1),
             qgs_list_size(ql2));
      return false;
    }

  size_t n = qgs_list_size(ql2);

  for (size_t i = 0 ; i < n ; i++)
    {
      const qgs_t
        *q1 = qgs_list_entry(ql1, i),
        *q2 = qgs_list_entry(ql2, i);

      if (q1 == NULL)
        {
          printf("list 1 item %zi NULL\n", i);
          return false;
        }

      if (q2 == NULL)
        {
          printf("list 2 item %zi NULL\n", i);
          return false;
        }

      if (! qgs_equal(q1, q2))
        {
          printf("list items %zi\n", i);
          return false;
        }
    }

  return true;
}

int main(int argc, char **argv)
{
  if (argc != 3)
    {
      fprintf(stderr, "usage: equal-qgs <path1> <path2>\n");
      return EXIT_FAILURE;
    }

  qgs_list_t *ql1;
  bool equal = false;

  if ((ql1 = qgs_list_read(argv[1])) != NULL)
    {
      qgs_list_t *ql2;

      if ((ql2 = qgs_list_read(argv[2])) != NULL)
        {
          equal = qgs_list_equal(ql1, ql2);
          qgs_list_destroy(ql2);
        }
      else
        fprintf(stderr, "failed read of %s\n", argv[2]);

      qgs_list_destroy(ql1);
    }
  else
    fprintf(stderr, "failed read of %s\n", argv[1]);

  return equal ? EXIT_SUCCESS : EXIT_FAILURE;
}
