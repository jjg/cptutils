#include <cptutils/svg-list-read.h>
#include <cptutils/svg-list.h>
#include <cptutils/svg.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>


static bool svg_name_equal(const unsigned char *n1,
                           const unsigned char *n2)
{
  return strcmp((const char*)n1, (const char*)n2) == 0;
}

static bool svg_id_equal(const unsigned char *n1,
                         const unsigned char *n2)
{
  return strcmp((const char*)n1, (const char*)n2) == 0;
}

static bool svg_value_equal(double x1, double x2)
{
  if (fabs(x1 - x2) > 1e-6)
    {
      printf("value %f != %f\n", x1, x2);
      return false;
    }

  return true;
}

static bool svg_opacity_equal(double x1, double x2)
{
  if (fabs(x1 - x2) > 1e-6)
    {
      printf("opacity %f != %f\n", x1, x2);
      return false;
    }

  return true;
}

static bool svg_colour_equal(rgb_t rgb1, rgb_t rgb2)
{
  if (rgb_dist(rgb1, rgb2) > 0.5)
    {
      printf("colour (%i, %i, %i) != (%i, %i, %i)\n",
             rgb1.red, rgb1.green, rgb1.blue,
             rgb2.red, rgb2.green, rgb2.blue);
      return false;
    }

  return true;
}

static bool svg_stop_equal(const svg_stop_t *s1, const svg_stop_t *s2)
{
  return
    svg_colour_equal(s1->colour, s2->colour) &&
    svg_value_equal(s1->value, s2->value) &&
    svg_opacity_equal(s1->opacity, s2->opacity);
}

static bool svg_node_equal(const svg_node_t *n1,
                           const svg_node_t *n2,
                           int n)
{
  if (n1 == NULL)
    {
      if (n2 == NULL)
        return true;
      else
        {
          printf("nodes NULL/not-NULL\n");
          return false;
        }
    }
  else
    {
      if (n2 == NULL)
        {
          printf("nodes not-NULL/NULL\n");
          return false;
        }
    }

  if (! svg_stop_equal(&(n1->stop), &(n2->stop)))
    {
      printf("stop %i\n", n);
      return false;
    }

  return svg_node_equal(n1->r, n2->r, n + 1);
}

static bool svg_equal(const svg_t *s1, const svg_t *s2)
{
  if (! svg_name_equal(s1->name, s2->name))
    {
      printf("names %s != %s\n",
             (const char*)s1->name,
             (const char*)s2->name);
      return false;
    }

  if (! svg_id_equal(s1->id, s2->id))
    {
      printf("id %s != %s\n",
             (const char*)s1->id,
             (const char*)s2->id);
      return false;
    }

  if (! svg_node_equal(s1->nodes, s2->nodes, 0))
    {
      printf("nodes\n");
      return false;
    }

  return true;
}

static bool svg_list_equal(const svg_list_t *sl1, const svg_list_t *sl2)
{
  if (svg_list_size(sl1) != svg_list_size(sl2))
    {
      printf("list sizes %zi != %zi\n",
             svg_list_size(sl1),
             svg_list_size(sl2));
      return false;
    }

  size_t n = svg_list_size(sl2);

  for (size_t i = 0 ; i < n ; i++)
    {
      const svg_t
        *s1 = svg_list_entry(sl1, i),
        *s2 = svg_list_entry(sl2, i);

      if (s1 == NULL)
        {
          printf("list 1 item %zi NULL\n", i);
          return false;
        }

      if (s2 == NULL)
        {
          printf("list 2 item %zi NULL\n", i);
          return false;
        }

      if (! svg_equal(s1, s2))
        {
          printf("list items %zi\n", i);
          return false;
        }
    }

  return true;
}

int main(int argc, char **argv)
{
  if (argc != 3)
    {
      fprintf(stderr, "usage: equal-svg <path1> <path2>\n");
      return EXIT_FAILURE;
    }

  svg_list_t *sl1;
  bool equal = false;

  if ((sl1 = svg_list_read(argv[1])) != NULL)
    {
      svg_list_t *sl2;

      if ((sl2 = svg_list_read(argv[2])) != NULL)
        {
          equal = svg_list_equal(sl1, sl2);
          svg_list_destroy(sl2);
        }
      else
        fprintf(stderr, "failed read of %s\n", argv[2]);

      svg_list_destroy(sl1);
    }
  else
    fprintf(stderr, "failed read of %s\n", argv[1]);

  return equal ? EXIT_SUCCESS : EXIT_FAILURE;
}
