#include <cptutils/cpt-read.h>
#include <cptutils/cpt.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#define Z_EPS 1e-6

static bool z_equal(double z1, double z2)
{
  if (fabs(z1 - z2) > Z_EPS)
    {
      printf("z: %lf != %lf\n", z1, z2);
      return false;
    }

  return true;
}

static bool model_equal(model_t m1, model_t m2)
{
  if (m1 != m2)
    {
      printf("models: %s/%s\n", model_name(m1), model_name(m2));
      return false;
    }

  return true;
}

static bool fill_equal(cpt_fill_t f1, cpt_fill_t f2)
{
  return cpt_fill_eq(f1, f2);
}

static bool range_equal(cpt_range_t r1, cpt_range_t r2)
{
  if (r1.present)
    {
      if (r2.present)
        {
          if (! z_equal(r1.min, r2.min))
            {
              printf("min\n");
              return false;
            }

          if (! z_equal(r1.max, r2.max))
            {
              printf("max\n");
              return false;
            }
        }
      else
        {
          printf("present/absent\n");
          return false;
        }
    }
  else
    {
      if (r2.present)
        {
          printf("absent/present\n");
          return false;
        }
    }

  return true;
}

static bool hinge_equal(cpt_hinge_t h1, cpt_hinge_t h2)
{
  if (h1.present)
    {
      if (h2.present)
        {
          if (h1.type != h2.type)
            {
              printf("differing types\n");
              return false;
            }

          if (h1.type == hinge_explicit)
            {
              if (! z_equal(h1.value, h2.value))
                {
                  printf("explicit hinge\n");
                  return false;
                }
            }
        }
      else
        {
          printf("present/absent\n");
          return false;
        }
    }
  else
    {
      if (h2.present)
        {
          printf("absent/present\n");
          return false;
        }
    }

  return true;
}

static bool sample_equal(const cpt_sample_t s1, const cpt_sample_t s2)
{
  if (! fill_equal(s1.fill, s2.fill))
    {
      printf("fills\n");
      return false;
    }

  if (! z_equal(s1.val, s2.val))
    {
      printf("values\n");
      return false;
    }

  return true;
}

static bool seg_equal(const cpt_seg_t *s1, const cpt_seg_t *s2)
{
  if (! sample_equal(s1->sample.left, s2->sample.left))
    {
      printf("left sample\n");
      return false;
    }

  if (! sample_equal(s1->sample.right, s2->sample.right))
    {
      printf("right sample\n");
      return false;
    }

  return true;
}

static bool segs_equal(const cpt_seg_t *s1, const cpt_seg_t *s2, int n)
{
  if (s1)
    {
      if (s2)
        {
          if (! seg_equal(s1, s2))
            {
              printf("segment %i\n", n);
              return false;
            }
          else
            return segs_equal(s1->right, s2->right, n + 1);
        }
      else
        {
          printf("segment %i: present/absent\n", n);
          return false;
        }
    }
  else
    {
      if (s2)
        {
          printf("segment %i: absent/present\n", n);
          return false;
        }
    }

  return true;
}

static bool segments_equal(const cpt_seg_t *s1, const cpt_seg_t *s2)
{
  return segs_equal(s1, s2, 1);
}

static bool cpt_equal(const cpt_t *cpt1, const cpt_t *cpt2)
{
  if (! model_equal(cpt1->model, cpt2->model))
    {
      printf("colour model\n");
      return false;
    }

  if (! model_equal(cpt1->interpolate, cpt2->interpolate))
    {
      printf("interpolation model\n");
      return false;
    }

  if (! fill_equal(cpt1->fg, cpt2->fg))
    {
      printf("foreground fill\n");
      return false;
    }

  if (! fill_equal(cpt1->bg, cpt2->bg))
    {
      printf("background fill\n");
      return false;
    }

  if (! fill_equal(cpt1->nan, cpt2->nan))
    {
      printf("NaN fill\n");
      return false;
    }

  if (! range_equal(cpt1->range, cpt2->range))
    {
      printf("range\n");
      return false;
    }

  if (! hinge_equal(cpt1->hinge, cpt2->hinge))
    {
      printf("hinge\n");
      return false;
    }

  if (! segments_equal(cpt1->segment, cpt2->segment))
    {
      printf("segments\n");
      return false;
    }

  return true;
}

int main(int argc, char **argv)
{
  if (argc != 3)
    {
      fprintf(stderr, "usage: equal-cpt <path1> <path2>\n");
      return EXIT_FAILURE;
    }

  cpt_t *cpt1;
  bool equal = false;

  if ((cpt1 = cpt_read(argv[1])) != NULL)
    {
      cpt_t *cpt2;

      if ((cpt2 = cpt_read(argv[2])) != NULL)
        {
          equal = cpt_equal(cpt1, cpt2);
          cpt_destroy(cpt2);
        }
      else
        fprintf(stderr, "failed read of %s\n", argv[2]);

      cpt_destroy(cpt1);
    }
  else
    fprintf(stderr, "failed read of %s\n", argv[1]);

  return equal ? EXIT_SUCCESS : EXIT_FAILURE;
}
