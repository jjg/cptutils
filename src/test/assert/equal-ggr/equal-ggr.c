#include <cptutils/ggr-read.h>
#include <cptutils/ggr.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#define F_EPS 1e-6

static bool float_equal(double f1, double f2)
{
  if (fabs(f1 - f2) > F_EPS)
    {
      printf("%lf != %lf\n", f1, f2);
      return false;
    }

  return true;
}

static bool seg_equal(const ggr_segment_t *s1, const ggr_segment_t *s2)
{
  if (! float_equal(s1->left, s2->left))
    {
      printf("left\n");
      return false;
    }

  if (! float_equal(s1->middle, s2->middle))
    {
      printf("middle\n");
      return false;
    }

  if (! float_equal(s1->right, s2->right))
    {
      printf("right\n");
      return false;
    }

  if (! float_equal(s1->r0, s2->r0))
    {
      printf("r0\n");
      return false;
    }

  if (! float_equal(s1->g0, s2->g0))
    {
      printf("g0\n");
      return false;
    }

  if (! float_equal(s1->b0, s2->b0))
    {
      printf("b0\n");
      return false;
    }

  if (! float_equal(s1->a0, s2->a0))
    {
      printf("a0\n");
      return false;
    }

  if (! float_equal(s1->r1, s2->r1))
    {
      printf("r1\n");
      return false;
    }

  if (! float_equal(s1->g1, s2->g1))
    {
      printf("g1\n");
      return false;
    }

  if (! float_equal(s1->b1, s2->b1))
    {
      printf("b1\n");
      return false;
    }

  if (! float_equal(s1->a1, s2->a1))
    {
      printf("a1\n");
      return false;
    }

  if (s1->type != s2->type)
    {
      printf("type\n");
      return false;
    }

  if (s1->color != s2->color)
    {
      printf("color\n");
      return false;
    }

  if (s1->ect_left != s2->ect_left)
    {
      printf("ect-left\n");
      return false;
    }

  if (s1->ect_right != s2->ect_right)
    {
      printf("ect-right\n");
      return false;
    }

  return true;
}

static bool segs_equal(const ggr_segment_t *s1, const ggr_segment_t *s2, int n)
{
  if (s1)
    {
      if (s2)
        {
          if (! seg_equal(s1, s2))
            {
              printf("segment %i\n", n);
              return false;
            }
          else
            return segs_equal(s1->next, s2->next, n + 1);
        }
      else
        {
          printf("segment %i: present/absent\n", n);
          return false;
        }
    }
  else
    {
      if (s2)
        {
          printf("segment %i: absent/present\n", n);
          return false;
        }
    }

  return true;
}

static bool segments_equal(const ggr_segment_t *s1, const ggr_segment_t *s2)
{
  return segs_equal(s1, s2, 1);
}

static bool name_equal(const char *n1, const char *n2)
{
  if (n1 == NULL)
    {
      if (n2 == NULL)
        return true;
      else
        {
          printf("NULL != %s\n", n2);
          return false;
        }
    }
  else
    {
      if (n2 == NULL)
        {
          printf("%s != NULL\n", n1);
          return false;
        }
      else
        {
          if (strcmp(n1, n2) == 0)
            return true;
          else
            {
              printf("%s != %s\n", n1, n2);
              return false;
            }
        }
    }
}

static bool ggr_equal(const ggr_t *ggr1, const ggr_t *ggr2)
{
  if (! name_equal(ggr1->name, ggr2->name))
    {
      printf("name\n");
      return false;
    }

  if (! segments_equal(ggr1->segments, ggr2->segments))
    {
      printf("segments\n");
      return false;
    }

  return true;
}

int main(int argc, char **argv)
{
  if (argc != 3)
    {
      fprintf(stderr, "usage: equal-ggr <path1> <path2>\n");
      return EXIT_FAILURE;
    }

  ggr_t *ggr1;
  bool equal = false;

  if ((ggr1 = ggr_read(argv[1])) != NULL)
    {
      ggr_t *ggr2;

      if ((ggr2 = ggr_read(argv[2])) != NULL)
        {
          equal = ggr_equal(ggr1, ggr2);
          ggr_destroy(ggr2);
        }
      else
        fprintf(stderr, "failed read of %s\n", argv[2]);

      ggr_destroy(ggr1);
    }
  else
    fprintf(stderr, "failed read of %s\n", argv[1]);

  return equal ? EXIT_SUCCESS : EXIT_FAILURE;
}
