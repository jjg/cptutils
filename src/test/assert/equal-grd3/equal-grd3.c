#include <cptutils/grd3-read.h>
#include <cptutils/grd3.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

static bool ver_equal(const int *v1, const int *v2)
{
  if (v1[0] != v2[0])
    {
      printf("major version: %i != %i\n", v1[0], v2[0]);
      return false;
    }

  if (v1[1] != v2[1])
    {
      printf("minor version: %i != %i\n", v1[1], v2[1]);
      return false;
    }

  return true;
}

static bool rgb_equal(const grd3_rgbseg_t *s1, const grd3_rgbseg_t *s2)
{
  if (s1->z != s2->z)
    {
      printf("z: %hu != %hu\n", s1->z, s2->z);
      return false;
    }

  if (s1->r != s2->r)
    {
      printf("red: %hu != %hu\n", s1->r, s2->r);
      return false;
    }

  if (s1->g != s2->g)
    {
      printf("green: %hu != %hu\n", s1->g, s2->g);
      return false;
    }

  if (s1->b != s2->b)
    {
      printf("blue: %hu != %hu\n", s1->b, s2->b);
      return false;
    }

  if (s1->midpoint != s2->midpoint)
    {
      printf("midpoint: %hu != %hu\n", s1->midpoint, s2->midpoint);
      return false;
    }

  return true;
}

static bool rgbs_equal(const grd3_t *g1, const grd3_t *g2)
{
  if (g1->rgb.n != g2->rgb.n)
    {
      printf("rgb stops: %i != %i\n", g1->rgb.n, g2->rgb.n);
      return false;
    }

  size_t n = g1->rgb.n;

  for (size_t i = 0 ; i < n ; i++)
    if (! rgb_equal(g1->rgb.seg + i, g2->rgb.seg + i))
      {
        printf("rgb stop %zi\n", i + 1);
        return false;
      }

  return true;
}

static bool op_equal(const grd3_opseg_t *s1, const grd3_opseg_t *s2)
{
  if (s1->z != s2->z)
    {
      printf("z: %hu != %hu\n", s1->z, s2->z);
      return false;
    }

  if (s1->opacity != s2->opacity)
    {
      printf("opacity: %hu != %hu\n", s1->opacity, s2->opacity);
      return false;
    }

  if (s1->midpoint != s2->midpoint)
    {
      printf("midpoint: %hu != %hu\n", s1->midpoint, s2->midpoint);
      return false;
    }

  return true;
}

static bool ops_equal(const grd3_t *g1, const grd3_t *g2)
{
  if (g1->op.n != g2->op.n)
    {
      printf("opacity stops: %i != %i\n", g1->op.n, g2->op.n);
      return false;
    }

  size_t n = g1->op.n;

  for (size_t i = 0 ; i < n ; i++)
    if (! op_equal(g1->op.seg + i, g2->op.seg + i))
      {
        printf("opacity stop %zi\n", i + 1);
        return false;
      }

  return true;
}

static bool grd3_equal(const grd3_t *g1, const grd3_t *g2)
{
  if (! ver_equal(g1->ver, g2->ver))
    {
      printf("version\n");
      return false;
    }

  if (! rgbs_equal(g1, g2))
    {
      printf("rgb stops\n");
      return false;
    }

  if (! ops_equal(g1, g2))
    {
      printf("opacity stops\n");
      return false;
    }

  return true;
}

int main(int argc, char **argv)
{
  if (argc != 3)
    {
      fprintf(stderr, "usage: equal-grd3 <path1> <path2>\n");
      return EXIT_FAILURE;
    }

  grd3_t *g1;
  bool equal = false;

  if ((g1 = grd3_read(argv[1])) != NULL)
    {
      grd3_t *g2;

      if ((g2 = grd3_read(argv[2])) != NULL)
        {
          equal = grd3_equal(g1, g2);
          grd3_destroy(g2);
        }
      else
        fprintf(stderr, "failed read of %s\n", argv[2]);

      grd3_destroy(g1);
    }
  else
    fprintf(stderr, "failed read of %s\n", argv[1]);

  return equal ? EXIT_SUCCESS : EXIT_FAILURE;
}
