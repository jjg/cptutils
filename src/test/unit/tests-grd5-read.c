#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grd5-read.h"
#include "fixture.h"

#include <cptutils/grd5-read.h>


CU_TestInfo tests_grd5_read[] = {
  {"fixtures", test_grd5_read_fixtures},
  {"handling of grd3 files", test_grd5_read_grd3},
  {"file does not exist", test_grd5_read_nofile},
  CU_TEST_INFO_NULL,
};

void test_grd5_read_fixtures(void)
{
  size_t n = 1024;
  char buf[n];
  const char *files[] = {
    "my-custom-gradient-3-rgb.grd",
    "my-custom-gradient-3-hsb.grd",
    "my-custom-gradient-3-lab.grd",
    "neverhappens-titi-montoya.grd",
    "short-book-colours.grd"
  };
  size_t nfile = sizeof(files) / sizeof(char*);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      CU_ASSERT_FATAL(fixture(buf, n, "grd5", files[i]) < n);

      grd5_t *grd5 = grd5_read(buf);

      CU_ASSERT_PTR_NOT_NULL(grd5);
      grd5_destroy(grd5);
    }
}

void test_grd5_read_grd3(void)
{
  size_t n = 1024;
  char buf[n];

  CU_ASSERT_FATAL(fixture(buf, n, "grd5", "ES_Coffe.grd") < n);

  grd5_t *grd5 = grd5_read(buf);

  CU_ASSERT_PTR_NULL(grd5);
}

void test_grd5_read_nofile(void)
{
  grd5_t *grd5 = grd5_read("tmp/no-such-file.grd");

  CU_ASSERT_PTR_NULL(grd5);
}
