#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grd3-read.h"
#include "fixture.h"

#include <cptutils/grd3-read.h>


CU_TestInfo tests_grd3_read[] = {
  {"fixtures", test_grd3_read_fixtures},
  {"file does not exist", test_grd3_read_nofile},
  CU_TEST_INFO_NULL,
};

void test_grd3_read_fixtures(void)
{
  size_t n = 1024;
  char path[n];
  const char *files[] = {
    "accented.jgd",
    "test.1.jgd",
    "test.2.jgd",
    "test.3.jgd",
    "test.4.jgd",
    "test.5.jgd"
  };
  size_t nfile = sizeof(files) / sizeof(char*);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      CU_ASSERT(fixture(path, n, "grd3", files[i]) < n);

      grd3_t *grd3 = grd3_read(path);

      CU_ASSERT_PTR_NOT_NULL(grd3);
      grd3_destroy(grd3);
    }
}

void test_grd3_read_nofile(void)
{
  CU_ASSERT_PTR_NULL(grd3_read("/no/such/file"));
}
