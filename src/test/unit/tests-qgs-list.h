#include <CUnit/CUnit.h>

extern CU_TestInfo tests_qgs_list[];

void test_qgs_list_new(void);
void test_qgs_list_destroy_null(void);
void test_qgs_list_next(void);
void test_qgs_list_revert(void);
void test_qgs_list_each(void);
void test_qgs_list_find(void);
void test_qgs_list_entry(void);
