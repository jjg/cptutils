#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-cpt-coerce.h"
#include "fixture-cpt.h"

#include <cptutils/cpt-coerce.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_cpt_coerce[] = {
  {"coerce model, RGB", test_cpt_coerce_rgb},
  {"coerce model, HSV", test_cpt_coerce_hsv},
  {"coerce model, none", test_cpt_coerce_none},
  CU_TEST_INFO_NULL,
};

void test_cpt_coerce_rgb(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT4-gebco.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT_EQUAL(cpt_coerce_model(cpt, coerce_rgb), 0);
  cpt_destroy(cpt);
}

void test_cpt_coerce_hsv(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT4-gebco.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT_EQUAL(cpt_coerce_model(cpt, coerce_hsv), 0);
  cpt_destroy(cpt);
}

void test_cpt_coerce_none(void)
{
  cpt_t *cpt = load_cpt_fixture("GMT4-gebco.cpt");
  CU_ASSERT_PTR_NOT_NULL_FATAL(cpt);
  CU_ASSERT_EQUAL(cpt_coerce_model(cpt, coerce_none), 0);
  cpt_destroy(cpt);
}
