#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-qgs-list-write.h"

#include <cptutils/qgs-list-write.h>

#include <unistd.h>
#include <stdio.h>


CU_TestInfo tests_qgs_list_write[] = {
  {"minimal", test_qgs_list_write_minimal},
  {"multiple", test_qgs_list_write_multiple},
  CU_TEST_INFO_NULL,
};

void test_qgs_list_write_minimal(void)
{
  qgs_list_t *list = qgs_list_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  qgs_t *qgs = qgs_list_next(list);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgs);
  CU_ASSERT_EQUAL(qgs_set_type(qgs, QGS_TYPE_INTERPOLATED), 0);
  CU_ASSERT_EQUAL(qgs_set_name(qgs, "froob"), 0);
  CU_ASSERT_EQUAL(qgs_alloc_entries(qgs, 2), 0);

  {
    rgb_t rgb = { 0, 0, 0 };
    qgs_entry_t entry = {
      .rgb = rgb,
      .opacity = 255,
      .value = 0.0,
    };
    CU_TEST_FATAL(qgs_set_entry(qgs, 0, &entry) == 0);
  }

  {
    rgb_t rgb = { 255, 255, 255 };
    qgs_entry_t entry = {
      .rgb = rgb,
      .opacity = 255,
      .value = 1.0,
    };
    CU_TEST_FATAL(qgs_set_entry(qgs, 1, &entry) == 0);
  }

  char
    dir[] = "tmp/test-qgswrite-minimal-XXXXXX",
    file[] = "tmp.qgs";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(qgs_list_write(list, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);
  qgs_list_destroy(list);
}

void test_qgs_list_write_multiple(void)
{
  qgs_list_t *list = qgs_list_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  qgs_t
    *qgs1 = qgs_list_next(list),
    *qgs2 = qgs_list_next(list);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgs1);
  CU_ASSERT_EQUAL(qgs_set_type(qgs1, QGS_TYPE_INTERPOLATED), 0);
  CU_ASSERT_EQUAL(qgs_set_name(qgs1, "froob"), 0);
  CU_ASSERT_EQUAL(qgs_alloc_entries(qgs1, 2), 0);

  CU_ASSERT_PTR_NOT_NULL_FATAL(qgs2);
  CU_ASSERT_EQUAL(qgs_set_type(qgs2, QGS_TYPE_INTERPOLATED), 0);
  CU_ASSERT_EQUAL(qgs_set_name(qgs2, "groob"), 0);
  CU_ASSERT_EQUAL(qgs_alloc_entries(qgs2, 2), 0);

  {
    rgb_t rgb = { 0, 0, 0 };
    qgs_entry_t entry = {
      .rgb = rgb,
      .opacity = 255,
      .value = 0.0,
    };
    CU_TEST_FATAL(qgs_set_entry(qgs1, 0, &entry) == 0);
    CU_TEST_FATAL(qgs_set_entry(qgs2, 0, &entry) == 0);
  }

  {
    rgb_t rgb = { 255, 255, 255 };
    qgs_entry_t entry = {
      .rgb = rgb,
      .opacity = 255,
      .value = 1.0,
    };
    CU_TEST_FATAL(qgs_set_entry(qgs2, 1, &entry) == 0);
    CU_TEST_FATAL(qgs_set_entry(qgs2, 1, &entry) == 0);
  }

  char
    dir[] = "tmp/test-qgswrite-multiple-XXXXXX",
    file[] = "tmp.qgs";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(qgs_list_write(list, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);
  qgs_list_destroy(list);
}
