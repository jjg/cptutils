#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-u32map.h"

#include <cptutils/u32map.h>


CU_TestInfo tests_u32map[] = {
  {"new", test_u32map_new},
  {"put, absent", test_u32map_put_absent},
  {"put, present", test_u32map_put_present},
  {"put, expand", test_u32map_put_expand},
  {"get, empty", test_u32map_get_empty},
  {"get, absent", test_u32map_get_absent},
  {"get. present", test_u32map_get_present},
  {"update, absent", test_u32map_update_absent},
  {"update, error", test_u32map_update_error},
  {"update, increment", test_u32map_update_increment},
  {"size", test_u32map_size},
  {"collisions", test_u32map_collisions},
  {"capacity", test_u32map_capacity},
  {"each", test_u32map_each},
  CU_TEST_INFO_NULL,
};

static const char *words[] = {
  "pigeon",
  "robin",
  "duck",
  "goose",
  "falcon",
  "raven",
  "blackbird",
  "pigeon"
};
static const size_t nword = sizeof(words) / sizeof(char*);

void test_u32map_new(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);
  u32map_destroy(map);
}

void test_u32map_put_absent(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);
  CU_ASSERT_EQUAL(u32map_put(map, "Hello", 7), 0);
  u32map_destroy(map);
}

void test_u32map_put_present(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);
  CU_ASSERT_EQUAL_FATAL(u32map_put(map, "Hello", 7), 0);
  CU_ASSERT_EQUAL(u32map_put(map, "Hello", 8), 0);
  u32map_destroy(map);
}

void test_u32map_put_expand(void)
{
  u32map_t *map = u32map_new(4);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);
  CU_ASSERT_FATAL(nword > u32map_capacity(map));

  for (size_t i = 0 ; i < nword ; i++)
    CU_ASSERT_EQUAL(u32map_put(map, words[i], i), 0);

  u32map_destroy(map);
}

void test_u32map_get_empty(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);
  uint32_t value = 0;
  CU_ASSERT_EQUAL(u32map_get(map, "meh", &value), 1);
  CU_ASSERT_EQUAL(value, 0);
  u32map_destroy(map);
}

void test_u32map_get_absent(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);

  const char *s1 = "Hello", *s2 = "meh";
  uint32_t value = 0;

  CU_ASSERT_EQUAL_FATAL(u32map_put(map, s1, 7), 0);
  CU_ASSERT_EQUAL(u32map_get(map, s2, &value), 1);
  CU_ASSERT_EQUAL(value, 0);

  u32map_destroy(map);
}

void test_u32map_get_present(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);

  const char *s1 = "Hello", *s2 = "Hello";
  uint32_t value = 0;

  CU_ASSERT_EQUAL_FATAL(u32map_put(map, s1, 7), 0);
  CU_ASSERT_EQUAL(u32map_get(map, s2, &value), 0);
  CU_ASSERT_EQUAL(value, 7);

  u32map_destroy(map);
}

static int f_update_absent(uint32_t v1, uint32_t *v2, void *arg)
{
  v2 = 0;
  return 0;
}

void test_u32map_update_absent(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);
  CU_ASSERT_EQUAL_FATAL(u32map_put(map, "hello", 7), 0);
  CU_ASSERT_EQUAL_FATAL(u32map_put(map, "goodbye", 8), 0);

  CU_ASSERT_EQUAL(u32map_update(map, "no", f_update_absent, NULL), -1);

  u32map_destroy(map);
}

static int f_update_error(uint32_t v1, uint32_t *v2, void *arg)
{
  return 42;
}

void test_u32map_update_error(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);
  CU_ASSERT_EQUAL_FATAL(u32map_put(map, "hello", 7), 0);
  CU_ASSERT_EQUAL_FATAL(u32map_put(map, "goodbye", 8), 0);

  CU_ASSERT_EQUAL(u32map_update(map, "hello", f_update_error, NULL), 42);

  uint32_t value = 0;
  CU_ASSERT_EQUAL(u32map_get(map, "hello", &value), 0);
  CU_ASSERT_EQUAL(value, 7);

  u32map_destroy(map);
}

static int f_update_increment(uint32_t v1, uint32_t *v2, void *arg)
{
  *v2 = v1 + 1;

  return 0;
}

void test_u32map_update_increment(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);

  for (size_t i = 0 ; i < nword ; i++)
    CU_ASSERT_EQUAL(u32map_put(map, words[i], 0), 0);

  for (size_t i = 0 ; i < nword ; i++)
    CU_ASSERT_EQUAL(u32map_update(map, words[i], f_update_increment, NULL), 0);

  uint32_t value;

  CU_ASSERT_EQUAL(u32map_get(map, "pigeon", &value), 0);
  CU_ASSERT_EQUAL(value, 2);

  CU_ASSERT_EQUAL(u32map_get(map, "robin", &value), 0);
  CU_ASSERT_EQUAL(value, 1);

  u32map_destroy(map);
}

void test_u32map_size(void)
{
  u32map_t *map = u32map_new(32);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);

  const char *s1 = "Hello", *s2 = "Hello";

  CU_ASSERT_EQUAL(u32map_size(map), 0);
  CU_ASSERT_EQUAL(u32map_put(map, s1, 7), 0);
  CU_ASSERT_EQUAL(u32map_size(map), 1);
  CU_ASSERT_EQUAL(u32map_put(map, s2, 8), 0);
  CU_ASSERT_EQUAL(u32map_size(map), 1);

  u32map_destroy(map);
}

void test_u32map_collisions(void)
{
  u32map_t *map = u32map_new(4);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);

  CU_ASSERT_EQUAL(u32map_collisions(map), 0);

  for (size_t i = 0 ; i < nword ; i++)
    CU_ASSERT_EQUAL(u32map_put(map, words[i], i), 0);

  CU_ASSERT_NOT_EQUAL(u32map_collisions(map), 0);

  u32map_destroy(map);
}

void test_u32map_capacity(void)
{
  u32map_t *map = u32map_new(4);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);

  size_t capacity = u32map_capacity(map);
  CU_ASSERT_FATAL(nword >= capacity);

  for (size_t i = 0 ; i < capacity ; i++)
    {
      CU_ASSERT_EQUAL(u32map_put(map, words[i], i), 0);
      CU_ASSERT_EQUAL(u32map_capacity(map), capacity);
    }

  u32map_destroy(map);
}

static int fsum(const char *key, uint32_t val, void *arg)
{
  if (key == NULL)
    return 1;

  uint32_t *sum = arg;
  *sum += val;

  return 0;
}

void test_u32map_each(void)
{
  u32map_t *map = u32map_new(4);
  CU_ASSERT_PTR_NOT_NULL_FATAL(map);

  for (size_t i = 0 ; i < nword ; i++)
    CU_ASSERT_EQUAL(u32map_put(map, words[i], i + 1), 0);

  uint32_t sum = 0;
  CU_ASSERT_EQUAL(u32map_each(map, fsum, &sum), 0);
  CU_ASSERT_EQUAL(sum, 35);

  u32map_destroy(map);
}
