#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cpt_read[];

void test_cpt_read_fixtures(void);
void test_cpt_read_range(void);
void test_cpt_read_hinge_explicit(void);
void test_cpt_read_hinge_soft(void);
void test_cpt_read_hinge_hard(void);
void test_cpt_read_categorical(void);
void test_cpt_read_nofile(void);
void test_cpt_read_hsv_regression(void);
