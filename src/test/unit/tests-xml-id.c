#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-xml-id.h"

#include <cptutils/xml-id.h>

#include <stdlib.h>


CU_TestInfo tests_xml_id[] = {
  {"sanitise, NULL", test_xml_id_sanitise_null},
  {"sanitise, empty", test_xml_id_sanitise_empty},
  {"sanitise, nice", test_xml_id_sanitise_nice},
  {"sanitise, bad", test_xml_id_sanitise_bad},
  CU_TEST_INFO_NULL,
};

void test_xml_id_sanitise_null(void)
{
  char *id = xml_id_sanitise(NULL);
  CU_ASSERT_PTR_NULL(id);
}

void test_xml_id_sanitise_empty(void)
{
  char *id = xml_id_sanitise("");
  CU_ASSERT_PTR_NULL(id);
}

void test_xml_id_sanitise_nice(void)
{
  char *id;

  id = xml_id_sanitise("a");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-a");
  free(id);

  id = xml_id_sanitise("a-b");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-a-b");
  free(id);

  id = xml_id_sanitise("a:b");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-a-b");
  free(id);

  id = xml_id_sanitise("a_b");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-a-b");
  free(id);

  id = xml_id_sanitise("_a");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-a");
  free(id);

  id = xml_id_sanitise("*&£a___b_£");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-a-b");
  free(id);

  id = xml_id_sanitise("__7__a7__b__");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-7-a7-b");
  free(id);

  id = xml_id_sanitise("-");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id");
  free(id);

  id = xml_id_sanitise("7");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-7");
  free(id);

  id = xml_id_sanitise("777777");
  CU_ASSERT_PTR_NOT_NULL_FATAL(id);
  CU_ASSERT_STRING_EQUAL(id, "id-777777");
  free(id);
}

void test_xml_id_sanitise_bad(void)
{
  char *id;

  id = xml_id_sanitise("");
  CU_ASSERT_PTR_NULL(id);

  id = xml_id_sanitise(NULL);
  CU_ASSERT_PTR_NULL(id);
}
