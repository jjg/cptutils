#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fixture.h"
#include "fixture-ggr.h"

#include <cptutils/ggr-read.h>

#include <stdlib.h>


ggr_t* load_ggr_fixture(const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "ggr", file) >= n)
    return NULL;

  return ggr_read(path);
}
