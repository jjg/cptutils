#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-pg-helper.h"

#include <stdlib.h>


typedef struct
{
  double z;
  rgb_t rgb;
  unsigned char alpha;
} build_pg_stop_t;

pg_t* build_pg(void)
{
  pg_t *pg = pg_new();

  if (pg == NULL)
    return NULL;

  /*
    we use decreasing stops here to force pg_write to reverse
    the direction, this gives us a bit more coverage
  */

  build_pg_stop_t stops[] = {
    {1.0, {0, 20, 30}, 255},
    {0.5, {20, 30, 0}, 200},
    {0.0, {30, 0, 20}, 100}
  };
  size_t nstop = sizeof(stops) / sizeof(build_pg_stop_t);

  int err = 0;

  for (size_t i = 0 ; i < nstop ; i++)
    err += pg_push(pg, stops[i].z, stops[i].rgb, stops[1].alpha);

  if (err == 0)
    return pg;

  pg_destroy(pg);
  return NULL;
}
