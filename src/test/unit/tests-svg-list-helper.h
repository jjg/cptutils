#include <cptutils/svg-list.h>

#include <stddef.h>

svg_list_t* build_svg_list(size_t);
