#include <CUnit/CUnit.h>

extern CU_TestInfo tests_path_base[];

void test_path_base_specific(void);
void test_path_base_wildcard(void);
void test_path_base_null(void);
