#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grdx-svg.h"

#include <cptutils/grdx-svg.h>


CU_TestInfo tests_grdx_svg[] = {
  {"2-segment conversion", test_grdx_svg_2seg},
  CU_TEST_INFO_NULL,
};

void test_grdx_svg_2seg(void)
{
  rgb_stop_t rgb_stops[] =
    {
      {4096, 1.0, 0.50, 0.0},
      {2048, 0.5, 0.25, 0.0},
      {0,    0.0, 0.00, 0.0}
    };
  size_t nrgb = sizeof(rgb_stops) / sizeof(rgb_stop_t);
  gstack_t *rgb_stack = gstack_new(sizeof(rgb_stop_t), nrgb, nrgb);

  CU_ASSERT_PTR_NOT_NULL_FATAL(rgb_stack);

  for (size_t i = 0 ; i < nrgb ; i++)
    CU_ASSERT_EQUAL_FATAL(gstack_push(rgb_stack, rgb_stops + i), 0);

  op_stop_t op_stops[] =
    {
      {4096, 0.5},
      {0,    0.5}
    };
  size_t nop = sizeof(op_stops) / sizeof(op_stop_t);
  gstack_t *op_stack = gstack_new(sizeof(op_stop_t), nop, nop);

  CU_ASSERT_PTR_NOT_NULL_FATAL(op_stack);

  for (size_t i = 0 ; i < nop ; i++)
    CU_ASSERT_EQUAL_FATAL(gstack_push(op_stack, op_stops + i), 0);

  svg_t *svg = svg_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(svg);

  CU_ASSERT_EQUAL(grdxsvg(rgb_stack, op_stack, svg), 0);
  CU_ASSERT_EQUAL(svg_num_stops(svg), 3);

  svg_destroy(svg);

  gstack_destroy(op_stack);
  gstack_destroy(rgb_stack);
}
