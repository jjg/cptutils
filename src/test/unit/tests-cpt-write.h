#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cpt_write[];

void test_cpt_write_fixtures(void);
void test_cpt_write_nosuchdir(void);

void test_cpt_write_opt_null(void);

void test_cpt_write_opt_colour_join_space_rgb(void);
void test_cpt_write_opt_colour_join_space_hsv(void);
void test_cpt_write_opt_colour_join_glyph_rgb(void);
void test_cpt_write_opt_colour_join_glyph_hsv(void);

void test_cpt_write_opt_model_case_upper_rgb(void);
void test_cpt_write_opt_model_case_upper_hsv(void);
void test_cpt_write_opt_model_case_lower_rgb(void);
void test_cpt_write_opt_model_case_lower_hsv(void);

void test_cpt_write_options_gmt4(void);
void test_cpt_write_options_gmt5(void);
void test_cpt_write_options_gmt6(void);
void test_cpt_write_options_default(void);
void test_cpt_write_options_bad_version(void);
