#include <CUnit/CUnit.h>

extern CU_TestInfo tests_pg[];

void test_pg_create(void);
void test_pg_push(void);
void test_pg_clone(void);
void test_pg_destroy_null(void);
