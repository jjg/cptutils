#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grd5.h"
#include "tests-grd5-helper.h"

#include <cptutils/grd5.h>


CU_TestInfo tests_grd5[] = {
  {"model lookup", test_grd5_model},
  CU_TEST_INFO_NULL,
};

static void string_code(const char *name, int code)
{
  grd5_string_t *str = build_grd5_string(name);
  CU_ASSERT_PTR_NOT_NULL(str);
  CU_ASSERT_EQUAL(grd5_model(str), code);
  grd5_string_destroy(str);
}

void test_grd5_model(void)
{
  string_code("RGBC", GRD5_MODEL_RGB);
  string_code("HSBC", GRD5_MODEL_HSB);
  string_code("LbCl", GRD5_MODEL_LAB);
  string_code("CMYC", GRD5_MODEL_CMYC);
  string_code("Grsc", GRD5_MODEL_GRSC);
  string_code("FrgC", GRD5_MODEL_FRGC);
  string_code("BckC", GRD5_MODEL_BCKC);
  string_code("BkCl", GRD5_MODEL_BOOK);
  string_code("UnsC", GRD5_MODEL_UNSC);
}
