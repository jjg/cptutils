#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-png.h"

#include <cptutils/png.h>


CU_TestInfo tests_png[] = {
  {"new", test_png_new},
  {"new, zero width", test_png_new_zerowidth},
  {"new, zero height", test_png_new_zeroheight},
  {"destroy NULL", test_png_destroy_null},
  CU_TEST_INFO_NULL,
};

void test_png_new(void)
{
  png_t *png = png_new(3, 4);
  CU_ASSERT_PTR_NOT_NULL_FATAL(png);
  CU_ASSERT_PTR_NOT_NULL(png->row);
  CU_ASSERT_EQUAL(png->width, 3);
  CU_ASSERT_EQUAL(png->height, 4);
  png_destroy(png);
}

void test_png_new_zerowidth(void)
{
  png_t *png = png_new(0, 4);
  CU_ASSERT_PTR_NULL(png);
}

void test_png_new_zeroheight(void)
{
  png_t *png = png_new(4, 0);
  CU_ASSERT_PTR_NULL(png);
}

void test_png_destroy_null(void)
{
  png_destroy(NULL);
}
