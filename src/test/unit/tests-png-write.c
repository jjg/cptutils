#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-png-write.h"
#include "tests-png-helper.h"

#include <cptutils/png-write.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_png_write[] = {
  {"simple", test_png_write_simple},
  {"no such directory", test_png_write_nosuchdir},
  CU_TEST_INFO_NULL
};

void test_png_write_simple(void)
{
  png_t *png = build_png();
  CU_ASSERT_PTR_NOT_NULL_FATAL(png);

  char
    dir[] = "tmp/test-pngwrite-XXXXXX",
    file[] = "file.png";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(png_write(png, path, "name"), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);
  png_destroy(png);
}

void test_png_write_nosuchdir(void)
{
  png_t *png = build_png();
  CU_ASSERT_PTR_NOT_NULL_FATAL(png);

  const char path[] = "tmp/nosuchdir/file.png";

  CU_ASSERT_NOT_EQUAL_FATAL(access(path, F_OK), 0);
  CU_ASSERT_NOT_EQUAL(png_write(png, path, "name"), 0);
  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);

  png_destroy(png);
}
