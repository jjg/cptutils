#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-svg-list-helper.h"

#include <stdio.h>


svg_list_t* build_svg_list(size_t n)
{
  svg_list_t *svglist;

  if ((svglist = svg_list_new()) == NULL)
    return NULL;

  for (size_t i = 0 ; i < n ; i++)
    {
      svg_t *svg;

      if ( (svg = svg_list_next(svglist)) == NULL )
	return NULL;

      sprintf((char*)svg->name, "id-%02zi", i);
    }

  return svglist;
}
