#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-tpm-write.h"

#include <cptutils/tpm-write.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_tpm_write[] = {
  {"simple example", test_tpm_write_simple },
  CU_TEST_INFO_NULL,
};

void test_tpm_write_simple(void)
{
  tpm_t *tpm = tpm_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(tpm);

  tpm_point_t point = {0};

  CU_ASSERT_EQUAL(tpm_push(tpm, &point), 0);
  CU_ASSERT_EQUAL(tpm_push(tpm, &point), 0);

  comment_t *comment = tpm_comment(tpm);
  CU_ASSERT_PTR_NOT_NULL_FATAL(tpm);

  CU_ASSERT_EQUAL(comment_push(comment, "hello"), 0);
  CU_ASSERT_EQUAL(comment_push(comment, "big boy"), 0);

  char
    dir[] = "tmp/test-tpm-write-XXXXXX",
    file[] = "tmp.tpm";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(tpm_write(tpm, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);
  tpm_destroy(tpm);
}
