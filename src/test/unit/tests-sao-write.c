#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-sao-write.h"
#include "tests-sao-helper.h"

#include <cptutils/sao-write.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_sao_write[] = {
  {"write", test_sao_write_write},
  {"no such directory", test_sao_write_nosuchdir},
  CU_TEST_INFO_NULL,
};

void test_sao_write_write(void)
{
  sao_t *sao = build_sao();
  CU_ASSERT_PTR_NOT_NULL_FATAL(sao);

  char
    dir[] = "tmp/test-saowrite-XXXXXX",
    file[] = "tmp.sao";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(sao_write(sao, path, "froob"), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);
  sao_destroy(sao);
}

void test_sao_write_nosuchdir(void)
{
  sao_t *sao = build_sao();
  CU_ASSERT_PTR_NOT_NULL_FATAL(sao);

  const char path[] = "/no/such/directory";

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_NOT_EQUAL(sao_write(sao, path, "groob"), 0);
  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);

  sao_destroy(sao);
}
