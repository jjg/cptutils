#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-comment-list-helper.h"

#include <stdio.h>


comment_list_t* build_comment_list(size_t n)
{
  comment_list_t *list;

  if ((list = comment_list_new()) == NULL)
    return NULL;

  for (size_t i = 0 ; i < n ; i++)
    {
      comment_t *comment;

      if ((comment = comment_list_next(list)) == NULL)
	return NULL;

      for (size_t j = 0 ; j < 8 ; j++)
        {
          char line[16];
          snprintf(line, 16, "line-%02zi-%zi", i, j);

          if (comment_push(comment, line) != 0)
            return NULL;
        }

      if (comment_finalise(comment) != 0)
        return NULL;
    }

  return list;
}
