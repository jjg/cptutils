#include <CUnit/CUnit.h>

extern CU_TestInfo tests_svg[];

void test_svg_new(void);
void test_svg_append(void);
void test_svg_prepend(void);
void test_svg_each_stop(void);
void test_svg_num_stops(void);
void test_svg_explicit(void);
void test_svg_interpolate(void);
void test_svg_flatten(void);
void test_svg_complete(void);
void test_svg_basename(void);
