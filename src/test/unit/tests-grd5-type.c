#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-grd5-type.h"

#include <cptutils/grd5-type.h>


CU_TestInfo tests_grd5_type[] = {
  {"lookup", test_grd5_type_type},
  CU_TEST_INFO_NULL,
};

void test_grd5_type_type(void)
{
  CU_ASSERT_EQUAL(grd5_type("bool"), TYPE_BOOL);
  CU_ASSERT_EQUAL(grd5_type("doub"), TYPE_DOUBLE);
  CU_ASSERT_EQUAL(grd5_type("enum"), TYPE_ENUM);
  CU_ASSERT_EQUAL(grd5_type("long"), TYPE_LONG);
  CU_ASSERT_EQUAL(grd5_type("Objc"), TYPE_OBJECT);
  CU_ASSERT_EQUAL(grd5_type("tdta"), TYPE_TDTA);
  CU_ASSERT_EQUAL(grd5_type("TEXT"), TYPE_TEXT);
  CU_ASSERT_EQUAL(grd5_type("patt"), TYPE_PATTERN);
  CU_ASSERT_EQUAL(grd5_type("UntF"), TYPE_UNTF);
  CU_ASSERT_EQUAL(grd5_type("VlLs"), TYPE_VAR_LEN_LIST);
  CU_ASSERT_EQUAL(grd5_type("caca"), TYPE_UNKNOWN);
}
