#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-pg.h"

#include <cptutils/pg.h>
#include <cptutils/pg-type.h>


CU_TestInfo tests_pg[] = {
  {"create", test_pg_create},
  {"push", test_pg_push},
  {"clone", test_pg_clone},
  {"destroy NULL", test_pg_destroy_null},
  CU_TEST_INFO_NULL,
};

void test_pg_create(void)
{
  pg_t *pg = pg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pg);
  pg_destroy(pg);
}

void test_pg_push(void)
{
  pg_t *pg = pg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pg);

  double z = 0;
  rgb_t rgb = { 0, 100, 200 };
  unsigned char alpha = 255;

  CU_ASSERT_EQUAL(pg_push(pg, z, rgb, alpha), 0);

  pg_destroy(pg);
}

void test_pg_clone(void)
{
  pg_t *pg1 = pg_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pg1);

  pg_stop_t stops[] = {
    {0, {0, 0, 0}, 0},
    {1, {2, 3, 4}, 5},
    {2, {3, 4, 5}, 6}
  };
  size_t n = sizeof(stops) / sizeof(pg_stop_t);

  for (size_t i = 0 ; i < n ; i++)
    {
      int err =
        pg_push(pg1,
                stops[i].value,
                stops[i].rgb,
                stops[i].alpha);
      CU_ASSERT_EQUAL(err, 0);
    }

  pg_t *pg2 = pg_clone(pg1);

  CU_ASSERT_PTR_NOT_NULL_FATAL(pg2);
  CU_ASSERT_EQUAL(gstack_size(pg1->stack), gstack_size(pg2->stack));

  for (size_t i = 0 ; i < n ; i++)
    {
      pg_stop_t stop1, stop2;

      CU_ASSERT_EQUAL(gstack_pop(pg1->stack, &stop1), 0);
      CU_ASSERT_EQUAL(gstack_pop(pg2->stack, &stop2), 0);

      CU_ASSERT_EQUAL(stop1.value, stop2.value);
      CU_ASSERT_EQUAL(stop1.rgb.red, stop2.rgb.red);
      CU_ASSERT_EQUAL(stop1.rgb.green, stop2.rgb.green);
      CU_ASSERT_EQUAL(stop1.rgb.blue, stop2.rgb.blue);
      CU_ASSERT_EQUAL(stop1.alpha, stop2.alpha);
    }

  pg_destroy(pg1);
  pg_destroy(pg2);
}

void test_pg_destroy_null(void)
{
  pg_destroy(NULL);
}
