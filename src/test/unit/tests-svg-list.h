#include <CUnit/CUnit.h>

extern CU_TestInfo tests_svg_list[];

void test_svg_list_new(void);
void test_svg_list_destroy_null(void);
void test_svg_list_next(void);
void test_svg_list_revert(void);
void test_svg_list_each(void);
void test_svg_list_find(void);
void test_svg_list_entry(void);
