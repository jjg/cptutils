#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-ggr-write.h"
#include "fixture.h"
#include "fixture-ggr.h"

#include <cptutils/ggr-write.h>

#include <stdio.h>
#include <unistd.h>


CU_TestInfo tests_ggr_write[] = {
  {"fixtures", test_ggr_write_fixtures},
  {"no such directory", test_ggr_write_nosuchdir},
  CU_TEST_INFO_NULL
};

void test_ggr_write_fixtures(void)
{
  const char *ggrs[] = {
    "Sunrise.ggr",
    "mars.ggr",
    "Rounded_edge.ggr",
    "Wood_2.ggr"
  };
  size_t nfile = sizeof(ggrs) / sizeof(char*);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      ggr_t *ggr = load_ggr_fixture(ggrs[i]);
      CU_ASSERT_PTR_NOT_NULL_FATAL(ggr);

      char path[] = "tmp/test-write-ggr-XXXXXX";
      CU_ASSERT_NOT_EQUAL_FATAL(mkstemp(path), -1);

      CU_ASSERT_EQUAL(ggr_write(ggr, path), 0);

      unlink(path);
      ggr_destroy(ggr);
    }
}

void test_ggr_write_nosuchdir(void)
{
  ggr_t *ggr = load_ggr_fixture("Sunrise.ggr");
  CU_ASSERT_PTR_NOT_NULL_FATAL(ggr);

  const char path[] = "tmp//no/such/directory";

  CU_ASSERT_NOT_EQUAL_FATAL(access(path, F_OK), 0);
  CU_ASSERT_NOT_EQUAL(ggr_write(ggr, path), 0);

  ggr_destroy(ggr);
}
