#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-colour-helper.h"
#include "tests-cpt-fill-helper.h"


cpt_fill_t build_cpt_fill_grey(int g)
{
  cpt_fill_t fill = {
    .type = cpt_fill_grey,
    .grey = g
  };
  return fill;
}

cpt_fill_t build_cpt_fill_rgb(int r, int g, int b)
{
  cpt_fill_t fill = {
    .type = cpt_fill_colour_rgb,
    .colour = {
      .rgb = build_rgb(r, g, b)
    }
  };
  return fill;
}

cpt_fill_t build_cpt_fill_hsv(double h, double s, double v)
{
  cpt_fill_t fill = {
    .type = cpt_fill_colour_hsv,
    .colour = {
      .hsv = build_hsv(h, s, v)
    }
  };
  return fill;
}
