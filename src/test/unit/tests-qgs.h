#include <CUnit/CUnit.h>

extern CU_TestInfo tests_qgs[];

void test_qgs_create(void);
void test_qgs_set_type(void);
void test_qgs_set_name(void);
void test_qgs_alloc_entries(void);
void test_qgs_set_entry(void);
