#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-comment-list-process.h"
#include "tests-comment-list-helper.h"
#include "fixture.h"

#include <cptutils/comment-list-process.h>

#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_comment_list_process[] = {
  {"process, write", test_comment_list_process_write},
  {"process, read", test_comment_list_process_read},
  {"process, generate", test_comment_list_process_generate},
  {"process, none", test_comment_list_process_none},
  CU_TEST_INFO_NULL,
};

void test_comment_list_process_write(void)
{
  char path[] = "tmp/test-comment-list-process-write-XXXXXX";
  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  comment_list_t *list = build_comment_list(5);
  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  comment_opt_t opt = {
    .action = action_none,
    .path = {
      .input = NULL,
      .output = path
    }
  };
  CU_ASSERT_EQUAL(comment_list_process(list, &opt), 0);
  comment_list_destroy(list);

  CU_ASSERT_EQUAL(close(fd), 0);
  CU_ASSERT_EQUAL(unlink(path), 0);
}

void test_comment_list_process_read(void)
{
  size_t n = 1024;
  char path[n];
  const char *file = "elevation-comments.xco";

  CU_ASSERT_FATAL(fixture(path, n, "xco", file) < n);

  comment_list_t *list = build_comment_list(2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  comment_opt_t opt = {
    .action = action_read,
    .path = {
      .input = path,
      .output = NULL
    }
  };
  CU_ASSERT_EQUAL(comment_list_process(list, &opt), 0);
  CU_ASSERT_EQUAL(comment_list_size(list), 2);

  comment_list_destroy(list);
}

void test_comment_list_process_generate(void)
{
  comment_list_t *list = build_comment_list(2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  comment_opt_t opt = {
    .action = action_generate,
    .path = {
      .input = NULL,
      .output = NULL
    }
  };
  CU_ASSERT_EQUAL(comment_list_process(list, &opt), 0);
  CU_ASSERT_EQUAL(comment_list_size(list), 2);

  comment_list_destroy(list);
}

void test_comment_list_process_none(void)
{
  comment_list_t *list = build_comment_list(2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(list);

  comment_opt_t opt = {
    .action = action_none,
    .path = {
      .input = NULL,
      .output = NULL
    }
  };
  CU_ASSERT_EQUAL(comment_list_process(list, &opt), 0);
  CU_ASSERT_EQUAL(comment_list_size(list), 2);

  comment_list_destroy(list);
}
