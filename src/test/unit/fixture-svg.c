#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fixture-svg.h"
#include "fixture.h"

#include <cptutils/svg-list-read.h>

#include <stdlib.h>


svg_list_t* load_svg_fixture(const char *file)
{
  size_t n = 1024;
  char path[n];

  if (fixture(path, n, "svg", file) >= n)
    return NULL;

  return svg_list_read(path);
}
