#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-comment.h"
#include "fixture.h"

#include <cptutils/comment.h>

#include <stdlib.h>
#include <unistd.h>

CU_TestInfo tests_comment[] = {
  {"new", test_comment_new},
  {"destroy", test_comment_destroy},
  {"push", test_comment_push},
  {"process, write", test_comment_process_write},
  {"process, read", test_comment_process_read},
  {"process, generate", test_comment_process_generate},
  {"process, none", test_comment_process_none},
  CU_TEST_INFO_NULL,
};

void test_comment_new(void)
{
  comment_t *comment = comment_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);
  comment_destroy(comment);
}

void test_comment_destroy(void)
{
  comment_destroy(NULL);
}

void test_comment_push(void)
{
  comment_t *comment = comment_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);
  CU_ASSERT_EQUAL(comment_push(comment, "foo\n"), 0);
  CU_ASSERT_EQUAL(comment_push(comment, "bar"), 0);
  CU_ASSERT_EQUAL(comment_count(comment), 2);

  comment_destroy(comment);
}

void test_comment_process_write(void)
{
  char path[] = "tmp/test-comment-write-XXXXXX";
  int fd = mkstemp(path);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  comment_t *comment = comment_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);
  CU_ASSERT_EQUAL_FATAL(comment_push(comment, "foo\n"), 0);
  CU_ASSERT_EQUAL_FATAL(comment_push(comment, "bar"), 0);
  CU_ASSERT_EQUAL_FATAL(comment_finalise(comment), 0);

  comment_opt_t opt = {
    .action = action_none,
    .path = {
      .input = NULL,
      .output = path
    }
  };

  CU_ASSERT_EQUAL(comment_process(comment, &opt), 0);

  comment_destroy(comment);

  CU_ASSERT_EQUAL(close(fd), 0);
  CU_ASSERT_EQUAL(unlink(path), 0);
}

void test_comment_process_read(void)
{
  size_t n = 1024;
  char path[n];

  CU_ASSERT_FATAL(fixture(path, n, "xco", "realistic.xco") < n);

  comment_t *comment = comment_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);

  comment_opt_t opt = {
    .action = action_read,
    .path = {
      .input = path,
      .output = NULL
    }
  };

  CU_ASSERT_EQUAL(comment_process(comment, &opt), 0);
  CU_ASSERT_EQUAL(comment_count(comment), 3);

  comment_destroy(comment);
}

void test_comment_process_generate(void)
{
  comment_t *comment = comment_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);

  comment_opt_t opt = {
    .action = action_generate,
    .path = {
      .input = NULL,
      .output = NULL
    }
  };

  CU_ASSERT_EQUAL(comment_process(comment, &opt), 0);
  CU_ASSERT_EQUAL(comment_count(comment), 1);

  comment_destroy(comment);
}

void test_comment_process_none(void)
{
  comment_t *comment = comment_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(comment);

  comment_opt_t opt = {
    .action = action_none,
    .path = {
      .input = NULL,
      .output = NULL
    }
  };

  CU_ASSERT_EQUAL(comment_process(comment, &opt), 0);
  CU_ASSERT_EQUAL(comment_count(comment), 0);

  comment_destroy(comment);
}
