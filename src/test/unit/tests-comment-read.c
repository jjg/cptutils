#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-comment-read.h"
#include "fixture.h"

#include <cptutils/comment-read.h>

CU_TestInfo tests_comment_read[] = {
  {"fixtures", test_comment_read_fixtures},
  {"file does not exist", test_comment_read_nofile},
  CU_TEST_INFO_NULL,
};

void test_comment_read_fixtures(void)
{
  size_t n = 1024;
  char path[n];
  const char *files[] = {
    "single.xco",
    "realistic.xco"
  };
  size_t nfile = sizeof(files) / sizeof(char*);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      CU_ASSERT_FATAL(fixture(path, n, "xco", files[i]) < n);

      comment_t *comment = comment_read(path);
      CU_ASSERT_PTR_NOT_NULL_FATAL(comment);

      comment_destroy(comment);
    }
}

void test_comment_read_nofile(void)
{
  comment_t *comment = comment_read("tmp/no-such-file");
  CU_ASSERT_PTR_NULL(comment);
}
