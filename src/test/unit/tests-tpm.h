#include <CUnit/CUnit.h>

extern CU_TestInfo tests_tpm[];

void test_tpm_create(void);
void test_tpm_push(void);
void test_tpm_comment(void);
void test_tpm_manicure(void);
void test_tpm_destroy_null(void);
