#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-cpt-helper.h"


cpt_seg_t* build_segment(cpt_fill_t f0, cpt_fill_t f1, double z0, double z1)
{
  cpt_seg_t *cpt_seg = cpt_seg_new();

  cpt_seg->sample.left.val = z0;
  cpt_seg->sample.right.val = z1;

  cpt_seg->sample.left.fill = f0;
  cpt_seg->sample.right.fill = f1;

  return cpt_seg;
}
