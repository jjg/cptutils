#include <CUnit/CUnit.h>

extern CU_TestInfo tests_xml_id[];

void test_xml_id_sanitise_null(void);
void test_xml_id_sanitise_empty(void);
void test_xml_id_sanitise_nice(void);
void test_xml_id_sanitise_bad(void);
