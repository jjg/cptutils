#ifndef FIXTURE_H
#define FIXTURE_H

#include <stdlib.h>

int fixture(char*, size_t, const char*, const char*);

#endif
