#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-pg-write.h"
#include "tests-pg-helper.h"

#include <cptutils/pg-write.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


CU_TestInfo tests_pg_write[] = {
  {"simple", test_pg_write_simple},
  {"no such directory", test_pg_write_nosuchdir},
  CU_TEST_INFO_NULL
};

void test_pg_write_simple(void)
{
  pg_t *pg = build_pg();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pg);

  char
    dir[] = "tmp/test-pgwrite-XXXXXX",
    file[] = "file.pg";

  CU_ASSERT_PTR_NOT_NULL_FATAL(mkdtemp(dir));

  size_t path_len = strlen(dir) + strlen(file) + 2;
  char path[path_len];

  CU_ASSERT(snprintf(path, path_len, "%s/%s", dir, file) < path_len);

  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);
  CU_ASSERT_EQUAL(pg_write(pg, path), 0);
  CU_ASSERT_EQUAL(access(path, F_OK), 0);

  unlink(path);
  rmdir(dir);
  pg_destroy(pg);
}

void test_pg_write_nosuchdir(void)
{
  pg_t *pg = build_pg();
  CU_ASSERT_PTR_NOT_NULL_FATAL(pg);

  const char path[] = "tmp/nosuchdir/file.pg";

  CU_ASSERT_NOT_EQUAL_FATAL(access(path, F_OK), 0);
  CU_ASSERT_NOT_EQUAL(pg_write(pg, path), 0);
  CU_ASSERT_NOT_EQUAL(access(path, F_OK), 0);

  pg_destroy(pg);
}
