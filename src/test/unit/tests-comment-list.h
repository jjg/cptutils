#include <CUnit/CUnit.h>

extern CU_TestInfo tests_comment_list[];

void test_comment_list_new(void);
void test_comment_list_destroy_null(void);
void test_comment_list_next(void);
void test_comment_list_revert(void);
void test_comment_list_each(void);
void test_comment_list_find(void);
void test_comment_list_entry(void);
