#ifndef FIXTURE_GGR_H
#define FIXTURE_GGR_H

#include <cptutils/ggr.h>

ggr_t* load_ggr_fixture(const char*);

#endif
