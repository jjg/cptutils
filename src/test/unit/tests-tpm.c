#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-tpm.h"

#include <cptutils/tpm.h>


CU_TestInfo tests_tpm[] = {
  {"create", test_tpm_create},
  {"push", test_tpm_push},
  {"comment", test_tpm_comment},
  {"manicure", test_tpm_manicure},
  {"destroy NULL", test_tpm_destroy_null},
  CU_TEST_INFO_NULL,
};

void test_tpm_create(void)
{
  tpm_t *tpm = tpm_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(tpm);
  tpm_destroy(tpm);
}

void test_tpm_push(void)
{
  tpm_t *tpm = tpm_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(tpm);

  tpm_point_t point = {0};

  CU_ASSERT_EQUAL(tpm_push(tpm, &point), 0);

  tpm_destroy(tpm);
}

void test_tpm_comment(void)
{
  tpm_t *tpm = tpm_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(tpm);

  comment_t *comment = tpm_comment(tpm);
  CU_ASSERT_PTR_NOT_NULL(comment);

  tpm_destroy(tpm);
}

void test_tpm_manicure(void)
{
  tpm_t *tpm = tpm_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(tpm);

  tpm_point_t point = {
    .lead = {0, 0, 0},
    .trail = {0, 0, 255}
  };

  point.fraction = 0.0;
  CU_ASSERT_EQUAL(tpm_push(tpm, &point), 0);
  point.fraction = 1.0;
  CU_ASSERT_EQUAL(tpm_push(tpm, &point), 0);

  CU_ASSERT_EQUAL(tpm_manicure(tpm), 0);
  CU_ASSERT_EQUAL(tpm_npoint(tpm), 2);

  tpm_destroy(tpm);
}

void test_tpm_destroy_null(void)
{
  tpm_destroy(NULL);
}
