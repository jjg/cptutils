#include <CUnit/CUnit.h>

extern CU_TestInfo tests_cpt_normalise[];

void test_cpt_normalise_gmt4(void);
void test_cpt_normalise_gmt5_unhinged(void);
void test_cpt_normalise_gmt5_hard_hinged(void);
void test_cpt_normalise_gmt5_soft_hinged(void);
void test_cpt_denormalise_gmt4(void);
void test_cpt_denormalise_gmt5_unhinged(void);
void test_cpt_denormalise_gmt5_hard_hinged(void);
void test_cpt_denormalise_gmt5_soft_hinged(void);
