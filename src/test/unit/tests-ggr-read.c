#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-ggr-read.h"
#include "fixture.h"
#include "fixture-ggr.h"

#include <cptutils/ggr-read.h>

#include <stdio.h>
#include <unistd.h>


CU_TestInfo tests_ggr_read[] = {
  {"fixtures", test_ggr_read_fixtures},
  {"file does not exits", test_ggr_read_nofile},
  CU_TEST_INFO_NULL
};

void test_ggr_read_fixtures(void)
{
  size_t n = 1024;
  char path[n];

  const char *ggrs[] = {
    "Sunrise.ggr",
    "mars.ggr",
    "Rounded_edge.ggr",
    "Wood_2.ggr"
  };
  size_t nfile = sizeof(ggrs) / sizeof(char*);

  for (size_t i = 0 ; i < nfile  ; i++)
    {
      CU_ASSERT_FATAL(fixture(path, n, "ggr", ggrs[i]) < n);

      ggr_t *ggr = ggr_read(path);
      CU_ASSERT_PTR_NOT_NULL(ggr);
      ggr_destroy(ggr);
    }
}

void test_ggr_read_nofile(void)
{
  ggr_t *ggr = ggr_read("tmp/no-such-file.ggr");
  CU_ASSERT_PTR_NULL(ggr);
}
