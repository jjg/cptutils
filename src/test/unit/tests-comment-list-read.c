#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "tests-comment-list-read.h"
#include "fixture.h"

#include <cptutils/comment-list-read.h>


CU_TestInfo tests_comment_list_read[] = {
  {"fixtures", test_comment_list_read_fixtures},
  {"file does not exist", test_comment_list_read_nofile},
  CU_TEST_INFO_NULL,
};

void test_comment_list_read_fixtures(void)
{
  size_t n = 1024;
  char path[n];
  const char *files[] = {
    "single.xco",
    "multiple.xco"
  };
  size_t nfile = sizeof(files) / sizeof(char*);

  for (size_t i = 0 ; i < nfile ; i++)
    {
      CU_ASSERT_FATAL(fixture(path, n, "xco", files[i]) < n);

      comment_list_t *list = comment_list_read(path);

      CU_ASSERT_PTR_NOT_NULL_FATAL(list);
      CU_ASSERT_NOT_EQUAL(comment_list_size(list), 0);
      comment_list_destroy(list);
    }
}

void test_comment_list_read_nofile(void)
{
  comment_list_t *commentlist = comment_list_read("tmp/no-such-file");
  CU_ASSERT_PTR_NULL(commentlist);
}
