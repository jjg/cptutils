#include <CUnit/CUnit.h>

extern CU_TestInfo tests_qgs_list_read[];

void test_qgs_list_read_fixtures(void);
void test_qgs_list_read_fixture_elevation(void);
void test_qgs_list_read_part_defective(void);
void test_qgs_list_read_nofile(void);
