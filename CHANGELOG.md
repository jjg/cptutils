Changelog
---------

### 2.0 (pending)

- Fix reading of QGS without `discrete` attribute
- Changes the layout of the comment file format (last time, promise)
- Comment format now has an XSD-schema, used to check validity of the
  comment output in acceptance tests.
- SVG metadata section is now explicitly namespaced (RDF/custom)
- SVG 2.0 support, versions 1.1 and 2.0 are read and one can write
  version 2.0 with the new `--svg2` option.

### 1.83 (07 Nov 2024)

- Added bash-completion scripts for the programs
- The **pssvg** option `--title` renamed to `--id-format` and now
  controls the `id` of the SVG output directly.
- Rework of how **pssvg** handles duplicate names in the input,
  those names are now included as the (nonstandard) `name` attribute
  of the output.
- Implemented comment handling for mutiple-gradient file formats; the
  comment-files are now a custom XML format.

### 1.82 (09 Jul 2024)

- Fix broken previews in _svg_ when input name has whitespace
- Handle hue-cycling in **cpthsv**
- Mixed colour-model _cpt_ inputs error on GMT v4 _cpt_ output
  unless the new `--model` option is given
- Consistent handling of comments
- Program **pssvg** now errors if titles in input are not valid
  as XML ids (fixed by specifying `--title` explicitly)

### 1.81 (05 Apr 2024)

- Bugfix for parsing of _cpt_ comments: many thanks to Dmitry
  Marakasov for the fix

### 1.80 (29 Mar 2024)

- Fix **cptpg** empty output file on non-monotone inputs
- Handle reading of _cpt_ comments with leading whitespace
- Acceptance tests improved with **equal-**_type_ assertion
  scripts
- Fix **svgpg** and **svgqgs** `--all` conversions to the
  incorrect types
- New program **qgssvg** converts QGIS colour-ramp to SVG
  gradient
- XML outputs (SVG gradients, QGIS colour-ramps) are now
  indented

### 1.79 (28 Jan 2024)

There are substantial changes in the handling of _cpt_ format
extensions introduced in GMT5:

- New program **cptupdate** updates to the newer (or older)
  versions of the _cpt_ format
- All programs writing _cpt_ have `--gmt4`, `--gmt5`, `--gmt6`
  options to select the GMT version of _cpt_ output, and a
  `--z-normalise` option to normalise the z-values to [0, 1]
- All programs reading _cpt_ have a `--hinge-active` option to
  activate a `SOFT_HINGE` in the input
- Programs which _both_ read and write _cpt_ gain `--z-normalise`
  and `--z-denormalise` and `--hinge` options which control how
  the the _cpt_ is mapped to/from the range 0/1 (or -1/1 in the
  hinged case).  See manuals for details.

The default output for programs writing _cpt_ is still version 4
but in the next major version (2.0), that will be switched to 6.

Other changes:

- Configuration of Python and JSON support is now explicit: rather
  than warn and disable if not found, configure will error.  One
  can use the `--without-python` or `--without-json` to configure
  in this case.
- The **cpthsv**, **cptclip** and **cptcont** programs now accept
  HSV inputs
- The **cptcat** program can now take a single file as argument

### 1.78 (26 Oct 2023)

- Fix symlinks for manual pages for **svgx** wrappers
- Updated fuzzing framework
- Fixed **gimplut** hang on unaligned _ggr_ inputs
- Fixed **svgx** crash on no-stop inputs
- Fixed **pssvg** crash on invalid noise-gradient input
- Documentation fixes
- Sanitisation of the `id` attribute in SVG output
- The **gradient-convert** program writes magic-bytes of gradient
  types when the `--capabilities` option is used

### 1.77 (06 Sep 2023)

- Added direct _svg_ to _pg_ (PostGIS) conversion to **svgx** and
  a new wrapper-script **svgpg**.  This conversion preserves the
  alpha channel (the previous indirect route via _cpt_ did not).
- The **gradient-convert** program uses Python type-annotations,
  correctness checked by **mypy** if `--enable-python-type-tests`
  is given at configuration; the option-parsing uses the newer
  `ArgumentParser` module giving a better `--help` (no functional
  changes).  Requires Python 3.7 or later.
- A new `--graphviz` option to **gradient-convert** prints the
  conversion graph in GraphViz dot format to _stdout_.
- Removed support for the ArcView Legend (avl) format.
- Removed unit-test dependency on **xmlstarlet**.
- Update BATS to version 1.10.0
- Added a `--with-python` option to package configuration.

### 1.76 (30 Mar 2023)

- Reading of _cpt_ files now handles floating-point RGB triples (as
  produced by **makecpt**(1) in some circumstances)
- All program error if `--verbose` is given when writing results to
  _stdout_ (i.e., when `--output` is not given)
- Consistent handling of output-type irrelevant options in **svgx**
- Corrected parsing of GMT5 dashed (HSV) and solidus (RGB) colours
- Internally, the `fill_t` enumerated type is changed: `fill_colour`
  is replaced by `fill_colour_rgb` or `fill_colour_hsv`, these are
  assigned to the (global) `COLOR_MODEL` value for GMT4 _cpt_ files,
  but according to the colour's form in GMT5 files
- The **gimsvg** program now errors on bad `--samples` option
- Update BATS to version 1.9.0

### 1.75 (12 Feb 2023)

- Handle _cpt_ dash colours (as in `H-S-V`)
- Handle lower-case `COLOR_MODEL`
- Add most generated files to version control, reducing tool
  dependencies on git checkout (package managers please note)
- CHANGELOG converted to Markdown

### 1.74 (09 Sep 2020)

- Remove dependency on "xml2" (by local compilation)
- Replace all `tmpnam` calls in test by `mkstemp`/`mkdtemp`

### 1.73 (08 Jul 2020)

- Use "hachure" rather than "hatchure" in **cptinfo** output
- Update manual pages to DocBook v5
- Update **gradient-convert** to (at least) Python 3.6

### 1.72 (07 Aug 2019)

- Now **svgx** writes Tecplot map

### 1.71 (06 Jun 2019)

- Fixes for acceptance tests on FreeBSD

### 1.70 (10 May 2019)

- Handle "short book colours" in _grd5_ (photoshop) files.

### 1.69 (30 May 2018)

- Improved handling of trailing whitespace parsing _cpt_ files
- Fix a compile issue for older **gcc** versions

### 1.68 (22 Feb 2017)

- Further portablilty fixes for FreeBSD
- henceforth, releases will be git tagged using the format "v1.68"

### 1.67 (12 Feb 2017)

- Several fixes from Dmitry Marakasov to allow compilation on BSD
  variants, for which many thanks!

### 1.66 (02 Aug 2016)

- Added test-suite for **gradient-convert**
- Fixed broken **gradient-convert** `--help` option

### 1.65 (14 Jun 2016)

- Fixed a bug in **pspsvg** handling input with implicit initial
  stops (bug introduced in the _grd3_ parse rewrite of 1.64)

### 1.64 (09 Jun 2016)

- The parser for _odb_ files (used by **avlcpt**) updated to bison 3
- Now **gradient-convert** works with Python 3 (thanks to Collin Anderson)
- Fixes for 77 defects found by Coverity, to whom many thanks
- Improved _grd3_ parse, better error messages for unsupported features

### 1.63 (20 Mar 2016)

- The _svg_ reader now handles RGB percentages
- Added **svgqgs** which converts svg to QGIS style
- Package maintainers: the test target now requires xmlstartlet

### 1.62 (18 Jan 2016)

- Added the **cptpg** program
- Removed border from _svg_ previews

### 1.61 (21 Dec 2015)

- Fix for UCS-2 to UTF-8 conversion of Japanese names titles
  in **pssvg**

### 1.60 (17 Dec 2015)

- Added `svga` colour support to **svgx**
- Use of uninitialised value bug in `ggr.c` fixed, many thanks
  to David Binderman for spotting this

### 1.59 (05 Jul 2015)

- GIMP gradients which are DOS encoded, and/or which have
  'endpoint colour types' can now be read by the **gimpsvg** and
  **gimplut** programs

### 1.58 (20 Apr 2015)

- The **cpthsv** program handles HSV model input
- The _odb_ flex scanner (for **avlcpt**) is now 8-bit
- The **svgx** program writes libxml2 errors to the backtrace
- Handle puzzling 0-component channels with colour model
  "UnsC" (unspecified)

### 1.57 (18 Dec 2014)

- Test-fixures & fixes for crashes found with AFL
- Scripts for AFL fuzzing
- Man-page sources converted to UTF-8 encoding
- Improved backtraces

### 1.56 (1 Oct 2014)

- Introduced a soft dependency on the jansson JSON library
- Added `--backtrace-file` and `--backtrace-format options`
  to all conversion programs
- The **gradient-convert** `-z` option does not make empty zipfiles
- The **gradient-convert** `-z` option returns correct error-code
- Removed **cpttext**, **cptcss** (unused and cheesy)
- Configuration scripts checks that compiler flags are handled

### 1.55 (10 Aug 2014)

- Added `-z` (zip) option to **gradient-convert**

### 1.54 (4 Jun 2014)

- Fixed error parsing geometry strings
- Fixed null-deference in **grd5read** (when given a _grd3_ file)
- Removed _ggr_ searching (unused and crufty)
- Improved acceptance testing
- Unit tests

### 1.53 (25 Apr 2014)

- Fixed a null-deference when writing a _cpt_ with a NULL name
  (typically when the _cpt_ is read from stdin)
- The **svgx** `-a` option now sanitises the filenames derived
  from the svg ids, replacing forward-slashes and leading dots
  by underscores.

### 1.52 (13 Mar 2014)

- The **pssvg** program now gives unique SVG ids for input with
  repeated titles
- The **gradient-convert** program detects _psp_ type from extension
- Fixed **pssgv** parse error on `HSBl` noise gradients

### 1.51 (27 Feb 2014)

- Some **pssvg** feature enhancements
  - The _grd5_ parser handles all known _grd5_ constructs
  - Fore/background colours handled via command-line
  - Conversion of gradients with CMYK & CIE-L*ab colour-stops
- The **svgx** `-o` options interpreted as output directory when `--all`
  option given
- The **gradient-convert** handles Photoshop _grd_ files

### 1.50 (13 Feb 2014)

- Bugfixes for **pssvg**

### 1.49 (12 Feb 2014)

- New program, **pssvg**, converts Photoshop gradients to _svg_
- Fixed missing include in **cpthsv**
- Added configuration check for `locale.h`

### 1.48 (3 Nov 2013)

- Added support for _cpt_ segment labels
- Replaced ageing lex/yacc _cpt_ parser by hand-written version
- Fixed missing ids in _svg_ files for programs writing _svg_ when
  reading from stdin

### 1.47 (8 July 2013)

- New program **cptcat**
- Added `setup.s`h and makefile fixes for full build from
  Github checkout

### 1.46 (14 Dec 2012)

- Fixed intermediate filenames for output not in current directory
- Added `--capabilities` option to **gradient-convert**
- Rationalised and simplified libraries to cope with stricter
  rules in recent versions of gcc (4.6.3)
- Fixed **svgpov** failure on oversized output
- Corrected absolute path to testfile in **cptcss** (gentoo patch)

### 1.45 (24 April 2012)

- Code cleanup for **svgx**
- The **svgx** program writes _svg_
- All programs writing _svg_ now have a `--preview` option which
  includes a simple preview in the file
- Quoted argument lists in **svgx** wrappers so they handle filenames
  containing spaces correctly
- Minor fixes and enhancements to **gradient-convert**
- A few minor fixes from static analysis (**cppcheck**)

### 1.44 (13 March 2012)

- Improved human-readable z-values in _cpt_ output
- Fixed incorrect **xycpt** behaviour when input had leading zeros
- Reinstated (now local) bottom level test target
- Renamed several files in `src/common`, for consistency
- Added the **gradient-convert** wrapper script (Python is needed to
  use it)

### 1.43 (28 Feb 2012)

- Manual pages are now generated from DocBook
- Several unimplemented options removed
- All tests now use data in the package itself

### 1.42 (23 Jan 2012)

Several fixes suggested by Volker Froehlich of Fedora:

- Applied patches adding `DESTDIR` and fixing `size_t` printing
- Updated FSF address in numerous files
- Bottom-level `dist` target now creates versioned tarballs

### 1.41 (16 Nov 2011)

- The **svgx** `-1` option becomes the default
- The **gimpsvg** program replaces **gimpcpt**
- The **pspsvg** program replaces **pspcpt**
- New program **svgpng**
- Fixed some UTF-8 issues in `common/svgread`
- Fixed opacity conversion in **svgpsp** (**svgx**)
- Improved character-set handling in **svgx**
- Fixed conversion of implicit initial and final stops in **svgx**

### 1.40 (1 Nov 2011)

- The **svgx** program now writes the astronomical SAO format for DS9
- Fixed accidental RBG colours in **cpttext** (some kind of mental
  block there)

### 1.39 (6 May 2011)

- The **svgx** program writes CSS3 gradients

### 1.38 (6 Nov 2010)

- Added extra error handling for _ggr_ and _psp_ parsers
- Installation directories created by `src/Makefile`
- The **svgx** progra, writes gnuplot files

### 1.37 (27 June 2010)

- Added a `-u` option to **xycpt** which allows input colours to
  to be specified a doubles in 0-1

### 1.36 (20 April 2010)

- Midpoint option added to **cptcont**
- Fixed **cptinfo** output on _cpt_ files with decreasing z
- New program **cptclip**

### 1.35 (4 Apr 2010)

- New program **cptcont**

### 1.34 (26 Feb 2010)

- Fixed accidental RBG colours in **cptcss**

### 1.33 (28 Dec 2009)

- New program **gplcpt**

### 1.32 (8 Oct 2009)

- Improved handling of midpoints in **pspcpt**

### 1.31 (8 Aug 2008)

Mostly **svgx**: several fixes in `svgread.c` and `svglist.c`

- Fixed allocation bug for more than 20 gradients in a _svg_ file
- Added parsing of whitespace in _svg_ style tags
- Set default unit opacity when no opacity-stop is given

### 1.30 (13 Apr 2008)

- New program **gimplut**

### 1.29 (16 Nov 2007)

- New program **cpthsv**
- Bugfix for formay strings in **cptcss**
- Moved `dp-simplify` out of **gimpcpt** and into **svg-simplify**
- Fixed fencepost error in `common/cpt.c`
- Updated author homepage

### 1.28 (28 Oct 2006)

- The **svgpsp** program supports the `-a` (convert all gradients)
  switch

### 1.27 (28 Sep 2006)

- Added the default version to _psp_ structure, fixing a bug whereby
  **svgpsp** produced _psp_ files with no version (these could not be
  read by **pspx**)
- Fixed command-line bug in **svgx**

### 1.26 (1 Sep 2006)

PaintShop Pro special, the _psp_ format is now completely reverse
engineered: see `src/common/README.psp`

- Now `pspread.c` parses the opacity gradient and midpoints of the
  segments.
- New `pspwrite.c` writes _psp_ gradients
- The `pspcpt` program prints out the opacity gradient and gives
  warnings if there are uncentred midpoints or any tranparency.
- The **svgx** program outputs the _psp_ format, and the wrapper
  script **svgpsp** is provided.

### 1.25 (28 Aug 2006)

- POVRay output now has the name beautified (converted to only
  alphanumerics and underscore) to avoid povray parse errors.

### 1.24 (04 Dec 2005)

- All _cpt_ writing  programs now have `-b` `-f` and `-n` flags

### 1.23 (03 Dec 2005)

- Cleanup for **xycpt**

### 1.22 (21 Nov 2005)

- New program **avlcpt**, converting the ArcView legend format (if
  it contains a gradient).
- Above uses `libodb` which parses ODB format files and serialises
  them into a convenient array structure. this might be of
  independent interest -- see `src/common/README.libodb`

### 1.21 (11 Nov 2005)

- Fixed bad default format string for td class label in **cptcss**

### 1.20 (21 Sep 2005)

- POVRay **svgx** output now works when extraction of all
  gradients (`-a`) is specified
- POVRay **svgx** output now has arbitrarily many stops (even
  though 20 is the maximum in the specification) if the
  new `-p` (permissive) flag is given

### 1.19 (29 Aug 2005)

- Added a POVRay colour-map output format for **svgx**, so we
  now have an **svgpov** wrapper

### 1.18 (20 July 2005)

- Added new program **cptcss** which writes the colours from a
  discrete _cpt_ file to a fragment of CSS (for  colouring the
  cells in a table). The original plan was to put this functionality
  into **cpttext**, but the output is a bit different in that there
  is no idea of a z-value in the _css_, just an order.

### 1.17 (3 June 2005)

- Removed redundant (but harmless) stops in **cptsvg**, which is now
  finished apart from support for HSV & non-colour fill types

### 1.16 (1 June 2005)

- New program **cptsvg**

### 1.15 (17 April 2005)

- Added `B` to the possible annotation types in `cptparse.y`
- Enabled debug output for _cpt_ parser

### 1.14 (unreleased)

- Fixed bug in **pspcpt** which gave zero-length z-slices for
  some files

### 1.13 (unreleased)

- New input filter for PaintShop Pro gradients. this is reverse
  engineered partial implementation which at least gets the colour
  samples. it does not extract
  - repetition
  - the alpha channel
  - angles
  - the smoothing parameter
- New program **pspcpt**, converts PhotoShop Pro gradients
  to _cpt_

### 1.12 (unreleased)

- New program **cpttext**, prints text in the colours of a specified
  _cpt_ file (just html the moment)

### 1.11 (unreleased)

- New program **xycpt**, converts column data to _cpt_ format

### 1.10 (unreleased)

- Split `libgimpcpt` to `libfill`, `libcpt` and `libggr`

### 1.9 (unreleased)

- Replaced the _cpt_ read function in `cpt.c` by the custom
  bison/flex parser, which now seems to be able to completely
  capture the contents of a _cpt_ file.  New files `cptparse.y`,
  `cptscan.l`, `cptio.c` and `cptio.h`
- The _cpt_ type in `cpt.h` has been extended to hold the extra data
  (annotations, hachures etc). New file `fill.h` defines a fill type,
  which could be colour, greyscale, hachure or file.
- All programs now compile with the new _cpt_ type, all cpt files in
  cpt-city are parsed by the new parser.

### 1.8

- Corrected minor glitch in `common/cpt.c`, now whitespace at the
  start of the line does not cause a scan failure.
- The **cptinfo** program now reads the file size.

### 1.7

- Added **cptinfo** program for the "cpt city" website

### 1.6

- Added _cpt_ simplification to **gimpcpt**, with some extra
  code needed in `common/cpt.c` and  `common/colour.c`

### 1.5

- The **gimpcpt** program repaired to work with the changes in
  `colour.c`
- First version of the **cptgimp** manual page

### 1.4

Implemented **cptgimp**
- Extended `colour.h` to handle the HSV & RGB colour types in
  _cpt_ files
- The program is lossless since the _ggr_ format can easily
  capture any _cpt_ RGB/HSV linear spline, natively.
- Got confused by the myriad colour types in `colour.c`, so
  reorganised the naming conversions, and standardised the
  interface.
- All the GMT standard palettes now seem to be converted
  correctly, sent a tarball to some hungry alpha testers
- This all breaks **gimpcpt**

### 1.3

- Converted **gimpcpt** to use **gengetopt**, which leads to tidying
  up of the Autonf setup
- Redid the test targets in gimpcpt

### 1.02

- Split code between `common` & `gimpcpt` diretcories
- Added `Common.mk`
- Tidied up Makefiles a bit

### 1.01

- Minor modification to handle the change to the GIMP's gradient file
  format: an additional line
  ```
  Name: <name>
  ```
  has been added to the files, and they are given the `.grd` extension.
  - We now check the second line of the file to see if it a name line,
    and if so save that to print in the output _cpt_ file
  - We search for gradients (still with typeglobs) with names
    `<name>` and `<name>.grd`
  - Header in output _cpt_ file tidied a little

### 1.00

- Minor documentation changes

### 0.94

- The file `src/gradient.c` now handles gradients which lie
  about the number of segments they contain (but issues a warning
  if the declared number differs from th number found). All of
  the AG gradients can now be converted.

### 0.93

- Animation target prettified
- Target `<gradient>.png` (make a POVRay rendering) added
- The `Makefile` tidied

### 0.92

- Added  `src/anim.ini`
- In `src/Makefile.in` extra target `*.gif` added, makes a gif
  animation of the gradient in RGB space, mmm.

### 0.91

- Corrections for `src/Makefile` and `INSTALL`

### 0.90

- Initial public release
