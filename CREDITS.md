Credits
-------

- The files `src/lib/utf8.*` are by Jeff Bezanson, original
  versions available [here][1], contributed to the public domain.

- The files `src/test/accept/bats` are a copy of the Bash Automated
  Testing System ([BATS][2]), MIT licence.

- The file `src/lib/stdcol.gperf` is a modified version of the
  file `cr-rgb.c` include in the Croco library by Dodji Seketeli,
  LGPLv2 licence.

- The file `src/lib/gmtcol.gperf` is a modified version of the files
  `gmt_colorname.h` and `gmt_colors_rgb.h` from the [GMT package][3],
  GPLv2 licence.

[1]: http://www.cprogramming.com/tutorial/unicode.html
[2]: https://github.com/bats-core/bats-core
[3]: https://www.generic-mapping-tools.org/
