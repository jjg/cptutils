AC_INIT([cptutils],
        [1.83],
        [j.j.green@gmx.co.uk],
        [cptutils],
        [https://gitlab.com/jjg/cptutils])

AC_CONFIG_MACRO_DIR([config/m4])
AC_CONFIG_AUX_DIR([config])

m4_include([config/aclocal.m4])

dnl | programs

AC_PROG_CC([gcc clang])

AX_CHECK_COMPILE_FLAG([-Wall],
  AX_APPEND_FLAG([-Wall], CFLAGS))
AX_CHECK_COMPILE_FLAG([-Wno-unused-but-set-variable],
  AC_SUBST(CFLAG_NUBSV, [-Wno-unused-but-set-variable]),
  AC_SUBST(CFLAG_NUBSV, []),
  [-Werror])

AC_PROG_MAKE_SET
AC_PROG_INSTALL

AC_PATH_PROG(XML2C, [xml2-config])
if test -z "$ac_cv_path_XML2C" ; then
  AC_MSG_ERROR(no xml2-config! please install libxml2)
else
  AX_APPEND_FLAG(`$XML2C --cflags`, CFLAGS)
  AX_APPEND_FLAG(`$XML2C --libs`, LIBS)
fi

AC_PATH_PROG(GGO, [gengetopt])
AC_PATH_PROG(GPERF, [gperf])
AC_PATH_PROG(XDGOPEN, [xdg-open])
AC_PATH_PROG(DOT, [dot])

dnl | python is a bit special, we allow the user to specify
dnl | a particular version, search for the full path and set
dnl | PYTHON, then basename that for the gradient-convert
dnl | shebang

opt_with_python="python3 python"

AC_ARG_WITH([python],
  AS_HELP_STRING([--with-python=NAME],
                 [python version (default python3, python)]),
  [opt_with_python=$withval]
)

if test "$opt_with_python" = 'no' ; then
  AC_MSG_WARN([python disabled, no gradient-convert])
else
  AC_PATH_PROGS(PYTHON, [$opt_with_python])
  if test -z "$ac_cv_path_PYTHON" ; then
    AC_MSG_ERROR([$opt_with_python not found])
  fi
  AC_MSG_CHECKING([version of $ac_cv_path_PYTHON])
  ac_python_needed='3.7.0'
  ac_python_version=`$ac_cv_path_PYTHON --version | cut -d' ' -f2`
  AC_MSG_RESULT([$ac_python_version])
  AX_COMPARE_VERSION([$ac_python_version], [lt], [$ac_python_needed],
    AC_MSG_ERROR([Python version at least $ac_python_needed required]))
  AC_SUBST(WITH_PYTHON, 1)
  AC_DEFINE(WITH_PYTHON, 1, [Define to 1 for Python support])
fi

dnl | older versions may need /etc/bash_completion.d

AC_ARG_WITH([bashcompletedir],
  AS_HELP_STRING([--with-bashcompletedir=DIR], [bash completion]),
  [opt_with_bashcompletedir=$withval],
  [opt_with_bashcompletedir='${datarootdir}/bash-completion/completions'],
)
AC_SUBST([bashcompletedir], [$opt_with_bashcompletedir])

dnl | the jansson library, needed for backtraces in JSON

opt_with_json='yes'

AC_ARG_WITH([json],
  AS_HELP_STRING([--with-json=NAME],
                 [include JSON support (default yes)]),
  [opt_with_json=$withval]
)

if test "$opt_with_json" != 'no' ; then
  AC_CHECK_LIB(jansson, json_pack, [], AC_MSG_ERROR(libjansson not found))
  AC_CHECK_HEADERS(jansson.h)
  AC_SUBST(WITH_JSON, 1)
  AC_DEFINE(WITH_JSON, 1, [Define to 1 for JSON support])
fi

dnl | required libraries

AC_CHECK_LIB(m, pow)
AC_CHECK_LIB(png, png_create_read_struct, [], AC_MSG_ERROR(libpng not found))
AC_SEARCH_LIBS(iconv_open, iconv, [], AC_MSG_ERROR(iconv not found))

dnl | header files

AC_CHECK_HEADERS(unistd.h)
AC_CHECK_HEADERS(iconv.h)
AC_CHECK_HEADERS(locale.h)
AC_CHECK_HEADERS(png.h)

dnl | library functions.

AC_CHECK_FUNCS(strdup)
AC_CHECK_FUNCS(getopt)
AC_CHECK_FUNCS(mkstemp)
AC_CHECK_FUNCS(mkdtemp)

dnl | special case, check for be64toh which is usually a
dnl | macro defined in endian.h

AC_CHECK_HEADERS([endian.h],
  AX_CHECK_DEFINE([endian.h], [be64toh], , AC_CHECK_FUNCS(be64toh)))
AC_CHECK_HEADERS([sys/endian.h],
  AX_CHECK_DEFINE([endian.h], [be64toh], , AC_CHECK_FUNCS(be64toh)))

dnl | misc

AX_COUNT_CPUS(
   [AC_SUBST(CPUS, $CPU_COUNT)],
   [AC_SUBST(CPUS, 1)]
)

AX_CHECK_DOCBOOK_XSLT()
AC_SUBST(HAVE_DOCBOOK, $HAVE_DOCBOOK_XSLT)

dnl | testing

opt_enable_test_unit=no
opt_enable_test_accept=no
opt_enable_test_python_type=no
opt_enable_coverage=no

AC_ARG_ENABLE(tests,
   AS_HELP_STRING([--enable-tests],[all testing support (default is no)]),
   [
     opt_enable_test_unit=$enableval
     opt_enable_test_accept=$enableval
   ])

AC_ARG_ENABLE(unit-tests,
   AS_HELP_STRING([--enable-unit-tests],
                  [unit testing support (default is no)]),
   [opt_enable_test_unit=$enableval])

AC_ARG_ENABLE(acceptance-tests,
   AS_HELP_STRING([--enable-acceptance-tests],
                  [acceptance testing support (default is no)]),
   [opt_enable_test_accept=$enableval])

AC_ARG_ENABLE(python-type-tests,
   AS_HELP_STRING([--enable-python-type-tests],
                  [python-type testing support (default is no)]),
   [opt_enable_test_python_type=$enableval])

if test $opt_enable_test_unit = yes; then
   AC_CHECK_LIB(cunit, CU_add_test)
   AC_SUBST(WITH_UNIT, yes)

   AC_MSG_CHECKING([CUnit version])
   AC_COMPILE_IFELSE([
     AC_LANG_SOURCE([[
       #include <CUnit/CUnit.h>
       int main(void) {
         CU_SuiteInfo suite;
         suite.pSetUpFunc = NULL;
         return 0; }
       ]])
     ],
     [
       AC_MSG_RESULT([2.1-3 or later])
       cunit213=1
     ],
     [
       AC_MSG_RESULT([2.1-2 or earlier])
       cunit213=0
     ])
   AC_DEFINE_UNQUOTED(CUNIT_213,
     $cunit213,
     [Define if CUnit version 2.1-3 or later])
else
   AC_SUBST(WITH_UNIT, no)
fi

if test $opt_enable_test_accept = yes; then
   AC_PATH_PROG([PARALLEL], [parallel])
   AC_PATH_PROG([XMLLINT], [xmllint])
   AS_IF([test x$XMLLINT = xno], AC_MSG_ERROR(acceptance tests need xmllint))
   AC_SUBST(WITH_ACCEPT, yes)
else
   AC_SUBST(WITH_ACCEPT, no)
fi

if test $opt_enable_test_python_type = yes; then
   AC_PATH_PROG([MYPY], [mypy], [no])
   AS_IF([test x$MYPY = xno], AC_MSG_ERROR(python-type tests need mypy))
   AC_SUBST(WITH_PYTHON_TYPE, yes)
else
   AC_SUBST(WITH_PYTHON_TYPE, no)
fi

AC_ARG_ENABLE(coverage,
   AS_HELP_STRING([--enable-coverage],
                  [code coverage (default is no)]),
   [opt_enable_coverage=$enableval])

if test $opt_enable_coverage = yes; then
   AC_PATH_PROG([GCOVR], [gcovr], [no])
   AS_IF([test x$GCOVR = xno], AC_MSG_ERROR([gcovr not found]))
   AX_APPEND_FLAG([--coverage], CFLAGS)
   AX_APPEND_FLAG([--coverage], LDFLAGS)
   AX_APPEND_FLAG([-fprofile-abs-path], CFLAGS)
   AX_APPEND_FLAG([-fprofile-abs-path], LDFLAGS)
   AC_SUBST(WITH_COVERAGE, yes)
else
   AC_SUBST(WITH_COVERAGE, no)
fi

dnl | outputs

AC_DEFINE(VERSION, PACKAGE_VERSION, "version of package")
AC_SUBST(PACKAGE_VERSION)

AC_CONFIG_HEADERS(src/lib/config.h)
AC_CONFIG_FILES([src/test/accept/config.bash])
AC_CONFIG_FILES([VERSION:config/VERSION.in])
AC_CONFIG_FILES([src/Common.mk])
AC_OUTPUT
